<?php echo $header; ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 main__filter" style="display: none;">
		<?php echo $content_top; ?>
		</div>
		<div class="col-xs-12 filter__openbutton">
			<div><?php echo $button_filter;?> <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></div>
		</div>
	</div>
</div>
<div class="container main">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="products <?php echo $class; ?>">
      <?php if ($products) { ?>
      <div class="row">
        <?php foreach ($products as $product) { ?>
<div class="col-xs-12 col-sm-6 col-md-3 catalog__boxproducts wow fadeInUp" data-wow-duration="2.5s" data-wow-delay="0.1s" style="visibility: visible; -webkit-animation: fadeInUp 2.5s 0.1s;">
	<a class="products__link catalog__link" href="<?php echo $product['href']; ?>">
		<div class="catalog__product">
			<div class="products__imagebox">
				<img class="products__image" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
			</div>
			<div class="catalog__descriptionbox">
				<p class="catalog__description-id">504-14</p>
				<h2 class="catalog__description-text"><?php echo $product['name']; ?></h2>
			</div>
			<div class="catalog__pricebox">
				<p class="catalog__price-text"><?php echo $product['price']; ?></p>
			</div>
		</div>
	</a>
</div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p style="margin:10px;"><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<div class="pagination catalog_pagination" style="display: block;">
<nav class="pagination__products">
<div class="of__result"><span></span> <?php echo $results; ?></div>
<?php echo $pagination; ?>
</nav>
<div class="totop__button" title="Back to Top"><span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span></div>
</div>
<script>

		$('.filter__openbutton div').click(function() {
			$('.main__filter').slideToggle(700);
			if (slideToggle = true) {
				$('.filter__openbutton span').toggleClass('glyphicon-triangle-top');
			}
		});
		$(function(){
			var $ul = $('.filter_1'),
				$button = $('.filter_other1'),
				n = true;
		   $ul.scroll(function () {
			if (this.scrollHeight - this.scrollTop === this.clientHeight || !this.scrollTop) {
				n = !n;
				$button.html( n ? 'Другие <span class="glyphicon glyphicon-triangle-bottom h-filer_other-icon" aria-hidden="true"></span>':'Другие <span class="glyphicon glyphicon-triangle-top h-filer_other-icon" aria-hidden="true"></span>');
			}
		});

		   $button.click(function () {
				$ul.stop().animate({
					scrollTop: n ? '+=100' : '-=100'
				}, 1000);
			});
		});	
	
</script>
<?php echo $footer; ?>

<?php echo $header; ?>
<div class="container main">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" style="margin-top:-18px;" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
		<div class="col-md-8 product__imgbox">
          <?php if ($thumb || $images) { ?>
			<div class="col-xs-12 col-smd-12 col-md-12 thumbnails" style="padding:0;">
			<?php if ($thumb) { ?>
			<a href="<?php echo $popup; ?>"><img class="product__img" src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"></a>
			<?php } ?>
            <?php if ($images) { ?>
            <div class="col-xs-12 col-smd-12 col-md-12 other__productimgbox">
           	 <?php foreach ($images as $image) { ?>
				<div class="col-xs-4 col-sm-2 col-md-2 other__imgproduct">
					<a href="<?php echo $image['popup']; ?>"><img style="width:100%;" src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>"></a>
				</div>
           	 <?php } ?>
            </div>
		<?php } ?>
			</div>
          <?php } ?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 newsletterbox" style="padding:0;">
				<div class="wholesaler__newsletter">
					<h3><?php echo $text_product_share; ?>:</h3>
				<ul class="contact__ul social__ul" style="margin-left:0; margin-top:0;">
					<li class="contact__list footer_social-list" ><a class="contact__link sociaL_icon2 icon_fb" href="#">
			<svg style="width:10px;" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="45 -127.9 155.7 319.9" enable-background="new 45 -127.9 155.7 319.9" xml:space="preserve">
			<g>
				<path d="M142.3,192c-19.6,0-38.4,0-57.7,0c0-49.8,0-99.3,0-149.4c-13.4,0-26.3,0-39.5,0c0-18.5,0-36.3,0-54.8c13.1,0,26,0,39.4,0
					c0-13.2-0.2-25.8,0.1-38.4c0.2-8,0.1-16.1,1.7-23.8c5-23.9,19.6-40,42.7-48c23.4-8.2,47.3-5.1,70.7-3.2c0,17.2,0,33.8,0,51.1
					c-2.6,0-4.3,0-6.1,0c-10.3,0-20.7-0.2-31,0c-13.9,0.2-19.7,5.5-20.1,19.2c-0.5,14.1-0.1,28.3-0.1,43.1c19.4,0,38.6,0,58.4,0
					c-0.9,18.6-1.7,36.4-2.6,54.7c-18.8,0-37,0-55.8,0C142.3,92.4,142.3,141.9,142.3,192z"/>
			</g>
			</svg>
				</a></li>
					<li class="contact__list footer_social-list"><a class="contact__link sociaL_icon2 icon_tw" href="#">
			<svg style="width:21px;" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="-22 22.2 20.8 20.8" enable-background="new -22 22.2 20.8 20.8" xml:space="preserve">
			<path d="M-4.6,43c-4.7,0-9.3,0-14,0c-1.8-0.5-2.9-1.7-3.4-3.4c0-4.7,0-9.3,0-14c0.5-1.8,1.7-2.9,3.4-3.4c4.7,0,9.3,0,14,0
				c1.8,0.5,2.9,1.7,3.4,3.4c0,4.7,0,9.3,0,14C-1.7,41.3-2.9,42.5-4.6,43z M-8.8,32c0.2-0.1,0.3-0.2,0.5-0.3c0.9-0.7,1.1-1.6,1-2.7
				c-0.2-1-0.8-1.6-1.7-1.8c-0.5-0.1-1.1-0.2-1.7-0.2c-1.6,0-3.2,0-4.8,0c0,3.8,0,7.5,0,11.4c2.1-0.1,4.3-0.1,6.3-0.3
				c1.6-0.2,2.6-1.5,2.7-2.9C-6.4,33.6-7,32.8-8.8,32z"/>
			<path fill="#FFFFFF" d="M-8.8,32c1.8,0.8,2.4,1.6,2.2,3.1c-0.1,1.4-1.2,2.7-2.7,2.9c-2.1,0.2-4.2,0.2-6.3,0.3c0-3.9,0-7.6,0-11.4
				c1.6,0,3.2,0,4.8,0c0.6,0,1.1,0.1,1.7,0.2c0.9,0.3,1.6,0.8,1.7,1.8c0.2,1.1-0.1,2-1,2.7C-8.4,31.8-8.6,31.9-8.8,32z M-12.7,36.2
				c0.9-0.1,1.6-0.1,2.4-0.3c0.6-0.1,0.9-0.7,0.8-1.3c0-0.6-0.3-1-0.9-1.1c-0.8-0.1-1.5-0.1-2.3-0.1C-12.7,34.4-12.7,35.2-12.7,36.2z
				 M-12.7,31.4c0.9-0.1,1.7,0.2,2.2-0.6c0.2-0.3,0.2-1,0-1.3c-0.5-0.7-1.4-0.4-2.2-0.4C-12.7,29.9-12.7,30.6-12.7,31.4z"/>
			<path d="M-12.7,36.2c0-1,0-1.9,0-2.9c0.8,0,1.6,0,2.3,0.1c0.6,0.1,0.9,0.5,0.9,1.1c0,0.6-0.2,1.2-0.8,1.3
				C-11.1,36.1-11.9,36.1-12.7,36.2z"/>
			<path d="M-12.7,31.4c0-0.8,0-1.6,0-2.3c0.8,0,1.7-0.3,2.2,0.4c0.2,0.3,0.2,1,0,1.3C-11,31.6-11.8,31.3-12.7,31.4z"/>
			</svg>
					</a></li>
					<li class="contact__list footer_social-list" style="padding-left:5px;"><a class="contact__link sociaL_icon2 icon_gl" href="#">

			<svg style="width: 20px;" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="65 -65 195 195" enable-background="new 65 -65 195 195" xml:space="preserve">
			<g>
				<path d="M154.3,73.2c-5-0.9-8.7-1.6-12.5-2.2c-11.1-1.9-20.8-6.9-29.7-13.6c-7.4-5.7-8.7-11.1-4.2-17.4c4-5.6,10.5-6.3,17.7-1.3
					c18.7,12.8,38.8,15,60,7.7c5.4-1.9,10.3-5.3,15.4-8.1c4.8-2.7,9.5-4.3,14.5-0.8c6.7,4.7,6.8,13.1,0.1,18.7
					c-10.2,8.5-21.9,13.9-35.1,15.7c-2.4,0.3-4.8,0.8-8.3,1.5c1.7,2.1,2.7,3.5,3.9,4.7c10.6,10.6,21.2,21.2,31.7,31.9
					c4,4.1,4.8,8.8,2.5,13.3c-2.4,4.9-7.1,7.8-12.2,6.6c-2.7-0.6-5.4-2.4-7.5-4.4c-8.2-7.8-16.2-15.8-24.1-24c-2.8-2.9-4.5-2.6-7.2,0.1
					c-7.8,8-15.8,15.8-23.7,23.7c-6.2,6.2-12.3,6.7-17.4,1.6c-5.3-5.3-5-12,1-18.1c10.2-10.3,20.5-20.5,30.7-30.8
					C151.4,76.5,152.4,75.3,154.3,73.2z"/>
				<path d="M211-14c0,27.5-20.5,47.9-48,47.9c-27.6,0-48-20.7-47.9-48.5c0.1-26.7,21.5-47.9,48.3-48C187.7-62.7,212.3-41.9,211-14z
					 M162.9,10.4c13.7,0,25.1-11.1,25.2-24.4c0-13.1-11.6-25-24.8-25.2c-12.9-0.2-25.1,11.9-25.2,24.8C138-0.9,149.3,10.4,162.9,10.4z"
					/>
			</g>
			</svg>

					</a></li>
				</ul>
				</div>
			</div>
		</div>	
<div class="col-md-4 product__infobox" id="product">
<ul class="breadcrumb net_products" style="padding:0;margin-top:-4px;">
<?php if($prev_prod) { ?>
	<li><a href="<?php echo $prev_product_url;; ?>"><?php echo $text_prev; ?></a></li>
<?php } ?>
<li><a href="http://i-collection.com/all-products"><?php echo $text_all_products; ?></a></li>
<?php if($next_prod) { ?>
	<li><a href="<?php echo $next_product_url; ?>"><?php echo $text_next; ?></a></li>
<?php } ?>
</ul>
<div class="id__products">ID <span><?php echo $sku; ?></span></div>
<div class="product__name"><?php echo $heading_title; ?></div>
<div class="col-md-12" style="padding: 0;">
<div class="col-xs-6 product__price" style="padding: 0;">
<?php if ($price) { ?>
<?php if (!$special) { ?>
<?php echo $price; ?>
<?php } else { ?>
<span style="text-decoration: line-through;"><?php echo $price; ?></span>
<?php echo $special; ?>
<?php } ?>
<?php if ($tax) { ?>
<?php echo $text_tax; ?> <?php echo $tax; ?>
<?php } ?>
<?php if ($discounts) { ?>
<?php foreach ($discounts as $discount) { ?>
<?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?>
<?php } ?>
<?php } ?>
<?php } ?>
</div>
<div class="col-xs-6 product-icon" style="padding: 0;position:relative;top:-2px;">
	<img style="float: left;width: 32px;margin-left: 5px;" src="<?php echo $thumbicon; ?>">
	<img style="float: left;width: 32px;margin-left: 5px;" src="<?php echo $thumbicon2; ?>">
	<img style="float: left;width: 32px;margin-left: 5px;" src="<?php echo $thumbicon3; ?>">
</div>
</div>
<div class="product__category">
<div class="col-md-6" style="padding: 0;"><?php echo $text_category; ?>:</div>
<div class="col-md-6 product__categorydesc" style="padding: 0;">
<?php foreach ($catprod as $catp) { ?>
<?php echo $catp['name']; ?> 
<?php } ?>
</div>

 <?php if ($attribute_groups) { ?>
		<?php foreach ($attribute_groups as $attribute_group) { ?>
			<div class="product__category">
				<div class="col-md-6 product__attr-name" style="padding: 0;"><?php echo $attribute_group['name']; ?>:</div>
				<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
				<div class="col-md-6 product__categorydesc" style="padding: 0;"><?php echo $attribute['name']; ?></div>
			</div>
			<?php } ?>
		<?php } ?>
<?php } ?>

<div class="product__category">
	<div class="col-md-6" style="padding: 0;"><?php echo $text_width; ?>:</div>
	<div class="col-md-6 product__categorydesc" style="padding: 0;"><?php echo $width; ?> <?php echo $text_width_ich; ?></div>
</div>
<div class="product__descriptionatr">
<p><?php echo $description; ?></p>
</div>
</div>
<?php if ($products) { ?>
<div class="product__manyimages">
<p><?php echo $text_color; ?>:</p>
 <?php foreach ($products as $product) { ?>
	<div class="col-md-3 many__images"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>"></a></div>
<?php } ?>
</div>
<?php } ?>
<div class="product__colum product__price-cart plusAndMinus integer">
	<p class="product_price-cart_qty" style="margin: 10px 0 10px;"><?php echo $text_product_quantity; ?></p>
	<div class="columblock  add-action columblock__icon minus">-</div>
	<div class="columblock columblock_input"><input type="text" class="colum__input" name="quantity" value="<?php echo $minimum; ?>" size="2"  id="input-quantity" style="width: 60px;text-align:center;line-height:38px;"></div>
	<div class="columblock add-action columblock__icon plus">+</div>
	<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
</div>
<div class="product__category product__width" style="margin-top:15px;">
		<?php if ($options) { ?>
			<?php foreach ($options as $option) { ?>
				<?php if ($option['type'] == 'text') { ?>
				<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> plusAndMinus">
                    <p style="margin: 10px 0 10px;"><?php echo $option['name']; ?></p>
                    <div class="columblock  add-action columblock__icon minus">-</div>
                    <div class="columblock columblock_input">
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="colum__input" style="width: 60px;text-align:center;line-height:38px;"/>
                    </div>
                    <div class="columblock add-action columblock__icon plus">+</div>
                </div>
				<?php } ?>
			<?php } ?>
		<?php } ?>
</div>
<div class="product__buttoncheck" style="margin-top: 26px;">
  <div class="button__addcart" id="button-cart"><?php echo $button_cart; ?></div>
  <div class="button__addvishlist" onclick="wishlist.add('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></div>
</div>
		</div>
      </div>
      </div>
    <?php echo $column_right; ?></div>
</div>
<div class="container-fluid">
<?php echo $content_bottom; ?>
</div>
<script>

</script>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				//$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart > button').html('<img class="userpanel__img" src="catalog/view/theme/icollection/design/icon/cart.png" alt="cart"><span id="cart-total"> (' + json['total'] + ')</span>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>
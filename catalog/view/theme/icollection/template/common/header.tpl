<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>">
<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>">
<?php } ?>
<?php if ($keywords) { ?>
	<meta name="keywords" content="<?php echo $keywords; ?>">
<?php } ?>
<?php if ($icon) { ?>
	<link href="<?php echo $icon; ?>" rel="icon">
<?php } ?>
<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>">
<?php } ?>
	<link href="catalog/view/theme/icollection/css/bootstrap.min.css" rel="stylesheet">
	<link href="catalog/view/theme/icollection/css/font-awesome.min.css" rel="stylesheet">
	<link href="catalog/view/theme/icollection/css/jquery-ui.min.css" rel="stylesheet">
	<link href="catalog/view/theme/icollection/css/animate.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
	<link href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
	<link href="catalog/view/theme/icollection/css/style.css" rel="stylesheet">
	<script src="catalog/view/theme/icollection/js/jquery.min.js"></script>
	<script src="catalog/view/theme/icollection/js/jquery-ui.min.js"></script>
	<script src="catalog/view/theme/icollection/js/bootstrap.min.js"></script>
	<script src="catalog/view/theme/icollection/js/common.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
	<script src="<?php echo $script; ?>"></script>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<!-- HEADER -->
<header class="container-fluid header">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 header__phonebox">
<div class="phone__imgbox">
	<img class="phone__img" src="image/catalog/design/icon/phone.png" alt="phone icon">
</div>
<div class="phone__citynamebox">
	<ul class="phone__ul">
		<li class="phone__list"><div class="col-xs-4 phone__cityname">Riga</div><div class="col-xs-8 phone__number"><?php echo $telephone ?></div></li>
	</ul>
</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 header__logobox">
			<a href="<?php echo $home; ?>">
				<img class="logo__img" src="<?php echo $logo; ?>" alt="<?php echo $name; ?>">
				<h1 class="logo__hidden-title"><?php echo $name; ?></h1>
				<h2 class="logo__tagline">Best Of Fabrics</h2>
			</a>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 header__panelbox">
			<div class="b-panel">
				<div class="panel__languagesbox"><?php echo $language; ?></div>
				<div class="panel__languagesbox"><?php echo $currency; ?></div>
			</div>
			<div class="panel__userpanelbox">
				<ul class="userpanel__ul">
					<?php if ($logged) { ?>
					<li class="dropdown userpanel__list"><a class="userpanel__link" href="<?php echo $account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs"><?php echo $text_account; ?></span></a>
						<ul class="dropdown-menu dropdown-menu-right dropdown_menu-width">
							<li><a class="userpanel__link" href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
							<li><a class="userpanel__link" href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
							<li><a class="userpanel__link" href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
							<li><a class="userpanel__link" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
						</ul>
					</li>
					<?php } else { ?>
					<li class="userpanel__list"><a class="userpanel__link" href="<?php echo $login; ?>" style="position: relative; top: 1px;"><i class="fa fa-key"></i> <span class="hidden-xs"><?php echo $text_login; ?></span></a></li>
					<li class="userpanel__list"><a class="userpanel__link" href="<?php echo $register; ?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="hidden-xs"><?php echo $text_register; ?></span></a></li>
					<?php } ?>
					<li class="userpanel__list"><a class="userpanel__link" id="wishlist-total" href="<?php echo $wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs"><?php echo $text_wishlist; ?></span></a></li>
					<?php echo $cart; ?>
				</ul>
			</div>
		</div>
	</div>
</header>
<!-- /.HEADER -->
<nav class="container-fluid navigation">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg12 nav__menu">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<p class="hidden-sm hidden-md hidden-lg menu__title" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">Меню</p>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				<?php foreach($headermenu as $header){?>
					<li class="menu__list"><a class="menu__link btn-ie9" href="<?php echo $header['link'] ?>"><?php echo $header['name']; ?></a></li>
				<?php } ?>
				</ul>
				<?php echo $search; ?>
			</div>
		</div>
	</div>
</nav>
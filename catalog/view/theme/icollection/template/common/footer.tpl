<div class="container-fluid footer">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer__bg"></div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-123 footer__logo">
			<a class="logo__link" href="#">
				<img class="logo__img footer_img" src="catalog/view/theme/icollection/design/logo/footer-logo.png" alt="footer">
				<p class="logo__tagline footer_logotagline">Best Of Fabrics</p>
			</a>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 footer__contact">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 contact__requisites">
				<h4 class="contact__heders"><?php echo $text_requisites; ?></h4>
				<?php echo $text_requisitesfield; ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 contact__information">
				<h4 class="contact__heders"><?php echo $text_information; ?></h4>
				<ul class="contact__ul">
					<?php foreach ($informations as $information) { ?>
					<li class="contact__list"><a class="contact__link" href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 contact__newsletter">
				<h4 class="contact__heders"><?php echo $text_follow_us; ?></h4>
				<ul class="social__ul">
					<li class="social__list"><a class="social__link social_darkicon fb_icon" href="#">
<svg style="width:10px;" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="45 -127.9 155.7 319.9" enable-background="new 45 -127.9 155.7 319.9" xml:space="preserve">
<g>
	<path style="fill:#fff;" d="M142.3,192c-19.6,0-38.4,0-57.7,0c0-49.8,0-99.3,0-149.4c-13.4,0-26.3,0-39.5,0c0-18.5,0-36.3,0-54.8c13.1,0,26,0,39.4,0
		c0-13.2-0.2-25.8,0.1-38.4c0.2-8,0.1-16.1,1.7-23.8c5-23.9,19.6-40,42.7-48c23.4-8.2,47.3-5.1,70.7-3.2c0,17.2,0,33.8,0,51.1
		c-2.6,0-4.3,0-6.1,0c-10.3,0-20.7-0.2-31,0c-13.9,0.2-19.7,5.5-20.1,19.2c-0.5,14.1-0.1,28.3-0.1,43.1c19.4,0,38.6,0,58.4,0
		c-0.9,18.6-1.7,36.4-2.6,54.7c-18.8,0-37,0-55.8,0C142.3,92.4,142.3,141.9,142.3,192z"/>
</g>
</svg>
					</a></li>
					<li class="social__list"><a class="social__link social_darkicon tw_icon" href="#">
<svg style="width:21px;" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="-22 22.2 20.8 20.8" enable-background="new -22 22.2 20.8 20.8" xml:space="preserve">
<path style="fill:#fff;" d="M-4.6,43c-4.7,0-9.3,0-14,0c-1.8-0.5-2.9-1.7-3.4-3.4c0-4.7,0-9.3,0-14c0.5-1.8,1.7-2.9,3.4-3.4c4.7,0,9.3,0,14,0
	c1.8,0.5,2.9,1.7,3.4,3.4c0,4.7,0,9.3,0,14C-1.7,41.3-2.9,42.5-4.6,43z M-8.8,32c0.2-0.1,0.3-0.2,0.5-0.3c0.9-0.7,1.1-1.6,1-2.7
	c-0.2-1-0.8-1.6-1.7-1.8c-0.5-0.1-1.1-0.2-1.7-0.2c-1.6,0-3.2,0-4.8,0c0,3.8,0,7.5,0,11.4c2.1-0.1,4.3-0.1,6.3-0.3
	c1.6-0.2,2.6-1.5,2.7-2.9C-6.4,33.6-7,32.8-8.8,32z"/>
<path fill="#0000;" d="M-8.8,32c1.8,0.8,2.4,1.6,2.2,3.1c-0.1,1.4-1.2,2.7-2.7,2.9c-2.1,0.2-4.2,0.2-6.3,0.3c0-3.9,0-7.6,0-11.4
	c1.6,0,3.2,0,4.8,0c0.6,0,1.1,0.1,1.7,0.2c0.9,0.3,1.6,0.8,1.7,1.8c0.2,1.1-0.1,2-1,2.7C-8.4,31.8-8.6,31.9-8.8,32z M-12.7,36.2
	c0.9-0.1,1.6-0.1,2.4-0.3c0.6-0.1,0.9-0.7,0.8-1.3c0-0.6-0.3-1-0.9-1.1c-0.8-0.1-1.5-0.1-2.3-0.1C-12.7,34.4-12.7,35.2-12.7,36.2z
	 M-12.7,31.4c0.9-0.1,1.7,0.2,2.2-0.6c0.2-0.3,0.2-1,0-1.3c-0.5-0.7-1.4-0.4-2.2-0.4C-12.7,29.9-12.7,30.6-12.7,31.4z"/>
<path style="fill:#fff;" d="M-12.7,36.2c0-1,0-1.9,0-2.9c0.8,0,1.6,0,2.3,0.1c0.6,0.1,0.9,0.5,0.9,1.1c0,0.6-0.2,1.2-0.8,1.3
	C-11.1,36.1-11.9,36.1-12.7,36.2z"/>
<path style="fill:#fff;" d="M-12.7,31.4c0-0.8,0-1.6,0-2.3c0.8,0,1.7-0.3,2.2,0.4c0.2,0.3,0.2,1,0,1.3C-11,31.6-11.8,31.3-12.7,31.4z"/>
</svg>
					</a></li>
					<li class="social__list"><a class="social__link social_darkicon go_icon" href="#">
<svg style="width: 20px;margin-left:4px;" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="65 -65 195 195" enable-background="new 65 -65 195 195" xml:space="preserve">
<g>
	<path style="fill:#fff"d="M154.3,73.2c-5-0.9-8.7-1.6-12.5-2.2c-11.1-1.9-20.8-6.9-29.7-13.6c-7.4-5.7-8.7-11.1-4.2-17.4c4-5.6,10.5-6.3,17.7-1.3
		c18.7,12.8,38.8,15,60,7.7c5.4-1.9,10.3-5.3,15.4-8.1c4.8-2.7,9.5-4.3,14.5-0.8c6.7,4.7,6.8,13.1,0.1,18.7
		c-10.2,8.5-21.9,13.9-35.1,15.7c-2.4,0.3-4.8,0.8-8.3,1.5c1.7,2.1,2.7,3.5,3.9,4.7c10.6,10.6,21.2,21.2,31.7,31.9
		c4,4.1,4.8,8.8,2.5,13.3c-2.4,4.9-7.1,7.8-12.2,6.6c-2.7-0.6-5.4-2.4-7.5-4.4c-8.2-7.8-16.2-15.8-24.1-24c-2.8-2.9-4.5-2.6-7.2,0.1
		c-7.8,8-15.8,15.8-23.7,23.7c-6.2,6.2-12.3,6.7-17.4,1.6c-5.3-5.3-5-12,1-18.1c10.2-10.3,20.5-20.5,30.7-30.8
		C151.4,76.5,152.4,75.3,154.3,73.2z"/>
	<path style="fill:#fff;"d="M211-14c0,27.5-20.5,47.9-48,47.9c-27.6,0-48-20.7-47.9-48.5c0.1-26.7,21.5-47.9,48.3-48C187.7-62.7,212.3-41.9,211-14z
		 M162.9,10.4c13.7,0,25.1-11.1,25.2-24.4c0-13.1-11.6-25-24.8-25.2c-12.9-0.2-25.1,11.9-25.2,24.8C138-0.9,149.3,10.4,162.9,10.4z"
		/>
</g>
</svg>
					</a></li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 contact__worktime">
				<h4 class="contact__heders"><?php echo $text_work_time; ?></h4>
				<ul class="contact__ul">
					<li class="contact__list"><?php echo $open; ?></li>
				</ul>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer__copyright">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 copyright"><small>© I-Collection 2015. Copyright</small></div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
		</div>
	</div>
</div>
</body></html>
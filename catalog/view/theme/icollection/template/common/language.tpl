<?php if (count($languages) > 1) { ?>
<div class="pull-right">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
  <div class="btn-group">
    <button class="btn btn-link dropdown-toggle language__button" data-toggle="dropdown">
    <?php foreach ($languages as $language) { ?>
    <?php if ($language['code'] == $code) { ?>
	<span class="hidden-xs hidden-sm hidden-md language__name"><?php echo $language['name']; ?></span> <span class="language__code">(<?php echo $language['code']; ?>)</span> <i class="fa fa-caret-down"></i></button>
    <?php } ?>
    <?php } ?>
    <ul class="dropdown-menu languages_dropdown-menu">
      <?php foreach ($languages as $language) { ?>
      <li><a href="<?php echo $language['code']; ?>"><?php echo $language['name']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
</div>
<?php } ?>

<?php echo $header; ?>
<div class="container main">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2 class="myaccount__header">Личный Кабинет</h2>
		<h3 class="myaccount__hello">Здраствуйте, <?php echo $firstname; ?> <?php echo $lastname; ?>!</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis molestias, reiciendis quis eveniet deserunt dolores iusto sed natus eaque doloremque animi ratione earum quaerat autem nulla dolore enim dolor quasi!</p>
		<div class="col-xs-12 col-md-6">
			<h4 class="myaccount__system-header">Контактная информация <a href="<?php echo $edit; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h4>
			<ul class="myaccount__ul">
				<li class="myaccount__list"><div class="col-md-3 myaccount_sysheder">Имя:</div><div class="col-md-9"><?php echo $firstname; ?></div></li>
				<li class="myaccount__list"><div class="col-md-3 myaccount_sysheder">Фамилия:</div><div class="col-md-9"><?php echo $lastname; ?></div></li>
				<li class="myaccount__list"><div class="col-md-3 myaccount_sysheder">Телефон:</div><div class="col-md-9"><?php echo $telephone; ?></div></li>
				<li class="myaccount__list"><div class="col-md-3 myaccount_sysheder">E-mail:</div><div class="col-md-9"><?php echo $email; ?></div></li>
				<li class="myaccount__list"><div class="col-md-3 myaccount_sysheder">Факс:</div><div class="col-md-9"><?php echo $fax; ?></div></li>
			</ul>
		</div>
		<div class="col-xs-12 col-md-6 myaccount__newsletter">
			<h4 class="myaccount__system-header">Подписка на рассылку новостей <a href="<?php echo $newsletter; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident sint veritatis quam eligendi deleniti saepe officia qui in culpa quo, adipisci delectus molestiae deserunt possimus beatae ab architecto, perferendis enim?</p>
          <div class="form-group">
            <label class="col-sm-2 control-label">Подписан(а)</label>
            <div class="col-sm-10">
              <?php if ($newsletters) { ?>
              <label class="radio-inline">
                <input type="radio" name="newsletters" value="1" checked="checked" disabled/>
                <?php echo $text_yes; ?> </label>
              <label class="radio-inline">
                <input type="radio" name="newsletters" value="0" disabled/>
                <?php echo $text_no; ?></label>
              <?php } else { ?>
              <label class="radio-inline">
                <input type="radio" name="newsletters" value="1" disabled/>
                <?php echo $text_yes; ?> </label>
              <label class="radio-inline">
                <input type="radio" name="newsletters" value="0" checked="checked" disabled/>
                <?php echo $text_no; ?></label>
              <?php } ?>
            </div>
          </div>
		</div>
		<div class="col-xs-12 col-md-6">
			<h4 class="myaccount__system-header">Изменить мои адреса <a href="<?php echo $address; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis molestias, reiciendis quis eveniet deserunt dolores iusto sed.</p>
		</div>
		<div class="col-xs-12 col-md-6">
			<h4 class="myaccount__system-header">Посмотреть закладки <a href="<?php echo $wishlist; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis molestias, reiciendis quis eveniet deserunt dolores iusto sed.</p>
		</div>
		<div class="col-xs-12 col-md-6">
			<h4 class="myaccount__system-header">История транзакций <a href="<?php echo $transaction; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis molestias, reiciendis quis eveniet deserunt dolores iusto sed.</p>
		</div>
		<div class="col-xs-12 col-md-6">
			<h4 class="myaccount__system-header">История заказов <a href="<?php echo $order; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis molestias, reiciendis quis eveniet deserunt dolores iusto sed.</p>
		</div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<?php foreach ($products as $product) { ?>
<div class="col-xs-6 col-sm-3 col-md-1 featured__products">
	<a class="products__link" href="<?php echo $product['href']; ?>">
		<img class="products__image" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
	</a>
</div>
<?php } ?>
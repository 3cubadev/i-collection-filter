<div class="catalog__menu__list">
<h3 class="catalog__title">Каталог</h3>
<ul class="catalog__ul">
<?php foreach ($categories as $category) { ?>
	<li class="catalog__list"><a class="catalog__link catalog_menulink catalog_menutitle" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?> <span class="glyphicon glyphicon-triangle-bottom hidden-md hidden-lg" aria-hidden="true"></span></a>
		<?php if ($category['children']) { ?>
		<ul class="catalog__dropdownmenu">
			<?php foreach ($category['children'] as $child) { ?>
			<li class="catalog__dropdownlist">
				<a class="catalog__dropdownlink" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
					<ul class="catalog__dropdownmenu">
						<?php foreach ($child['children'] as $child) { ?>
						<li class="catalog__dropdownlist">
							<a class="catalog__dropdownlink" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
						</li>
						<?php } ?>
					</ul>
				</li>
			<?php } ?>
		</ul>
  <?php } ?>
	</li>
<?php } ?>
</ul>
</div>
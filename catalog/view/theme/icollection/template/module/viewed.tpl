<h4 class="recently-viewed__title"><?php echo $heading_title; ?></h4>
<div class="row recently-viewed_row">
  <?php foreach ($products as $product) { ?>
  <div class="col-xs-6 col-sm-4 col-md-1 recently_vievedbox">
    <a href="<?php echo $product['href']; ?>"><img class="products__image" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
  </div>
  <?php } ?>
</div>

<div class="<?php echo $css; ?> main__banner">
<?php foreach ($banners as $banner) { ?>
	<?php if ($price) { ?>
		<a class="banner__link banner_smoth" href="<?php echo $banner['link']; ?>">
			<img class="banner__img" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>">
			<h3 class="banner__heder banner_stocktitle">
				<?php echo $title; ?><br><span class="banner__stock"><?php echo $price; ?></span>
			</h3>
			<p class="banner__text"><?php echo $description; ?></p>
		</a>
	<?php } else { ?>
		<a class="banner__link banner_smoth" href="<?php echo $banner['link']; ?>">
			<img class="banner__img" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>">
			<h3 class="banner__heder"><?php echo $title; ?></h3>
			<p class="banner__text"><?php echo $description; ?></p>
		</a>
	<?php } ?>
<?php } ?>
</div>
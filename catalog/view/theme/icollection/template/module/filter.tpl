<h2 class="filter__heder"><?php echo $heading_title; ?></h2>
<div class="filter__mobileToggle">
<?php foreach ($filter_groups as $filter_group) { ?>
	<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 filter__box">
		<h3 class="filter__title"><?php echo $filter_group['name']; ?></h3>
		<ul class="filter__ul">
		<?php foreach ($filter_group['filter'] as $filter) { ?>
			<li class="filter__list">
			<?php if (in_array($filter['filter_id'], $filter_category)) { ?>
				<input class="filter__input" type="checkbox" name="filter[]" id="filter_<?php echo $filter['filter_id']; ?>" value="<?php echo $filter['filter_id']; ?>" checked="checked" />
				<label class="filter__label" for="filter_<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></label>
			<?php } else { ?>
				<input class="filter__input" type="checkbox" name="filter[]" id="filter_<?php echo $filter['filter_id']; ?>" value="<?php echo $filter['filter_id']; ?>"/>
				<label class="filter__label" for="filter_<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></label>
			<?php } ?>
			</li>
		<?php } ?>
		</ul>
	</div>
<?php } ?>
<div class="col-xs-12 no_padding text-center">
<div class="btn-icoll filter_buttonsearch" id="button-filter"><?php echo $button_search; ?></div>
</div>
</div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	filter = [];

	$('input[name^=\'filter\']:checked').each(function(element) {
		filter.push(this.value);
	});

	location = '<?php echo $action; ?>&filter=' + filter.join(',');
});
//--></script>

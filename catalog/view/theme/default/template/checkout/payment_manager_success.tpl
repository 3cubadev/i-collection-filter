<?php if ($instruction){ ?><div id="instruction-form"><?php echo $instruction; ?></div><?php } ?>
<div class="buttons">
    <div class="right pull-right">
        <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="button btn btn-primary" />
    </div>
</div>
<script type="text/javascript"><!--
$(document).ready(function(){
    if (!$('#instruction-form').parent().hasClass('content') && !$('#instruction-form').parent().hasClass('simplecheckout-block-content'))
    {
        $('#instruction-form').attr('class','content');
    }
});
$('#button-confirm').bind('click', function() {
	$.ajax({ 
		type: 'get',
		url: 'index.php?route=checkout/payment_manager/confirm',
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},
		success: function() {
			location = '<?php echo $continue; ?>';
		}
	});
});
//--></script>
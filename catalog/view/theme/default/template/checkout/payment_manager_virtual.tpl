<div class="buttons">
    <div class="pull-right">
        <input type="button" value="<?php echo $button_complete; ?>" id="button-complete" class="btn btn-primary" />
    </div>
</div>
<script type="text/javascript"><!--
$('#button-complete').on('click', function() {
	$.ajax({ 
		type: 'get',
		url: 'index.php?route=checkout/payment_manager/complete&order_id=<?php echo $order_id; ?>',
		cache: false,
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},		
		success: function() {
			location = '<?php echo $continue; ?>';
		}		
	});
});
//--></script> 
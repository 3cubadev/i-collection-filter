<?php echo $header; ?>
<style>
.buttons{display:none;}
</style>
<?php if (OC_VERSION == '2.0.x'){ ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <div class="row">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <?php echo $content_top; ?>
            <h1><?php echo $heading_title; ?></h1>
            <div class="table-responsive form main-content">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <td class="text-left"><?php echo $column_name; ?></td>
                            <td class="text-left"><?php echo $column_model; ?></td>
                            <td class="text-right right"><?php echo $column_quantity; ?></td>
                            <td class="text-right right"><?php echo $column_price; ?></td>
                            <td class="text-right right"><?php echo $column_total; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($products as $product) { ?>
                        <tr>
                            <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            <?php foreach ($product['option'] as $option) { ?>
                            <br />
                            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                            <?php } ?>
                            </td>
                            <td class="text-left"><?php echo $product['model']; ?></td>
                            <td class="text-right right"><?php echo $product['quantity']; ?></td>
                            <td class="text-right right"><?php echo $product['price']; ?></td>
                            <td class="text-right right"><?php echo $product['total']; ?></td>
                        </tr>
                        <?php } ?>
                        <?php foreach ($vouchers as $voucher) { ?>
                        <tr>
                            <td class="text-left"><?php echo $voucher['description']; ?></td>
                            <td class="text-left"></td>
                            <td class="text-right right">1</td>
                            <td class="text-right right"><?php echo $voucher['amount']; ?></td>
                            <td class="text-right right"><?php echo $voucher['amount']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <?php foreach ($totals as $total) { ?>
                        <tr>
                            <td colspan="4" class="text-right"><strong><?php echo $total['title']; ?>:</strong></td>
                            <td class="text-right"><?php echo $total['text']; ?></td>
                        </tr>
                        <?php } ?>
                    </tfoot>
                </table>
            </div>
            <div id="payment_form"><?php echo $payment_form; ?></div>
            <div id="payment-buttons" class="buttons">
                <div class="left pull-left"><a href="<?php echo $cancel; ?>" class="button btn btn-primary"><?php echo $button_cancel; ?></a></div>
                <div class="right pull-right"><a href="#" onclick="clickPay(); return false;" class="button btn btn-primary"><?php echo $button_pay; ?></a></div>
            </div>
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php } else { ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content">
    <?php echo $content_top; ?>
    <h1><?php echo $heading_title; ?></h1>
    <div class="table-responsive checkout-product main-content">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <td class="text-left"><?php echo $column_name; ?></td>
                    <td class="text-left"><?php echo $column_model; ?></td>
                    <td class="right"><?php echo $column_quantity; ?></td>
                    <td class="right"><?php echo $column_price; ?></td>
                    <td class="right"><?php echo $column_total; ?></td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product) { ?>
                <tr>
                    <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                    <?php foreach ($product['option'] as $option) { ?>
                    <br />
                    &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                    <?php } ?>
                    </td>
                    <td class="text-left"><?php echo $product['model']; ?></td>
                    <td class="right"><?php echo $product['quantity']; ?></td>
                    <td class="right"><?php echo $product['price']; ?></td>
                    <td class="right"><?php echo $product['total']; ?></td>
                </tr>
                <?php } ?>
                <?php foreach ($vouchers as $voucher) { ?>
                <tr>
                    <td class="left"><?php echo $voucher['description']; ?></td>
                    <td class="left"></td>
                    <td class="right">1</td>
                    <td class="right"><?php echo $voucher['amount']; ?></td>
                    <td class="right"><?php echo $voucher['amount']; ?></td>
                </tr>
                <?php } ?>
            </tbody>
            <tfoot>
                <?php foreach ($totals as $total) { ?>
                <tr>
                    <td colspan="4" class="right"><strong><?php echo $total['title']; ?>:</strong></td>
                    <td class="right"><?php echo $total['text']; ?></td>
                </tr>
                <?php } ?>
            </tfoot>
        </table>
    </div>
    <div id="payment_form" class="payment_form" style="border: 1px dashed #ddd; padding: 10px;" class="main-content"><?php echo $payment_form; ?></div>
    <div id="payment-buttons" class="buttons">
        <div class="left text-left"><a href="<?php echo $cancel; ?>" class="button btn btn-primary"><?php echo $button_cancel; ?></a></div>
        <div class="right text-right"><a href="#" onclick="clickPay(); return false;" class="button btn btn-primary"><?php echo $button_pay; ?></a></div>
    </div>
    <?php echo $content_bottom; ?>
</div>
<?php } ?>

<script type="text/javascript">
$(document).ready(function(){
    if ($('#button-confirm, #payment_form .buttons a, #payment_form input[type="submit"], #payment_form input[type="button"], #payment_form button, #payment_form .button').length > 1)
    {
        $('#button-confirm, #payment_form .buttons a, #payment_form input[type="submit"], #payment_form input[type="button"], #payment_form button, #payment_form .button').first().hide();
    }
    $('#payment-buttons').show();
});

function clickPay()
{
    $('#button-confirm, #payment_form .buttons a, #payment_form input[type="submit"], #payment_form input[type="button"], #payment_form button, #payment_form .button').first().click();
}
</script>
<?php echo $footer; ?>
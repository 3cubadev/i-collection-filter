<div class="buttons">
    <div class="right pull-right">
        <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="button btn btn-primary" />
    </div>
</div>
<script type="text/javascript"><!--
$('#button-confirm').bind('click', function() {
	$.ajax({ 
		type: 'get',
		url: 'index.php?route=checkout/payment_manager/confirm',
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},
		success: function() {
			location = '<?php echo $continue; ?>';
		}
	});
});
//--></script> 
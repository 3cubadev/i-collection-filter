<?php
// Text
$_['text_information']  = 'Other information';
$_['text_requisites']   = 'Requisite';
$_['text_requisitesfield'] = '<ul class="contact__ul"><li class="contact__list">Montenegro LTD</li><li class="contact__list">LV40103845311</li><li class="contact__list">Norvik Banka Riga</li><li class="contact__list">LV39LATB0002210061664</li><li class="contact__list">LATBLV22XXX</li><li class="contact__list">Mail to: Terbatas str. 49/51-2, Riga</li><li class="contact__list">LV-1011, Latvia</li><li class="contact__list">Ship to: Maskavas str. 12, Riga</li><li class="contact__list">LV-1050, Latvia</li><li class="contact__list">+371 24422833</li></ul>';
$_['text_work_time']    = 'Business hours';
$_['text_follow_us']    = 'Follow us';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
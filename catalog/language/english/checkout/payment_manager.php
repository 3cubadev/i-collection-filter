<?php
// Heading
$_['heading_title']             = 'Your Order Has Been Accepted!';
$_['heading_title_pay']         = 'Complete payment for order #%s';

// Text
$_['text_basket']               = 'Shopping Cart';
$_['text_checkout']             = 'Checkout';
$_['text_success']              = 'Success';
$_['text_account']              = 'Account';
$_['text_order_history']        = 'Order History';
$_['text_complete']             = 'Order is successfully completed';

// Column
$_['column_name']               = 'Product';
$_['column_model']              = 'Model';
$_['column_quantity']           = 'Quantity';
$_['column_price']              = 'Price';
$_['column_total']              = 'Total';

// Button
$_['button_pay']                = 'Pay order';
$_['button_complete']           = 'Complete order';
$_['button_cancel']             = 'Cancel';

// Error
$_['error_invalid_order']       = 'Invalid order!';
?>
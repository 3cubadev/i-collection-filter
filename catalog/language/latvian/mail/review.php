<?php
// Text
$_['text_subject']	= '%s - Atsauksme par preci';
$_['text_waiting']	= 'Jums ir jauna atsauksme par preci.';
$_['text_product']	= 'Prece: %s';
$_['text_reviewer']	= 'Atsauksmes autors: %s';
$_['text_rating']	= 'Vērtējums: %s';
$_['text_review']	= 'Atsauksmes teksts:';

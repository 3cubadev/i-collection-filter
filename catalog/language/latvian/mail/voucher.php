<?php
// Text
$_['text_subject']  = '%s Jums ir atsūtījis dāvanu karti!';
$_['text_greeting'] = 'Apsveicam! Jūs esat saņēmis dāvanu karti %s vērtībā';
$_['text_from']     = 'Šo dāvanu karti Jums ir nosūtījis(-usi) %s';
$_['text_message']  = 'Pie dāvanu kartes ir šāds ziņojums';
$_['text_redeem']   = 'Lai atprečotu šo dāvanu karti, pierakstiet šo aptprečošanas kodu:<b>%s</b> Pēc tam klikšķinet uz šeit redzamās saites un veiciet sev tīkamu pirkumu, izmantojot šo dāvanu karti. Jūs varēsiet ievadīt dāvanu kartes kodu pirms pirkuma noformēšanas.';
$_['text_footer']   = 'Ja Jums ir jautājumi, lūdzu, rakstiet uz šo pašu e-pasta adresi.';
?>
<?php
// Text
$_['text_subject']  = '%s - Paldies par reģistrēšanos!';
$_['text_welcome']  = 'Paldies par reģistrēšanos! Laipni lūdzam %s!';
$_['text_login']    = 'Jūsu profils tagad ir izveidots, un Jūs varat autorizēties ar savu e-pasta adresi un paroli šajā interneta vietnē:';
$_['text_approval'] = 'Lai varētu autorizēties, Jūsu profilam jābūt apstiprinātam. Kad tas būs apstiprināts, Jūs varēsiet autorizēties ar savu e-pasta adresi un paroli, apmeklējot mūsu interneta vietni šādā adresē:';
$_['text_services'] = 'Pēc autorizēšanās Jūs varēsiet piekļūt papildu pakalpojumiem, tādiem kā savu veikto pasūtījumu pārskats, rēķinu drukāšana un sava profila informācijas rediģēšana.';
$_['text_thanks']   = 'Ar cieņu,';
$_['text_new_customer']   = 'Jauns pircējs';
$_['text_signup']         = 'Ir reģistrējies jauns pircējs:';
$_['text_website']        = 'Interneta vietne:';
$_['text_customer_group'] = 'Pircēju grupa:';
$_['text_firstname']      = 'Vārds:';
$_['text_lastname']       = 'Uzvārds:';
$_['text_email']          = 'E-pasts:';
$_['text_telephone']      = 'Tālruņa numurs:';
?>
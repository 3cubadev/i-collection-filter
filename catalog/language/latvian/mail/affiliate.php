<?php
// Text
$_['text_subject']  = '%s - partneru programma';
$_['text_welcome']  = 'Paldies par jūsu reģistrēšanos %s partneru programmā!';
$_['text_login']                = 'Jūsu profils tagad ir izveidots un jūs varat autorizēties, izmantojot jūsu e-pasta adresi un paroli atverot šo saiti:';
$_['text_approval'] = 'Lai varētu autorizēties, Jūsu profilam jābūt apstiprinātam. Kad tas būs apstiprināts, Jūs varēsiet autorizēties ar savu e-pasta adresi un paroli, apmeklējot mūsu interneta vietni šādā adresē:';
$_['text_services'] = 'Pēc autorizēšanās Jūs varēsiet ģenerēt uzskaites kodus, sekot saviem komisijas maksājumiem un rediģēt savu profila informāciju.';
$_['text_thanks']   = 'Paldies,';
$_['text_new_affiliate']        = 'Jauns partneris';
$_['text_signup']		        = 'Ir reģistrējies jauns partneris:';
$_['text_store']		        = 'Interneta vietne:';
$_['text_firstname']	        = 'Vārds:';
$_['text_lastname']		        = 'Uzvārds:';
$_['text_company']		        = 'Uzņēmums:';
$_['text_email']		        = 'E-pasts:';
$_['text_telephone']	        = 'Tālrunis:';
$_['text_website']		        = 'Interneta vietne:';
$_['text_order_id']             = 'Pasūtījuma Nr.:';
$_['text_transaction_subject']  = '%s - Partneria komisija';
$_['text_transaction_received'] = 'Jūs esat saņemis %s komisiju!';
$_['text_transaction_total']    = 'Jūsu kopējā komisiju summa tagad ir %s.';
?>
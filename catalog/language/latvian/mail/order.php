<?php
// Text
$_['text_new_subject']          = '%s - pasūtījums %s';
$_['text_new_greeting']         = 'Paldies par jūsu interesi par %s piedāvātajām precēm. Jūsu pasūtījums ir saņemts un tiks apstrādāts, tiklīdz būs apstiprināts maksājums.';
$_['text_new_received']         = 'Jūs esat saņēmis (-usi) pasūtījumu.';
$_['text_new_link']             = 'Klikšķiniet uz saites apakšā, lai apskatītu savu pasūtījumu:';
$_['text_new_order_detail']     = 'Pasūtījuma detalizācija';
$_['text_new_instruction']      = 'Instrukcija';
$_['text_new_order_id']         = 'Pasūtījuma Nr.:';
$_['text_new_date_added']       = 'Pievienots:';
$_['text_new_order_status']     = 'Pasūtījuma statuss:';
$_['text_new_payment_method']   = 'Maksājuma veids:';
$_['text_new_shipping_method']  = 'Piegādes veids:';
$_['text_new_email']  			= 'E-pasta adrese:';
$_['text_new_telephone']  		= 'Tālruņa nr.:';
$_['text_new_ip']  				= 'IP adrese:';
$_['text_new_payment_address']  = 'Maksātāja adrese';
$_['text_new_shipping_address'] = 'Piegādes adrese';
$_['text_new_products']         = 'Preces';
$_['text_new_product']          = 'Prece';
$_['text_new_model']            = 'Modelis';
$_['text_new_quantity']         = 'Daudzums';
$_['text_new_price']            = 'Cena';
$_['text_new_order_total']      = 'Pasūtījuma summa';	
$_['text_new_total']            = 'Kopā';	
$_['text_new_download']         = 'Jūs varēsiet klikšķināt uz šo saiti, lai piekļūtu savām lejupielādējamajām precēm, tiklīdz būs saņemts maksājuma apstiprinājums:';
$_['text_new_comment']          = 'Jūsu pasūtījuma papildinformācija:';
$_['text_new_footer']           = 'Ja Jums ir kādi jautājumi, lūdzu, rakstiet uz šo pašu epasta adresi.';
$_['text_update_subject']       = '%s - Pasūtījuma jauninājums %s';
$_['text_update_order']         = 'Pasūtījuma Nr.:';
$_['text_update_date_added']    = 'Pasūtījums veikts:';
$_['text_update_order_status']  = 'Jūsu pasūtījuma jaunais statuss ir:';
$_['text_update_comment']       = 'Papildu informācija par Jūsu pasūtījumu:';
$_['text_update_link']          = 'Lūdzu, klikšķiniet uz šo saiti, lai apskatītu savu pasūtījumu:';
$_['text_update_footer']        = 'Ja Jums ir jautājumi, lūdzu, rakstiet uz šo pašu epasta adresi.';
?>
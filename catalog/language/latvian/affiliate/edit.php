<?php
// Heading 
$_['heading_title']     = 'Mana profila informācija';

// Text
$_['text_account']      = 'Profils';
$_['text_edit']         = 'Rediģēt informāciju';
$_['text_your_details'] = 'Jūsu personas dati';
$_['text_your_address'] = 'Jūsu adrese';
$_['text_success']      = 'Jūsu profils tika veiksmīgi rediģēts.';

// Entry
$_['entry_firstname']   = 'Vārds';
$_['entry_lastname']    = 'Uzvārds';
$_['entry_email']       = 'E-pasts';
$_['entry_telephone']   = 'Tālruņa numurs';
$_['entry_fax']         = 'Fakss';
$_['entry_company']     = 'Uzņēmums';
$_['entry_website']     = 'Interneta adrese';
$_['entry_address_1']   = 'Adrese 1';
$_['entry_address_2']   = 'Adrese 2';
$_['entry_postcode']    = 'Pasta indekss';
$_['entry_city']        = 'Pilsēta';
$_['entry_country']     = 'Valsts';
$_['entry_zone']        = 'Reģions';

// Error
$_['error_exists']      = 'Kļūda: Šāda e-pasta adrese jau ir reģistrēta!!';
$_['error_firstname']   = 'Jūsu vārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']    = 'Jūsu uzvārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_email']       = 'E-pasta adrese ir ievadīta nepareizi!';
$_['error_telephone']   = 'Tālruņa numuram jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_address_1']   = 'Jūsu adresei ir jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_city']        = 'Jūsu pilsētas nosaukumam ir jābūt no 2 līdz 128 rakstzīmēm!!';
$_['error_country']     = 'Lūdzu, norādiet savu valsti!';
$_['error_zone']        = 'Lūdzu, norādiet savu reģionu!';
$_['error_postcode']    = 'Jūsu pasta indeksam ir jābūt no 2 līdz 10 rakstzīmēm!';
?>
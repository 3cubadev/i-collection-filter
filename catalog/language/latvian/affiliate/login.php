<?php
// Heading 
$_['heading_title']                 = 'Partneru programma';

// Text
$_['text_account']                  = 'Profils';
$_['text_login']                    = 'Autorizēties';
$_['text_description']              = '<p>%s partneru programma ir bezmaksas un ļauj tās biedriem nopelnīt naudu, izvietojot savā interneta vietnē saiti vai saites (linkus), kuras reklamē  %s vai tā atsevišķas preces. Partneru programmas dalībnieks nopelna komisijas naudu par jebkuru preci, kura ir pārdota pircējam, kurš uzklikšķinājis uz partnera saiti. Standarta komisijas likme patlaban ir %s.</p><p>Papildu informācija ir atrodama mūsu jautājumu un atbilžu lapā vai partneru programmas noteikumos.</p>';
$_['text_new_affiliate']            = 'Jauns partneris';
$_['text_register_account']         = '<p>Es vēl neesmu partneru programmas dalībnieks.</p><p>Klikšķiniet uz Turpināt, lai izveidotu jaunu partnera profilu.  Lūdzu ievērojiet, ka šis profils nav nekādā veidā saistīts ar jūsu klienta profilu.</p>';
$_['text_returning_affiliate']      = 'Partnera autorizēšanās';
$_['text_i_am_returning_affiliate'] = 'Es esmu reģistrēts partneris.';
$_['text_forgotten']                = 'Aizmirsta parole';

// Entry
$_['entry_email']                   = 'Partnera e-pasts';
$_['entry_password']                = 'Parole';

// Error
$_['error_login']                   = 'Kļūda: Nav atrasta tāda e-pasta adrese un/vai parole.';
$_['error_attempts']               = 'Kļūda: Jūsu profilam ir pārsniegs atļauto autorizēšanās mēģinājumu skaits. Lūdzu mēģiniet atkal pēc 1 stundas.';
$_['error_approved']                = 'Kļūda: Jūsu profilam jābūt apstiprinātam pirms autorizēšanās.';
?>

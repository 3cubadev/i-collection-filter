<?php
// Heading
$_['heading_title'] = 'Jūsu partnera profils ir izveidots!';

// Text 
$_['text_message']  = '<p>Apsveicam! Jūsu jaunais profils ir veiksmīgi izveidots!</p> <p>Tagad jūs esat viens no %s partneriem.</p> <p>Lūdzu rakstiet mums e-pastu, ja jums ir JEBKĀDI jautājumi par šī veikala partneriu sistēmas darbību.</p> <p>Pieteikšanās apstiprinājums ir nosūtīts uz e-pasta adresi, kuru jūs norādījāt. Lūdzu <a href="%s">sazinieties ar mums</a>, ja jūs stundas laikā nesaņemat apstiprinājumu.</p>';
$_['text_approval'] = '<p>Paldies par Jūsu %s partnera profila reģistrēšanu!</p><p>Jums tiks paziņots pa e-pastu, tiklīdz Jūsu profils būs aktivizēts.<p><p>Ja Jums ir JEBKĀDI jautājumi par šīs vietnes darbību, lūdzu, <a href="%s">sazinieties ar mums</a>.</p>'; 
$_['text_account']  = 'Profils';
$_['text_success']  = 'Pabeigts';
?>
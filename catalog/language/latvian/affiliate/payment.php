<?php
// Heading 
$_['heading_title']             = 'Maksāšanas veids';

// Text
$_['text_account']              = 'Profils';
$_['text_payment']              = 'Maksājums';
$_['text_your_payment']         = 'Informācija par maksājumu';
$_['text_your_password']        = 'Jūsu parole';
$_['text_cheque']               = 'Čeks';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Pārskaitījums';
$_['text_success']              = 'Jūsu profils tika veiksmīgi rediģēts.';

// Entry
$_['entry_tax']                 = 'Reģistrācijas numurs (vai personas kods)';
$_['entry_payment']             = 'Maksājuma veids';
$_['entry_cheque']              = 'Čeka saņēmēja vārds';
$_['entry_paypal']              = 'PayPal profila e-pasts';
$_['entry_bank_name']           = 'Bankas nosaukums';
$_['entry_bank_branch_number']  = 'ABA/BSB numurs (Filiāles numurs)';
$_['entry_bank_swift_code']     = 'SWIFT kods';
$_['entry_bank_account_name']   = 'Saņēmēja vārds';
$_['entry_bank_account_number'] = 'Konta numurs';
?>
<?php
// Heading 
$_['heading_title']        = 'Mans partnera profils';

// Text
$_['text_account']         = 'Profils';
$_['text_my_account']      = 'Mans partnera profils';
$_['text_my_tracking']     = 'Mana uzskaite';
$_['text_my_transactions'] = 'Mani darījumi';
$_['text_edit']            = 'Rediģēt savu profilu';
$_['text_password']        = 'Mainīt paroli';
$_['text_payment']         = 'Rediģēt maksājumu iestatījumus';
$_['text_tracking']        = 'Pielāgots partnera uzskaites kods';
$_['text_transaction']     = 'Apskatīt darījumu vēsturi';
?>
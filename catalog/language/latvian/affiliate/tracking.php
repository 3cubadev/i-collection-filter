<?php
// Heading 
$_['heading_title']    = 'Partnera uzskaite';

// Text
$_['text_account']     = 'Profils';
$_['text_description'] = 'Lai nodrošinātu, ka Jums tiek samaksāts par no Jūsu interneta vietnes novirzīto klientu pirkumiem, mums ir nepieciešams izvietot uzskaites kodu interneta saitē (URL), ar kuru apmeklēt mūsu interneta vietni. Jūs varat izmatot zemāk atrodamos rīkus, lai ģenerētu saiti uz %s interneta vietni.';

// Entry
$_['entry_code']        = 'Jūsu uzskaites kods';
$_['entry_generator']   = 'Izvietojamās saites URL ģenerēšana';
$_['entry_link']        = 'Izvietojamā saite';

// Help
$_['help_generator']  = 'Ierakstiet preces nosaukumu, kurai vēlaties ģenerēt uzskaites URL. Type in the name of a product you would like to link to';
?>
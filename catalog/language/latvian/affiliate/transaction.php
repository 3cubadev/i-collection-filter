<?php
// Heading 
$_['heading_title']      = 'Jūsu darījumi';

// Column
$_['column_date_added']  = 'Pievienots';
$_['column_description'] = 'Apraksts';
$_['column_amount']      = 'Summa (%s)';

// Text
$_['text_account']       = 'Konts';
$_['text_transaction']   = 'Jūsu darījumi';
$_['text_balance']       = 'Jūsu pašreizējā bilance ir:';
$_['text_empty']         = 'Jums vēl nav neviena darījuma!';
?>
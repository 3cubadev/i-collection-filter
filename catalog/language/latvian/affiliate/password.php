<?php
// Heading 
$_['heading_title']  = 'Paroles maiņa';

// Text
$_['text_account']   = 'Profils';
$_['text_password']  = 'Jūsu parole';
$_['text_success']   = 'Jūsu parole ir veiksmīgi nomainīta!';

// Entry
$_['entry_password'] = 'Parole';
$_['entry_confirm']  = 'Atkārtojiet paroli';

// Error
$_['error_password'] = 'Jūsu parolei ir jābūt no 4 līdz 20 rakstzīmēm!';
$_['error_confirm']  = 'Jūsu paroles nesakrīt!';
?>

<?php
/*
* Opencart Localization by 1.Partneris info@partneris.net
* All rights reserved.
* Source here: http://www.partneris.lv/latvian-lang-pck-01
* 
*/

// Locale
$_['code']                  = 'Lv';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd.m.Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'd.m.Y H:i:s';
$_['decimal_point']         = ',';
$_['thousand_point']        = ' ';

// Text
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_yes']              = 'Jā';
$_['text_no']               = 'Nē';
$_['text_none']             = ' --- Nav izvēlēts --- ';
$_['text_select']           = ' --- Izvēlieties --- ';
$_['text_all_zones']        = 'Visi reģioni';
$_['text_pagination']       = 'Rāda no %d līdz %d kopā %d (Kopā lapu - %d)';
$_['text_loading']          = 'Gaidiet...';

// Buttons
$_['button_address_add']    = 'Pievienot adresi';
$_['button_back']           = 'Atpakaļ';
$_['button_continue']       = 'Turpināt';
$_['button_cart']           = 'Ielikt grozā';
$_['button_cancel']                 = 'Atcelt';
$_['button_compare']        = 'Pievienot salīdzināšanai';
$_['button_wishlist']       = 'Pievienot vēlmju sarakstam';
$_['button_checkout']       = 'Noformēt pasūtījumu';
$_['button_confirm']        = 'Apstiprināt pasūtījumu';
$_['button_coupon']         = 'Izmantot kuponu';
$_['button_delete']         = 'Dzēst';
$_['button_download']       = 'Lejupielādēt';
$_['button_edit']           = 'Rediģēt';
$_['button_filter']         = 'Filtrēt';
$_['button_new_address']    = 'Pievienot jaunu adresi';
$_['button_change_address'] = 'Mainīt adresi';
$_['button_reviews']        = 'Atsauksmes';
$_['button_write']          = 'Rakstīt atsauksmi';
$_['button_login']          = 'Autorizēties';
$_['button_update']         = 'Veikt izmaiņas';
$_['button_remove']         = 'Noņemt';
$_['button_reorder']        = 'Atkārtoti pasūtīt';
$_['button_return']         = 'Atteikt preci';
$_['button_shopping']       = 'Turpināt iepirkties';
$_['button_search']         = 'Meklēt';
$_['button_shipping']       = 'Piemērot piegādi';
$_['button_submit']         = 'Nosūtīt';
$_['button_guest']          = 'Bez reģistrācijas';
$_['button_view']           = 'Apskatīties';
$_['button_voucher']        = 'Izmantot dāvanu karti';
$_['button_upload']         = 'Augšupielādēt failu';
$_['button_reward']         = 'Izmantot bonusa punktus';
$_['button_quote']          = 'Pieprasīt piedāvājumu';
$_['button_list']           = 'Saraksts';
$_['button_grid']           = 'Režģis';
$_['button_map']            = 'Skatīt Google karti';

// Error
$_['error_exception']       = 'Kļūdas kods(%s): %s failā %s rinda %s';
$_['error_upload_1']        = 'Brīdinājums: Augšupielādētais fails pārsniedz php.ini direktīvā norādīto upload_max_filesize!';
$_['error_upload_2']        = 'Brīdinājums: Augšupielādētais fails pārsniedz HTML formā specificēto MAX_FILE_SIZE direktīvu!';
$_['error_upload_3']        = 'Brīdinājums: Augšupielādējamais fails ir augšupielādēts tikai daļēji!';
$_['error_upload_4']        = 'Brīdinājums: Neviens fails netika augšupielādēts';
$_['error_upload_6']        = 'Brīdinājums: Nav atrasta pagaidu mape (temporary folder)!';
$_['error_upload_7']        = 'Brīdinājums: Nevar ierakstīt failu diskā!';
$_['error_upload_8']        = 'Brīdinājums: Papildinājums ir pārtraucis faila augšupielādi!';
$_['error_upload_999']      = 'Brīdinājums: Nav atrasts kļūdas numurs!';

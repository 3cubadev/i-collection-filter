<?php
// Text
$_['text_home']     = 'Sākums';
$_['text_wishlist'] = 'Vēlmju saraksts (%s)';
$_['text_shopping_cart']     = 'Pirkumu grozs';
$_['text_category']      = 'Preču grupas';
$_['text_account']  = 'Mans profils';
$_['text_register']      = 'Reģistrēties';
$_['text_login']         = 'Autorizēties';
$_['text_order']         = 'Pasūtījumu vēsture';
$_['text_transaction']   = 'Darījumi';
$_['text_download']      = 'Lejupielādes';
$_['text_logout']        = 'Iziet';
$_['text_checkout'] = 'Noformēt pirkumu';
$_['text_search']   = 'Meklēt';
$_['text_all']           = 'Skatīt visu';
?>

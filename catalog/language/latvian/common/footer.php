<?php
// Text
$_['text_information']  = 'Cita informācija';
$_['text_service']      = 'Klientu serviss';
$_['text_requisites']   = 'Rekvizīti';
$_['text_requisitesfield'] = '<ul class="contact__ul"><li class="contact__list">Montenegro SIA</li><li class="contact__list">LV40103845311</li><li class="contact__list">Norvik Banka Riga</li><li class="contact__list">LV39LATB0002210061664</li><li class="contact__list">LATBLV22XXX</li><li class="contact__list">Jur. adrese: Terbatas iela 49/51-2, Riga</li><li class="contact__list">LV-1011, Latvija</li><li class="contact__list">Faktiskā adrese: Maskavas iela 12, Riga</li><li class="contact__list">LV-1050, Latvija</li><li class="contact__list">+371 24422833</li></ul>';
$_['text_work_time']    = 'Darba laiks';
$_['text_follow_us']    = 'Seko mums';
$_['text_extra']        = 'Ekstras';
$_['text_contact']      = 'Kontakti';
$_['text_return']       = 'Atteikumi';
$_['text_sitemap']      = 'Vietnes karte';
$_['text_manufacturer'] = 'Ražotāji';
$_['text_voucher']      = 'Dāvanu kartes';
$_['text_affiliate']    = 'Partneri';
$_['text_special']      = 'Īpašais piedāvājums';
$_['text_account']      = 'Profils';
$_['text_order']        = 'Pasūtījumu vēsture';
$_['text_wishlist']     = 'Vēlmju saraksts';
$_['text_newsletter']   = 'Jaunumi';
$_['text_powered']      = 'Darbojas uz <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
?>
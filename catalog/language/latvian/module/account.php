<?php
// Heading 
$_['heading_title']    = 'Profils';

// Text
$_['text_register']    = 'Reģistrēties';
$_['text_login']       = 'Autorizēties';
$_['text_logout']      = 'Iziet';
$_['text_forgotten']   = 'Aizmirsta parole';
$_['text_account']     = 'Mans profils';
$_['text_edit']        = 'Rediģēt profilu';
$_['text_password']    = 'Parole';
$_['text_address']     = 'Adrešu grāmatas';
$_['text_wishlist']    = 'Vēlmju saraksts';
$_['text_order']       = 'Pasūtījumu vēsture';
$_['text_download']    = 'Lejupielādes';
$_['text_reward']      = 'Bonusa punkti';
$_['text_return']      = 'Atteikumi';
$_['text_transaction'] = 'Darījumi';
$_['text_newsletter']  = 'Jaunumu abonēšana';
$_['text_recurring']    = 'Periodiskie maksājumi';
?>
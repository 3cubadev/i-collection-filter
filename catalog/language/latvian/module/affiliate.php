<?php
// Heading 
$_['heading_title']    = 'Partneris';

// Text
$_['text_register']    = 'Reģistrēties';
$_['text_login']       = 'Autorizēties';
$_['text_logout']      = 'Iziet';
$_['text_forgotten']   = 'Aizmirsta parole';
$_['text_account']     = 'Mans profils';
$_['text_edit']        = 'Rediģēt profilu';
$_['text_password']    = 'Parole';
$_['text_payment']     = 'Apmaksas iestatījumi';
$_['text_tracking']    = 'Partnera uzskaite';
$_['text_transaction'] = 'Darījumi';
?>

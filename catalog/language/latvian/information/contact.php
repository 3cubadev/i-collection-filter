<?php
// Heading
$_['heading_title']  = 'Mūsu kontakti';

// Text 
$_['text_location']  = 'Mūsu atrašanās vieta';
$_['text_store']     = 'Mūsu veikali';
$_['text_contact']   = 'Sazināšanās veidlapa';
$_['text_address']   = 'Adrese';
$_['text_telephone'] = 'Tālruņa numurs';
$_['text_fax']       = 'Fakss';
$_['text_open']      = 'Darba laiki';
$_['text_comment']   = 'Komentāri';
$_['text_success']   = '<p>Jūsu pieprasījums ir veiksmīgi nosūtīts veikala administrācijai!</p>';

// Entry Fields
$_['entry_name']     = 'Vārds';
$_['entry_email']    = 'E-pasta adrese';
$_['entry_enquiry']  = 'Jautājums';
$_['entry_captcha']  = 'Ievadiet kodu, kas redzams attēlā';

// Email
$_['email_subject']  = 'Jautājums %s';

// Errors
$_['error_name']     = 'Vārdam jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_email']    = 'E-pasta adrese ir ievadīta nepareizi!';
$_['error_enquiry']  = 'Jautājumam jābūt no 10 līdz 3000 rakstzīmēm!';
$_['error_captcha']  = 'Attēlā redzamais kods ir ievadīts nepareizi!';
?>

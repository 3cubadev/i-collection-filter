<?php
// Heading
$_['heading_title']    = 'Vietnes karte';

// Text
$_['text_special']     = 'Īpašie piedāvājumi';
$_['text_account']     = 'Mans profils';
$_['text_edit']        = 'Profila informācija';
$_['text_password']    = 'Parole';
$_['text_address']     = 'Adrešu grāmata';
$_['text_history']     = 'Pasūtījumu vēsture';
$_['text_download']    = 'Lejupielādes';
$_['text_cart']        = 'Grozs';
$_['text_checkout']    = 'Noformēt pasūtījumu';
$_['text_search']      = 'Meklēt';
$_['text_information'] = 'Informācija<span style="display:none;"> <a href="http://partneris.lv">Opencart Latviski no 1.Partneris</a></span>';
$_['text_contact']     = 'Sazināties ar mums';
?>

<?php
// Text
$_['text_upload']    = 'Jūsu fails ir sekmīgi augšupielādēts!';

// Error
$_['error_filename'] = 'Faila nosaukumā jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_filetype'] = 'Nederīgs faila tips!';
$_['error_upload']   = 'Jānorāda augšupielāde!';
?>
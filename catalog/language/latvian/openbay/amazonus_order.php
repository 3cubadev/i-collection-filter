<?php
// Text
$_['text_paid_amazon'] 			= 'Samaksāts Amazon ASV vietnē';
$_['text_total_shipping'] 		= 'Piegāde';
$_['text_total_shipping_tax'] 	= 'Piegādes nodoklis';
$_['text_total_giftwrap'] 		= 'Dāvanas iesaiņojums';
$_['text_total_giftwrap_tax'] 	= 'Dāvanas iesaņojuma nodoklis';
$_['text_total_sub'] 			= 'Starpsumma';
$_['text_tax'] 					= 'Nodoklis';
$_['text_total'] 				= 'Kopā';
?>
<?php
// Text
$_['text_title']       = 'Bankas pārskaitījums';
$_['text_instruction'] = 'Bankas pārskaitījuma norādījumi';
$_['text_description'] = 'Lūdzu, pārskaitiet kopējo summu uz šādu bankas kontu:';
$_['text_payment']     = 'Pasūtījums tiks apstrādāts, tiklīdz nauda būs pārskaitīta mūsu maksājuma kontā.';
?>
<?php
// Heading
$_['express_text_title']            = 'Apstipriniet pasūtījumu';

// Text
$_['text_title']				= 'PayPal Express pirkuma noformēšana';
$_['button_continue']               = 'Turpināt';
$_['text_cart']                     = 'Pirkumu grozs';
$_['text_shipping_updated']         = 'Atjaunots piegādes serviss';
$_['text_trial']                    = '%s ik pa %s %s līdz %s maksājumam(-iem), pēc tam ';
$_['text_recurring']                = '%s ik pa %s %s';
$_['text_recurring_item']		= 'Atkārtots maksājums';
$_['text_length']                   = ' līdz %s maksājumiem';

// Entry
$_['express_entry_coupon']          = 'Ievadiet šeit savu kupona kodu:';

// Button
$_['button_express_coupon']         = 'Pievienot';
$_['button_express_confirm']        = 'Apstiprināt';
$_['button_express_login']          = 'Doties uz "PayPal"';
$_['button_express_shipping']       = 'Atjaunināt piegādes veidu';
$_['button_cancel_recurring']	= 'Pārtraukt atkārtotus maksājumus';

// Error
$_['error_heading_title']           = 'Ir gadījusies kļūda';
$_['error_too_many_failures']       = 'Jūsu maksājums nav izdevies pārāk daudz reižu';
?>
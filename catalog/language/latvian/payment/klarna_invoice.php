<?php
// Text
$_['text_title']				= 'Klarna rēķins - apmaksājams 14 dienu laikā';
$_['text_terms_fee']			= '<span id="klarna_invoice_toc"></span> (+%s)<script type="text/javascript">var terms = new Klarna.Terms.Invoice({el: \'klarna_invoice_toc\', eid: \'%s\', country: \'%s\', charge: %s});</script>';
$_['text_terms_no_fee']			= '<span id="klarna_invoice_toc"></span><script type="text/javascript">var terms = new Klarna.Terms.Invoice({el: \'klarna_invoice_toc\', eid: \'%s\', country: \'%s\'});</script>';
$_['text_additional']			= 'Klarna rēķinam nepieciešama papildu informācija pirms viņi var izpildīt jūsu pasūtījumu.';
$_['text_male']            = 'Vīrietis';
$_['text_female']          = 'Sieviete';
$_['text_year']            = 'Gads';
$_['text_month']           = 'Mēnesis';
$_['text_day']             = 'Diena';
$_['text_comment']				= 'Klarna rēķina Nr.: %s' . "\n" . '%s/%s: %.4f';

// Entry
$_['entry_gender']				= 'Dzimums';
$_['entry_pno']					= 'Personas kods';
$_['entry_dob']					= 'Dzimšanas datums';
$_['entry_phone_no']			= 'Telefona numurs';
$_['entry_street']				= 'Iela';
$_['entry_house_no']			= 'Mājas Nr.';
$_['entry_house_ext']			= 'Korupusa Nr.';
$_['entry_company']				= 'Uzņēmuma reģistrācijas Nr.';

// Help
$_['help_pno']					= 'Lūdzu norādiet savu sociālās apdrošināšanas identifikatoru šeit.';
$_['help_phone_no']				= 'Lūdzu norādiet savu telefona numuru.';
$_['help_street']				= 'Lūdzu ievērojiet, ka maksājot ar Klarna, piegāde ver tikt veikta tikai uz reģistrētu adresi..';
$_['help_house_no']				= 'Lūdzu norādiet savu mājas numuru.';
$_['help_house_ext']			= 'Lūdzu norādiet mājas korpusa numuru šeit. Piem. A, B, C, sarkans, zils utt.';
$_['help_company']				= 'Lūdzu norādiet sava uzņēmuma reģistrācijas numuru.';

// Error
$_['error_deu_terms']      = 'Jums jāpiekrīt Klarna privātuma politikai (Datenschutz)';
$_['error_address_match']  = 'Maksātāja un piegādes adresēm ir jāsakrīt, ja jūs vēlaties izmantot Klarna rēķinu';
$_['error_network']        = 'Ir notikusi kļūda savienšanās mēģinājumā ar Klarna sistēmu. Lūdzu mēģiniet vēlreiz pēc kāda brīža.';
?>
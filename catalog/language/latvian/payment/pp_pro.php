<?php
// Text
$_['text_title']           = 'Kredītkarte vai debetkarte (drošu apstrādi nodrošina "PayPal")';
$_['text_wait']            = 'Lūdzu, gaidiet!';
$_['text_credit_card']     = 'Kredītkartes dati';
$_['text_loading']				= 'Ielādējas...';

// Entry
$_['entry_cc_type']        = 'Kartes tips';
$_['entry_cc_number']      = 'Kartes numurs';
$_['entry_cc_start_date']  = 'Karte derīga no';
$_['entry_cc_expire_date'] = 'Kartes derīguma termiņš';
$_['entry_cc_cvv2']        = 'Kartes drošības kods (CVV2)';
$_['entry_cc_issue']       = 'Kartes izdošanas numurs';

// Help
$_['help_start_date']			= '(ja pieejams)';
$_['help_issue']				= '(tikai "Maestro" un "Solo" kartēm)';
?>
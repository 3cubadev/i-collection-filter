<?php
// Text
$_['text_title'] = 'Kredītkarte / Debetkarte (Paymate)';
$_['text_unable'] = 'Nav iespējams atras vai atjaunot Jūsu pasūtījuma statusu';
$_['text_declined'] = '"Paymate" noraidīja maksājumu';
$_['text_failed'] = '"Paymate" darījums nav izdevies';
$_['text_failed_message'] = '<p>Diemžēl ir notikusi "Paymate" darījuma apstrādes kļūda.</p><p><b>Kļūda: </b>%s</p><p>Lūdzu, pārbaudiet Jūsu "Paymate" konta bilanci, pirms mēģiniet atkārtoti apstrādāt šo darījumu.</p><p> Ja Jūs domājat, ka šis darījums ir pabeigts veiksmīgi, vai tiek uzrādīts kā samazīnājums Jūsu "PayMate" kontā, lūdzu, <a href="%s">sazinieties ar mums</a>, nosūtot mums sava pasūtījuma informāciju.</p>';
$_['text_basket']         = 'Grozs';
$_['text_checkout']       = 'Noformēt pirkumu';
$_['text_success']        = 'Izpildīts';
?>

<?php
// Text
$_['text_title']           = 'Kartes kredīta / debeta kartes (interneta maksājumu programmatūra)';
$_['text_credit_card']     = 'Kredītkartes dati';

// Entry
$_['entry_cc_owner']       = 'KArtes īpašnieks';
$_['entry_cc_number']      = 'Kartes numurs';
$_['entry_cc_expire_date'] = 'Kartes derīguma termiņš';
$_['entry_cc_cvv2']        = 'Kartes drošības kods (CVV2)';
?>
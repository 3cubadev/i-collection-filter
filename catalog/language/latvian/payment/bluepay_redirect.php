<?php
// Text
$_['text_title']				= 'Kredītkarte / Debeta karte (BluePay)';
$_['text_credit_card']			= 'Kartes dati';
$_['text_description']			= 'Preces %s Pasūtījuma Nr.: %s';
$_['text_card_type']			= 'Kartes veids: ';
$_['text_card_name']			= 'Kartes nosaukums: ';
$_['text_card_digits']			= 'Pēdējie cipari: ';
$_['text_card_expiry']			= 'Derīga līdz: ';

// Returned text
$_['text_transaction_error']	= 'Darījuma apstrādē bija kļūda - ';

// Entry
$_['entry_card']				= 'Jauna vai esoša karte: ';
$_['entry_card_existing']		= 'Esoša';
$_['entry_card_new']			= 'Jauna';
$_['entry_card_save']			= 'Atcerēties karted datus';
$_['entry_cc_owner']			= 'Kartes īpašnieks';
$_['entry_cc_number']			= 'Kartes numurs';
$_['entry_cc_start_date']		= 'Kartes derīguma sākuma datums';
$_['entry_cc_expire_date']		= 'Kartes derīguma beigu datums';
$_['entry_cc_cvv2']				= 'Kartes drošības kods (CVV2)';
$_['entry_cc_address']			= 'Adrese';
$_['entry_cc_city']				= 'Pilsēta';
$_['entry_cc_state']			= 'Štats';
$_['entry_cc_zipcode']			= 'Pasta indekss';
$_['entry_cc_phone']			= 'Tālruņa nr.';
$_['entry_cc_email']			= 'E-pasts';
$_['entry_cc_choice']			= 'Izvēlieties esošu karti';
?>
<?php

$_['text_title'] = 'Kredīta / debeta karte';
$_['button_confirm'] = 'Apstiprināt';

$_['text_postcode_check'] = 'Pasta indeksa pārbaude: %s';
$_['text_security_code_check'] = 'CVV2 pārbaude: %s';
$_['text_address_check'] = 'Adreses pārbaude: %s';
$_['text_not_given'] = 'Nav norādīts';
$_['text_not_checked'] = 'Nav pārbaudīts';
$_['text_match'] = 'Sakrīt';
$_['text_not_match'] = 'Nesakrīt';
$_['text_payment_details'] = 'Maksājuma detaļas';

$_['entry_card_type'] = 'Kartes tips';
?>
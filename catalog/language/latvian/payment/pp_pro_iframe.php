<?php
// Text
$_['text_title'] = 'Kredītkartes vai debeta kartes';
$_['text_secure_connection'] = 'Veidoju drošu savienojumu...';

//Errors
$_['error_connection'] = "Nevar izveidot savienojumu ar "PayPal". Lūdzu, sazinieties ar veikala administratoru vai izvēlieties citu maksājuma veidu.";
?>
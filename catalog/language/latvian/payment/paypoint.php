<?php
// Heading
$_['heading_title']     = 'Thank you for shopping with %s .... ';

// Text
$_['text_title']        = 'Kredītkarte / Debetkarte (PayPoint)';
$_['text_response']     = 'Atbilde no "PayPoint":';
$_['text_success']      = '... Jūsu maksājums ir veiksmīgi saņemts.';
$_['text_success_wait'] = '<b><span style="color: #FF0000">Lūdzu, gaidiet...</span></b> kamēr mēs pabeidzam apstrādāt Jūsu pasūtījumu.<br>Ja 10 sekunžu laikā netiekat automātiski pārvirzīts(-ta), lūdzu, klikšķiniet <a href="%s">šeit</a>.';
$_['text_failure']      = '... Jūsu maksājums ir atcelts!';
$_['text_failure_wait'] = '<b><span style="color: #FF0000">Lūdzu, gaidiet...</span></b><br>Ja 10 sekunžu laikā netiekat automātiski pārvirzīts, lūdzu, klikšķiniet <a href="%s">šeit</a>.';										  
?>
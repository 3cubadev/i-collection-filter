<?php
// Text
$_['text_title']				= 'Kredīta vai debeta karte (SagePay)';
$_['text_credit_card']			= 'Kartes datis';
$_['text_description']			= 'Preces par %s Pasūtījuma nr: %s';
$_['text_card_type']			= 'Kartes tips: ';
$_['text_card_name']			= 'Kartes nosaukums: ';
$_['text_card_digits']			= 'Pēdējie cipari: ';
$_['text_card_expiry']			= 'Derīguma termiņš: ';
$_['text_trial']				= '%s katru %s %s līdz %s maksājumiem tad ';
$_['text_recurring']			= '%s katru %s %s';
$_['text_length']				= ' līdz %s maksājumiem';
$_['text_success']				= 'Jūsu maksājums ir apstiprināts.';
$_['text_decline']				= 'Jūsu maksājums ir atteikts.';
$_['text_bank_error']			= 'Ir radusies kļūda, apstrādājot jūsu pieprasījumu ar banku.';
$_['text_transaction_error']	= 'Ir radusies kļūda, apstrādājot jūsu transakciju.';
$_['text_generic_error']		= 'Ir radusies kļūda, apstrādājot jūsu pieprasījumu.';
$_['text_hash_failed']			= 'Hash pārbaude nav sekmīga. Lūdzu nemēģiniet atkārtot jūsu maksājumu, jo maksājuma status nav zināms. Lūdzu sazinieties ar tirgotāju.';
$_['text_link']					= 'Lūdzu klikšķiniet <a href="%s">šeit</a> lai turpinātu';

// Entry
$_['entry_card']				= 'Jauna vai iepriekš saglabāta karte: ';
$_['entry_card_existing']		= 'Iepriekš saglabāta';
$_['entry_card_new']			= 'Jauna';
$_['entry_card_save']			= 'Atcerēties kartes datus turpmākai lietošanai.';
$_['entry_cc_choice']			= 'Izvēlieties iepriekš saglabātu karti';
?>
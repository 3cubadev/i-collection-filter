<?php
// Text
$_['text_title']				= 'Kredīta vai debeta karte';
$_['text_credit_card']			= 'Kredītkartes dati';
$_['text_wait']					= 'Lūdzu uzgaidiet!';

// Entry
$_['entry_cc_number']			= 'Kartes numurs';
$_['entry_cc_name']				= 'Kartes īpašnieks';
$_['entry_cc_expire_date']		= 'Kartes derīguma beigu datums';
$_['entry_cc_cvv2']				= 'Kartes drošības kods (CVV2)';

// Help
$_['help_start_date']			= '(ja norādīts)';
$_['help_issue']				= '(tikai Maestro un Solo kartēm)';

// Text
$_['text_result']				= 'Rezultāts: ';
$_['text_approval_code']		= 'Apstiprinājuma kods: ';
$_['text_reference_number']		= 'References numurs: ';
$_['text_card_number_ref']		= 'Kartes pēdējie 4 cipari: xxxx ';
$_['text_card_brand']			= 'Kartes zīmols: ';
$_['text_response_code']		= 'Atbildes kods: ';
$_['text_fault']				= 'Kļūdas teksts: ';
$_['text_error']				= 'Kļūdas teksts: ';
$_['text_avs']					= 'Adreses pārbaude: ';
$_['text_address_ppx']			= 'Adrese nav norādīta, vai arī kartes izdevējs nav pārbaudījis adresi';
$_['text_address_yyy']			= 'Kartes izdevējs apstiprināja, ka adrese un pasta indeks sakrīt ar viņa datiem';
$_['text_address_yna']			= 'Kartes izdevējs apstiprināja, ka adrese sakrīt ar viņa datiem, bet pasta indekss nesakrīt';
$_['text_address_nyz']			= 'Kartes izdevējs apstiprināja, ka pasta indekss sakrīt ar viņa datiem, bet adrese nesakrīt';
$_['text_address_nnn']			= 'Ne adrese, ne pasta indeks nesakrīt ar kartes izdevēja datiem';
$_['text_address_ypx']			= 'Kartes izdevējs apstiprināja, ka adrese sakrīt ar viņa datiem. Kartes izdevējs nav pārbaudījis pasta indeksu.';
$_['text_address_pyx']			= 'Kartes izdevējs apstiprināja, ka pasta indekss sakrīt ar viņa datiem. Kartes izdevējs nav pārbaudījis adresi.';
$_['text_address_xxu']			= 'Kartes izdevējs nav pāŗbaudījis AVS informāciju';
$_['text_card_code_verify']		= 'Drošības kods: ';
$_['text_card_code_m']			= 'Kartes drošības kods sakrīt';
$_['text_card_code_n']			= 'Kartes drošības kods nesakrīt';
$_['text_card_code_p']			= 'Nav apstrādāts';
$_['text_card_code_s']			= 'Tirgotājs ir norādījis, ka uz kartes nav drošības koda';
$_['text_card_code_u']			= 'Kartes izdevējs nav sertificēts, un/vai nav norādījis šifrēšanas atslēgas';
$_['text_card_code_x']			= 'Nav saņemta atbilde no kredītkaršu asociācijas';
$_['text_card_code_blank']		= 'Tukšai atbildei būtu jānozīmē, ka nav ticis nosūtīts kods un, ka nav norādīts, ka uz kartes nav koda';
$_['text_card_accepted']		= 'Kartes, kuras tiek pieņemtas: ';
$_['text_card_type_m']			= 'Mastercard';
$_['text_card_type_v']			= 'Visa (Credit/Debit/Electron/Delta)';
$_['text_card_type_c']			= 'Diners';
$_['text_card_type_a']			= 'American Express';
$_['text_card_type_ma']			= 'Maestro';
$_['text_card_new']				= 'Jauna karte';
$_['text_response_proc_code']	= 'Apstrādātāja kods: ';
$_['text_response_ref']			= 'References numurs: ';

// Error
$_['error_card_number']			= 'Lūdzu pārliecinieties, ka ir pareizi norādīts kartes numurs';
$_['error_card_name']			= 'Lūdzu pārliecinieties, ka ir pareizi norādīts kartes īpašnieka vārds';
$_['error_card_cvv']			= 'Lūdzu pārliecinieties, ka ir pareizi norādīts CVV2 kods';
$_['error_failed']				= 'Nav iespējams apstrādāt karti, lūdzu sazinieties ar tirgotāju';
?>
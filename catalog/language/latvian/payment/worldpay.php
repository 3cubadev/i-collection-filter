<?php
// Heading
$_['heading_title']     = 'Paldies par Jūsu pirkumu %s .... ';

// Text
$_['text_title']        = 'Kredītkarte / Debetkarte (WorldPay)';
$_['text_response']     = 'Atbildē "WorldPay":';
$_['text_success']      = '... Jūsu maksājums tika veiksmīgi saņemts.';
$_['text_success_wait'] = '<b><span style="color: #FF0000">Uzgaidiet...</span></b><br><a href="%s">Noklikšķiniet šeit...</a>.';
$_['text_failure']      = '... Jūsu maksājums tika atteikts!!';
$_['text_failure_wait'] = '<b><span style="color: #FF0000">Uzgaidiet...</span></b><br>Ja pāradresācija nav notikusi, noklikšķiniet <a href="%s">šeit</a>.';										  
$_['text_pw_mismatch']  = '"CallbackPW" nesakrīt. Pasūtījumam nepieciešama izmeklēšana.';
?>
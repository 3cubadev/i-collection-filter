<?php
// Text
$_['text_title']   = 'Čeks / Skaidra nauda - instrukcijas';
$_['text_instruction'] = 'Čeks / Skaidra nauda';
$_['text_payable'] = 'Saņēmējs: ';
$_['text_address'] = 'Adrese: ';
$_['text_payment'] = 'Pasūtījums netiks izsūtīts, iekams mēs nesaņemsim maksājumu.';
?>
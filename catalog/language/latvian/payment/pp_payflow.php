<?php
// Text
$_['text_title']				= 'Kredīta vai debeta karta (PayPal drošā apstrāde)';
$_['text_credit_card']			= 'Kredītkartes dati';
$_['text_start_date']			= '(ja norādīts)';
$_['text_issue']				= '(tikai Maestro un Solo kartēm)';
$_['text_wait']					= 'Lūdzu uzgaidiet!';

// Entry
$_['entry_cc_owner']			= 'Kartes īpašnieks:';
$_['entry_cc_type']				= 'Kartes veids:';
$_['entry_cc_number']			= 'Kartes numurs:';
$_['entry_cc_start_date']		= 'Kartes derīguma sākuma datums:';
$_['entry_cc_expire_date']		= 'Kartes derīguma beigu datums:';
$_['entry_cc_cvv2']				= 'Kartes drošības kods (CVV2):';
$_['entry_cc_issue']			= 'Kartes izdošanas numurs:';

// Error
$_['error_required']			= 'Uzmanību! Ir jānorāda visa pieprasītā maksājuma informācija.';
$_['error_general']				= 'Uzmanību! Ir radusies vispārēja rakstura kļūme, izpildot transakciju. Lūdzu mēģiniet vēlreiz.';
$_['error_config']				= 'Uzmanību! Maksājuma moduļa konfigurācijas kļūme. Lūdzu pārbaudiet savus autorizācijas datus.';
$_['error_address']				= 'Uzmanību! Nesakrīt maksātāja adreses, pilsētas, reģiona un pasta indeksa informācija. Lūdzu mēģiniet vēlreiz.';
$_['error_declined']			= 'Uzmanību! Šī transakcija ir atteikta. Lūdzu mēģiniet vēlreiz.';
$_['error_invalid']				= 'Uzmanību! Norādītā kredītkartes informācija ir nederīga. Lūdzu mēģiniet vēlreiz.';
?>
<?php

$_['text_title'] = 'Kredīta / debeta karte';
$_['text_card_details'] = 'Kartes dati';
$_['text_wait'] = 'Apstrādājam jūsu maksājumu';
$_['text_auth_code'] = 'Apstiprinājuma kods: %s';
$_['text_postcode_check'] = 'Pasta indeksa pārbaude: %s';
$_['text_security_code_check'] = 'CVV2 pārbaude: %s';
$_['text_address_check'] = 'Adreses pārbaude: %s';
$_['text_3d_secure_check'] = '3D Secure: %s';
$_['text_not_given'] = 'Nav norādīts';
$_['text_not_checked'] = 'Nav pārbaudīts';
$_['text_match'] = 'Sakrīt';
$_['text_not_match'] = 'Nesakrīt';
$_['text_authenticated'] = 'Autenticēts';
$_['text_not_authenticated'] = 'Nav autenticēts';
$_['text_authentication_not_completed'] = 'Mēģinājums nav pabeigts';
$_['text_unable_to_perform'] = 'Nav iespējams izpildīt';
$_['text_transaction_declined'] = 'Jūsu banka atteica maksājumu. Lūdzu izmantojiet citu maksājuma veidu.';
$_['text_transaction_failed'] = 'Neizdevās izpildīt maksājumu. Lūdzu pārbaudiet datus, kurus norādījāt.';
$_['text_connection_error'] = 'Lūdzu mēģiniet vēlāk, vai izmantojiet citu maksājuma veidu.';

$_['entry_type'] = "Kartes tips";
$_['entry_number'] = "Kartes numurs";
$_['entry_expire_date'] = "Derīguma termiņš";
$_['entry_cvv2'] = "Drošības kods (CVV2)";

$_['button_confirm'] = 'Apstiprināt';

$_['error_failure'] = 'Nevarējām pabeigt transakciju. Mēģiniet vēlāk vēl, vai izmantojiet citu maksājuma veidu.';
?>
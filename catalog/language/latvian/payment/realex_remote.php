<?php
// Text
$_['text_title']				= 'Kredīta vai debeta karte';
$_['text_credit_card']			= 'Kredītkartes dati';
$_['text_wait']					= 'Ldūzu uzgaidiet!';
$_['text_result']				= 'Rezultāts';
$_['text_message']				= 'Paziņojums';
$_['text_cvn_result']			= 'CVN rezultāts';
$_['text_avs_postcode']			= 'AVS pasta indekss';
$_['text_avs_address']			= 'AVS adrese';
$_['text_eci']					= 'ECI (3D secure) rezultāts';
$_['text_tss']					= 'TSS rezultāts';
$_['text_card_bank']			= 'Kartes izdevēja banka';
$_['text_card_country']			= 'Kartes valsts';
$_['text_card_region']			= 'Kartes reģions';
$_['text_last_digits']			= 'Pēdējie 4 cipari';
$_['text_order_ref']			= 'Pasūtījuma reference';
$_['text_timestamp']			= 'Laika zīmogs';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Switch';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';
$_['text_auth_code']			= 'Autorizācijas kods';
$_['text_3d_s1']				= 'Cardholder Not Enrolled, liability shift';
$_['text_3d_s2']				= 'Unable To Verify Enrolment, no liability shift';
$_['text_3d_s3']				= 'Invalid Response From Enrolment Server, no liability shift';
$_['text_3d_s4']				= 'Enrolled, But Invalid Response From ACS (Access Control Server), no liability shift';
$_['text_3d_s5']				= 'Successful Authentication, liability shift';
$_['text_3d_s6']				= 'Authentication Attempt Acknowledged, liability shift';
$_['text_3d_s7']				= 'Incorrect Password Entered, no liability shift';
$_['text_3d_s8']				= 'Authentication Unavailable, no liability shift';
$_['text_3d_s9']				= 'Invalid Response From ACS, no liability shift';
$_['text_3d_s10']				= 'RealMPI Fatal Error, no liability shift';

// Entry
$_['entry_cc_type']				= 'Kartes tips';
$_['entry_cc_number']			= 'Kartes numurs';
$_['entry_cc_name']				= 'Kartes īpašnieka vārds';
$_['entry_cc_expire_date']		= 'Kartes derīguma termiņš';
$_['entry_cc_cvv2']				= 'Kartes drošības kods (CVV2)';
$_['entry_cc_issue']			= 'Kartes izdošanas numurs';

// Help
$_['help_start_date']			= '(ja norādīts)';
$_['help_issue']				= '(tikai Maestro un Solo kartēm)';

// Error
$_['error_card_number']			= 'Lūdzu pārbaudiet kartes numura pareizību';
$_['error_card_name']			= 'Lūdzu pārbaudiet kartes īpasnieka vārda pareizību';
$_['error_card_cvv']			= 'Lūdzu pārbaudiet CVV2 koda pareizību';
$_['error_3d_unable']			= 'Merchant requires 3D secure but unable to verify with your bank, please try later';
$_['error_3d_500_response_no_payment'] = 'No kartes apstrādātāja ir saņemta nederīga atbilde. Maksājums nav veikts.';
$_['error_3d_unsuccessful']		= '3D secure authorisation failed';
?>
<?php
// Heading
$_['text_title']				= 'Kredīta / Debeta karte (Realex)';

// Button
$_['button_confirm']			= 'Apstiprināt';

// Entry
$_['entry_cc_type']				= 'Kartes tips';

// Text
$_['text_success']				= 'Jūsu maksājums ir apstiprināts.';
$_['text_decline']				= 'Jūsu maksājums ir atteikts';
$_['text_bank_error']			= 'Ir radusies kļūme apstrādājot jūsu pieprasījumu bankā.';
$_['text_generic_error']		= 'Ir radusies kļūme apstrādājot jūsu pieprasījumu.';
$_['text_hash_failed']			= 'Hash pārbaude ir nesekmīga. Lūdzu nemēģiniet atkārtot jūsu maksājumu, jo maksājuma statuss nav zināms. Lūdzu sazinieties ar tirgotāju.';
$_['text_link']					= 'Lūdzu klikšķiniet <a href="%s">šeit</a> lai turpinātu';
$_['text_select_card']			= 'Lūdzu izvēlieties jūsu kartes tipu';
$_['text_result']				= 'Autorizēšanās rezultāts';
$_['text_message']				= 'Ziņojums';
$_['text_cvn_result']			= 'CVN rezultāts';
$_['text_avs_postcode']			= 'AVS pasta indekss';
$_['text_avs_address']			= 'AVS adrese';
$_['text_eci']					= 'ECI (3D secure) rezultāts';
$_['text_tss']					= 'TSS rezultāts';
$_['text_order_ref']			= 'Pasūtījuma reference';
$_['text_timestamp']			= 'Laika zīmogs';
$_['text_card_type']			= 'Kartes tips';
$_['text_card_digits']			= 'Kartes numurs';
$_['text_card_exp']				= 'Kartes derīguma termiņš';
$_['text_card_name']			= 'Kartes nosaukums';
$_['text_3d_s1']				= 'Cardholder Not Enrolled, liability shift';
$_['text_3d_s2']				= 'Unable To Verify Enrolment, no liability shift';
$_['text_3d_s3']				= 'Invalid Response From Enrolment Server, no liability shift';
$_['text_3d_s4']				= 'Enrolled, But Invalid Response From ACS (Access Control Server), no liability shift';
$_['text_3d_s5']				= 'Successful Authentication, liability shift';
$_['text_3d_s6']				= 'Authentication Attempt Acknowledged, liability shift';
$_['text_3d_s7']				= 'Incorrect Password Entered, no liability shift';
$_['text_3d_s8']				= 'Authentication Unavailable, no liability shift';
$_['text_3d_s9']				= 'Invalid Response From ACS, no liability shift';
$_['text_3d_s10']				= 'RealMPI Fatal Error, no liability shift';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Switch';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';
?>
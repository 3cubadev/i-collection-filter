<?php
// Text
$_['text_title'] = 'Kredīta vai debeta karte';
$_['text_secure_connection'] = 'Tiek veidots drošs savienojums...';

//Errors
$_['error_connection'] = "Nevar izveidot savienojumu ar "PayPal". Lūdzu, sazinieties ar veikala administratoru vai izvēlieties citu maksājuma veidu!";
?>
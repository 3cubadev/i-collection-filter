<?php
// Heading
$_['text_title']				= 'Kredīta / debeta karte (First Data)';

// Button
$_['button_confirm']			= 'Turpināt';

// Text
$_['text_new_card']				= 'Jauna karte';
$_['text_store_card']			= 'Atcerēties manas kartes datus';
$_['text_address_response']		= 'Adreses verifikācija: ';
$_['text_address_ppx']			= 'Adrese nav norādīta, vai arī kartes izdevējs nav pārbaudījis adresi';
$_['text_address_yyy']			= 'Kartes izdevējs apstiprināja, ka adrese un pasta indeks sakrīt ar viņa datiem';
$_['text_address_yna']			= 'Kartes izdevējs apstiprināja, ka adrese sakrīt ar viņa datiem, bet pasta indekss nesakrīt';
$_['text_address_nyz']			= 'Kartes izdevējs apstiprināja, ka pasta indekss sakrīt ar viņa datiem, bet adrese nesakrīt';
$_['text_address_nnn']			= 'Ne adrese, ne pasta indeks nesakrīt ar kartes izdevēja datiem';
$_['text_address_ypx']			= 'Kartes izdevējs apstiprināja, ka adrese sakrīt ar viņa datiem. Kartes izdevējs nav pārbaudījis pasta indeksu.';
$_['text_address_pyx']			= 'Kartes izdevējs apstiprināja, ka pasta indekss sakrīt ar viņa datiem. Kartes izdevējs nav pārbaudījis adresi.';
$_['text_address_xxu']			= 'Kartes izdevējs nav pāŗbaudījis AVS informāciju';
$_['text_card_code_verify']		= 'Drošības kods: ';
$_['text_card_code_m']			= 'Kartes drošības kods sakrīt';
$_['text_card_code_n']			= 'Kartes drošības kods nesakrīt';
$_['text_card_code_p']			= 'Nav apstrādāts';
$_['text_card_code_s']			= 'Tirgotājs ir norādījis, ka uz kartes nav drošības koda';
$_['text_card_code_u']			= 'Kartes izdevējs nav sertificēts, un/vai nav norādījis šifrēšanas atslēgas';
$_['text_card_code_x']			= 'Nav saņemta atbilde no kredītkaršu asociācijas';
$_['text_card_code_blank']		= 'Tukšai atbildei būtu jānozīmē, ka nav ticis nosūtīts kods un, ka nav norādīts, ka uz kartes nav koda';
$_['text_card_type_m']			= 'Mastercard';
$_['text_card_type_v']			= 'Visa (Credit/Debit/Electron/Delta)';
$_['text_card_type_c']			= 'Diners';
$_['text_card_type_a']			= 'American Express';
$_['text_card_type_ma']			= 'Maestro';
$_['text_card_type_mauk']		= 'Maestro UK/Solo';
$_['text_response_code_full']	= 'Apstiprinājuma kods: ';
$_['text_response_code']		= 'Pilns atbildes kods: ';
$_['text_response_card']		= 'Izmantotā karte: ';
$_['text_response_card_type']	= 'Kartes veids: ';
$_['text_response_proc_code']	= 'Apstrādātāja kods: ';
$_['text_response_ref']			= 'References numurs: ';

// Error
$_['error_failed']				= 'Jūsu maksājums neizdevās apstrādāt. Lūdzu mēģiniet vēlreiz!';
?>
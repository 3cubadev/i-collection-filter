<?php
//Headings
$_['heading_title'] = 'Amazon maksājumi';
$_['heading_address'] = 'Izvēlieties piegādes adresi';
$_['heading_payment'] = 'Izvēlieties maksājuma veidu';
$_['heading_confirm'] = 'Pasūtījumu pārskats';

//Text
$_['text_back'] = 'Atpakaļ';
$_['text_cart'] = 'Grozs';
$_['text_confirm'] = 'Apstiprināt';
$_['text_continue'] = 'Turpināt';
$_['text_cba'] = 'Amazon maksājumi';
$_['text_enter_coupon'] = 'Ievadiet kupona kodu. Ja Jums tā nav, tad atstājiet tukšu.';
$_['text_coupon'] = 'Kupons';
$_['text_tax_other'] = 'Nodokļi / apstrādes komisijas maksa';
$_['text_payment_failed'] = 'Jūsu maksājums neizdevās. Lūdzu, vērsieties pēc palīdzības pie veikala administratora, vai izmantojiet citu maksāšanas veidu!';
$_['text_success_title']		= 'Jūsu pasūtījums ir izveidots!';
$_['text_payment_success'] = 'Pasūtījums noformēts veiksmīgi. Pasūtījuma detaļas skatīt turpinājumā';

//Errors
$_['error_payment_method'] = 'Lūdzu, izvēlieties maksājuma veidu!';
$_['error_shipping'] = 'Lūdzu, izvēlieties piegādes veidu!';
$_['error_shipping_address'] = 'Lūdzu, izvēlieties piegādes adresi!';
$_['error_shipping_methods'] = 'Radās kļūda, nolasot Jūsu adresi no Amazon. Lai saņemtu palīdzību, sazinieties ar veikala administratoru';
$_['error_no_shipping_methods'] = 'Izvēlētajai adresei nav atrasts neviens piegādes veids. Lūdzu, izvēlieties citu piegādes adresi!';
?>
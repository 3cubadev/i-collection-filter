<?php
// Text
$_['text_title']    = 'PayPal';
$_['text_testmode']	= 'Brīdinājums: Maksājumu pieņemšana ir testa režīmā (Sandbox Mode). Nauda no Jūsu konta netiks pārskaitīta.';
$_['text_total']	= 'Piegāde, apstrāde, atlaides un nodokļi';
?>
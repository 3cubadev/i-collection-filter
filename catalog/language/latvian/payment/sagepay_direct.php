<?php
// Text
$_['text_title']           = 'Kredītkarte / Debetkarte (SagePay)';
$_['text_credit_card']     = 'Kredītkartes informācija';
$_['text_card_type']			= 'Kartes tips: ';
$_['text_card_name']			= 'Kartes nosaukums: ';
$_['text_card_digits']			= 'Pēdējie cipari: ';
$_['text_card_expiry']			= 'Derīga līdz: ';
$_['text_trial']				= '%s katru %s %s līdz %s maksājumiem, tad ';
$_['text_recurring']			= '%s katru %s %s';
$_['text_length']				= ' līdz %s maksājumiem';

// Entry
$_['entry_card']				= 'Jauna vai esoša karte: ';
$_['entry_card_existing']		= 'Esoša';
$_['entry_card_new']			= 'Jauna';
$_['entry_card_save']			= 'Atcerēties kartes datus';
$_['entry_cc_owner']       = 'Kartes īpašnieks';
$_['entry_cc_type']        = 'Kartes veids';
$_['entry_cc_number']      = 'Kartes numurs';
$_['entry_cc_start_date']  = 'Karte derīga no';
$_['entry_cc_expire_date'] = 'Kartes derīguma termiņš';
$_['entry_cc_cvv2']        = 'Kartes drošības kods (CVV2)';
$_['entry_cc_issue']       = 'Kartes izdošanas numurs';
$_['entry_cc_choice']			= 'Izvēlēties esošu karti';

// Help
$_['help_start_date']      = '(ja pieejamas)';
$_['help_issue']           = '(tikai kartēm "Maestro" un "Solo")';
?>
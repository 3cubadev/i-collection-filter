<?php
// Text
$_['text_title']           = 'Kredītkarte vai debetkarte (Drošu apstrādi nodrošina "Perpetual Payments")';
$_['text_credit_card']     = 'Kredītkartes dati';
$_['text_transaction']     = 'Darījuma identifikators:';
$_['text_avs']             = 'AVS/CVV:';
$_['text_avs_full_match']  = 'Pilna sakritība';
$_['text_avs_not_match']   = 'Nav sakritības';
$_['text_authorisation']   = 'Autorizācijas kods:';

// Entry
$_['entry_cc_number']      = 'Kartes numurs';
$_['entry_cc_start_date']  = 'Karte derīga no';
$_['entry_cc_expire_date'] = 'Kartes derīguma termiņš';
$_['entry_cc_cvv2']        = 'Kartes drošības kods (CVV2)';
$_['entry_cc_issue']       = 'Kartes izdošanas numurs';

// Help
$_['help_start_date']			= '(ja norādīts)';
$_['help_issue']				= '(tikai Maestro un Solo kartēm)';
?>
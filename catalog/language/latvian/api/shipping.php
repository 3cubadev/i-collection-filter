<?php
// Text
$_['text_address']       = 'Piegādes adreses iestatīšana ir sekmīgi veikta!';
$_['text_method']        = 'Piegādes veida iestatīšana ir sekmīgi veikta!';

// Error
$_['error_permission']   = 'Uzmanību! Jums nav atļauts piekļūt API!';
$_['error_firstname']    = 'Vārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']     = 'Uzvārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_address_1']    = 'Ailē Adrese 1 ir jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_city']         = 'Ailē Pilsēta ir jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_postcode']     = 'Pasta indeksā šai valstij ir jābūt no 2 līdz 10 rakstzīmēm!';
$_['error_country']      = 'Lūdzu izvēlieties valsit!';
$_['error_zone']         = 'Lūdzu izvēlieties reģionu!';
$_['error_custom_field'] = '%s ir obligāti jānorāda!';
$_['error_address']      = 'Uzmanību! Piegādes adrese ir obligāti jānorāda!';
$_['error_method']       = 'Uzmanību! Piegādes veids ir obligāti jānorāda!';
$_['error_no_shipping']  = 'Uzmanību! Nav pieejams neviens piegādes veids!';
?>
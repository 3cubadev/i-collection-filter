<?php
// Text
$_['text_success']     = 'Pabeigts: Pirkumu grozs ir sekmīgi mainīts!';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts piekļūt API!';
$_['error_stock']      = 'Preces, kas apzīmētas ar *** nav pieejamas šādā daudzumā vai pašreiz nav noliktavā!';
$_['error_minimum']    = '%s minimālais pasūtījuma daudzums ir %s!';
$_['error_store']      = 'Šī prece nav nopērkama jūsu izvēlētajā veikalā!';
$_['error_required']   = '%s ir obligāti jānorāda!';
?>
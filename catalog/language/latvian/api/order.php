<?php
// Text
$_['text_success']           = 'Jūs sekmīgi pabeidzāt pasūtījumu rediģēšanu';

// Error
$_['error_permission']       = 'Uzmanību! Jums nav atļauts piekļūt API!';
$_['error_customer']         = 'Nav norādīta nepieciešamā informācija par klientu!';
$_['error_payment_address']  = 'Maksātāja adrese ir obligāti jānorāda!';
$_['error_payment_method']   = 'Maksājuma veids  ir obligāti jānorāda!';
$_['error_shipping_address'] = 'Piegādes adrese  ir obligāti jānorāda!';
$_['error_shipping_method']  = 'Piegādes veids  ir obligāti jānorāda!';
$_['error_stock']            = 'Preces, kas apzīmētas ar *** nav pieejamas šādā daudzumā vai pašreiz nav noliktavā!';
$_['error_minimum']          = '%s minimālais pasūtījuma daudzums ir %s';
$_['error_not_found']        = 'Uzmanību! Pasūtījums nav atrasts!';
?>
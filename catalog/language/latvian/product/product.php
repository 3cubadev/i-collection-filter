<?php
// Text
$_['text_search']              = 'Meklēt';
$_['text_brand']               = 'Ražotājs';
$_['text_manufacturer']        = 'Ražotājs:';
$_['text_model']               = 'Modelis:';
$_['text_reward']              = 'Bonusa punkti:';
$_['text_points']              = 'Cena bonusa punktos:';
$_['text_stock']               = 'Pieejamība:';
$_['text_instock']             = 'Pieejams';
$_['text_tax']                 = 'Bez nodokļa:';
$_['text_discount']            = '%s vai vairāk %s';
$_['text_option']              = 'Papildu izvēles:';
$_['text_minimum']             = 'Šim produktam ir minimālais pasūtīšanas daudzums ir %s.';
$_['text_reviews']             = '%s atsauksmes';
$_['text_write']               = 'Rakstīt atsauksmi';
$_['text_login']               = 'Lūdzu <a href="%s">autorizējieties</a> vai <a href="%s">reģistrējieties</a> lai rakstītu atsauksmi';
$_['text_no_reviews']          = 'Šai precei atsauksmju nav.';
$_['text_note']                = '<span style="color: #FF0000;">Piezīme:</span> HTML netiek pieņemts, lūdzu, izmantojiet neformatētu tekstu.';
$_['text_success']             = 'Paldies par Jūsu atsauksmi. Atsauksme tika nosūtīta apstiprināšanai administrācijai.';
$_['text_related']             = 'Saistītā prece';
$_['text_tags']                = 'Atslēgvārdi:';
$_['text_error']               = 'Prece nav atrasta!';
$_['text_payment_recurring']   = 'Periodisks maksājums';
$_['text_trial_description']   = '%s ik pa %d %s līdz %d maksājumam(-iem), pēc tam';
$_['text_payment_description'] = '%s ik pa %d %s līdz %d maksājumiem';
$_['text_payment_cancel']      = '%s ik pa %d %s līdz atcelšanai';
$_['text_day']                 = 'dienai(-ām)';
$_['text_week']                = 'nedēļai(-ām)';
$_['text_semi_month']          = 'pusei(-ēm) mēneša';
$_['text_month']               = 'mēnesim (mēnešiem)';
$_['text_year']                = 'gadam(-iem)';

$_['text_prev']        = 'Iepriekšējais';
$_['text_next']        = 'Nākamais';
$_['text_all_products']        = 'Visi produkti';

$_['text_category']        = 'Kategorija';
$_['text_width']        = 'Platums';
$_['text_width_ich']        = 'sm';
$_['text_color']        = 'Krāsa';
$_['text_product_quantity']        = 'Daudz';
$_['text_product_share']        = 'Dalīties';

// Entry
$_['entry_qty']                = 'Daudzums';
$_['entry_name']               = 'Jūsu vārds';
$_['entry_review']             = 'Jūsu atsauksmes';
$_['entry_rating']             = 'Vērtējums';
$_['entry_good']               = 'Labs';
$_['entry_bad']                = 'Slikts';

// Tabs
$_['tab_description']          = 'Apraksts';
$_['tab_attribute']            = 'Specifikācija';
$_['tab_review']               = 'Atsauksmes (%s)';

// Error
$_['error_name']               = 'Kļūda: Jūsu vārdam ir jābūt no 3 līdz 25 rakstzīmēm!';
$_['error_text']               = 'Kļūda: Atsauksmes tekstam ir jābūt no 25 līdz 1000 rakstzīmēm!';
$_['error_rating']             = 'Kļūda: Lūdzu, izvēlieties vērtējumu!';
$_['error_captcha']            = 'Kļūda: Kods no attēla ir ievadīts nepareizi!';
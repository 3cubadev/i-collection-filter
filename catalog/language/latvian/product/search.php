<?php
// Heading
$_['heading_title']     = 'Meklēšana';
$_['heading_tag']		= 'Tegs (birka) - ';

// Text
$_['text_search']       = 'Meklēšanas rezultāts';
$_['text_keyword']      = 'Atslēgvārdi';
$_['text_category']     = 'Visas preču grupas';
$_['text_sub_category'] = 'Meklēt preču apakšgrupās';
$_['text_empty']        = 'Nav nevienas preces, kas atbilst Jūsu meklēšanas kritērijam.';
$_['text_quantity']     = 'Daudzums:';
$_['text_manufacturer'] = 'Ražotājs:';
$_['text_model']        = 'Preces kods:'; 
$_['text_points']       = 'Bonusa punkti:'; 
$_['text_price']        = 'Cena:'; 
$_['text_tax']          = 'Bez nodokļa:'; 
$_['text_reviews']      = 'Balstoties uz %s atsauksmēm.'; 
$_['text_compare']      = 'Preču salīdzināšana (%s)'; 
$_['text_sort']         = 'Kārtošanas secība:';
$_['text_default']      = 'Bez kārtošanas';
$_['text_name_asc']     = 'Nosaukums (A - Ž)';
$_['text_name_desc']    = 'Nosaukums (Ž - A)';
$_['text_price_asc']    = 'Cena, sākot ar zemāko';
$_['text_price_desc']   = 'Cena, sākot ar augstāko';
$_['text_rating_asc']   = 'Vērtējums, sākot ar zemāko)';
$_['text_rating_desc']  = 'Vērtējums, sākot ar augstāko';
$_['text_model_asc']    = 'Modelis (A - Ž)';
$_['text_model_desc']   = 'Modelis (Ž - A)';
$_['text_limit']        = 'Skatīt vienlaicīgi:';

// Entry
$_['entry_search']      = 'Meklēt';
$_['entry_description'] = 'Meklēt preces aprakstā';
?>
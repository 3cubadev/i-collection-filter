<?php
// Heading
$_['heading_title']     = 'Īpašie piedāvājumi';

// Text
$_['text_empty']        = 'Nav īpašo piedāvājumu.';
$_['text_quantity']     = 'Daudzums:';
$_['text_manufacturer'] = 'Ražotājs:';
$_['text_model']        = 'Preces kods:'; 
$_['text_points']       = 'Bonusa punkti:'; 
$_['text_price']        = 'Cena:'; 
$_['text_tax']          = 'Bez nodokļa:'; 
$_['text_compare']      = 'Preču salīdzināšana (%s)'; 
$_['text_sort']         = 'Kārtošanas secība:';
$_['text_default']      = 'Bez kārtošanas';
$_['text_name_asc']     = 'Nosaukums (A - Ž)';
$_['text_name_desc']    = 'Nosaukums (Ž - A)';
$_['text_price_asc']    = 'Cena, sākot ar zemāko';
$_['text_price_desc']   = 'Cena, sākot ar augstāko';
$_['text_rating_asc']   = 'Vērtējums, sākot ar zemāko)';
$_['text_rating_desc']  = 'Vērtējums, sākot ar augstāko';
$_['text_model_asc']    = 'Modelis (A - Ž)';
$_['text_model_desc']   = 'Modelis (Ž - A)';
$_['text_limit']        = 'Skatīt vienlaicīgi:';
?>
<?php
// Heading
$_['heading_title']     = 'Preču salīdzināšana';

// Text
$_['text_product']      = 'Preču specifikācija';
$_['text_name']         = 'Prece';
$_['text_image']        = 'Attēls';
$_['text_price']        = 'Cena';
$_['text_model']        = 'Modelis';
$_['text_manufacturer'] = 'Ražotājs';
$_['text_availability'] = 'Pieejamība';
$_['text_instock']      = 'Pieejams';
$_['text_rating']       = 'Vērtējums';
$_['text_reviews']      = 'Balstīts uz %s atsauksmēm.';
$_['text_summary']      = 'Apraksts';
$_['text_weight']       = 'Svars';
$_['text_dimension']    = 'Izmēri (Garums x Platums x Augstums)';
$_['text_compare']      = 'Preču salīdzināšana (%s)';
$_['text_success']      = 'Izpildīts: Jūs pievienojāt <a href="%s">%s</a> savai <a href="%s">preču salīdzināšanai</a>!';
$_['text_remove']       = 'Jūs sekmīgi izmainījāt savu salīdzināšanas sarakstu!';
$_['text_empty']        = 'Jūs vēl neesat izvēlējies(-usies) nevienu precis salīdzināšanai.';
?>

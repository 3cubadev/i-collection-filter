<?php
// Heading 
$_['heading_title']      = 'Preču atteikumi';

// Text
$_['text_account']       = 'Profils';
$_['text_return']        = 'Informācija par atteikumu';
$_['text_return_detail'] = 'Atteikuma detaļas';
$_['text_description']   = '<p>Lūdzu aizpildiet veidlapu, lai saņemtu preču atteikuma autorizācijas numuru.</p>';
$_['text_order']         = 'Informācija par pasūtījumu';
$_['text_product']       = 'Informācija par preci un atteikuma iemesls';
$_['text_message']       = '<p>Pateicamies par Jūsu atteikuma pieprasījuma nosūtīšanu. Jūsu pieprasījums ir nosūtīts atbildīgajai nodaļai tā apstrādei.</p><p> Jums tiks paziņots uz e-pastu par jūsu pieprasījuma statusu.</p>';
$_['text_return_id']     = 'Atteikuma Nr.:';
$_['text_order_id']      = 'Pasūtījuma Nr.:';
$_['text_date_ordered']  = 'Pasūtījuma datums:';
$_['text_status']        = 'Statuss:';
$_['text_date_added']    = 'Pievienots:';
$_['text_comment']       = 'Atteikuma papildinformācija';
$_['text_history']       = 'Attiekumu vēsture';
$_['text_empty']         = 'Jums nav neviena atteikuma līdz šim!';
$_['text_agree']         = 'Esmu iepazinies(-usies) ar sadaļu <a class="colorbox" href="%s" alt="%s"><b>%s</b></a> un piekrītu visiem minētajiem noteikumiem.';


// Column
$_['column_return_id']   = 'Atteikuma Nr.';
$_['column_order_id']    = 'Pasūtījuma Nr.';
$_['column_status']      = 'Stāvoklis';
$_['column_date_added']  = 'Pievienots';
$_['column_customer']      = 'Klients';
$_['column_product']        = 'Preces nosaukums';
$_['column_model']       = 'Modelis';
$_['column_quantity']    = 'Skaits';
$_['column_price']       = 'Cena';
$_['column_opened']      = 'Atpakots';
$_['column_comment']     = 'Papildinformācija';
$_['column_reason']      = 'Iemesls';
$_['column_action']      = 'Darbība';




// Entry
$_['entry_order_id']     = 'Pasūtījuma Nr.';
$_['entry_date_ordered'] = 'Pasūtījuma datums';
$_['entry_firstname']    = 'Vārds';
$_['entry_lastname']     = 'Uzvārds';
$_['entry_email']        = 'E-pasts';
$_['entry_telephone']    = 'Tālrunis';
$_['entry_product']      = 'Preces nosaukums';
$_['entry_model']        = 'Preces kods';
$_['entry_quantity']     = 'Daudzums';
$_['entry_reason']       = 'Atteikuma iemesli';
$_['entry_opened']       = 'Prece ir atpakota';
$_['entry_fault_detail'] = 'Bojājuma apraksts vai cita papildu informācija';
$_['entry_captcha']      = 'Ievadiet kodu, kas redzams attēlā';

// Error
$_['text_error']         = 'Šāds atteikums nav atrasts!';
$_['error_order_id']     = 'Jānorāda pasūtījuma numurs!';
$_['error_firstname']    = 'Jūsu vārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']     = 'Jūsu uzvārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_email']        = 'Jūsu e-pasta adrese ir ievadīta nepareizi!';
$_['error_telephone']    = 'Tālruņa numuram jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_product']      = 'Preces nosaukumam jābūt no  3 līdz 254 rakstzīmēm!';
$_['error_model']        = 'Preces kodam jābūt no 3 līdz 63 rakstzīmēm!';
$_['error_reason']       = 'Jums ir jānorāda atteikuma iemesls!';
$_['error_captcha']      = 'Kods no attēla ir ievadīts nepareizi!';
$_['error_agree']        = 'Uzmanību! Jums ir jāpiekrīt sadaļas %s noteikumiem!';
?>
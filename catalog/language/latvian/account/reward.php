<?php
// Heading 
$_['heading_title']      = 'Jūsu bonusa punkti';

// Column
$_['column_date_added']  = 'Pievienots';
$_['column_description'] = 'Apraksts';
$_['column_points']      = 'Punkti';

// Text
$_['text_account']       = 'Profils';
$_['text_reward']        = 'Bonusa punkti';
$_['text_total']         = 'Jūsu kopējais bonusa punktu skaits ir:';
$_['text_empty']         = 'Jums pagaidām nav neviena bonusa punkta!';
?>
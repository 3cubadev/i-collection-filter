<?php
// Heading 
$_['heading_title']     = 'Adrešu grāmata';

// Text
$_['text_account']      = 'Mans profils';
$_['text_address_book'] = 'Manas adreses';
$_['text_edit_address'] = 'Rediģēt adresi';
$_['text_add']       = 'Jūsu adrese tika veiksmīgi pievienota!';
$_['text_edit']       = 'Jūsu adrese tika veiksmīgi labota!';
$_['text_delete']       = 'Jūsu adrese tika veiksmīgi dzēsta!';
$_['text_empty']           = 'Jūsu profilā nav nevienas adreses.';

// Entry
$_['entry_firstname']   = 'Vārds';
$_['entry_lastname']    = 'Uzvārds';
$_['entry_company']     = 'Uzņēmums';
$_['entry_address_1']   = 'Adrese 1';
$_['entry_address_2']   = 'Adrese 2';
$_['entry_postcode']    = 'Pasta indekss';
$_['entry_city']        = 'Pilsēta';
$_['entry_country']     = 'Valsts';
$_['entry_zone']        = 'Reģions';
$_['entry_default']     = 'Primārā adrese';

// Error
$_['error_delete']      = 'Jums jābūt vismaz 1 adresei!';
$_['error_default']     = 'Jūs nevarat dzēst savu primāro adresi!';
$_['error_firstname']   = 'Jūsu vārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']    = 'Jūsu uzvārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_vat']         = 'Nederīgs PVN reģistrācijas numurs!';
$_['error_address_1']   = 'Jūsu adresei ir jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_postcode']    = 'Jūsu pasta indeksam ir jābūt no 2 līdz 10 rakstzīmēm!';
$_['error_city']        = 'Jūsu pilsētas nosaukumam ir jābūt no 2 līdz 128 rakstzīmēm!';
$_['error_country']     = 'Lūdzu, norādiet savu valsti!';
$_['error_zone']        = 'Lūdzu, norādiet savu reģionu!';
$_['error_custom_field']   = '%s ir obligāti jānorāda!';
?>

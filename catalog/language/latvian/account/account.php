<?php
// Heading 
$_['heading_title']      = 'Mans profils';

// Text
$_['text_account']       = 'Mans profils';
$_['text_my_account']    = 'Profila pamatinformācija';
$_['text_my_orders']     = 'Mani pasūtījumi';
$_['text_my_newsletter'] = 'Abonēt jaunumus';
$_['text_edit']   = 'Rediģēt profila informāciju';
$_['text_password']      = 'Mainīt paroli';
$_['text_address']       = 'Rediģēt manas adreses';
$_['text_wishlist']      = 'Rediģēt manu vēlmju sarakstu';
$_['text_order']         = 'Pasūtījumu vēsture';
$_['text_download']      = 'Manas lejupielādes';
$_['text_reward']        = 'Jūsu bonus punkti'; 
$_['text_return']        = 'Apskatīt manus atteikuma pieprasījumus'; 
$_['text_transaction']   = 'Jūsu darījumi'; 
$_['text_newsletter']    = 'Jaunumu un īpašo piedāvājumu saņemšana';
$_['text_recurring']    = 'Periodiskie maksājumi';
$_['text_transactions']  = 'Darījumi';
?>
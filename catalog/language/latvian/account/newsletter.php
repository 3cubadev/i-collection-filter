<?php
// Heading 
$_['heading_title']    = 'Jaunumu saņemšana uz e-pastu.';

// Text
$_['text_account']     = 'Mans profils';
$_['text_newsletter']  = 'Abonēt';
$_['text_success']     = 'Jūsu abonēšanas dati ir veiksmīgi saglabāti!';

// Entry
$_['entry_newsletter'] = 'Vēlos saņemt ziņas par jaunām precēm un piedāvājumiem:';
?>

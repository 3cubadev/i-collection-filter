<?php
$_['heading_title'] = 'Periodiskie maksājumi';
$_['button_continue'] = 'Turpināt';
$_['button_view'] = 'Skatīt';
$_['text_empty'] = 'Nav atrasts neviens periodiskais maksājums';
$_['text_product'] = 'Prece: ';
$_['text_order'] = 'Pasūtījums: ';
$_['text_quantity'] = 'Daudzums: ';
$_['text_account'] = 'Konts';
$_['text_action'] = 'Darbība';
$_['text_recurring'] = 'Periodiskais maksājums';
$_['text_transactions'] = 'Darījumi';
$_['button_return'] = 'Atgriezties';
$_['text_empty_transactions'] = 'Šim periodiskajam maksājumam nav darījumu';

$_['column_date_added'] = 'Izveidots';
$_['column_type'] = 'Veids';
$_['column_amount'] = 'Summa';
$_['column_status'] = 'Statuss';
$_['column_product'] = 'Prece';
$_['column_action'] = 'Darbība';
$_['column_recurring_id'] = 'Periodiskā maksājuma ID';

$_['text_recurring_detail'] = 'Periodiskā maksājuma detaļas';
$_['text_recurring_id'] = 'Periodiskā maksājuma ID: ';
$_['text_payment_method'] = 'Apmaksas veids: ';
$_['text_date_added'] = 'Izveidots: ';
$_['text_recurring_description'] = 'Apraksts: ';
$_['text_status'] = 'Statuss: ';
$_['text_ref'] = 'Atsauce: ';

$_['text_status_active'] = 'Aktīvs';
$_['text_status_inactive'] = 'Nav aktīvs';
$_['text_status_cancelled'] = 'Atcelts';
$_['text_status_suspended'] = 'Apturēts';
$_['text_status_expired'] = 'Beidzies derīguma termiņš';
$_['text_status_pending'] = 'Gaida';

$_['text_transaction_date_added'] = 'Izveidots';
$_['text_transaction_payment'] = 'Maksājums';
$_['text_transaction_outstanding_payment'] = 'Nenokārtots maksājums';
$_['text_transaction_skipped'] = 'Izlaists maksājums';
$_['text_transaction_failed'] = 'Maksājums neizdevās';
$_['text_transaction_cancelled'] = 'Atcelts';
$_['text_transaction_suspended'] = 'Apturēts';
$_['text_transaction_suspended_failed'] = 'Aizturēts neizpildīta maksājuma dēļ';
$_['text_transaction_outstanding_failed'] = 'Maksājums neizdevās';
$_['text_transaction_expired'] = 'Beidzies derīguma termiņš';

$_['error_not_cancelled'] = 'Kļūda: %s';
$_['error_not_found'] = 'Neizdevās atcelt periodisko maksājumu';
$_['text_cancelled'] = 'Periodiskais maksājums ir atcelts';
?>
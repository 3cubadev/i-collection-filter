<?php
// Heading
$_['heading_title'] = 'Jūsu profils ir veiksmīgi izveidots!';

// Text
$_['text_message']  = '<p>Apsveicam! Jūsu profils ir veiksmīgi izveidots.</p><p>Tagad Jūs varat izmantot papildu iespējas: apskatīt pirkumu vēsturi, rēķinus, mainīt profila informāciju u.t.t.</p><p>Ja Jums rodas jautājumi, lūdzu, <a href="%s">sazinieties ar mums</a>.</p><p>Apstiprinājuma vēstule tika nosūtīta uz Jūsu norādīto e-pastu.</p>';
$_['text_approval'] = '<p>Paldies par reģistrēšanos %s lapā!</p><p>Jums tiks paziņots pa e-pastu, tiklīdz Jūsu profils būs aktivizēts</p><p>Ja Jums ir JEBKĀDI jautājumi par šīs vietnes darbību, lūdzu, <a href="%s">sazinieties ar mums</a>.</p>';
$_['text_account']  = 'Profils';
$_['text_success']  = 'Pabeigts';
?>

<?php
// Heading 
$_['heading_title']         = 'Pasūtījumu vēsture';

// Text
$_['text_account']          = 'Profils';
$_['text_order']            = 'Informācija par pasūtījumu';
$_['text_order_detail']     = 'Pasūtījuma detalizācija';
$_['text_invoice_no']       = 'Rēķina Nr.:';
$_['text_order_id']         = 'Pasūtījuma Nr.:';
$_['text_date_added']       = 'Pievienots';
$_['text_shipping_address'] = 'Piegādes adrese:';
$_['text_shipping_method']  = 'Piegādes veids:';
$_['text_payment_address']  = 'Maksātāja adrese';
$_['text_payment_method']   = 'Maksāšanas veids:';
$_['text_comment']          = 'Pasūtījuma papildinformācija';
$_['text_history']          = 'Pasūtījuma vēsture';
$_['text_success']          = 'Jūs sekmīgi pievienojāt savam grozam preces no pasūtījuma Nr.%s!';
$_['text_empty']            = 'Jums nav iepriekš veiktu pasūtījumu!';
$_['text_error']            = 'Šāds pasūtījums nav atrasts!';

// Column
$_['column_order_id']       = 'Pasūtījuma Nr.';
$_['column_product']        = 'Preču skaits';
$_['column_customer']       = 'Klients';
$_['column_name']           = 'Preces nosaukums';
$_['column_model']          = 'Modelis';
$_['column_quantity']       = 'Skaits';
$_['column_price']          = 'Cena';
$_['column_total']          = 'Kopā';
$_['column_action']         = 'Darbība';
$_['column_date_added']     = 'Pievienots';
$_['column_status']         = 'Statuss';
$_['column_comment']        = 'Papildinformācija';

// Error
$_['error_reorder']         = '%s pagaidām nav pieejams atkārtotai pasūtīšanai.';
?>

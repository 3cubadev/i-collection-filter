<?php
// Heading 
$_['heading_title']   = 'Aizmirsta parole';

// Text
$_['text_account']    = 'Profils';
$_['text_forgotten']  = 'Aizmirsta parole';
$_['text_your_email'] = 'Jūsu e-pasta adrese';
$_['text_email']      = 'Lūdzu, ievadiet savu e-pasta adresi, ar kuru esat reģistrējies(-usies), noklikšķiniet uz "Tālāk", lai savā e-pastā saņemtu paroli.';
$_['text_success']    = 'Jaunā parole ir veiksmīgi nosūtīta uz Jūsu E-pasta adresi.';

// Entry
$_['entry_email']     = 'E-pasta adrese';

// Error
$_['error_email']     = 'Jūsu e-pasta adrese ir ievadīta nepareizi!';
?>

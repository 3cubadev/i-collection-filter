<?php
// Heading 
$_['heading_title']        = 'Profila reģistrēšana';

// Text
$_['text_account']         = 'Profils';
$_['text_register']        = 'Reģistrēšana';
$_['text_account_already'] = 'Ja Jūs jau esat reģistrēts lietotājs, lūdzu, autorizējieties <a href="%s">šeit</a>.';
$_['text_your_details']    = 'Jūsu personas dati';
$_['text_your_address']    = 'Jūsu adrese';
$_['text_newsletter']      = 'Jaunumu saņemšana pa e-pastu';
$_['text_your_password']   = 'Jūsu parole';
$_['text_agree']           = 'Esmu iepazinies(-usies) ar sadaļu "<a class="colorbox" href="%s" alt="%s"><b>%s</b></a>" un piekrītu visiem minētajiem noteikumiem';


// Entry
$_['entry_customer_group'] = 'Uzņēmuma veids';
$_['entry_firstname']      = 'Vārds';
$_['entry_lastname']       = 'Uzvārds';
$_['entry_email']          = 'E-pasts';
$_['entry_telephone']      = 'Tālrunis';
$_['entry_fax']            = 'Fakss';
$_['entry_company']        = 'Uzņēmums';
$_['entry_address_1']      = 'Adrese 1';
$_['entry_address_2']      = 'Adrese 2';
$_['entry_postcode']       = 'Pasta indekss';
$_['entry_city']           = 'Pilsēta';
$_['entry_country']        = 'Valsts';
$_['entry_zone']           = 'Reģions';
$_['entry_newsletter']     = 'Vēlos saņemt ziņas par jaunām precēm un piedāvājumiem';
$_['entry_password']       = 'Parole';
$_['entry_confirm']        = 'Apstipriniet paroli';


// Error
$_['error_exists']         = 'Kļūda: šāda e-pasta adrese jau ir reģistrēta!';
$_['error_firstname']      = 'Jūsu vārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']       = 'Jūsu uzvārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_email']          = 'Jūsu e-pasta adrese ir ievadīta nepareizi!';
$_['error_telephone']      = 'Jūsu tālrunim ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_address_1']      = 'Jūsu adresei ir jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_city']           = 'Jūsu pilsētai ir jābūt no 2 līdz 128 rakstzīmēm!';
$_['error_postcode']       = 'Jūsu pasta indeksam ir jābūt no 2 līdz 10 rakstzīmēm!';
$_['error_country']        = 'Lūdzu, norādiet savu valsti!';
$_['error_zone']           = 'Lūdzu, norādiet savu reģionu!';
$_['error_custom_field']   = '%s ir obligāti jānorāda!';
$_['error_password']       = 'Jūsu parolei ir jābūt no 4 līdz 20 rakstzīmēm!';
$_['error_confirm']        = 'Jūsu paroles nesakrīt!';
$_['error_agree']          = 'Jūs neesat apstiprinājis(-usi) sadaļas %s noteikumus!';
?>

<?php
// Heading 
$_['heading_title']      = 'Jūsu darījumi';

// Column
$_['column_date_added']  = 'Pievienots';
$_['column_description'] = 'Apraksts';
$_['column_amount']      = 'Summa (%s)';

// Text
$_['text_account']       = 'Profils';
$_['text_transaction']   = 'Jūsu darījumi';
$_['text_total']         = 'Jūsu pašreizējā bilance ir:';
$_['text_empty']         = 'Jums nav neviena darījuma!';
?>
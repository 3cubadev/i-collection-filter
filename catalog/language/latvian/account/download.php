<?php
// Heading 
$_['heading_title']   = 'Jūsu Lejupielādes';

// Text
$_['text_account']    = 'Profils';
$_['text_downloads']  = 'Lejupielādes';
$_['text_empty']        = 'Jūs vēl neesat veikuši nevienu pasūtījumu, kurā būtu lejuplādējama prece!';

// Column
$_['column_order_id']   = 'Pasūtījuma ID';
$_['column_name']       = 'Nosaukums';
$_['column_size']       = 'Lejupielādes apjoms';
$_['column_date_added'] = 'Pievienots';
?>

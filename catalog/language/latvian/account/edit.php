<?php
// Heading 
$_['heading_title']       = 'Mana profila informācija';

// Text
$_['text_account']        = 'Mans profils';
$_['text_edit']           = 'Rediģēt profila informāciju';
$_['text_your_details']   = 'Jūsu personas dati';
$_['text_success']        = 'Jūsu profils tika veiksmīgi labots!';

// Entry
$_['entry_firstname']     = 'Vārds';
$_['entry_lastname']      = 'Uzvārds';
$_['entry_email']         = 'E-pasts';
$_['entry_telephone']     = 'Tālrunis';
$_['entry_fax']           = 'Fakss';

// Error
$_['error_exists']        = 'Šāda e-pasta adrese jau ir reģistrēta!';
$_['error_firstname']     = 'Jūsu vārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']      = 'Jūsu uzvārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_email']         = 'Jūsu e-pasta adrese ir ievadīta nepareizi!';
$_['error_telephone']     = 'Jūsu tālrunim ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_custom_field'] = '%s ir obligāti jānorāda!';
?>

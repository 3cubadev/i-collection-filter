<?php
// Heading 
$_['heading_title'] = 'Mans vēlmju saraksts';

// Text
$_['text_account']  = 'Profils';
$_['text_instock']  = 'Pieejams';
$_['text_wishlist'] = 'Vēlmju saraksts (%s)';
$_['text_login']    = 'Jums <a href="%s">jāautorizējas</a> vai <a href="%s">jāreģistrējas,</a> lai pievienotu <a href="%s">%s</a> savam <a href="%s"> vēlmju sarakstam</a>!';
$_['text_success']  = 'Pabeigts: Jūs pievienojāt <a href="%s">%s</a> savam <a href="%s">vēlmju sarakstam</a>!';
$_['text_exists']   = '<a href="%s">%s</a> jau ir jūsu <a href="%s">vēlmju sarakstā</a>!';
$_['text_remove']   = 'Jūs sekmīgi rediģējāt savu vēlmju sarakstu!';
$_['text_empty']    = 'Jūsu vēlmu saraksts pagaidām ir tukšs.';

// Column
$_['column_image']  = 'Attēls';
$_['column_name']   = 'Preces nosaukums';
$_['column_model']  = 'Modelis';
$_['column_stock']  = 'Pieejamība';
$_['column_price']  = 'Vienības cena';
$_['column_action']   = 'Darbība';
?>

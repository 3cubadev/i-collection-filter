<?php
// Heading 
$_['heading_title']    = 'Dāvanu kartes iegāde';

// Text
$_['text_account']     = 'Profils';
$_['text_voucher']     = 'Dāvanu karte';
$_['text_description'] = 'Šī dāvanu karte tiks nosūtīta adresātam, tiklīdz Jūs to būsiet apmaksājis(-usi).';
$_['text_agree']       = 'Es saprotu, ka dāvanu kartes nevar tikt atgrieztas atpakaļ.';
$_['text_message']     = '<p>Paldies par dāvanu kartes iegādi! Dāvanu karte ar tās izmantošanas instrukciju tiks nosūtīta adresātam pa e-pastu, tiklīdz Jūs būsiet pabeidzis(-usi) pasūtījumu.</p>';
$_['text_for']         = '%s Dāvanu karte, saņems %s';

// Entry
$_['entry_to_name']    = 'Saņēmēja vārds';
$_['entry_to_email']   = 'Saņēmēja e-pasta adrese';
$_['entry_from_name']  = 'Jūsu vārds';
$_['entry_from_email'] = 'Jūsu e-pasta adrese';
$_['entry_theme']      = 'Dāvanu kartes noformējums';
$_['entry_message']    = 'Papildu ziņa';
$_['entry_amount']     = 'Summa';

// Help
$_['help_message']     = 'Nav obligāta';
$_['help_amount']      = 'Summai jābūt no %s līdz %s';

// Error
$_['error_to_name']    = 'Saņēmēja vārda garumam ir jābūt no 1 līdz 64 rakstzīmēm!';
$_['error_from_name']  = 'Jūsu vārda garumam ir jābūt no 1 līdz 64 rakstzīmēm!';
$_['error_email']      = 'Nederīga E-pasta adrese!';
$_['error_theme']      = 'Lūdzu, izvēlieties noformējumu!';
$_['error_amount']     = 'Summai jābūt no %s līdz %s!';
$_['error_agree']      = 'Uzmanību! Jums ir jāpiekrīt noteikumam, ka dāvanu karte netiks pieņemta atpakaļ!';
?>

<?php
// Heading 
$_['heading_title']                = 'Autorizēšanās';

// Text
$_['text_account']                 = 'Profils';
$_['text_login']                   = 'Autorizēties';
$_['text_new_customer']       = 'Jauns lietotājs';
$_['text_register']                = 'Reģistrēt jaunu profilu';
$_['text_register_account']          = 'Pēc reģistrēšanās Jūs varēsiet pilnībā izmantot šīs vietnes iespējas, tostarp: ātri un ērti noformēt pasūtījumus, sekot līdzi pasūtījuma apstrādei un apskatīt savus iepriekš veiktos pasūtījumus.';
$_['text_returning_customer']      = 'Reģistrēts lietotājs';
$_['text_i_am_returning_customer'] = 'Esmu reģistrēts lietotājs';
$_['text_forgotten']      = 'Aizmirsta parole';


// Entry
$_['entry_email']          = 'E-pasta adrese';
$_['entry_password']               = 'Parole';

// Error
$_['error_login']                  = 'Kļūda: Nav atrasta tāda e-pasta adrese un/vai parole.';
$_['error_attempts']               = 'Kļūda: Jūsu profilam ir pārsniegs atļauto autorizēšanās mēģinājumu skaits. Lūdzu mēģiniet atkal pēc 1 stundas.';
$_['error_approved']               = 'Kļūda: Jūsu profilam jābūt apstiprinātam pirms autorizēšanās.'; 
?>

<?php
// Heading 
$_['heading_title'] = 'Profils';

// Text
$_['text_message']  = '<p>Jūs veiksmīgi esat izgājis(-usi) no sava profila.</p><p>Jūsu iepirkuma grozs ir saglabāts un tiks atjaunots nākamajā pieslēgšanās reizē.</p>';
$_['text_account']  = 'Profils';
$_['text_logout']   = 'Iziet';
?>
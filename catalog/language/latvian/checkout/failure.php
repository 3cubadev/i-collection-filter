<?php
// Heading
$_['heading_title'] = 'Nesekmīgs maksājums!';

// Text
$_['text_basket']   = 'Pirkumu grozs';
$_['text_checkout'] = 'Pirkuma noformēšana';
$_['text_failure']  = 'Neizdevās veikt maksājumu';
$_['text_message']  = '<p>Maksājuma izpildes gaitā ir radusies problēma un pasūtījums nav pabeigts.</p>

<p>Iespējamie iemesli ir:</p>
<ul>
  <li>kontā nepietiek līdzekļu,</li>
  <li>Nesekmīga autorizācija.</li>
</ul>

<p>Lūdzu mēģiniet pasūtīt vēlreiz, izmantojot citu maksājuma veidu.</p>

<p>Ja joprojām problēmu nezidodas apiet, lūdzu <a href="%s">sazinieties ar mums</a>, norādot pasūtījuma, kuru mēģiniet veit, detaļas.</p>
';
?>
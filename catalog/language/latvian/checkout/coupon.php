<?php
// Heading
$_['heading_title'] = 'Izmantojiet kupona kodu';

// Text
$_['text_success']  = 'Jūsu kupona atlaide ir sekmīgi piemērota!';

// Entry
$_['entry_coupon']  = 'Ierakstiet jūsu kupona kodu šeit';

// Error
$_['error_coupon']  = 'Uzmanību! Kupons vai nu nav derīgs, vai arī ir sasniedzis izmantojamo reižu skaitu!';
$_['error_empty']   = 'Uzmanību! Lūdzu norādiet kupona kodu!';
?>
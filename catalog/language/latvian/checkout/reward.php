<?php
// Heading
$_['heading_title'] = 'Izmantot bonusa punktus (ir pieejami %s)';

// Text
$_['text_success']  = 'Jūsu bonusa punktu atlaide ir sekmīgi piemērota!';

// Entry
$_['entry_reward']  = 'Bonusa punktu skaits, kurus vēlaties izmantot (maks. %s)';

// Error
$_['error_reward']  = 'Uzmanību! Lūdzu norādiet bonusa punktu skaitu, kuru vēlaties izmantot!';
$_['error_points']  = 'Uzmanību! Jums nav norādītais skaits (%s) bonusa punktu!';
$_['error_maximum'] = 'Uzmanību! Maksimālais bonusa punktu skaits, kuru jūsu varat piemērot ir %s!';
?>
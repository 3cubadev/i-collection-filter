<?php
// Heading  
$_['heading_title']   			= 'Pirkumu grozs';

// Text
$_['text_success']    			= 'Pabeigts: Jūs pievienojāt <a href="%s">%s</a> savam <a href="%s">pirkumu grozam</a>!';
$_['text_remove']            	= 'Pirkumu grozs ir sekmīgi mainīts!';
$_['text_login']      			= 'Uzmanību: Jums ir <a href="%s">jāautorizējas</a> vai <a href="%s">jāizveido konts,</a> lai redzētu cenas!';
$_['text_items']             	= '%s prece(s) - %s';
$_['text_points']            	= 'Bonusa punkti: %s';
$_['text_next']              	= 'Ko Jūs vēlaties darīt tālāk?';
$_['text_next_choice']       	= 'Izvēlieties, ja Jums ir atlaižu kods vai bonusa punkti, kurus vēlaties piemērot, vai, ja vēlaties prognozēt piegādes izmaksas.';
$_['text_empty']             	= 'Grozs pagaidām ir tukšs!';
$_['text_day']               	= 'dienai(-ām)';
$_['text_week']              	= 'nedēļai(-ām)';
$_['text_semi_month']        	= 'pusei(-ēm) mēneša';
$_['text_month']             	= 'mēnesim (mēnešiem)';
$_['text_year']              	= 'gadam(-iem)';
$_['text_trial']       			= '%s ik pa %s %s līdz %s maksājumiem, pēc tam ';
$_['text_recurring']   			= '%s ik pa %s %s';
$_['text_length']      			= ' līdz %s māksājumiem(-am)';
$_['text_until_cancelled']      = 'līdz atcelšanai!';
$_['text_recurring_item']    	= 'Periodisks maksājums';
$_['text_payment_recurring']    = 'Atkārtotai maksājumi';
$_['text_trial_description'] 	= '%s ik pa %d %s līdz %d maksājumam(-iem), pēc tam';
$_['text_payment_description']  = '%s ik pa %d %s līdz %d maksājumiem';
$_['text_payment_cancel'] 		= '%s ik pa %d %s līdz atcelšanai';

// Column
$_['column_image']    			= 'Attēls';
$_['column_name']     			= 'Preces nosaukums';
$_['column_model']    			= 'Modelis';
$_['column_quantity'] 			= 'Daudzums';
$_['column_price']    			= 'Cena';
$_['column_total']    			= 'Kopā';

// Error
$_['error_stock']     			= 'Preces, kas apzīmētas ar *** nav pieejamas šādā daudzumā vai pašreiz nav noliktavā!';	
$_['error_minimum']   			= '%s minimālais pasūtījuma daudzums ir %s!';	
$_['error_required']  			= '%s ir obligāts!';	
$_['error_product']          	= 'Uzmanību! Jūsu grozā nav preču!';	
$_['error_recurring_required'] 	= 'Lūdzu, izvēlieties periodisku maksājumu!';
?>
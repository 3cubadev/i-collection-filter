<?php
// Heading
$_['heading_title']        = 'Piegādes maksas un nodokļu prognozēšana';

// Text
$_['text_success']         = 'Jūsu piegādes prognoze ir sekmīgi piemērota!';
$_['text_shipping']        = 'Norādiet savu piegādes vietu, lai saņemtu piegādes prognozi.';
$_['text_shipping_method'] = 'Lūdzu norādiet vēlamāko piegādes veidu, kuru vēlaties izmantot šim pasūtījumam.';

// Entry
$_['entry_country']        = 'Valsts';
$_['entry_zone']           = 'Reģions';
$_['entry_postcode']       = 'Pasta indekss';

// Error
$_['error_postcode']       = 'Pasta indeksā jābūt no 2 līdz 10 rakstzīmēm!';
$_['error_country']        = 'Lūdzu izvēlieties valsti!';
$_['error_zone']           = 'Lūdzu izvēlieties reģionu!';
$_['error_shipping']       = 'Uzmanību! Ir jānorāda piegādes vieds!';
$_['error_no_shipping']    = 'Uzmanību! Nav pieejams neviens piegādes veids. Lūdzu <a href="%s">sazinieties ar mums</a>, lai atrisinātu šo jautājumu!';
?>
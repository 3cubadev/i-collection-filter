<?php
// Heading
$_['heading_title'] = 'Izmantot dāvanu karti';

// Text
$_['text_success']  = 'Dāvanu kartes atlaide ir sekmīgi piemērota!';

// Entry
$_['entry_voucher'] = 'Norādiet savas dāvanu kartes kodu šeit';

// Error
$_['error_voucher'] = 'Uzmanību! Dāvanu karte ir vai nu nederīga, vai jau ir izmantota!';
$_['error_empty']   = 'Uzmanību! Lūdzu norādiet dāvanu kartes kodu!';
?>
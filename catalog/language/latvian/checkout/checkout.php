<?php
// Heading 
$_['heading_title']                  = 'Pirkuma noformēšana';

// Text
$_['text_cart']                      = 'Pirkumu grozs';
$_['text_checkout_option']           = '1.solis: Noformēšanas veida izvēle';
$_['text_checkout_account']          = '2.solis: Profila informācija un informācija rēķina sagatavošanai';
$_['text_checkout_payment_address']  = '2.solis: Informācija rēķina sagatavošanai';
$_['text_checkout_shipping_address'] = '3.solis: Piegādes informācija';
$_['text_checkout_shipping_method']  = '4.solis: Piegādes veida izvēle';
$_['text_checkout_payment_method']   = '5.solis: Apmaksas veida izvēle';
$_['text_checkout_confirm']          = '6.solis: Pasūtījuma apstiprināšana';
$_['text_modify']                    = 'Rediģēt »';
$_['text_new_customer']              = 'Jauns pircējs';
$_['text_returning_customer']        = 'Reģistrēts pircējs';
$_['text_checkout']                  = 'Noformēšanas veida izvēle:';
$_['text_i_am_returning_customer']   = 'Es esmu jau reģistrēts pircējs';
$_['text_register']                  = 'Reģistrēt profilu';
$_['text_guest']                     = 'Noformēt kā viesim';
$_['text_register_account']          = 'Reģistrējoties Jūs varēsiet: Ātri un ērti noformēt pasūtījumus, sekot līdzi pasūtījuma apstrādei un apskatīt savus iepriekš veiktos pasūtījumus.';
$_['text_forgotten']                 = 'Aizmirsta parole';
$_['text_your_details']              = 'Jūsu personas dati';
$_['text_your_address']              = 'Jūsu adrese';
$_['text_your_password']             = 'Jūsu parole';
$_['text_agree']                     = 'Es esmu iepazinies(-usies) ar sadaļu <a class="colorbox" href="%s" alt="%s"><b>%s</b></a> un piekrītu visiem tajā izklāstītajiem noteikumiem';
$_['text_address_new']               = 'Es vēlos lietot jaunu adresi';
$_['text_address_existing']          = 'Es vēlos lietot jau reģistrētu adresi';
$_['text_shipping_method']           = 'Lūdzu, izvēlieties šī pasūtījuma piegādes veidu.';
$_['text_payment_method']            = 'Lūdzu, izvēlieties šī pasūtījuma apmaksas veidu.';
$_['text_comments']                  = 'Papildinformācija par Jūsu pasūtījumu';
$_['text_recurring_item']   		 = 'Periodisks maksājums';
$_['text_payment_recurring']   		 = 'Periodisks maksājums';
$_['text_trial_description'] 		 = '%s ik pa %d %s līdz %d maksājumam(-iem), pēc tam';
$_['text_payment_description']       = '%s ik pa %d %s līdz %d maksājumiem';
$_['text_payment_until_canceled_description'] = '%s ik pa %d %s līdz atcelšanai';
$_['text_day']               = 'dienai(-ām)';
$_['text_week']              = 'nedēļai(-ām)';
$_['text_semi_month']        = 'pusei(-ēm) mēneša';
$_['text_month']             = 'mēnesim (mēnešiem)';
$_['text_year']              = 'gadam(-iem)';

// Column
$_['column_name']                    = 'Preces nosaukums';
$_['column_model']                   = 'Modelis';
$_['column_quantity']                = 'Daudzums';
$_['column_price']                   = 'Cena';
$_['column_total']                   = 'Kopā';

// Entry
$_['entry_email_address']            = 'E-pasta adrese';
$_['entry_email']                    = 'E-pasts';
$_['entry_password']                 = 'Parole';
$_['entry_confirm']                  = 'Atkārtojiet paroli';
$_['entry_firstname']                = 'Vārds';
$_['entry_lastname']                 = 'Uzvārds';
$_['entry_telephone']                = 'Tālruņa numurs';
$_['entry_fax']                      = 'Fakss';
$_['entry_address']                  = 'Izvēlieties adresi';
$_['entry_company']        = 'Uzņēmums';
$_['entry_customer_group'] = 'Uzņēmuma veids';
$_['entry_address_1']                = 'Adrese 1';
$_['entry_address_2']                = 'Adrese 2';
$_['entry_postcode']                 = 'Pasta indekss';
$_['entry_city']                     = 'Pilsēta';
$_['entry_country']                  = 'Valsts';
$_['entry_zone']                     = 'Reģions';
$_['entry_newsletter']               = 'Es vēlos saņemt %s jaunumus.';
$_['entry_shipping'] 	             = 'Mana piegādes un maksātāja adrese ir viena un tā pati.';

// Error
$_['error_warning']                  = 'Jūsu pasūtījuma apstrādē ir radusies kļūda! Ja Jums atkārtoti neizdodas veikt pasūtījumu, lūdzu, mēģiniet izmantot citu apmaksas veidu vai sazinieties ar mums, <a href="%s">klikšķinot šo saiti</a>.';
$_['error_login']                    = 'Uzmanību! Nav atrasta tāda e-pasta adrese un/vai parole.';
$_['error_attempts']               = 'Uzmanību! Jūsu profilam ir pārsniegs atļauto autorizēšanās mēģinājumu skaits. Lūdzu mēģiniet atkal pēc 1 stundas.';
$_['error_approved']               = 'Uzmanību! Jūsu kontu ir nepieciešams apstiprināt.';
$_['error_exists']                   = 'Uzmanību! Šāda e-pasta adrese jau ir reģistrēta!';
$_['error_firstname']   = 'Jūsu vārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']    = 'Jūsu uzvārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_email']          = 'Jūsu e-pasta adrese ir ievadīta nepareizi!';
$_['error_telephone']      = 'Jūsu tālrunim ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_password']       = 'Jūsu parolei ir jābūt no 3 līdz 20 rakstzīmēm!';
$_['error_confirm']                  = 'Ievadītās paroles nesakrīt!';
$_['error_address_1']   = 'Jūsu adresei ir jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_city']        = 'Jūsu pilsētas nosaukumam ir jābūt no 2 līdz 128 rakstzīmēm!';
$_['error_postcode']    = 'Jūsu pasta indeksam ir jābūt no 2 līdz 10 rakstzīmēm!';
$_['error_country']     = 'Lūdzu, norādiet savu valsti!';
$_['error_zone']        = 'Lūdzu, norādiet savu reģionu!';
$_['error_agree']                    = 'Uzmanību! Jums ir jāpiekrīt sadaļas %s noteikumiem!';
$_['error_address']                  = 'Uzmanību! Jums ir jānorāda adrese!';
$_['error_shipping']                 = 'Uzmanību! Piegādes veida izvēle ir obligāta!';
$_['error_no_shipping']              = 'Uzmanību! Nav izvēlēts neviens piegādes veids norādītajai adresei. Lūdzu, <a href="%s">sazinieties ar mums,</a> lai atrisinātu šo jautājumu!';
$_['error_payment']                  = 'Uzmanību! Ir nepieciešams norādīt apmaksas veidu!';
$_['error_no_payment']               = 'Uzmanību! Nav izvēlēts neviens apmaksas veids norādītajai adresei. Lūdzu, <a href="%s">sazinieties ar mums,</a>  lai atrisinātu šo jautājumu';
$_['error_custom_field']             = '%s ir obligāti jānorāda!';
?>
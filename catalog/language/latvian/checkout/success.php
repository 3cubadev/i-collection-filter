<?php
// Heading
$_['heading_title'] = 'Jūsu pasūtījums ir sagatavots!';

// Text
$_['text_basket']   = 'Pirkumu grozs';
$_['text_checkout'] = 'Pasūtījuma noformēšana';
$_['text_success']  = 'Pabeigts';
$_['text_customer'] = '<p>Jūsu pasūtījums ir veiksmīgi sagatavots!</p><p>Jūs varat apskatīt savu pasūtījumu vēsturi, dodoties uz <a href="%s">Profils</a> lapu un noklikšķot uz <a href="%s">Pasūtījumu vēsture</a>.</p><p>Ja jūsu pasūtījums ir saistīts ar lejupielādi, Jūs varat apskatīt un lejupielādēt failus, ejot uz <a href="%s">lejupielādes</a> lapu.</p><p>Ja Jums rodas kādi jautājumi, sazinieties ar <a href="%s">administrāciju</a>.</p><p>Paldies par pirkumu!</p>';
$_['text_guest']    = '<p>Jūsu pasūtījums ir veiksmīgi sagatavots!</p><p>Ja jums rodas kādi jautājumi, sazinieties ar <a href="%s">administrāciju</a>.</p><p>Paldies par pirkumu!</p>';
?>

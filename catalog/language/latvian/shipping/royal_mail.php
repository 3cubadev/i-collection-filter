<?php
// Text
$_['text_title']                = 'Royal Mail';
$_['text_weight']               = 'Svars:';
$_['text_insurance']            = 'Apdrošināts līdz:';
$_['text_1st_class_standard']   = 'Pirmās klases standarta pasts';
$_['text_1st_class_recorded']   = 'Pirmās klases ierakstīts pasts';
$_['text_2nd_class_standard']   = 'Otrās klases standarta pasts';
$_['text_2nd_class_recorded']   = 'Otrās klases ierakstīts pasts';
$_['text_special_delivery_500']  = 'Special Delivery Next Day (&pound;500)';
$_['text_special_delivery_1000'] = 'Special Delivery Next Day (&pound;1000)';
$_['text_special_delivery_2500'] = 'Special Delivery Next Day (&pound;2500)';
$_['text_standard_parcels']     = 'Standarta pakas';
$_['text_airmail']              = 'Avio pasts';
$_['text_international_signed'] = 'Starptautiski parakstīts';
$_['text_airsure']              = 'Airsure';
$_['text_surface']              = 'Surface';
?>
<?php
// Text
$_['text_information']  = 'Прочая информация';
$_['text_requisites']   = 'Реквизиты';
$_['text_requisitesfield']   = '<ul class="contact__ul"><li class="contact__list">Montenegro OOO</li><li class="contact__list">LV40103845311</li><li class="contact__list">Norvik Banka Riga</li><li class="contact__list">LV39LATB0002210061664</li><li class="contact__list">LATBLV22XXX</li><li class="contact__list">Юридический адрес: Тербатас 49/51-2, Рига</li><li class="contact__list">LV-1011, Latvija</li><li class="contact__list">Фактический адрес: Масквас 12, Рига</li><li class="contact__list">LV-1050, Latvija</li><li class="contact__list">+371 24422833</li></ul>';
$_['text_work_time']    = 'Рабочее время';
$_['text_follow_us']    = 'Следить за нами';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Обратная связь';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнерская программа';
$_['text_special']      = 'Акции';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Закладки';
$_['text_newsletter']   = 'Рассылка';
$_['text_powered']      = '%s &copy; %s';

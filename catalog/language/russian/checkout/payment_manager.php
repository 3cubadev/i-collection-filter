<?php
// Heading
$_['heading_title']             = 'Ваш заказ принят на проверку!';
$_['heading_title_pay']         = 'Оплата заказа №%s';

// Text
$_['text_basket']               = 'Корзина';
$_['text_checkout']             = 'Оформление заказа';
$_['text_success']              = 'Готово';
$_['text_account']              = 'Учетная Запись';
$_['text_order_history']        = 'История Заказов';
$_['text_complete']             = 'Заявка успешно подтверждена';

// Column
$_['column_name']               = 'Товар';
$_['column_model']              = 'Модель';
$_['column_quantity']           = 'Количество';
$_['column_price']              = 'Цена';
$_['column_total']              = 'Итого';

// Button
$_['button_pay']                = 'Оплатить заказ';
$_['button_complete']           = 'Подтвердить заказ';
$_['button_cancel']             = 'Отменить';

// Error
$_['error_invalid_order']       = 'Неверный заказ!';
?>
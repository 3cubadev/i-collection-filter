<?php
class ModelCatalogHeadermenu extends Model {
	public function addHeadermenu($data) {
		$this->event->trigger('pre.admin.headermenu.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "headermenu SET link = '" .$data['link'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

		$headermenu_id = $this->db->getLastId();

		foreach ($data['headermenu_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "headermenu_description SET headermenu_id = '" . (int)$headermenu_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}

		if (isset($data['link'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "headermenu SET link = '" . $this->db->escape($data['link']) . "' WHERE headermenu_id = '" . (int)$headermenu_id . "'");
		}

		$this->cache->delete('headermenu');

		$this->event->trigger('post.admin.headermenu.add', $headermenu_id);

		return $headermenu_id;
	}

	public function editHeadermenu($headermenu_id, $data) {
		$this->event->trigger('pre.admin.headermenu.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "headermenu SET link = '" .$data['link'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE headermenu_id = '" . (int)$headermenu_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "headermenu_description WHERE headermenu_id = '" . (int)$headermenu_id . "'");

		foreach ($data['headermenu_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "headermenu_description SET headermenu_id = '" . (int)$headermenu_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}

		$this->cache->delete('headermenu');

		$this->event->trigger('post.admin.headermenu.edit', $headermenu_id);
	}

	public function deleteHeadermenu($headermenu_id) {
		$this->event->trigger('pre.admin.headermenu.delete', $headermenu_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "headermenu WHERE headermenu_id = '" . (int)$headermenu_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "headermenu_description WHERE headermenu_id = '" . (int)$headermenu_id . "'");

		$this->cache->delete('headermenu');

		$this->event->trigger('post.admin.headermenu.delete', $headermenu_id);
	}

	public function getHeadermenu($headermenu_id) {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "headermenu where headermenu_id='" . $headermenu_id . "'";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getHeadermenus($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "headermenu i LEFT JOIN " . DB_PREFIX . "headermenu_description id ON (i.headermenu_id = id.headermenu_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
			$sort_data = array(
				'id.name',				
				'i.link'
			);		
		
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY id.name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}		

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	
			
			$query = $this->db->query($sql);
			
			return $query->rows;
		} else {
			$headermenu_data = $this->cache->get('headermenu.' . (int)$this->config->get('config_language_id'));
		
			if (!$headermenu_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "headermenu i LEFT JOIN " . DB_PREFIX . "headermenu_description id ON (i.headermenu_id = id.headermenu_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.name");
	
				$headermenu_data = $query->rows;
			
				$this->cache->set('headermenu.' . (int)$this->config->get('config_language_id'), $headermenu_data);
			}	
	
			return $headermenu_data;			
		}
	}

	public function getHeadermenuDescriptions($headermenu_id) {
		$headermenu_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "headermenu_description WHERE headermenu_id = '" . (int)$headermenu_id . "'");

		foreach ($query->rows as $result) {
			$headermenu_description_data[$result['language_id']] = array(
				'name'             => $result['name']
			);
		}

		return $headermenu_description_data;
	}

	public function getTotalHeadermenus() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "headermenu");

		return $query->row['total'];
	}
	
}

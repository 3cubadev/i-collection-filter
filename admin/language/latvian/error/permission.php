<?php
// Heading
$_['heading_title']   = 'Piekļuve liegta!'; 

// Text
$_['text_permission'] = 'Jums nav atļauts piekļūt šai lapai. Ja Jums atļauja ir nepieciešama, sazinieties ar administratoru.';
<?php
// Heading
$_['heading_title']  = 'Kļūda!';

// Text
$_['text_not_found'] = 'Pieprasītā vietne nav atrasta. Lūdzu, sazinieties ar administratoru, ja problēma atkārtojas.';
<?php
// Text
$_['text_subject']  = '%s - Paroles atiestatīšanas pieprasījums';
$_['text_greeting'] = '%s Vietnes administrēšanai tika pieprasīta jauna parole.';
$_['text_change']   = 'Lai atiestatītu paroli, apmeklējiet šo adresi:';
$_['text_ip']       = 'Pieprasījums tika veikts no šādas IP adreses: %s';
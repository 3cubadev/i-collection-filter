<?php
// Text
$_['text_subject']       = '%s - Atteikums %s';
$_['text_return_id']     = 'Atteikuma Nr.:';
$_['text_date_added']    = 'Atteikuma datums:';
$_['text_return_status'] = 'Jūsu atteikuma pašreizējais stāvoklis:';
$_['text_comment']       = 'Papildinformācija pie Jūsu atteikuma:';
$_['text_footer']        = 'Ja jums ir kādi jautājumi, lūdzu, atbildiet uz šo e-pastu.';
<?php
// Text
$_['text_approve_subject']  = '%s - Jūsu profils ir aktivizēts!';
$_['text_approve_welcome']  = 'Esiet laipni aicināti, un paldies par reģistrēšanos %s vietnē!';
$_['text_approve_login']    = 'Jūsu profils ir izveidots, un tagad Jūs varat autorizēties savā profilā, izmantojot savu e-pasta adresi un paroli mūsu interneta vietnē, vai arī adresē:';
$_['text_approve_services'] = 'Pēc reģistrēšanās  Jūs varēsiet piekļūt pārējiem pakalpojumiem, tostarp: veikto pasūtījumu pārskatam, rēķinu drukāšanai un Jūsu profila rediģēšanai.';
$_['text_approve_thanks']   = 'Paldies,';
$_['text_transaction_subject']  = '%s - Profila kredītpunkti';
$_['text_transaction_received'] = 'Jūs esat saņēmis kredītpunktus %s vērtībā!';
$_['text_transaction_total']    = 'Jūsu kopējā kredītpunktu summa tagad ir %s.' . "\n\n" . 'Jūsu profila kredītpunktu vērtība tiks automātiski atskaitīta no Jūsu nākamā pirkuma summas.';
$_['text_reward_subject']       = '%s - Bonusa punkti';
$_['text_reward_received']      = 'Jūs esat saņēmis %s bonusa punktus!';
$_['text_reward_total']         = 'Jūsu kopējā bonusa punktu summa tagad ir %s.';
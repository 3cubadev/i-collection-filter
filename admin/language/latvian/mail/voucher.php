<?php
// Text
$_['text_subject']  = '%s Jums ir nosūtījis(-usi) dāvanu karti';
$_['text_greeting'] = 'Apsveicam, Jūs esat saņēmis(-usi) dāvanu karti %s vērtībā';
$_['text_from']     = 'Šo dāvanu karti Jums ir nosūtījis(-usi) %s';
$_['text_message']  = 'ar šādu ziņu';
$_['text_redeem']   = 'Lai atprečotu šo dāvanu karti, pierakstiet savu dāvanu kartes kodu, kurš ir <b>%s</b>,  tad noklikšķiniet uz saites apakšā un pērciet preci, kurai Jūs gribat šo dāvanu karti izmantot. Jūs varat ierakstīt dāvanu kartes kodu pirkumu groza lapā, pirms noformējat pirkumu.';
$_['text_footer']   = 'Lūdzu, atbildiet uz šo epastu, ja Jums ir kādi jautājumi.';
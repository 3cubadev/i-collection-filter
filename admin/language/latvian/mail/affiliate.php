<?php
// Text
$_['text_approve_subject']      = '%s - Jūsu partnera profils ir aktivizēts!';
$_['text_approve_welcome']      = 'Esiet laipni aicināti, un paldies par reģistrēšanos %s vietnē!';
$_['text_approve_login']        = 'Jūsu profils tagad ir izveidots, un Jūs varat autorizēties ar savu e-pasta adresi un paroli mūsu vietnē, vai arī šajā adresē:';
$_['text_approve_services']     = 'Pēc autorizēšanās Jums būs iespēja ģenerēt uzskaites kodu, redzēt komisijas maksājumu gaitu un rediģēt savu profila informāciju.';
$_['text_approve_thanks']       = 'Paldies,';
$_['text_transaction_subject']  = '%s - Partnera komisijas maksājums';
$_['text_transaction_received'] = 'Jūs esat saņēmuši %s komisijas maksājumu!';
$_['text_transaction_total']    = 'Jūsu kopējā komisijas maksājumu bilance tagad ir %s.';
<?php
// Heading
$_['heading_title']    = 'Google Base';

// Text   
$_['text_feed']        = 'Vietnes datu padeve';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt Google Base padeves rediģēšanu!';
$_['text_edit']        = 'Rediģēt Google Base';

// Entry
$_['entry_status']     = 'Stāvoklis:';
$_['entry_data_feed']  = 'Datu padeves URL:';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt Google Base datu padevi!';
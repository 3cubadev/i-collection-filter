<?php
// Heading
$_['heading_title']    = 'Google Sitemap';

// Text
$_['text_feed']        = 'Vietnes datu padeve';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt Google Sitemap datu padeves rediģēšanu!';
$_['text_edit']        = 'Rediģēt Google Sitemap';

// Entry
$_['entry_status']     = 'Stāvoklis:';
$_['entry_data_feed']  = 'Datu plūsmas URL:';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt Google Sitemap datu padevi!';
<?php
// Heading
$_['heading_title']     = 'Izkārtojumi';

// Text
$_['text_success']      = 'Jūs veiksmīgi pabeidzāt izkārtojumu rediģēšanu!';
$_['text_list']           = 'Izkārtojumu saraksts';
$_['text_add']            = 'Pievienot izkārtojumu';
$_['text_edit']           = 'Rediģēt izkārtojumu';
$_['text_default']      = 'Galvenā vietne';
$_['text_content_top']    = 'Satura augšā';
$_['text_content_bottom'] = 'Satura apakšā';
$_['text_column_left']    = 'Kreisajā malā';
$_['text_column_right']   = 'Labajā malā';

// Column
$_['column_name']       = 'Izkārtojuma nosaukums';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_name']        = 'Izkārtojuma nosaukums:';
$_['entry_store']       = 'Veikals:';
$_['entry_route']       = 'Ceļš:';
$_['entry_module']        = 'Modulis';
$_['entry_position']      = 'Pozīcija';
$_['entry_sort_order']    = 'Kārtošanas secība';

// Error
$_['error_permission']  = 'Uzmanību! Jums nav atļaujas modificēt izkārtojumus!';
$_['error_name']        = 'Dizaina nosaukumam jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_default']     = 'Uzmanību! Šis izkārtojums nevar tikt dzēsts, jo tas pašreiz tiek izmantots kā vietnes noklusējuma izkārtojums!';
$_['error_store']       = 'Uzmanību! Šis izkārtojums nevar tikt dzēsts, jo tas pašreiz tiek izmantots %s vietnē(s)!';
$_['error_product']      = 'Uzmanību! Šis izkārtojums nevar tikt dzēsts, jo tas pašreiz tiek izmantots %s precē(s)!';
$_['error_category']     = 'Uzmanību! Šis izkārtojums nevar tikt dzēsts, jo tas pašreiz tiek izmantots %s preču grupā(s)!';
$_['error_information']  = 'Uzmanību! Šis izkārtojums nevar tikt dzēsts, jo tas pašreiz tiek izmantots %s informācijas lapā(s)!';
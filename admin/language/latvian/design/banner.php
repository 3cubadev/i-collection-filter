<?php
// Heading
$_['heading_title']      = 'Baneri';

// Text
$_['text_success']       = 'Jūs sekmīgi pabeidzāt baneru rediģēšanu!';
$_['text_list']          = 'Baneru saraksts';
$_['text_add']           = 'Pievienot baneri';
$_['text_edit']          = 'Rediģēt baneri';
$_['text_default']       = 'Galvenā vietne';

// Column
$_['column_name']        = 'Banera nosaukumus';
$_['column_status']      = 'Stāvoklis';
$_['column_action']      = 'Darbība';

// Entry
$_['entry_name']         = 'Banera nosaukums';
$_['entry_title']        = 'Virsraksts';
$_['entry_link']         = 'Saite';
$_['entry_image']        = 'Attēls';
$_['entry_status']       = 'Stāvoklis';
$_['entry_sort_order']   = 'Kārtošanas secība';

// Error
$_['error_permission']   = 'Uzmanību! Jums nav atļauts rediģēt banerus!';
$_['error_name']         = 'Banera nosaukumam ir jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_title']        = 'Banera virsrakstam ir jābūt no 2 līdz 64 rakstzīmēm!';
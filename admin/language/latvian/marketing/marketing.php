<?php
// Heading
$_['heading_title']     = 'Marketinga Atsekošana';

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt marketinga atsekošanu!';
$_['text_list']         = 'Marketina atsekošanas saraksts';
$_['text_add']          = 'Pievienot marketinga atsekošanu';
$_['text_edit']         = 'Rediģēt marketinga atsekošanu';

// Column
$_['column_name']       = 'Kampaņas nosaukums';
$_['column_code']       = 'Kods';
$_['column_clicks']     = 'Klikšķi';
$_['column_orders']     = 'Pasūtījumi';
$_['column_date_added'] = 'Pievienošanas datums';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_name']        = 'Kampaņas nosaukums';
$_['entry_description'] = 'Kampaņas apraksts';
$_['entry_code']        = 'Atsekošanas kods';
$_['entry_example']     = 'Piemēri';
$_['entry_date_added']  = 'Pievienošanas datums';

// Help
$_['help_code']         = 'Atsekošanas kods, kurš tiks izmanotots marketinga atsekošanas kampaņā.';
$_['help_example']      = 'Jums jāpievieno atsekošanas kods URL saites beigās.';

// Error
$_['error_permission']  = 'Jums nav atļauts rediģēt marketinga atsekošanu!';
$_['error_name']        = 'Kampaņas nosaukumā jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_code']        = 'Atsekošanas kods ir obligāts!';
<?php
// Heading
$_['heading_title']       = 'Kuponi';

// Text
$_['text_success']        = 'Jūs sekmīgi pabeidzāt rediģēt kuponus!';
$_['text_list']           = 'Kuponu saraksts';
$_['text_add']            = 'Pievienot kuponu';
$_['text_edit']           = 'Rediģēt kuponu';
$_['text_percent']        = 'Procents';
$_['text_amount']         = 'Fiksēts daudzums';

// Column
$_['column_name']         = 'Kupona nosaukums';
$_['column_code']         = 'Kods';
$_['column_discount']     = 'Atlaida';
$_['column_date_start']   = 'Sākuma datums';
$_['column_date_end']     = 'Beigu datums';
$_['column_status']       = 'Stāvoklis';
$_['column_order_id']     = 'Pasūtījuma Nr.';
$_['column_customer']     = 'Klients';
$_['column_amount']       = 'Summa';
$_['column_date_added']   = 'Pievienošanas datums';
$_['column_action']       = 'Darbība';

// Entry
$_['entry_name']          = 'Kupona nosaukums';
$_['entry_code']          = 'Kods';
$_['entry_type']          = 'Veids';
$_['entry_discount']      = 'Atlaide';
$_['entry_logged']        = 'Pircēju autorizēšanās';
$_['entry_shipping']      = 'Bezmaksas piegāde';
$_['entry_total']         = 'Kopējā summa';
$_['entry_category']      = 'Preču grupa';
$_['entry_product']       = 'Preces';
$_['entry_date_start']    = 'Sākuma datums';
$_['entry_date_end']      = 'Beigu datums';
$_['entry_uses_total']    = 'Izmantošanas reizes uz kuponu';
$_['entry_uses_customer'] = 'Izmantošanas reizes uz pircēju';
$_['entry_status']        = 'Stāvoklis';

// Help
$_['help_code']           = 'Kods, kas jāievada pircējam, lai saņemtu atlaidi.';
$_['help_type']           = 'Fiksēta summa vai procenti no pirkuma.';
$_['help_logged']         = 'Pircējam jābūt autorizētam, lai izmantotu kuponu.';
$_['help_total']          = 'Kopīgā summa, kura ir jāsasniedz, lai kupons būtu derīgs.';
$_['help_category']       = 'Izvēlēties visas preces noteiktajā preču grupā.';
$_['help_product']        = 'Izvēlēties atsevišķas preces uz kurām attieksies kupons. Neizvēlieties nevienu preci, lai kupons attiektos uz visu grozu.';
$_['help_uses_total']     = 'Maksimālais kupona izmantošanas reižu skaits, kuru var izmantot visi pircēji kopā. Atstājiet tukšu, lai izmanotšanas reižu skaits būtu neierobežots';
$_['help_uses_customer']  = 'Maksimālais kupona izmantošanas reižu skaits vienam pircējam. Atstājiet tukšu, lai izmanotšanas reižu skaits būtu neierobežots';

// Error
$_['error_permission']    = 'Jums nav atļauts rediģēt kuponus!';
$_['error_exists']        = 'Uzmanību! Šāds kupona kods jau tiek izmantots!';
$_['error_name']          = 'Kupona nosaukumam jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_code']          = 'Kodam jābūt no 3 līdz 10 rakstzīmēm!';
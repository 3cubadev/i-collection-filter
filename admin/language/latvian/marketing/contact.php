<?php
// Heading
$_['heading_title']        = 'Epasts';

// Text
$_['text_success']         = 'Jūsu ziņojums ir sekmīgi nosūtīts!';
$_['text_sent']            = 'Jūsu ziņojums ir sekmīgi nosūtīts %s no %s adresātiem!';
$_['text_list']            = 'E-pasta saraksts';
$_['text_default']         = 'Galvenais';
$_['text_newsletter']      = 'Visi ziņu abonētāji';
$_['text_customer_all']    = 'Visi pircēji';
$_['text_customer_group']  = 'Pircēju grupa';
$_['text_customer']        = 'Atsevišķi pircēji';
$_['text_affiliate_all']   = 'Visi partneri';
$_['text_affiliate']       = 'Atsevišķi partneri';
$_['text_product']         = 'Preces';

// Entry
$_['entry_store']          = 'No';
$_['entry_to']             = 'Kam';
$_['entry_customer_group'] = 'Pircēju grupa';
$_['entry_customer']       = 'Pircējs';
$_['entry_affiliate']      = 'Partneris';
$_['entry_product']        = 'Preces';
$_['entry_subject']        = 'Temats';
$_['entry_message']        = 'Ziņa';

// Help
$_['help_customer']       = 'Automātiska ievades pabeigšana';
$_['help_affiliate']      = 'Automātiska ievades pabeigšana';
$_['help_product']        = 'Sūtīt tika klientiem, kas ir pasūtījuši preces no šī saraksta. (Automātiska ievades pabeigšana)';

// Error
$_['error_permission']     = 'Jums nav atļauts sūtīt e-pastus!';
$_['error_subject']        = 'E-pasta temats ir obligāts!';
$_['error_message']        = 'E-pasta ziņa ir obligāta!';
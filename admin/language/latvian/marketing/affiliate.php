<?php
// Heading
$_['heading_title']             = 'Partneri';

// Text
$_['text_success']              = 'Jūs sekmīgi pabeidzāt rediģēt partnerus!';
$_['text_approved']             = 'Jūs apstiprinājāt %s profilus!';
$_['text_list']                 = 'Partneru saraksts';
$_['text_add']                  = 'Pievienot partneri';
$_['text_edit']                 = 'Rediģēt partneri';
$_['text_balance']              = 'Bilance';
$_['text_cheque']               = 'Čeks';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bankas pārskaitījums';

// Column
$_['column_name']               = 'Partnera nosaukums';
$_['column_email']              = 'E-pasts';
$_['column_code']               = 'Partnera kods';
$_['column_balance']            = 'Bilance';
$_['column_status']             = 'Stāvoklis';
$_['column_approved']           = 'Apstiprināts';
$_['column_date_added']         = 'Pievienošanas datums';
$_['column_description']        = 'Apraksts';
$_['column_amount']             = 'Summa';
$_['column_action']             = 'Darbība';

// Entry
$_['entry_firstname']           = 'Vāŗds';
$_['entry_lastname']            = 'Uzvārds';
$_['entry_email']               = 'E-pasts';
$_['entry_telephone']           = 'Tālrunis';
$_['entry_fax']                 = 'Fakss';
$_['entry_status']              = 'Stāvoklis';
$_['entry_password']            = 'Parole';
$_['entry_confirm']             = 'Atkārot';
$_['entry_company']             = 'Uzņēmums';
$_['entry_website']             = 'Web vietne';
$_['entry_address_1']           = 'Addrese 1';
$_['entry_address_2']           = 'Addrese 2';
$_['entry_city']                = 'Pilsēta';
$_['entry_postcode']            = 'Pasta indekss';
$_['entry_country']             = 'Valsts';
$_['entry_zone']                = 'Reģions';
$_['entry_code']                = 'Partnera kods';
$_['entry_commission']          = 'Komisija (%)';
$_['entry_tax']                 = 'Reģistrācijas numurs.';
$_['entry_payment']             = 'Apmaksas veids';
$_['entry_cheque']              = 'Čeka saņēmēja nosaukums';
$_['entry_paypal']              = 'PayPal konta E-pasts';
$_['entry_bank_name']           = 'Bankas Nosaukums';
$_['entry_bank_branch_number']  = 'ABA/BSB numurs (Filiāles Nr.)';
$_['entry_bank_swift_code']     = 'SWIFT kods';
$_['entry_bank_account_name']   = 'Konta nosaukums';
$_['entry_bank_account_number'] = 'Konta numurs';
$_['entry_amount']              = 'Summa';
$_['entry_description']         = 'Apraksts';
$_['entry_name']                = 'Partnera nosaukums';
$_['entry_approved']            = 'Apstiprināts';
$_['entry_date_added']          = 'Pievienošanas datums';

// Help
$_['help_code']                 = 'Uzskaties kods, kas tiks lietots lai atsekotu nosūtīšanu.';
$_['help_commission']           = 'Procents, ko partneris saņem par katru pasūtījumu.';

// Error
$_['error_permission']          = 'Jums nav atļauts rediģēt partnerus!';
$_['error_exists']              = 'Šāda e-pasta adrese ir jau reģistrēta!';
$_['error_firstname']           = 'Vārdam jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']            = 'Uzvārdam jābūt no 1 līdz 32 rakstzīmēm';
$_['error_email']               = 'Izskatās, ka epasta adrese nav derīga!';
$_['error_cheque']              = 'Čeka saņēmēja nosaukums ir obligāts!';
$_['error_paypal']              = 'Izskatās, ka PayPal konta e-pasta adrese nav derīga!!';
$_['error_bank_account_name']   = 'Konta nosaukums ir obligāts!';
$_['error_bank_account_number'] = 'Konta numurs ir obligāts!';
$_['error_telephone']           = 'Tālruņa numuram jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_password']            = 'Parolei jābūt no 4 līdz 20 rakstzīmēm!';
$_['error_confirm']             = 'Parole un paroles atkārtojums nesakrīt!';
$_['error_address_1']           = 'Adresei 1 ir jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_city']                = 'Pilsētai ir jābūt no 2 līdz 128 rakszīmēm!';
$_['error_postcode']            = 'Šajā valstī pasta indeksam jābūt no 2 līdz 10 rakstzīmēm!';
$_['error_country']             = 'Lūdzu norādiet valsti!';
$_['error_zone']                = 'Lūdzu norādiet reģionu!';
$_['error_code']                = 'Partnera kods ir obligāts!';
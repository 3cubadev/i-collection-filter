<?php
// Heading
$_['heading_title']    = 'Lietotāju grupa';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt lietotāju grupas!';
$_['text_list']         = 'Lietotāju grupa';
$_['text_add']          = 'Pievienot lietotāju grupu';
$_['text_edit']         = 'Rediģēt lietotāju grupu';

// Column
$_['column_name']      = 'Lietotāju grupas nosaukums';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_name']       = 'Lietotāju grupas nosaukums';
$_['entry_access']     = 'Piekļuves atļauja';
$_['entry_modify']     = 'Rediģēšanas atļauja';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt lietotāju grupas!';
$_['error_name']       = 'Lietotāju grupas nosaukumam ir jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_user']       = 'Šī lietotāju grupa nevar tikt dzēsta, jo patlaban tā tiek izmantota %s lietotājam(-iem)!';
<?php
// Heading
$_['heading_title']     = 'Lietotāji';

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt lietotājus!';
$_['text_list']         = 'Lietotāju saraksts';
$_['text_add']          = 'Pievienot lietotāju';
$_['text_edit']         = 'Rediģēt lietotāju';

// Column
$_['column_username']   = 'Lietotājvārds';
$_['column_status']     = 'Stāvoklis';
$_['column_date_added'] = 'Pievienots';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_username']   = 'Lietotājvārds';
$_['entry_user_group'] = 'Lietotāju grupa';
$_['entry_password']   = 'Parole';
$_['entry_confirm']    = 'Apstipriniet paroli';
$_['entry_firstname']  = 'Vārds';
$_['entry_lastname']   = 'Uzvārds';
$_['entry_email']      = 'E-pasts';
$_['entry_image']      	= 'Image';
$_['entry_status']     = 'Stāvoklis';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt lietotājus!';
$_['error_account']    = 'Jūs nevarat izdzēst savu lietotājvārdu!';
$_['error_exists']     = 'Warning: Username is already in use!';
$_['error_username']   = 'Jūsu lietotājvārdam ir jābūt no 3 līdz 20 rakstzīmēm!';
$_['error_password']   = 'Jūsu parolei ir jābūt no 4 līdz 20 rakstzīmēm!';
$_['error_confirm']    = 'Jūsu paroles nesakrīt!';
$_['error_firstname']  = 'Jūsu vārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']   = 'Jūsu uzvārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
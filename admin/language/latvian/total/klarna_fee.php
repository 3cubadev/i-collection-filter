<?php
// Heading
$_['heading_title']    = 'Klarna maksa';

// Text
$_['text_total']       = 'Pasūtījuma summas';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt Klarna maksa summas rediģēšanu!';
$_['text_edit']        = 'Klarna maksa summas rediģēšana';
$_['text_sweden']      = 'Zviedrija';
$_['text_norway']      = 'Norvēģija';
$_['text_finland']     = 'Somija';
$_['text_denmark']     = 'Dānija';
$_['text_germany']     = 'Vācija';
$_['text_netherlands'] = 'Nīderlande';

// Entry
$_['entry_total']      = 'Pasūtījuma kopsumma';
$_['entry_fee']        = 'Rēķina sagatvošanas maksa';
$_['entry_tax_class']  = 'Nodokļi';
$_['entry_status']     = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt šo pasūtījuma summas paplašinājumu!';
<?php
// Heading
$_['heading_title']    = 'Maza pasūtījuma maksa';

// Text
$_['text_total']       = 'Pasūtījuma summas';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt pasūtījuma summas rediģēšanu!';
$_['text_edit']        = 'Maza pasūtījuma maksas rediģēšana';

// Entry
$_['entry_total']      = 'Pasūtījumu kopsumma';
$_['entry_fee']        = 'Izdevumi';
$_['entry_tax_class']  = 'Nodokļi';
$_['entry_status']     = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

// Help
$_['help_total']       = 'Pirkuma noformēšanas summa, kura pasūtījumam ir jāsasniedz, lai šī pasūtījuma summa kļūtu neaktīva.';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt šo pasūtījuma summas paplašinājumu!';
﻿<?php
// Heading
$_['heading_title']    = 'Kuponi';

// Text
$_['text_total']       = 'Pasūtījuma summas';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt kuponu summu!';
$_['text_edit']        = 'Kuponu summas rediģēšana';

// Entry
$_['entry_status']     = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt kuponu summu!';
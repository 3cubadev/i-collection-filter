<?php
// Heading
$_['heading_title']    = 'Starpsumma';

// Text
$_['text_total']       = 'Pasūtījuma summa';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt pasūtījuma summas rediģēšanu!';
$_['text_edit']        = 'Starpsummas summas rediģēšana';

// Entry
$_['entry_status']     = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt šo pasūtījuma summas paplašinājumu!';
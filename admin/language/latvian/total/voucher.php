<?php
// Heading
$_['heading_title']    = 'Dāvanu karte';

// Text
$_['text_total']       = 'Pasūtījuma summas';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt Dāvanu kartes summas rediģēšanu!';
$_['text_edit']        = 'Dāvanu kartes summas rediģēšana';

// Entry
$_['entry_status']     = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt Dāvanu kartes summas paplašinājumu!';
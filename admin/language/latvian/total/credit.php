<?php
// Heading
$_['heading_title']    = 'Vietnes kredītpunkti';

// Text
$_['text_total']       = 'Pasūtījuma summas';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt vietnes kredītpunktu summas rediģēšanu!';
$_['text_edit']        = 'Vietnes kredītpunktu summas rediģēšana';

// Entry
$_['entry_status']     = 'Stāvoklis:';
$_['entry_sort_order'] = 'Kārtošanas secība:';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt vietnes kredītpunktu summu!';
<?php
// Heading
$_['heading_title']    = 'Papildu maksa par apstrādi';

// Text
$_['text_total']       = 'Pasūtījuma summas';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt "Papildu maksa par apstrādi" summas rediģēšanu!';
$_['text_edit']        = 'Papildu maksa par apstrādi summas rediģēšana';

// Entry
$_['entry_total']      = 'Pasūtījuma kopsumma';
$_['entry_fee']        = 'Maksa';
$_['entry_tax_class']        = 'Nodokļi';
$_['entry_status']     = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

// Help
$_['help_total']       = 'Pirkuma noformēšanas summa, kura pasūtījumam ir jāsasniedz, lai šī pasūtījuma summa kļūtu aktīva.';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt "Papildu maksa par apstrādi" summu!';
<?php
// Heading
$_['heading_title']     = 'Piegāde';

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt piegādes!';
$_['text_list']         = 'Piegāžu saraksts';

// Column
$_['column_name']       = 'Nosaukums';
$_['column_status']     = 'Stāvoklis';
$_['column_sort_order'] = 'Kārtošanas secība';
$_['column_action']     = 'Darbība';

// Error
$_['error_permission']  = 'Jums nav atļauts rediģēt piegādes!';
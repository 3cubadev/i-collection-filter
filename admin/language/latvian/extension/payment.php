<?php
// Heading
$_['heading_title']     = 'Maksājumu veidi';

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt maksājumu veidus';
$_['text_list']         = 'Maksājumu veidu saraksts';

// Column
$_['column_name']       = 'Nosaukums';
$_['column_status']     = 'Stāvoklis';
$_['column_sort_order'] = 'Kārtošanas secība';
$_['column_action']     = 'Darbība';

// Error
$_['error_permission']  = 'Jums nav atļauts rediģēt maksājumu veidus!';
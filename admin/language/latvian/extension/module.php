<?php
// Heading
$_['heading_title']     = 'Moduļi';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt moduļus!';
$_['text_layout']      = 'Pēc tam, kad jūs esat instalējuši vai konfigurējuši moduli, jūs varat pievienot to izkārtojumam <a href="%s" class="alert-link">šeit</a>!';
$_['text_add']         = 'Pievienot moduli';
$_['text_list']        = 'Moduļu saraksts';

// Column
$_['column_name']       = 'Nosaukums';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_code']       = 'Modulis';
$_['entry_name']       = 'Moduļa nosaukums';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt moduļus!';
$_['error_name']       = 'Moduļa nosaukumam jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_code']       = 'Ir jānorāda paplašinājums!';
<?php
// Heading
$_['heading_title']     = 'Pasūtījuma starsummas';

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt starpsummas!';
$_['text_list']         = 'Pasūtījumu starpsummu saraksts';

// Column
$_['column_name']       = 'Nosaukums';
$_['column_status']     = 'Stāvoklis';
$_['column_sort_order'] = 'Kārtošanas secība';
$_['column_action']     = 'Darbība';

// Error
$_['error_permission']  = 'Jums nav atļauts rediģēt pasūtījuma starpsummas!';
<?php
// Heading
$_['heading_title']    = 'Pret-krāpšanas pasākumi';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt pret-krāpšanas pasākumus!';
$_['text_list']        = 'Pret-krāpšanas saraksts';

// Column
$_['column_name']      = 'Nosaukums';
$_['column_status']    = 'Stāvoklis';
$_['column_action']    = 'Darbība';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav tiesību rediģēt pret-krāpšanas pasākumus!';
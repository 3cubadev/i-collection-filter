<?php
// Heading
$_['heading_title']     = 'Modifikācijas';

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt modifikācijas!';
$_['text_refresh']      = 'Tiklīdz jūs ieslēdzat / izslēdzat vai izdzēšata kādu modifikāciju, jums ir jāklikšķina uz atjaunošanas pogas, lai izveidotu no jauna jūsu modifikāciju kešatmiņu!';
$_['text_list']         = 'Modifikāciju saraksts';

// Column
$_['column_name']       = 'Modifikācijas nosaukums';
$_['column_author']     = 'Autors';
$_['column_version']    = 'Versija';
$_['column_status']     = 'Stāvoklis';
$_['column_date_added'] = 'Pievienošanas datums';
$_['column_action']     = 'Darbība';

// Error
$_['error_permission']  = 'Uzmanību! Jums nav atļauts rediģēt modifikācijas!';
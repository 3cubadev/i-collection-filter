<?php
// Heading
$_['heading_title']        = 'Paplašinājumu instalētājs';

// Text
$_['text_success']         = 'Jūs sekmīgi instalējāt paplašinājumu!';
$_['text_unzip']           = 'Ekstraktēju failus!';
$_['text_ftp']             = 'Kopēju failus!';
$_['text_sql']             = 'Izpildu SQL!';
$_['text_xml']             = 'Piemēŗoju modifikācijas!';
$_['text_php']             = 'Izpildu PHP!';
$_['text_remove']          = 'Dzēšu pagaidu failus!';
$_['text_clear']           = 'Jūs sekmīgi izdzēsāt visus pagaidu failus!';

// Entry
$_['entry_upload']         = 'Augšupielādēt failu';
$_['entry_overwrite']      = 'Faili, kuri tiks pārrakstīti';
$_['entry_progress']       = 'Progress';

// Help
$_['help_upload']          = 'Modifikācijai jābūt ar paplašinājumu ".ocmod.zip" vai ".ocmod.xml".';

// Error
$_['error_permission']     = 'Uzmanību! Jums nav tiesību modificēt paplašinājumus!';
$_['error_temporary']      = 'Uzmanību. Ir atrasti pagaidu faili, kuri ir jāizdzēš. Klikšķiniet uz dzēšanas pogas, lai tos izdzēstu!';
$_['error_upload']         = 'Neizdevās augšupielādēt failu!';
$_['error_filetype']       = 'Nederīgs faila veids!';
$_['error_file']           = 'Fails nav atrasts!';
$_['error_unzip']          = 'Neizdevās atvērt Zip failu!';
$_['error_code']           = 'Modifikācijai ir jābūt ar unikālu ID kodu!';
$_['error_exists']         = 'Modifikācija %s izmanot to pašu ID kodu, kuru jūs mēģinat augšupielādēt!';
$_['error_directory']      = 'Mape ar failiem augšupielādei nav atrasta!';
$_['error_ftp_status']     = 'Ir nepieciešams aktivizēt FTP sistēmas iestatījumos!';
$_['error_ftp_connection'] = 'Neizdevās pieslēgties kā %s:%s';
$_['error_ftp_login']      = 'Neizdevās autorizēties kā %s';
$_['error_ftp_root']       = 'Neizdevās iestatīt pamata mapi kā %s';
$_['error_ftp_directory']  = 'Neizdevās atvērt mapi %s';
$_['error_ftp_file']       = 'Neizdevās augšupielādēt failu %s';

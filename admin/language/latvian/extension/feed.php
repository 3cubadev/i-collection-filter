<?php
// Heading
$_['heading_title']  = 'Vietnes datu padeve';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt datu padeves!';
$_['text_list']        = 'Datu padeves moduļu saraksts';

// Column
$_['column_name']    = 'Nosaukums';
$_['column_status']  = 'Stāvoklis';
$_['column_action']  = 'Darbība';

// Error
$_['error_permission']  = 'Jums nav atļauts rediģēt vietnes datu padeves!';
<?php
// Heading
$_['heading_title']       = 'Partneri';

$_['text_module']         = 'Moduļi';
$_['text_success']        = 'Jūs sekmīgi pabeidzāt rediģēt partneru moduli!';
$_['text_edit']        = 'Partneru moduļa rediģēšana';

// Entry
$_['entry_status']        = 'Stāvoklis:';

// Error
$_['error_permission']    = 'Uzmanību! Jums nav atļauts rediģēt partneru moduli!';
<?php
// Heading
$_['heading_title']        = 'Autorizēšanās ar PayPal';

//Text
$_['text_module']          = 'Moduļi';
$_['text_success']         = 'Jūs sekmīgi pabeidzāt rediģēt PayPal autorizēšanās moduli!';
$_['text_edit']            = 'PayPal autorizēšanās moduļa rediģēšana';
$_['text_button_grey']     = 'Pelēks';
$_['text_button_blue']     = 'Zils (ieteiktais)';

//Entry
$_['entry_client_id']      = 'Klienta ID';
$_['entry_secret']         = 'Secret';
$_['entry_sandbox']        = 'Sandbox Mode';
$_['entry_debug']          = 'Debug Logging';
$_['entry_customer_group'] = 'Pircēju grupa';
$_['entry_button']         = 'Pogas krāsa';
$_['entry_seamless']       = 'Atļaut "Seamless Checkout"';
$_['entry_locale']         = 'Locale';
$_['entry_return_url']     = 'Return URL';
$_['entry_status']         = 'Stāvoklis';

//Help
$_['help_sandbox']         = 'Use sandbox (testing) environment?';
$_['help_customer_group']  = 'Kādā grupā tiks iekļauti tikko reģistrējušies pircēji?';
$_['help_debug_logging']   = 'Enabling this will allow data to be added to your error log to help debug any problems.';
$_['help_seamless']        = 'Allows auto-login when customers choose PayPal Express Checkout. To use this, the option must be enabled in your Log in With PayPal account. You must also use the same account as the one used in Express Checkout.';
$_['help_locale']          = 'This is the PayPal locale setting for your store languages';
$_['help_return_url']      = 'This needs to be added in the PayPal app configuration under app redirect URLs.';

//Error
$_['error_permission']     = 'Jums nav atļauts rediģēt PayPal autorizēšanās moduli!';
$_['error_client_id']      = 'Klienta ID ir obligāts!';
$_['error_secret']         = 'Secret ir obligāts!';
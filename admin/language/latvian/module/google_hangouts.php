<?php
// Heading
$_['heading_title']    = 'Google Hangouts';

// Text
$_['text_module']      = 'Moduļi';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt Google Hangouts moduli!';
$_['text_edit']        = 'Google Hangouts Moduļa rediģēšana';

// Entry
$_['entry_code']       = 'Google Talk kods';
$_['entry_status']     = 'Stāvoklis';

// Help
$_['help_code']        = 'Goto <a href="https://developers.google.com/+/hangouts/button" target="_blank">Create a Google Hangout chatback badge</a> and copy &amp; paste the generated code into the text box.';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt Google Hangouts moduli!';
$_['error_code']       = 'Kods ir obligāts';
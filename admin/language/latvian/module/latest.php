<?php
// Heading
$_['heading_title']       = 'Jaunākās preces';

// Text
$_['text_module']         = 'Moduļi';
$_['text_success']        = 'Jūs sekmīgi pabeidzāt rediģēt jaunāko preču moduli!';
$_['text_edit']        = 'Jaunāko preču moduļa rediģēšana';

// Entry
$_['entry_name']       = 'Moduļa nosaukums';
$_['entry_limit']      = 'Limits';
$_['entry_width']      = 'Platums';
$_['entry_height']     = 'Augstums';
$_['entry_status']     = 'Stāvoklis';

// Error
$_['error_permission']    = 'Uzmanību! Jums nav atļauts rediģēt jaunāko preču moduli!';
$_['error_name']       = 'Moduļa nosaukumā jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_width']      = 'Platums ir obligāts!';
$_['error_height']     = 'Augstums ir obligāts!';
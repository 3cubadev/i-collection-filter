<?php
// Heading
$_['heading_title']     = 'HTML Bloks';

// Text
$_['text_module']       = 'Moduļi';
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt HTML bloka moduli!';
$_['text_edit']         = 'HTML bloka moduļa rediģēšana';

// Entry
$_['entry_name']        = 'Moduļa nosaukums';
$_['entry_title']       = 'Virsraksts';
$_['entry_description'] = 'Saturs';
$_['entry_status']      = 'Stāvoklis';

// Error
$_['error_permission']  = 'Jums nav atļauts rediģēt HTML bloka moduli!';
$_['error_name']        = 'Moduļa nosaukumuā jābūt no 3 līdz 64 rakstzīmēm!';
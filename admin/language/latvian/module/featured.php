<?php
// Heading
$_['heading_title']    = 'Izlase';

// Text
$_['text_module']      = 'Moduļi';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt izlases moduli!';
$_['text_edit']        = 'Izlases moduļa rediģēšana';

// Entry
$_['entry_name']       = 'Moduļa nosaukums';
$_['entry_product']    = 'Preces';
$_['entry_limit']      = 'Limits';
$_['entry_width']      = 'Platums';
$_['entry_height']     = 'Augstums';
$_['entry_status']     = 'Stāvoklis';

// Help
$_['help_product']     = '(Automātiska ievades pabeigšana)';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt izlases moduli!';
$_['error_name']       = 'Moduļa nosaukumā ir jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_width']      = 'Platums ir obligāts!';
$_['error_height']     = 'Augstums ir obligāts!';
<?php
// Heading
$_['heading_title']    = 'Slīdrāde';

// Text
$_['text_module']      = 'Moduļi';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt slīdrādes moduli!';
$_['text_edit']        = 'Edit Slideshow Module';

// Entry
$_['entry_name']       = 'Nosaukums';
$_['entry_banner']     = 'Baneris';
$_['entry_width']      = 'Platums';
$_['entry_height']     = 'Augstums';
$_['entry_status']     = 'Stāvoklis';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt slīdrādes moduli!';
$_['error_name']       = 'Moduļa nosaukumā jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_width']      = 'Platums ir obligāts!';
$_['error_height']     = 'Augstums ir obligāts!';
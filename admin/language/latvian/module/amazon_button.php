<?php
// Heading
$_['heading_title']    = 'Pirkuma noformēšana ar Amazon pogu';

$_['text_module']      = 'Moduļi';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt Amazon maksājumu moduli!';
$_['text_edit']        = 'Amazon maksājumu moduļa rediģēšana';
$_['text_left']        = 'Pa kreisi';
$_['text_right']       = 'Pa labi';
$_['text_center']      = 'Centrēts';

// Entry
$_['entry_name']       = 'Moduļa nosaukums';
$_['entry_align']      = 'Izlīdzināt';
$_['entry_status']     = 'Stāvoklis';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt Amazon maksājumu moduli!';
$_['error_name']       = 'Moduļa nosaukumā jābūt no 3 līdz 64 rakstzīmēm!';
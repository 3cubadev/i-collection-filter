<?php
// Heading
$_['heading_title']     = 'eBay izlase';

// Text
$_['text_module']       = 'Moduļi';
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt eBay izlases modui!';
$_['text_edit']        	= 'eBay moduļa rediģēšana';
$_['text_list']         = 'Izkārtojumu saraksts';
$_['text_register']     = 'Jums ir jāreģistrē un jāieslēdz OpenBay Pro priekš eBay!';
$_['text_about'] 		= 'Ebay izlases modulis atļauj jums attēlot preces no eBay konta tieši jūsu internetveikalā.';
$_['text_latest']       = 'Jaunākās preces';
$_['text_random']       = 'Gadījuma raksturs parādīšanai';

// Entry
$_['entry_name']        = 'Moduļa nosaukums';
$_['entry_username']    = 'eBay lietotājvārds';
$_['entry_keywords']    = 'Meklēšanas atslēgvārdi';
$_['entry_description'] = 'Iekļaut meklējumā aprakstu';
$_['entry_limit']       = 'Limits';
$_['entry_length']      = 'Garums';
$_['entry_width']       = 'Plautms';
$_['entry_height']      = 'Augstums';
$_['entry_site']   		= 'eBay Vietne';
$_['entry_sort']   		= 'Kārtot pēc';
$_['entry_status']   	= 'Stāvoklis';

// Error 
$_['error_permission'] = 'Uzmanību: Jums nav atļauts rediģēt eBay izlases moduli!';
$_['error_name']        = 'Moduļa nosaukumā jābūt no 3 līdz 64 rakszīmēm!';
$_['error_width']      = 'Platums ir obligāts!';
$_['error_height']     = 'Augstums ir obligāts!';
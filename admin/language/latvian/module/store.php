<?php
// Heading
$_['heading_title']       = 'Vietnes izvēle';

// Text
$_['text_module']         = 'Moduļi';
$_['text_success']        = 'Jūs sekmīgi pabeidzāt rediģēt vietnes izvēles moduli!';
$_['text_edit']        = 'Vietnes izvēles moduļa rediģēšana';

// Entry
$_['entry_admin']         = 'Tikai administratoriem';
$_['entry_status']        = 'Stāvoklis';

// Error
$_['error_permission']    = 'Uzmanību! Jums nav atļauts rediģēt vietnes izvēles moduli!';
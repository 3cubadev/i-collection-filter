<?php
// Heading
$_['heading_title']    = 'Informācija';

// Text
$_['text_module']         = 'Moduļi';
$_['text_success']        = 'Jūs sekmīgi pabeidzāt rediģēt informācijas moduli!';
$_['text_edit']        = 'Informācijas moduļa rediģēšana';

/// Entry
$_['entry_status']        = 'Stāvoklis';

// Error
$_['error_permission']    = 'Uzmanību! Jums nav atļauts rediģēt informācijas moduli!';
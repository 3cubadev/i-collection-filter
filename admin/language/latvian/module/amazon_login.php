<?php
// Heading
$_['heading_title'] = 'Autorizēšanās ar Amazon';

//Text
$_['text_module'] = 'Moduļi';
$_['text_success'] = 'Jūs sekmīgi pabeidzāt rediģēt Amazon autorizēšanās moduli!';
$_['text_content_top'] = 'Satura augšā';
$_['text_content_bottom'] = 'Satura apakšā';
$_['text_column_left'] = 'Kreisajā pusē';
$_['text_column_right'] = 'Labajā pusē';
$_['text_lwa_button'] = 'Autorizēties ar Amazon';
$_['text_login_button'] = 'Autorizēties';
$_['text_a_button'] = 'A';
$_['text_gold_button'] = 'Zelta';
$_['text_darkgray_button'] = 'Tumši pelēks';
$_['text_lightgray_button'] = 'Gaiši pelēks';
$_['text_small_button'] = 'Mazs';
$_['text_medium_button'] = 'Vidējs';
$_['text_large_button'] = 'Liels';
$_['text_x_large_button'] = 'Ļoti liels';

//Entry
$_['entry_button_type'] = 'Pogas veids';
$_['entry_button_colour'] = 'Pogas krāsa';
$_['entry_button_size'] = 'Pogas izmērs';
$_['entry_layout'] = 'Izkārtojums';
$_['entry_position'] = 'Pozīcija';
$_['entry_status'] = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

//Error
$_['error_permission'] = 'Jums nav atļauts rediģēt Amazon autorizēšanās moduli!';
<?php
// Heading
$_['heading_title']    = 'Preču grupas'; 

// Text
$_['text_module']      = 'Moduļi';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt preču grupu moduli!';
$_['text_edit']        = 'Preču grupu moduļa rediģēšana';

// Entry
$_['entry_status']        = 'Stāvoklis:';

// Error
$_['error_permission']    = 'Uzmanību! Jums nav atļauts rediģēt preču grupu moduli!';
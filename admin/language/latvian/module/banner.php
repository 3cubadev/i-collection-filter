<?php
// Heading
$_['heading_title']       = 'Baneris';

// Text
$_['text_module']         = 'Moduļi';
$_['text_success']        = 'Jūs sekmīgi pabeidzāt rediģēt banera moduli!';
$_['text_edit']        = 'Banera moduļa rediģēšana';

// Entry
$_['entry_name']       = 'Moduļa nosaukums';
$_['entry_banner']        = 'Baneris';
$_['entry_dimension']     = 'Izmēri (platums x augstums) un izmēra maiņas veids';
$_['entry_width']      = 'Platums';
$_['entry_height']     = 'Augstums';
$_['entry_status']        = 'Stāvoklis';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt banera moduli!';
$_['error_name']       = 'Moduļa nosaukumā jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_width']      = 'Platums ir obligāts!';
$_['error_height']     = 'Augstums ir obligāts!';
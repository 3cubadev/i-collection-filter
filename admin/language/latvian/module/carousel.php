<?php
// Heading
$_['heading_title']       = 'Karuselis';

// Text
$_['text_module']         = 'Moduļi';
$_['text_success']        = 'Jūs sekmīgi pabeidzāt rediģēt karuseļa modeli!';
$_['text_edit']        = 'Karuseļa moduļa rediģēšana';

// Entry
$_['entry_name']       = 'Moduļa nosaukums';
$_['entry_banner']     = 'Karuselis';
$_['entry_width']      = 'Platums';
$_['entry_height']     = 'Augstums';
$_['entry_status']     = 'Stāvoklis';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt karuseļa moduli!';
$_['error_name']       = 'Moduļa nosaukumā ir jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_width']      = 'Platums ir obligāts!';
$_['error_height']     = 'Augstums ir obligāts!';
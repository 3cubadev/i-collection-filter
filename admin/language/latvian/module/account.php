<?php
// Heading
$_['heading_title']       = 'Profils';

$_['text_module']         = 'Moduļi';
$_['text_success']        = 'Jūs sekmīgi pabeidzāt rediģēt profila moduli!';
$_['text_edit']        = 'Profila moduļa rediģēšana';

// Entry
$_['entry_status']        = 'Stāvoklis:';

// Error
$_['error_permission']    = 'Uzmanību! Jums nav atļauts rediģēt profila moduli!';
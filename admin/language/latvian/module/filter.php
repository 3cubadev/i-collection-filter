<?php
// Heading
$_['heading_title']    = 'Filtri'; 

// Text
$_['text_module']      = 'Moduļi';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt filtru moduli!';
$_['text_edit']        = 'Filtru moduļa rediģēšana';

// Entry
$_['entry_status']     = 'Stāvoklis:';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt filtru moduli!';
<?php
// Heading
$_['heading_title']    = 'PayPal Express pirkuma noformēšanas pogas modulis';

// Text
$_['text_module']      = 'Moduļi';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt PayPal Express pirkuma noformēšanas pogas moduli!';
$_['text_edit']        = 'PayPal Express pirkuma noformēšanas pogas moduļa rediģēšana';

// Entry
$_['entry_status']     = 'Stāvoklis';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt PayPal Express pirkuma noformēšanas pogas moduli!';
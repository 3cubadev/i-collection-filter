<?php
// Heading
$_['heading_title']			= 'Regulāro maksājumu profili';

// Text
$_['text_success']          = 'Jūs sekmīgi pabeidzāt rediģēt regulāro maksājumu profilus!';
$_['text_list']             = 'Regulāro maksājumu profilu saraksts';
$_['text_add']              = 'Pievienot regulāro maksājumu profilu';
$_['text_edit']             = 'Rediģēt regulāro maksājumu profilu';
$_['text_day']				= 'Diena';
$_['text_week']				= 'Nedēļa';
$_['text_semi_month']		= 'Katrs otrais mēnesis';
$_['text_month']			= 'Mēnesis';
$_['text_year']				= 'gats';
$_['text_recurring']	    = '<p><i class="fa fa-info-circle"></i> Regulārie maksājumi tiek aprēķināti izejot no biežuma un cikla.</p><p>Piemēram, ja jūs lietojat biežumu "nedēļa" un ciklu 2, tad pircējam vajadzēs maksāt ik pēc katrām divām nedēļām.</p><p>Ilgums ir reižu skaits, cik pircējs veiks maksājumu, norādiet 0, ja vēlaties, lai maksājumi tiktu veikti neierobežoti ilgi, līdz tie tiks atcetlti.</p>';
$_['text_profile']			= 'Regulāŗo maksājumu profils';
$_['text_trial']			= 'Izmēģinājuma profils';

// Entry
$_['entry_name']			= 'Nosaukums';
$_['entry_price']			= 'Cena';
$_['entry_duration']		= 'Ilgums';
$_['entry_cycle']			= 'Cikls';
$_['entry_frequency']		= 'Biežums';
$_['entry_trial_price']		= 'Izmēģinājuma cena';
$_['entry_trial_duration']	= 'Izmēģinājuma ilgums';
$_['entry_trial_status']	= 'Izmēģinājuma stāvoklis';
$_['entry_trial_cycle']	    = 'Izmēģinājuma cikls';
$_['entry_trial_frequency']	= 'Izmēģinājuma biežums';
$_['entry_status']			= 'Stāvoklis';
$_['entry_sort_order']		= 'Kārtošanas secība';

// Column
$_['column_name']			= 'Nosaukums';
$_['column_sort_order']	    = 'Kārtošanas secība';
$_['column_action']         = 'Darbība';

// Error
$_['error_warning']         = 'Uzmanību! Lūdzu pārliecinieties, vai visi dati ievadīti pareizi!';
$_['error_permission']		= 'Uzmanību! Jums nav atļauts rediģēt regulāro maksājumu profilus!';
$_['error_name']			= 'Profila nosaukumam jābūt no 3 līdz 254 rakstzīmēm!';
$_['error_product']			= 'Uzmanību! Šis regulāro maksājumu profils nevar tikt izdzēsts, jo tas patreiz tiek lietots %s produktos (produktā)!';
<?php
// Heading
$_['heading_title']     = 'Fitri';

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt filtru rediģēšanu!';
$_['text_list']         = 'Filtru saraksts';
$_['text_add']          = 'Pievienot filtru';
$_['text_edit']         = 'Rediģēt filtru';

// Column
$_['column_group']      = 'Filtru grupa';
$_['column_sort_order'] = 'Kārtošanas secība';
$_['column_action']     = 'Izpildīt';

// Entry
$_['entry_group']       = 'Filtru grupas nosaukums';
$_['entry_name']        = 'Filtra nosaukums';
$_['entry_sort_order']  = 'Kārtošanas secība';

// Error
$_['error_permission']  = 'Uzmanību! Jums nav atļauts rediģēt filtrus!';
$_['error_group']       = 'Filtru grupas nosaukumam ir jābūt no 1 līdz 64 rakstzīmēm!';
$_['error_name']        = 'Filtra nosaukumam ir jābūt no 1 līdz 64 rakstzīmēm!';
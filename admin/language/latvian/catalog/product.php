<?php
// Heading
$_['heading_title']          = 'Preces'; 

// Text  
$_['text_success']           = 'Jūs sekmīgi rediģējāt preci!';
$_['text_list']              = 'Preču saraksts';
$_['text_add']               = 'Pievienot preci';
$_['text_edit']              = 'Rediģēt preci';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Galvenais veikals';
$_['text_option']            = 'Opcija';
$_['text_option_value']      = 'Opcijas vērtība';
$_['text_percent']           = 'Procents';
$_['text_amount']            = 'Fiksēts lielums';

// Column
$_['column_name']            = 'Nosaukums';
$_['column_model']           = 'Modelis';
$_['column_image']           = 'Attēls';
$_['column_price']           = 'Cena';
$_['column_quantity']        = 'Daudzums';
$_['column_status']          = 'Stāvoklis';
$_['column_action']          = 'Darbība';

// Entry
$_['entry_name']             = 'Preces nosaukums';
$_['entry_description']      = 'Apraksts';
$_['entry_meta_title'] 	     = 'Nosaukuma metatags';
$_['entry_meta_keyword'] 	 = 'Atslēgvārdu metagags';
$_['entry_meta_description'] = 'Apraksta metatags';
$_['entry_keyword']          = 'SEO atslēgvārds';
$_['entry_model']            = 'Modelis';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Atrašanās vieta';
$_['entry_shipping']         = 'Nepieciešama piegāde'; 
$_['entry_manufacturer']     = 'Ražotājs';
$_['entry_store']            = 'Veikali';
$_['entry_date_available']   = 'Datums, no kura pieejama';
$_['entry_quantity']         = 'Daudzums';
$_['entry_minimum']          = 'Minimālais daudzums';
$_['entry_stock_status']     = 'Stāvoklis, ja nav pieejams';
$_['entry_price']            = 'Cena';
$_['entry_tax_class']        = 'Nodoklis';
$_['entry_points']           = 'Bonusa punkti';
$_['entry_option_points']    = 'Bonusa punkti';
$_['entry_subtract']         = 'Samazināt skaitu noliktavā';
$_['entry_weight_class']     = 'Svara mērvienība';
$_['entry_weight']           = 'Svars';
$_['entry_dimension']        = 'Izmēri (garums x platums x augstums)';
$_['entry_length_class']     = 'Garuma mērvienība';
$_['entry_length']           = 'Garums';
$_['entry_width']            = 'Platums';
$_['entry_height']           = 'Augstums';
$_['entry_image']            = 'Preces attēls';
$_['entry_customer_group']   = 'Pircēju grupa';
$_['entry_date_start']       = 'Sākuma datums';
$_['entry_date_end']         = 'Beigu datums';
$_['entry_priority']         = 'Prioritāte';
$_['entry_attribute']        = 'Aribūti';
$_['entry_attribute_group']  = 'Atribūtu grupa';
$_['entry_text']             = 'Teksts';
$_['entry_option']           = 'Opcija';
$_['entry_option_value']     = 'Opcijas vērtība';
$_['entry_required']         = 'Obligāts';
$_['entry_status']           = 'Stāvoklis';
$_['entry_sort_order']       = 'Kārtošanas secība';
$_['entry_category']         = 'Preču grupas';
$_['entry_filter']           = 'Filtri';
$_['entry_download']         = 'Lejupielādes';
$_['entry_related']          = 'Saistītā prece';
$_['entry_tag']          	 = 'Tagi (preces atslēgvārdi)';
$_['entry_reward']           = 'Bonusa punkti';
$_['entry_layout']           = 'Atšķirīgs lapas izkārtojums';
$_['entry_recurring']        = 'Periodiskais maksājums';

// Help
$_['help_keyword']           = 'Nelietojiet atstarpes. Aizvietojiet tās ar domu zīmi (-). Šim atslēgvārdam jābūt unikālam starp visiem pārējiem SEO atslēgvārdiem.';
$_['help_sku']               = 'Noliktavas kods (Stock Keeping Unit)';
$_['help_upc']               = 'Universālais prceces kods (Universal Product Code)';
$_['help_ean']               = 'European Article Number';
$_['help_jan']               = 'Japanese Article Number';
$_['help_isbn']              = 'International Standard Book Number';
$_['help_mpn']               = 'Ražotāja preces kods (Manufacturer Part Number)';
$_['help_manufacturer']      = '(Automātiska ievades pabeigšana)';
$_['help_minimum']           = 'Minimālais daudzums, ko var pasūtīt';
$_['help_stock_status']      = 'Stāvoklis, kurš tiek parādīts, ja prece nav pieejama uzreiz (nav noliktavā)';
$_['help_points']            = 'Bonusa punktu skaits, kas nepieciešams, lai nopirktu šo preci. Ja Jūs nevēlaties, ka šī prece ir nopērkama ar bonusa punktiem, atstājiet "0".';
$_['help_category']          = '(Automātiska ievades pabeigšana)';
$_['help_filter']            = '(Automātiska ievades pabeigšana)';
$_['help_download']          = '(Automātiska ievades pabeigšana)';
$_['help_related']           = '(Automātiska ievades pabeigšana)';
$_['help_tag']          	 = 'Atdalīt ar komatiem';

// Error
$_['error_warning']          = 'Uzmanību! Lūdzu pārliecinieties, vai visi dati ievadīti pareizi!';
$_['error_permission']       = 'Uzmanību! Jums nav atļauts rediģēt preces!';
$_['error_name']             = 'Preces nosaukumam ir jābūt no 3 līdz <255 rakstzīmēm!';
$_['error_meta_title']       = 'Nosaukuma metatagam jābūt no 3 līdz 255 rakstzīmēm!';
$_['error_model']            = 'Modeļa nosaukumam ir jābūt no 3 līdz <64 rakstzīmēm!';
$_['error_keyword']         = 'Šāds SEO atslēgvārds jau tiek izmantots!';

$_['entry_new_filter']       = 'Filtri (Saraksts)';

$_['filter_add'] = 'Pievienot jaunu';

$_['sort_by_order'] = 'Kārtot pēc secības';
$_['sort_by_popularity'] = 'Kārtot pēc popularitātes';
$_['add_filter'] = 'Pievienot jaunu filtru';
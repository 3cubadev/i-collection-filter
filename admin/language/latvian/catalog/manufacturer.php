<?php
// Heading
$_['heading_title']      = 'Ražotāji';

// Text
$_['text_success']       = 'Jūs sekmīgi pabeidzāt rediģēt informāciju par ražotāju!';
$_['text_list']          = 'Ražotāju saraksts';
$_['text_add']           = 'Pievienot ražotāju';
$_['text_edit']          = 'Rediģēt ražotāju';
$_['text_default']       = 'Galvenais veikals';
$_['text_percent']       = 'Procents';
$_['text_amount']        = 'Fiksēta likme';

// Column
$_['column_name']        = 'Ražotājs';
$_['column_sort_order']  = 'Kārtošanas secība';
$_['column_action']      = 'Darbība';

// Entry
$_['entry_name']         = 'Ražotāja vārds (nosaukums)';
$_['entry_store']        = 'Veikals';
$_['entry_keyword']      = 'SEO Atslēgvārds';
$_['entry_image']        = 'Ražotāja attēls (logotips)';
$_['entry_sort_order']   = 'Kārtošanas secība';
$_['entry_type']         = 'Tips';

// Help
$_['help_keyword']       = 'Nelietojiet atstarpes. Aizvietojiet tās ar domu zīmi (-). Šim atslēgvārdam jābūt unikālam starp visiem pārējiem SEO atslēgvārdiem.';

// Error
$_['error_permission']   = 'Uzmanību! Jums nav atļauts rediģēt ražotājus!';
$_['error_name']         = 'Ražotāja nosaukumam ir jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_keyword']      = 'Šāds SEO atslēgvārds jau tiek izmantots!';
$_['error_product']      = 'Uzmanību! Šis ražotājs nevar tikt dzēsts, jo pašreiz tas tiek izmantots %s precē(s)!';
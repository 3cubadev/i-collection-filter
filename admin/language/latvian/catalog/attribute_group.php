<?php
// Heading
$_['heading_title']     = 'Atribūtu grupas';

// Text
$_['text_success']      = 'Jūs esat sekmīgi modificējis atribūtu grupas!';
$_['text_list']         = 'Atribūtu grupu saraksts';
$_['text_add']          = 'Pievienot attribūtu grupu';
$_['text_edit']         = 'Rediģēt atribūtu grupu';

// Column
$_['column_name']       = 'Atribūtu grupas nosaukums';
$_['column_sort_order'] = 'Kārtošanas secība';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_name']        = 'Atribūtu grupas nosaukums';
$_['entry_sort_order']  = 'Kārtošanas secība';

// Error
$_['error_permission']  = 'Uzmanību! Jums nav atļauts rediģēt atribūtu grupas!';
$_['error_name']        = 'Atribūtu grupas nosaukumam jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_attribute']   = 'Uzmanību! Šī atribūtu grupa pašreiz nevar tikt izdzēsta, jo tā tiek izmantota %s atribūtiem!';
$_['error_product']     = 'Uzmanību! Šī atribūtu grupa pašreiz nevar tikt izdzēsta, jo tā tiek izmantota %s precē(s)!';
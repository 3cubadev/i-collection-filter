<?php
// Heading
$_['heading_title']          = 'Informācijas lapas';

// Text
$_['text_success']           = 'Jūs sekmīgi pabeidzāt rediģēt informācijas lapu!';
$_['text_list']              = 'Informācijas lapu saraksts';
$_['text_add']               = 'Pievienot informācijas lapu';
$_['text_edit']              = 'Rediģēt informācijas lapu';
$_['text_default']           = 'Galvenais veikals';

// Column
$_['column_title']           = 'Informācijas virsraksts';
$_['column_sort_order']      = 'Kārtošanas secība';
$_['column_action']          = 'Darbība';

// Entry
$_['entry_title']            = 'Informācijas lapas virsraksts';
$_['entry_description']      = 'Teksts';
$_['entry_store']            = 'Veikali';
$_['entry_meta_title']       = 'Nosaukuma metatags';
$_['entry_meta_keyword']     = 'Atslēgvārdu metatags';
$_['entry_meta_description'] = 'Apraksta metatags';
$_['entry_keyword']          = 'SEO atslēgvārds';
$_['entry_bottom']           = 'Rādīt kājenē';
$_['entry_status']           = 'Stāvoklis';
$_['entry_sort_order']       = 'Kārtošanas secība';
$_['entry_layout']           = 'Atšķirīgs lapas izkārtojums';

// Help
$_['help_keyword']           = 'Nelietojiet atstarpes. Aizvietojiet tās ar domu zīmi (-). Šim atslēgvārdam jābūt unikālam starp visiem pārējiem SEO atslēgvārdiem.';
$_['help_bottom']            = 'Rādīt lapas apakšā - kājenē.';

// Error 
$_['error_warning']         = 'Uzmanību! Lūdzu pārliecinieties, ka visi dati ir ievadīti pareizi!!';
$_['error_permission']      = 'Uzmanību! Jums nav atļauts rediģēt informācijas lapas!';
$_['error_title']           = 'Virsrakstam ir jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_description']     = 'Aprakstam ir jābūt vismaz 3 rakstzīmēm!';
$_['error_meta_title']      = 'Nosaukuma metatagam jābūt no 3 līdz 255 rakstzīmēm!';
$_['error_keyword']         = 'Šāds SEO atslēgvārds jau tiek izmantots!';
$_['error_account']         = 'Uzmanību! Šī informācijas lapa nevar tikt izdzēsta, jo pašreiz tā tiek izmantota kā interneta vietnes profila noteikumi (privātuma politika)!';
$_['error_checkout']        = 'Uzmanību! Šī informācijas lapa nevar tikt izdzēsta, jo pašreiz tā tiek izmantota kā preču iegādes noteikumi!';
$_['error_affiliate']       = 'Uzmanību! Šī informācijas lapa nevar tikt izdzēsta, jo pašreiz tā tiek izmantota kā interneta vietnes partneru noteikumi!';
$_['error_return']          = 'Warning: This information page cannot be deleted as it is currently assigned as the store return terms!';
$_['error_store']           = 'Šī informācijas lapa nevar tikt izdzēsta, jo pašreiz tā tiek izmantota %s veikalā (-os)!';
<?php
// Heading
$_['heading_title']    = 'Lejupielādes';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt lejupielādes!';
$_['text_list']         = 'Lejupielāžu saraksts';
$_['text_add']          = 'Pievienot lejupielādi';
$_['text_edit']         = 'Rediģēt lejupielādi';
$_['text_upload']      = 'Jūsu fails ir sekmīgi augšupielādēts!';

// Column
$_['column_name']      = 'Lejupielādes nosaukums';
$_['column_date_added'] = 'Pievienošanas datums';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_name']       = 'Lejupielādes nosaukums';
$_['entry_filename']   = 'Faila nosaukums';
$_['entry_mask']       = 'Maska';

// Help
$_['help_filename']     = 'Jūs varat augšupielādēt, izmantojot augšupielādes pogu, vai arī izmantot FTP, lai augšupielādētu failus "Downloads" mapē, un norādīt detaļas zemāk.';
$_['help_mask']         = 'Ir ieteicams, ka faila nosaukums un maska ir atšķirīgi - tas nedos iespēju piekļūt Jūsu failiem tiešā veidā ar interneta saites norādi.';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt lejupielādes!';
$_['error_name']       = 'Nosaukumam ir jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_upload']     = 'Ir jāielādē fails!';
$_['error_filename']   = 'Faila nosaukumam ir jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_exists']     = 'Tāds fails nav atrasts!';
$_['error_mask']       = 'Maskai jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_filetype']   = 'Nederīgs faila tips!';
$_['error_product']    = 'Šī lejupielāde nevar tikt izdzēsta, jo pašreiz tā tiek izmantota %s precē(s)!';
<?php
// Heading
$_['heading_title']          = 'Preču grupas';

// Text
$_['text_success']           = 'Jūs sekmīgi pabeidzāt rediģēt preču grupas!';
$_['text_list']              = 'Preču grupu saraksts';
$_['text_add']               = 'Pievienot preču grupu';
$_['text_edit']              = 'Rediģēt preču grupu';
$_['text_default']           = 'Galvenais veikals';

// Column
$_['column_name']            = 'Nosaukums';
$_['column_sort_order']      = 'Kārtošanas secība';
$_['column_action']          = 'Darbība';

// Entry
$_['entry_name']             = 'Preču grupas nosaukums';
$_['entry_description']      = 'Apraksts';
$_['entry_meta_title'] 	     = 'Nosaukuma metatags';
$_['entry_meta_keyword'] 	 = 'Atslēgvārdu metatags';
$_['entry_meta_description'] = 'Apraksta metatags';
$_['entry_keyword']          = 'SEO Atslēgvārds';
$_['entry_parent']           = 'Iepriekšējā līmeņa preču grupa';
$_['entry_filter']           = 'Filtri';
$_['entry_store']            = 'Veikali';
$_['entry_image']            = 'Preču grupas attēls';
$_['entry_top']              = 'Rādīt augšējā izvēlnē';
$_['entry_column']           = 'Kolonnu skaits preču apakšgrupai';
$_['entry_sort_order']       = 'Kārtošanas secība';
$_['entry_status']           = 'Stāvoklis';
$_['entry_layout']           = 'Atšķirīgs lapas izkārtojums';

// Help
$_['help_filter']            = '(Automātiska izvēles pabeigšana)';
$_['help_keyword']           = 'Nelietojiet atstarpes. Aizvietojiet tās ar domu zīmi (-). Šim atslēgvārdam jābūt unikālam starp visiem pārējiem SEO atslēgvārdiem.';
$_['help_top']               = 'Nosaka, vai rādīt augšējā izvēlnē. Tas darbojas tikai pirmā līmeņa preču grupām.';
$_['help_column']            = 'Nosaka, cik kolonnās rādīt otrā līmeņa prečugrupas augšējā izvēlnē. Darbojas tikai pirmā līmeņa preču grupām.';

// Error 
$_['error_warning']          = 'Uzmanību! Lūdzu, pārliecinieties, ka visi dati ir ievadīti pareizi!';
$_['error_permission']       = 'Uzmanību! Jums nav atļauts rediģēt preču grupas!';
$_['error_name']             = 'Preču grupas nosaukumam ir jābūt no 2 līdz 32 rakstzīmēm!';
$_['error_meta_title']       = 'Nosaukuma metatagam jābūt no 3 līdz 255 rakstzīmēm!';
$_['error_keyword']          = 'Šāds SEO atslēgvārds jau tiek izmantots!';
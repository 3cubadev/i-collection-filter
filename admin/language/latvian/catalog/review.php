<?php
// Heading
$_['heading_title']     = 'Atsauksmes';

// Text
$_['text_success']      = 'Jūs sekmīgi rediģējāt atsauksmes!';
$_['text_list']         = 'Atsauksmju saraksts';
$_['text_add']          = 'Pievienot atsauksmi';
$_['text_edit']         = 'Rediģēt atsauksmi';

// Column
$_['column_product']    = 'Prece';
$_['column_author']     = 'Autors';
$_['column_rating']     = 'Vērtējums';
$_['column_status']     = 'Stāvoklis';
$_['column_date_added'] = 'Pievienots';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_product']     = 'Prece';
$_['entry_author']      = 'Autors';
$_['entry_rating']      = 'Vērtējums';
$_['entry_status']      = 'Stāvoklis';
$_['entry_text']        = 'Teksts';
$_['entry_date_added']  = 'Pievienošanas datums';

// Help
$_['help_product']      = '(Automātiska ievades pabeigšana)';

// Error
$_['error_permission']  = 'Uzmanību! Jums nav atļauts rediģēt atsauksmes!';
$_['error_product']     = 'Nepieciešams izvēlēties preci!';
$_['error_author']      = 'Autora vārdam ir jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_text']        = 'Atsauksmes tekstam ir jābūt vismaz 1 rakstzīmei!';
$_['error_rating']      = 'Vērtējums ir obligāts!';
<?php
// Heading
$_['heading_title']       = 'Opcijas';

// Text
$_['text_success']        = 'Jūs sekmīgi pabeidzāt rediģēt opcijas!';
$_['text_list']           = 'Opciju saraksts';
$_['text_add']            = 'Pievienot opciju';
$_['text_edit']           = 'Rediģēt opciju';
$_['text_choose']         = 'Izvēlieties';
$_['text_select']         = 'Nolaižams saraksts (Select)';
$_['text_radio']          = 'Radiopoga';
$_['text_checkbox']       = 'Izvēles rūtiņa (Checkbox)';
$_['text_image']          = 'Attēls';
$_['text_input']          = 'Teksta ievade';
$_['text_text']           = 'Teksta aile';
$_['text_textarea']       = 'Teksta laukums';
$_['text_file']           = 'Fails';
$_['text_date']           = 'Datums';
$_['text_datetime']       = 'Datums un laiks';
$_['text_time']           = 'Laiks';

// Column
$_['column_name']         = 'Nosaukums';
$_['column_sort_order']   = 'Kārtošanas secība';
$_['column_action']       = 'Darbība';

// Entry
$_['entry_name']         = 'Opcijas nosaukums';
$_['entry_type']         = 'Veids';
$_['entry_option_value'] = 'Opcijas vērtības nosaukums';
$_['entry_image']        = 'Attēls';
$_['entry_sort_order']   = 'Kārtošanas secība';

// Error
$_['error_permission']   = 'Brīdinājums! Jums nav atļauts rediģēt papildu izvēles!';
$_['error_name']         = 'Papildu izvēles nosaukumam ir jābūt no 1 līdz 128 rakstzīmēm!';
$_['error_type']         = 'Brīdinājums! Ir jānorāda izvēles vērtības!';
$_['error_option_value'] = 'Izvēles vērtībām ir jābūt no 1 līdz 128 rakstzīmēm!';
$_['error_product']      = 'Brīdinājums! Šī papildu izvēle nevar tikt dzēsta, jo tā pašreiz tiek izmantota %s precē(s)!';
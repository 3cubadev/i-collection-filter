<?php
// Heading
$_['heading_title']          = 'Atribūti';

// Text
$_['text_success']           = 'Jūs esat sekmīgi modificējuši atribūtus!';
$_['text_list']              = 'Atribūtu saraksts';
$_['text_add']               = 'Pievienot atribūtu';
$_['text_edit']              = 'Rediģēt atribūtu';

// Column
$_['column_name']            = 'Atribūta nosaukums';
$_['column_attribute_group'] = 'Atribūtu grupa';
$_['column_sort_order']      = 'Kārtošanas secība';
$_['column_action']          = 'Darbība';

// Entry
$_['entry_name']            = 'Atribūta nosaukums';
$_['entry_attribute_group'] = 'Atribūtu grupa';
$_['entry_sort_order']      = 'Kārtošanas secība';

// Error
$_['error_permission']      = 'Uzmanību! Jums nav atļauts rediģēt atribūtus!';
$_['error_name']            = 'Atribūta nosaukumam jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_product']         = 'Uzmanību! Šis atribūts pašreiz nevar tikt izdzēsts, jo tas tiek izmantots %s precē(s)!';
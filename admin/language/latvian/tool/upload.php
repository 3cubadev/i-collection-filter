<?php
// Heading
$_['heading_title']     = 'Augšupielādes';

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt augšupielādes!';
$_['text_list']         = 'Augšupielāžu saraksts';

// Column
$_['column_name']       = 'Augšupielādes nosaukums';
$_['column_filename']   = 'Faila nosaukums';
$_['column_date_added'] = 'Pievienošanas datums';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_name']        = 'Augšupielādes nosaukums';
$_['entry_filename']    = 'Faila nosaukums';
$_['entry_date_added'] 	= 'Pievienošanas datums';

// Error
$_['error_permission']  = 'Jums nav atļauts rediģēt augšupielādes';
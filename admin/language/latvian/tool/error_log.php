<?php
// Heading
$_['heading_title'] = 'Kļūdu reģistrs';

// Text
$_['text_success']  = 'Kļūdu reģistrs ir sekmīgi iztīrīts!';
$_['text_list']        = 'Kļūdu saraksts';

// Error
$_['error_warning']	   = 'Warning: Your error log file %s is %s!';
$_['error_permission'] = 'Jums nav atļauts iztukšot kļūdu reģistru!';
<?php
// Heading
$_['heading_title']    = 'Vietnes rezerves kopēšana / atjaunošana';

// Text
$_['text_backup']      = 'Lejupielādēt rezerves kopiju';
$_['text_success']     = 'Rezerves kopija ir sekmīgi importēta!';
$_['text_list']        = 'Augšupielāžu saraksts';

// Entry
$_['entry_restore']    = 'Atjaunot no rezerves kopijas:';
$_['entry_backup']     = 'Izveidot rezerves kopiju:';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt rezerves kopijas!';
$_['error_backup']     = 'Jums jāzivēlas vismaz viena tabula rezerves kopēšanai!';
$_['error_empty']      = 'Augšupielādētais fails ir tukšs!';
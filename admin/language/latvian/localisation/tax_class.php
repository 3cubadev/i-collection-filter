<?php
// Heading
$_['heading_title']     = 'Nodokļu veidi';

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt nodokļu veidus!';
$_['text_list']         = 'Nodokļu veidu saraksts';
$_['text_add']          = 'Pievienot nodokļu veidu';
$_['text_edit']         = 'Rediģēt nodokļu veidu';
$_['text_shipping']     = 'Piegādes adrese';
$_['text_payment']      = 'Maksātāja adrese';
$_['text_store']        = 'Veikala adrese';

// Column
$_['column_title']      = 'Nodokļu veida nosaukums';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_title']       = 'Nodokļu veida nosaukums';
$_['entry_description'] = 'Apraksts';
$_['entry_rate']        = 'Nodokļu likme';
$_['entry_based']       = 'Balstīts uz';
$_['entry_geo_zone']    = 'Darbības reģions';
$_['entry_priority']    = 'Prioritāte';

// Error
$_['error_permission']  = 'Jums nav atļauts rediģēt nodokļu veidus!';
$_['error_title']       = 'Nodokļu veida nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_description'] = 'Aprakstam ir jābūt no 3 līdz 255 rakstzīmēm!';
$_['error_product']     = 'Šis nodokļa veids nevar tikt dzēsts, jo pašreiz tas tiek izmantots %s precē(s)!';
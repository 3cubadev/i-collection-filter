<?php
// Heading
$_['heading_title']          = 'Reģions';

// Text
$_['text_success']           = 'Jūs sekmīgi pabeidzāt rediģēt reģionu!';
$_['text_list']              = 'Reģionu saraksts';
$_['text_add']               = 'Pievienot reģionu';
$_['text_edit']              = 'Rediģēt reģionu';

// Column
$_['column_name']            = 'Nosaukums';
$_['column_code']            = 'Reģiona kods';
$_['column_country']         = 'Valsts';
$_['column_action']          = 'Darbība';

// Entry
$_['entry_status']           = 'Stāvoklis';
$_['entry_name']             = 'Reģiona nosaukums';
$_['entry_code']             = 'Reģiona kods';
$_['entry_country']          = 'Valsts';

// Error
$_['error_permission']       = 'Uzmanību! Jums nav atļauts rediģēt reģionus!';
$_['error_name']             = 'Reģiona nosaukumam ir jābūt no 3 līds 128 rakstzīmēm!';
$_['error_default']          = 'Uzmanību! Šis reģions nevar tikt dzēsts, jo pašreiz tas ir norādīts kā noklusējuma reģions vietnei!';
$_['error_store']            = 'Uzmanību! Šis reģions nevar tikt dzēsts, jo pašreiz tas  tiek izmantots %s veikalā(-os)!';
$_['error_address']          = 'Uzmanību! Šis reģions nevar tikt dzēsts, jo pašreiz tas tiek izmantots %s adrešu grāmatas ierakstā(-os)!';
$_['error_affiliate']        = 'Uzmanību! Šis reģions nevar tikt dzēsts, jo pašreiz tas tiek izmantots %s partnerim(-iem)!';
$_['error_zone_to_geo_zone'] = 'Šis reģions nevar tikt dzēsts, jo pašreiz tas tiek izmantots %s darbības reģionā(-os)!';
<?php
// Heading
$_['heading_title']        = 'Nodokļu likmes';

// Text
$_['text_success']         = 'Jūs sekmīgi pabeidzāt rediģēt nodokļu likmes!';
$_['text_list']            = 'Nodokļa likmju saraksts';
$_['text_add']             = 'Pievienot nodokļa likmi';
$_['text_edit']            = 'Rediģēt nodokļa likmi';
$_['text_percent']         = 'Procenti';
$_['text_amount']          = 'Fiksēta likme';

// Column
$_['column_name']          = 'Nodokļa nosaukums';
$_['column_rate']          = 'Nodokļa likme';
$_['column_type']          = 'Veids';
$_['column_geo_zone']      = 'Darbības reģions';
$_['column_date_added']    = 'Pievienots';
$_['column_date_modified'] = 'Rediģēts';
$_['column_action']        = 'Darbība';

// Entry
$_['entry_name']           = 'Nodokļa nosaukums';
$_['entry_rate']           = 'Nodokļa likme';
$_['entry_type']           = 'Veids';
$_['entry_customer_group'] = 'Klientu grupa';
$_['entry_geo_zone']       = 'Darbības reģions';

// Error
$_['error_permission']  = 'Jums nav atļauts rediģēt nodokļu likmes!';
$_['error_tax_rule']       = 'Uzmanību: Šī nodokļa likme nevar tikt dzēsta, jo pašreiz tā tiek izmantota %s nodokļu veidos(-ā)!';
$_['error_name']           = 'Nodokļa nosaukumam jābūt no  3 līdz 32 rakstzīmēm!';
$_['error_rate']           = 'Ir jānorāda nodokļa likme!';
<?php
// Heading
$_['heading_title']     = 'Valoda';  

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt valodas!'; 
$_['text_list']         = 'Valodu saraksts';
$_['text_add']          = 'Pievienot valodu';
$_['text_edit']         = 'Rediģēt valodu';

// Column
$_['column_name']       = 'Valoda';
$_['column_code']       = 'Kods';
$_['column_sort_order'] = 'Kārtošanas secība';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_name']        = 'Valodas Nosaukums';
$_['entry_code']        = 'Kods';
$_['entry_locale']      = 'Lokāle';
$_['entry_image']       = 'Attēls';
$_['entry_directory']   = 'Mape';
$_['entry_status']      = 'Stāvoklis';
$_['entry_sort_order']  = 'Kārtošanas secība';

// Help
$_['help_code']         = 'piem.: en. Nemainiet, ja tā ir Jūsu veikala galvenā valoda.';
$_['help_locale']       = 'piem.: en_US.UTF-8,en_US,en-gb,en_gb,english';
$_['help_image']        = 'piem.: gb.png';
$_['help_directory']    = 'valodas mapes nosaukums (reģistrjūtīgs)';
$_['help_status']       = 'Paslēpt vai parādīt valodas pārslēgšanas izvēlnē';

// Error
$_['error_permission']  = 'Jums nav atļauts rediģēt valodu uzstādījumus!';
$_['error_name']        = 'Valodas nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_code']        = 'Valodas kodam ir jābūt vismaz 2 rakstzīmēm!';
$_['error_locale']      = 'Ir jānorāda lokāle!';
$_['error_image']       = 'Attēla faila vārdam ir jābūt no 3 līds 64 rakstzīmēm!';
$_['error_directory']   = 'Nepieciešams norādīt mapi (direktoriju)!';
$_['error_default']     = 'Šī valoda nevar tikt dzēsta, jo pašreiz tā ir norādīta kā noklusējuma valoda šai vietnei!';
$_['error_admin']       = 'Šī valoda nevar tikt dzēsta, jo pašreiz tā ir norādīta kā administratora valoda!';
$_['error_store']       = 'Šī valoda nevar tikt dzēsta, jo pašreiz tā ir norādīta %s vietnē(s)!';
$_['error_order']       = 'Šī valoda nevar tikt dzēsta, jo pašreiz tā ir norādīta %s pasūtījumam(-iem)!';
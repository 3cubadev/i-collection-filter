<?php
// Heading
$_['heading_title']          = 'Valsts';

// Text
$_['text_success']           = 'Jūs veiksmīgi pabeidzāt rediģēt informāciju par valsti!';
$_['text_list']               = 'Valstu saraksts';
$_['text_add']                = 'Pievienot valsti';
$_['text_edit']               = 'Rediģēt valsti';

// Column
$_['column_name']            = 'Valsts nosaukums';
$_['column_iso_code_2']      = 'Kods ISO (2)';
$_['column_iso_code_3']      = 'Kods ISO (3)';
$_['column_action']          = 'Darbība';

// Entry
$_['entry_name']             = 'Valsts nosaukums';
$_['entry_iso_code_2']       = 'Kods ISO (2)';
$_['entry_iso_code_3']       = 'Kods ISO (3)';
$_['entry_address_format']   = 'Adreses formāts';
$_['entry_postcode_required'] = 'Pasta indekss nepieciešams';
$_['entry_status']            = 'Stāvoklis';

// Help
$_['help_address_format']     = 'Vārds = {firstname}<br />Uzvārds = {lastname}<br />Uzņēmums = {company}<br />Adrese 1 = {address_1}<br />Adrese 2 = {address_2}<br />Pilsēta = {city}<br />Pasta indekss = {postcode}<br />Reģions = {zone}<br />Reģiona kods = {zone_code}<br />Valsts = {country}';

// Error
$_['error_permission']       = 'Uzmanību! Jums nav atļauts rediģēt informāciju par valsti!';
$_['error_name']             = 'Valsts nosaukumam ir jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_default']          = 'Uzmanību! Šī valsts nevar tikt dzēsta, jo pašreiz tā ir norādīta kā noklusējuma valsts vietnei!';
$_['error_store']            = 'Uzmanību! Šī valsts nevar tikt dzēsta, jo pašreiz tā ir norādīta %s veikalā(-os)!';
$_['error_address']          = 'Uzmanību! Šī valsts nevar tikt dzēsta, jo pašreiz tā ir norādīta %s adrešu grāmatas ierakstā(-os)!';
$_['error_affiliate']         = 'Uzmanību! Šī valsts nevar tikt dzēsta, jo pašreiz tā ir norādīta %s partnerim(-iem)!';
$_['error_zone']             = 'Uzmanību! Šī valsts nevar tikt dzēsta, jo pašreiz tā ir norādīta %s reģionam(-iem)!';
$_['error_zone_to_geo_zone'] = 'Uzmanību! Šī valsts nevar tikt dzēsta, jo pašreiz tā ir norādīta %s darbību reģionam(-iem)!';
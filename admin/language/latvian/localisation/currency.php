<?php
// Heading
$_['heading_title']        = 'Valūta';  

// Text
$_['text_success']         = 'Jūs sekmīgi pabeidzāt valūtu rediģēšanu!';
$_['text_list']            = 'Valūtu saraksts';
$_['text_add']             = 'Pievienot valūtu';
$_['text_edit']            = 'Rediģēt valūtu';

// Column
$_['column_title']         = 'Valūtas nosaukums';
$_['column_code']          = 'Kods'; 
$_['column_value']         = 'Vērtība';
$_['column_date_modified'] = 'Pēdējo reizi atjaunots';
$_['column_action']        = 'Darbība';

// Entry
$_['entry_title']          = 'Valūtas nosaukums:';
$_['entry_code']           = 'Kods:';
$_['entry_value']          = 'Nozīme:';
$_['entry_symbol_left']    = 'Rakstzīme pa kreisi:';
$_['entry_symbol_right']   = 'Rakstzīme pa labi:';
$_['entry_decimal_place']  = 'Cipari aiz komata:';
$_['entry_status']         = 'Stāvoklis:';

// Help
$_['help_code']            = 'Nemainiet kodu, ja tā ir Jūsu veikala galvenā valūta. Tam ir jābūt korektam <a href="http://www.xe.com/iso4217.php" target="_blank">ISO kodam</a>.';
$_['help_value']           = 'Iestatiet 1.00000, ja tā ir jūsu galvenā valūta.';

// Error
$_['error_permission']     = 'Jums nav atļauts rediģēt valūtu uzstādījumus!';
$_['error_title']          = 'Valūtas nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_code']           = 'Valūtas kodā ir jābūt 3 rakstzīmēm!';
$_['error_default']        = 'Šī valūta nevar tikt dzēsta, jo pašreiz tā ir norādīta kā noklusējuma valūta vietnei!';
$_['error_store']          = 'Šī valūta nevar tikt dzēsta, jo pašreiz tā ir norādīta %s veikalam(-iem)!';
$_['error_order']          = 'Šī valūta nevar tikt dzēsta, jo pašreiz tā ir norādīta %s pasūtījumam(-iem)!';
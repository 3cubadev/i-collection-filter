<?php
// Heading
$_['heading_title']    = 'Atteikuma iemesli';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt atteikuma iemeslu!';
$_['text_list']        = 'Atteikuma iemeslu saraksts';
$_['text_add']         = 'Pievienot atteikuma iemeslu';
$_['text_edit']        = 'Rediģēt atteikuma iemeslu';

// Column
$_['column_name']      = 'Atteikuma iemesla nosaukums';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_name']       = 'Atteikuma iemesla nosaukums';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt atteikuma iemeslus';
$_['error_name']       = 'Atteikuma iemesla nosaukumam jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_return']     = 'Uzmanību! Šis atteikuma iemesls nevar tikt dzēsts, jo tas ir norādīts %s atpakaļ sūtāmajai(-jām) precei(-ēm)!';
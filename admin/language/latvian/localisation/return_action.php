<?php
// Heading
$_['heading_title']    = 'Atteikuma darbības';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt atteikuma darbību!';
$_['text_list']        = 'Atteikuma darbību saraksts';
$_['text_add']         = 'Pievienot atteikuma darbību';
$_['text_edit']        = 'Rediģēt atteikuma darbību';

// Column
$_['column_name']      = 'Atteikuma darbības nosaukums "Action Name"';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_name']       = 'Atteikuma darbības nosaukums:';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt atteikuma darbības!';
$_['error_name']       = 'Atteikuma darbības nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_return']     = 'Uzmanību! Šī atteikuma darbība nevar tikt dzēsta, jo pašreiz tā ir norādīta %s atpakaļ nosūtāmajai(-jām) precei(-ēm)!';
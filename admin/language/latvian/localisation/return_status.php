<?php
// Heading
$_['heading_title']    = 'Atteikuma stāvoklis';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt atteikuma stāvokli!';
$_['text_list']        = 'Atteikuma stāvokļu saraksts';
$_['text_add']         = 'Pievienot atteikuma stāvokli';
$_['text_edit']        = 'Rediģēt atteikuma stāvokli';

// Column
$_['column_name']      = 'Atteikuma stāvokļa nosaukums';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_name']       = 'Atteikuma stāvokļa nosaukums:';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt atteikuma stāvokļa nosaukumus!';
$_['error_name']       = 'Atteikuma stāvokļa nosaukumam jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_default']    = 'Uzmanību! Šis atteikumu stāvoklis nevar tikt dzēsts, jo tas ir norādīts kā noklusējuma atteikuma stāvoklis!';
$_['error_return']     = 'Uzmanību! Šis atteikumu stāvoklis nevar tikt dzēsts, jo tas ir norādīts %s atteikumam(-iem)!';
<?php
// Heading
$_['heading_title']    = 'Svara mērvienība';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt svara mērvienības rediģēšanu!';
$_['text_list']        = 'Svara mērvienību saraksts';
$_['text_add']         = 'Pievienot svara mērvienību';
$_['text_edit']        = 'Rediģēt svara mērvienību';

// Column
$_['column_title']     = 'Svara mērvienības nosaukums';
$_['column_unit']      = 'Mērvienība';
$_['column_value']     = 'Vērtība';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_title']      = 'Svara mērvienības nosaukums:';
$_['entry_unit']       = 'Mērvienība:';
$_['entry_value']      = 'Vērtība:';

// Help
$_['help_value']       = 'Norādiet 1.00000, ja tā ir svara mērvienība pēc noklusējuma.';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt garuma mērvienības!';
$_['error_title']      = 'Svara mērvienības nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_unit']       = 'Mērvienībai ir jābūt no 1 līdz 4 rakstzīmēm!';
$_['error_default']    = 'Šī svara mērvienība nevar tikt dzēsta, jo pašreiz tā ir norādīta kā noklusētā svara mērvienība vietnei!';
$_['error_product']    = 'Šī svara mērvienība nevar tikt dzēsta, jo pašreiz tā ir izmantota %s precē(s)!';
<?php
// Heading
$_['heading_title']    = 'Garuma mērvienība';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt garuma mērvienības rediģēšanu!';
$_['text_list']        = 'Garuma mērvienību saraksts';
$_['text_add']         = 'Pievienot garuma mērvienību';
$_['text_edit']        = 'Rediģēt garuma mērvienību';

// Column
$_['column_title']     = 'Garuma mērvienības nosaukums';
$_['column_unit']      = 'Mērvienība';
$_['column_value']     = 'Vērtība';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_title']      = 'Garuma mērvienības nosaukums:';
$_['entry_unit']       = 'Mērvienība:';
$_['entry_value']      = 'Vērtība:';

// Help
$_['help_value']       = 'Iestatiet 1.00000, ja tā ir galvenā garuma mērvienība.';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt garuma mērvienības!';
$_['error_title']      = 'Garuma mērvienības nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_unit']       = 'Mērvienībai ir jābūt no 1 līdz 4 rakstzīmēm!';
$_['error_default']    = 'Šī mērvienība nevar tikt dzēsta, jo pašreiz tā ir norādīta kā noklusētā garuma mērvienība vietnei!';
$_['error_product']    = 'Šī mērvienība nevar tikt dzēsta, jo pašreiz tā ir izmantota %s precē(s)!';
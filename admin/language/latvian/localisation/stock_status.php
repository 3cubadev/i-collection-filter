<?php
// Heading
$_['heading_title']    = 'Pieejamības stāvoklis';

// Text
$_['text_success']     = 'Jūs sekmīgi rediģējāt pieejamības stāvokli!';
$_['text_list']        = 'Pieejamības stāvokļu saraksts';
$_['text_add']         = 'Pievienot pieejamības stāvokli';
$_['text_edit']        = 'Rediģēt pieejamības stāvokli';

// Column
$_['column_name']      = 'Pieejamības stāvokļa nosaukums';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_name']       = 'Pieejamības stāvokļa nosaukums';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt pieejamības stāvokļa nosaukumus!';
$_['error_name']       = 'Pieejamības stāvokļa nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_product']    = 'Šis pieejamības stāvoklis nevar tikt dzēsts, jo pašreiz tas ir izmantots %s precē(s)';
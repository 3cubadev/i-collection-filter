<?php
// Heading
$_['heading_title']    = 'Veikalu atrašanās vietas';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt veikalu atrašanās vietas!';
$_['text_list']        = 'Veikalu atrašanās vietu saraksts';
$_['text_add']         = 'Pievienot vaikala atrašanās vietu';
$_['text_edit']        = 'Rediģēt vaikala atrašanās vietu';
$_['text_default']     = 'Galvenā';
$_['text_time']        = 'Darbības laiki';
$_['text_geocode']     = 'Nesekmīgs ģeokods šāda iemesla dēļ:';

// Column
$_['column_name']      = 'Veikala nosaukums';
$_['column_address']   = 'Adrese';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_name']       = 'Veikala nosaukums';
$_['entry_address']    = 'Adrese';
$_['entry_geocode']    = 'Ģeokods';
$_['entry_telephone']  = 'Tālrunis';
$_['entry_fax']        = 'Fakss';
$_['entry_image']      = 'Attēls';
$_['entry_open']       = 'Darbības laiki';
$_['entry_comment']    = 'Komentāri';

// Help
$_['help_geocode']     = 'Lūdzu norādiet manuāli sava veikala atrašanās vietas ģeokodu.';
$_['help_open']        = 'Lūdzu norādiet veikala darbības laikus.';
$_['help_comment']     = 'Izmantojiet šo aili, jebkādām īpašām piezīmēm, kuras vēlaties nodot saviem pircējiem, piem. - viekals nepieņem čekus.';

// Error
$_['error_permission'] = 'Jums nav tiesību rediģēt Veikalu atrašanās vietas!';
$_['error_name']       = 'Veikala nosaukumam ir jābūt vismaz vienai rakstzīmei!';
$_['error_address']    = 'Adresei jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_telephone']  = 'Tālruņa numuram jābūt no 3 līdz 32 rakstzīmēm!';
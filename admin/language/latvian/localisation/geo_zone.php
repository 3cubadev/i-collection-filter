<?php
// Heading
$_['heading_title']      = 'Darbības reģions';

// Text
$_['text_success']       = 'Jūs sekmīgi pabeidzāt reģionu rediģēšanu!';
$_['text_list']          = 'Darbības reģionu saraksts';
$_['text_add']           = 'Pievienot darbības reģionu';
$_['text_edit']          = 'Rediģēt darbības reģionu';

// Column
$_['column_name']        = 'Nosaukums';
$_['column_description'] = 'Apraksts';
$_['column_action']      = 'Darbība';

// Entry
$_['entry_name']         = 'Darbības reģiona nosaukums:';
$_['entry_description']  = 'Apraksts:';
$_['entry_country']      = 'Stāvoklis:';
$_['entry_zone']         = 'Reģions:';

// Error
$_['error_permission']   = 'Jums nav atļauts rediģēt darbības reģionu';
$_['error_name']         = 'Darbības reģiona nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_description']  = 'Aprakstam ir jābūt no 3 līdz 255 rakstzīmēm!';
$_['error_tax_rate']     = 'Uzmanību! Šis darbības reģions nevar tikt dzēsts, jo pašreiz tas ir norādīts vienā vai vairākos nodokļu likmes ierakstos!';
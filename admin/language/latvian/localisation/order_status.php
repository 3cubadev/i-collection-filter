<?php
// Heading
$_['heading_title']    = 'Pasūtījuma stāvoklis';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt pasūtījuma stāvokli!';
$_['text_list']        = 'Pasūtījuma stāvokļu saraksts';
$_['text_add']         = 'Pievienot pasūtījuma stāvokli';
$_['text_edit']        = 'Rediģēt pasūtījuma stāvokli';

// Column
$_['column_name']      = 'Nosaukums';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_name']       = 'Pasūtījuma stāvoklis:';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt pasūtījuma stāvokļus!';
$_['error_name']       = 'Pasūtījuma stāvokļa nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_default']    = 'Šis pasūtījuma stāvoklis nevar tikt dzēsts, jo pašreiz tas ir norādīts kā noklusējuma pasūtījuma stāvoklis vietnei!';
$_['error_download']   = 'Šis pasūtījuma stāvoklis nevar tikt dzēsts, jo pašreiz tas tiek izmantots kā lejupielādes noklusējuma stāvoklis!';
$_['error_store']      = 'Šis pasūtījuma stāvoklis nevar tikt dzēsts, jo pašreiz tas tiek izmantots %s veikalā(-os)!';
$_['error_order']      = 'Šis pasūtījuma stāvoklis nevar tikt dzēsts, jo pašreiz tas tiek izmantots %s pasūtījumā(-os)!';
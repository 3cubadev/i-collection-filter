<?php
// Heading
$_['heading_title']    = 'Attēlu pārvaldība';

// Text
$_['text_uploaded']    = 'Fails ielādēts!';
$_['text_directory']   = 'Mape sekmīgi izveidota!';
$_['text_delete']      = 'Fails vai mape tika dzēsti!';

// Entry
$_['entry_search']     = 'Meklēt..';
$_['entry_folder']     = 'Mapes nosaukums';

// Error
$_['error_permission'] = 'Jums nav atļauts veikt šajā sadaļā izmaiņas bez administrācijas atļaujas!';
$_['error_filename']   = 'Faila nosaukumam ir jābūt no 3 līdz 255 rakstzīmēm!';
$_['error_folder']     = 'Uzmanību! Mapes nosaukmam ir jābūt no 3 līdz 255 rakstzīmēm!';
$_['error_exists']     = 'Fails vai mape ar tādu nosaukumu jau eksistē!';
$_['error_directory']  = 'Izvēlieties mapi!';
$_['error_filetype']   = 'Nederīgs faila tips!';
$_['error_upload']     = 'Fails netika ielādēts nezināma iemesla dēļ!';
$_['error_delete']     = 'Mapi neizdodas izdzēst!';
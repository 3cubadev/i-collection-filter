<?php
// header
$_['heading_title']   = 'Aizmirsta parole';

// Text
$_['text_forgotten']  = 'Aizmirsta parole';
$_['text_your_email'] = 'Jūsu e-pasta adrese';
$_['text_email']      = 'Ierakstiet savu e-pasta adresi, kas ir piesaistīta Jūsu profilam. Uzklikšķiniet uz "Nosūtīt", lai Jums tiktu nosūtīta paroles atiestatīšanas saite.';
$_['text_success']    = 'E-pasts ar apstiprināšanas saiti ir nosūtīts uz norādīto e-pasta adresi.';

// Entry
$_['entry_email']     = 'E-pasta adrese';
$_['entry_password']  = 'Jaunā parole';
$_['entry_confirm']   = 'Atkārtojiet paroli';

// Error
$_['error_email']     = 'Kļūda: Šāda e-pasta adrese nav atrasta mūsu datu bāzē, lūdzu, mēģiniet vēlreiz!';
$_['error_password']  = 'Parolei jābūt no 3 līdz 20 rakstzīmēm!';
$_['error_confirm']   = 'Ievadītās paroles nesakrīt!';
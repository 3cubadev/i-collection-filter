<?php
// header
$_['heading_title']  = 'Paroles atiestatīšana';

// Text
$_['text_reset']     = 'Jūsu paroles atiestatīšana!';
$_['text_password']  = 'Ievadiet jauno paroli, kuru vēlaties lietot.';
$_['text_success']   = 'Jūsu parole ir sekmīgi mainīta.';

// Entry
$_['entry_password'] = 'Parole:';
$_['entry_confirm']  = 'Atkārtojiet paroli:';

// Error
$_['error_password'] = 'Parolei jābūt no 5 līdz 20 rakstzīmēm!';
$_['error_confirm']  = 'Ievadītās paroles nesakrīt!';
<?php
// Heading
$_['heading_title']                = 'Vadības panelis';

// Text
$_['text_order_total']             = 'Pasūtījumu skaits';
$_['text_customer_total']          = 'Klientu skaits';
$_['text_sale_total']              = 'Apgrozījums';
$_['text_online_total']            = 'Pircēji tiešsaistē';
$_['text_map']                     = 'Pasaules karte';
$_['text_sale']                    = 'Pārdošanas analīze';
$_['text_activity']                = 'Nesenās aktivitātes';
$_['text_recent']                  = 'Pēdējie pasūtījumi';
$_['text_order']                   = 'Pasūtījumi';
$_['text_customer']                = 'Klienti';
$_['text_day']                     = 'Šodiem';
$_['text_week']                    = 'Nedēļa';
$_['text_month']                   = 'Mēnesis';
$_['text_year']                    = 'Gads';
$_['text_view']                    = 'Skatīt vairāk...';

// Error
$_['error_install']                = 'Uzmanību! Install mape joprojām eksistē un tai vajadzētu tik izdzēstai drošības apsvērumu dēļ!';
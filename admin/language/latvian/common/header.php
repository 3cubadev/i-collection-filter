<?php
// Heading
$_['heading_title']          = 'Vadības panelis';

// Text
$_['text_order']             = 'Pasūtījumi';
$_['text_order_status']      = 'Pasūtījumu stāvokļi';
$_['text_complete_status'] = 'Pabeigts';
$_['text_customer']          = 'Pircēji';
$_['text_online']          = 'Pircēji tiešsaistē';
$_['text_approval']        = 'Gaida apstiprinājumu';
$_['text_product']           = 'Preces';
$_['text_stock']           = 'Nav noliktavā';
$_['text_review']           = 'Atsauksmes';
$_['text_return']           = 'Atteikumi';
$_['text_affiliate']         = 'Partneri';
$_['text_store']           = 'Veikali';
$_['text_front']             = 'Skatīt veikalu';
$_['text_help']              = 'Palīdzība';
$_['text_homepage']        = 'Mājas lapa';
$_['text_support']          = 'Forums';
$_['text_documentation']    = 'Dokumentācija';
$_['text_logout']            = 'Iziet';
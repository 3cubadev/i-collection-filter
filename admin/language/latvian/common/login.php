<?php
// header
$_['heading_title']  = 'Interneta vietnes vadība';

// Text
$_['text_heading']   = 'Interneta vietnes vadība';
$_['text_login']     = 'Ievadiet savu lietotājvārdu un paroli';
$_['text_forgotten'] = 'Aizmirsta parole';

// Entry
$_['entry_username'] = 'Lietotājvārds';
$_['entry_password'] = 'Parole';

// Button
$_['button_login']   = 'Autorizēties';

// Error
$_['error_login']    = 'Šāds lietotājvārds un / vai parole neeksistē!';
$_['error_token']    = 'Ir noticis autorizācijas noilgums, lūdzu, autorizējieties no jauna!';
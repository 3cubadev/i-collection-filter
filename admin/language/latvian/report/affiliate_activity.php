<?php
// Heading
$_['heading_title']     = 'Partneru aktivitātes atskaite';

// Text
$_['text_list']         = 'Partneru aktivitāšu saraksts';
$_['text_edit']         = '<a href="affiliate_id=%d">%s</a> rediģēja savu profilu.';
$_['text_forgotten']    = '<a href="affiliate_id=%d">%s</a> pieprasīja jaunu paroli.';
$_['text_login']        = '<a href="affiliate_id=%d">%s</a> autorizējās.';
$_['text_password']     = '<a href="affiliate_id=%d">%s</a> rediģēja sava profila paroli.';
$_['text_payment']      = '<a href="affiliate_id=%d">%s</a> rediģēja savu maksātāja adresi.';
$_['text_register']     = '<a href="affiliate_id=%d">%s</a> reģistrēja jaunu profilu.';

// Column
$_['column_affiliate']  = 'Partneri';
$_['column_comment']    = 'Komentāri';
$_['column_ip']         = 'IP';
$_['column_date_added'] = 'Pievienošanas datums';

// Entry
$_['entry_affiliate']   = 'Partneris';
$_['entry_ip']          = 'IP';
$_['entry_date_start']  = 'Sākuma datums';
$_['entry_date_end']    = 'Beigu datums';
<?php
// Heading
$_['heading_title']     = 'Tiešsaistes apmeklējumu atskaite';

// Text
$_['text_list']         = 'Pircēji tiešsaistē';
$_['text_guest']        = 'Viesis';

// Column
$_['column_ip']         = 'IP';
$_['column_customer']   = 'Pircējs';
$_['column_url']        = 'Pēdējā apmeklētā lapa';
$_['column_referer']    = 'Nosūtītājs (Referer)';
$_['column_date_added'] = 'Pēdējais klikšķis';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_ip']          = 'IP';
$_['entry_customer']    = 'Pircējs';
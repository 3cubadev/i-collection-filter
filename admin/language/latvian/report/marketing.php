<?php
// Heading
$_['heading_title']    = 'Marketinga atskaite';

// Text
$_['text_list']         = 'Marketinga saraksts';
$_['text_all_status']   = 'Visi stāvokļi';

// Column
$_['column_campaign']  = 'Kampaņas nosaukums';
$_['column_code']      = 'Kods';
$_['column_clicks']    = 'Klikšķi';
$_['column_orders']    = 'Pasūtījumu sk.';
$_['column_total']     = 'Kopā';

// Entry
$_['entry_date_start'] = 'Sākuma datums';
$_['entry_date_end']   = 'Beigu datums';
$_['entry_status']     = 'Pasūtījuma stāvoklis';
<?php
// Heading
$_['heading_title']         = 'Pircēju kredītu atskaite';

// Column
$_['text_list']             = 'Pircēju kredītu saraksts';
$_['column_customer']       = 'Pircēju vārds';
$_['column_email']          = 'E-pasts';
$_['column_customer_group'] = 'Pircēju grupa';
$_['column_status']         = 'Stāvoklis';
$_['column_total']          = 'Kopā';
$_['column_action']         = 'Darbība';

// Entry
$_['entry_date_start']      = 'Sākuma datums';
$_['entry_date_end']        = 'Beigu datums';
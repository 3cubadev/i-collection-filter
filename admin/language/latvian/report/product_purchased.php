<?php
// Heading
$_['heading_title']     = 'Nopirkto preču atskaite';

// Text
$_['text_list']         = 'Nopirkto preču saraksts';
$_['text_all_status']   = 'Visi stāvokļi';

// Column
$_['column_date_start'] = 'Sākuma datums';
$_['column_date_end']   = 'Beigu datums';
$_['column_name']       = 'Preces nosaukums';
$_['column_model']      = 'Modelis';
$_['column_quantity']   = 'Skaits';
$_['column_total']      = 'Kopā';

// Entry
$_['entry_date_start']  = 'Sākuma datums';
$_['entry_date_end']    = 'Beigu datums';
$_['entry_status']      = 'Pasūtījuma stāvoklis';
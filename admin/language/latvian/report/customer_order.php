<?php
// Heading
$_['heading_title']         = 'Pircēju pasūtījumu atskaite';

// Text
$_['text_list']             = 'Pircēju pasūtījumu saraksts';
$_['text_all_status']       = 'Visi stāvokļi';

// Column
$_['column_customer']       = 'Pircēja vārds';
$_['column_email']          = 'E-pasts';
$_['column_customer_group'] = 'Pircēju grupa';
$_['column_status']         = 'Stāvoklis';
$_['column_orders']         = 'Pasūtījumu skaits';
$_['column_products']       = 'Preču skaits';
$_['column_total']          = 'Kopā';
$_['column_action']         = 'Darbība';

// Entry
$_['entry_date_start']      = 'Sākuma datums';
$_['entry_date_end']        = 'Beigu datums';
$_['entry_status']          = 'Pasūtījuma stāvoklis';
<?php
// Heading
$_['heading_title']    = 'Kuponu atskaite';

// Text
$_['text_list']        = 'Kuponu saraksts';

// Column
$_['column_name']      = 'Kupona nosaukums';
$_['column_code']      = 'Kods';
$_['column_orders']    = 'Pasūtījumi';
$_['column_total']     = 'Kopā';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_date_start'] = 'Sākuma datums';
$_['entry_date_end']   = 'Beigu datums';
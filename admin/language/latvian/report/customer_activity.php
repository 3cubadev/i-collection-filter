<?php
// Heading
$_['heading_title']       = 'Pircēju aktivitāšu atskaite';

// Text
$_['text_list']           = 'Pircēju aktivitāšu saraksts';
$_['text_address_add']    = '<a href="customer_id=%d">%s</a> pievienoja jaunu adresi.';
$_['text_address_edit']   = '<a href="customer_id=%d">%s</a> rediģēja savu adresi.';
$_['text_address_delete'] = '<a href="customer_id=%d">%s</a> izdzēsa vienu no savām adresēm.';
$_['text_edit']           = '<a href="customer_id=%d">%s</a> rediģēja savu profilu.';
$_['text_forgotten']      = '<a href="customer_id=%d">%s</a> pieprasīja jaunu paroli.';
$_['text_login']          = '<a href="customer_id=%d">%s</a> autorizējās.';
$_['text_password']       = '<a href="customer_id=%d">%s</a> rediģēja savu profila paroli.';
$_['text_register']       = '<a href="customer_id=%d">%s</a> reģistrēja jaunu profilu.';
$_['text_return_account'] = '<a href="customer_id=%d">%s</a> iesniedza preces atteikumu.';
$_['text_return_guest']   = '%s iesniedza preces atteikumu.';
$_['text_order_account']  = '<a href="customer_id=%d">%s</a> izveidoja <a href="order_id=%d">jaunu pasūtījumu</a>.';
$_['text_order_guest']    = '%s izveidoja <a href="order_id=%d">jaunu pasūtījumu</a>.';

// Column
$_['column_customer']     = 'Pircējs';
$_['column_comment']      = 'Komentārs';
$_['column_ip']           = 'IP';
$_['column_date_added']   = 'Pievienošanas datums';

// Entry
$_['entry_customer']      = 'Pircējs';
$_['entry_ip']            = 'IP';
$_['entry_date_start']    = 'Sākuma datums';
$_['entry_date_end']      = 'Beigu datums';
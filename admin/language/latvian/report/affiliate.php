<?php
// Heading
$_['heading_title']     = 'Partneru komisiju atskaite';

// Text
$_['text_list']         = 'Partneru komisiju saraksts';

// Column
$_['column_affiliate']  = 'Partnera nosaukums';
$_['column_email']      = 'E-pasts';
$_['column_status']     = 'Stāvoklis';
$_['column_commission'] = 'Komisija';
$_['column_orders']     = 'Pasūtījumu sk.';
$_['column_total']      = 'Kopā';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_date_start']  = 'Sākuma datums';
$_['entry_date_end']    = 'Beigu datums';
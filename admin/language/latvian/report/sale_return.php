<?php
// Heading
$_['heading_title']     = 'Atteikumu atskaite';

// Text
$_['text_list']         = 'Atteikumu saraksts';
$_['text_year']         = 'Gada';
$_['text_month']        = 'Mēneša';
$_['text_week']         = 'Nedēļas';
$_['text_day']          = 'Dienas';
$_['text_all_status']   = 'Visi stāvokļi';

// Column
$_['column_date_start'] = 'Sākuma datums';
$_['column_date_end']   = 'Beigu datums';
$_['column_returns']    = 'Atteikumu skaits';

// Entry
$_['entry_date_start']  = 'Sākuma datums';
$_['entry_date_end']    = 'Beigu datums';
$_['entry_group']       = 'Grupēt pēc';
$_['entry_status']      = 'Atteikuma stāvoklis';
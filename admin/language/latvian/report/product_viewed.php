<?php
// Heading
$_['heading_title']  = 'Atskaite par skatītāko preci';

// Text
$_['text_list']        = 'Skatītāko preču saraksts';
$_['text_success']   = 'Apskates reģistrs ir sekmīgi iztīrīts!';

// Column
$_['column_name']    = 'Nosaukums';
$_['column_model']   = 'Modelis';
$_['column_viewed']  = 'Apskatīts';
$_['column_percent'] = 'Procenti';

// Error
$_['error_permission'] = 'Jums nav atļauts atiestatīt atskaiti par skatītāko preci!';
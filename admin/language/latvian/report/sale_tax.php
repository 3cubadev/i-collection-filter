<?php
// Heading
$_['heading_title']     = 'Nodokļu atskaite';

// Text
$_['text_list']         = 'Nodokļu saraksts';
$_['text_year']         = 'Gada';
$_['text_month']        = 'Mēneša';
$_['text_week']         = 'Nedēļas';
$_['text_day']          = 'Dienas';
$_['text_all_status']   = 'Visi stāvokļi';

// Column
$_['column_date_start'] = 'Sākuma datums';
$_['column_date_end']   = 'Beigu datums';
$_['column_title']      = 'Nodokļa nosaukums';
$_['column_orders']     = 'Pasūtījumu skaits';
$_['column_total']      = 'Kopā';

// Entry
$_['entry_date_start']  = 'Sākuma datums';
$_['entry_date_end']    = 'Beigu datums';
$_['entry_group']       = 'Grupēt pēc';
$_['entry_status']      = 'Pasūtījuma stāvoklis';
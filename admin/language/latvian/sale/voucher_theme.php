<?php
// Heading
$_['heading_title']      = 'Dāvanu kartes noformējumi';

// Text
$_['text_success']       = 'Jūs sekmīgi pabeidzāt rediģēt dāvanu karšu noformējumus!';
$_['text_list']         = 'Dāvanu kartes noformējumu saraksts';
$_['text_add']          = 'Pievienot dāvanu kartes noformējumu';
$_['text_edit']         = 'Rediģēt dāvanu kartes noformējumu';

// Column
$_['column_name']        = 'Dāvanu kartes noformējuma nosaukums';
$_['column_action']      = 'Darbība';

// Entry
$_['entry_name']         = 'Dāvanu kartes noformējuma nosaukums:';
$_['entry_description']  = 'Dāvanu kartes noformējuma apraksts:';
$_['entry_image']        = 'Attēls:';

// Error
$_['error_permission']   = 'Uzmanību! Jums nav atļauts rediģēt Dāvanu kartes noformējumus!';
$_['error_name']         = 'Dāvanu kartes noformējuma nosaukumam jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_image']        = 'Norādiet attēlu!';
$_['error_voucher']      = 'Uzmanību! Šis Dāvanu kartes noformējums nevar tikt izdzēsts, jo tas pašreiz ir norādīts %s dāvanu kartei(-ēm)!';
<?php
// Heading
$_['heading_title']    = 'Pircēju grupa';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt pircēju grupas rediģēšanu!';
$_['text_list']         = 'Pircēju grupu saraksts';
$_['text_add']          = 'Pievienot pircēju grupu';
$_['text_edit']         = 'Rediģēt pircēju grupu';

// Column
$_['column_name']      = 'Pircēju grupas nosaukums';
$_['column_sort_order']         = 'Kārtošanas secība';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_name']       = 'Pircēju grupas nosaukums';
$_['entry_description']         = 'Apraksts';
$_['entry_approval']            = 'Apstiprināt jaunus pircējus';
$_['entry_sort_order']          = 'Kārtošanas secība';

// Help
$_['help_approval']     = 'Administratoram ir jāapstiprina pircējs, iekams viņš var autorizēties.';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt pricēju grupas!';
$_['error_name']       = 'Grupas nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_default']    = 'Uzmanību! Šī pricēju grupa nevar tikt dzēsta, jo pašreiz tā ir noklusētā pircēju grupa šai vietnei!';
$_['error_store']      = 'Uzmanību! Šī pricēju grupa nevar tikt dzēsta, jo pašreiz tā tiek izmantota %s veikalā(-os)!';
$_['error_customer']   = 'Uzmanību! Šī pricēju grupa nevar tikt dzēsta, jo pašreiz tā ir nozīmēta %s pircējam(-iem)!';
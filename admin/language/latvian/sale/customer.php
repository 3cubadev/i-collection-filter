<?php
// Heading
$_['heading_title']         = 'Pircējs';

// Text
$_['text_success']          = 'Jūs sekmīgi pabeidzāt pircēja rediģēšanu!';
$_['text_list']             = 'Pircēju saraksts';
$_['text_add']              = 'Pievienot pircēju';
$_['text_edit']             = 'Rediģēt pircēju';
$_['text_default']          = 'Galvenais';
$_['text_balance']          = 'Bilance:';
$_['text_add_ban_ip']       = 'Pievienot aizliegto IP adresi';
$_['text_remove_ban_ip']    = 'Noņemt aizliegto IP adresi';

// Column
$_['column_name']           = 'Pircēja vārds';
$_['column_email']          = 'E-pasts';
$_['column_customer_group'] = 'Pircēju grupa';
$_['column_status']         = 'Stāvoklis';
$_['column_date_added']     = 'Pievienots';
$_['column_comment']        = 'Piezīmes';
$_['column_description']    = 'Apraksts';
$_['column_amount']         = 'Summa';
$_['column_points']         = 'Punkti';
$_['column_ip']             = 'IP';
$_['column_total']          = 'Kopā profilu';
$_['column_action']         = 'Darbība';

// Entry
$_['entry_customer_group']  = 'Pircēju grupa';
$_['entry_firstname']       = 'Vārds';
$_['entry_lastname']        = 'Uzvārds';
$_['entry_email']           = 'E-pasts';
$_['entry_telephone']       = 'Tālruņa numurs';
$_['entry_fax']             = 'Fakss';
$_['entry_newsletter']      = 'Jaunumu abonēšana';
$_['entry_status']          = 'Stāvoklis';
$_['entry_approved']        = 'Apstiprināts';
$_['entry_safe']            = 'Drošs';
$_['entry_password']        = 'Parole';
$_['entry_confirm']         = 'Atkārtojiet paroli';
$_['entry_company']         = 'Uzņēmums';
$_['entry_address_1']       = 'Adrese 1';
$_['entry_address_2']       = 'Adrese 2';
$_['entry_city']            = 'Pilsēta';
$_['entry_postcode']        = 'Pasta indekss';
$_['entry_country']         = 'Valsts';
$_['entry_zone']            = 'Reģions';
$_['entry_default']         = 'Galvenā adrese';
$_['entry_comment']         = 'Piezīmes';
$_['entry_description']     = 'Apraksts';
$_['entry_amount']          = 'Summa';
$_['entry_points']          = 'Bonusa punkti';
$_['entry_name']            = 'Pircēja vārds';
$_['entry_ip']              = 'IP';
$_['entry_date_added']      = 'Pievienošanas datums';

// Help
$_['help_safe']             = 'Iestatiet uz "Jā", ja nevēlaties, lai šo pircēju pārbaudītu pret-krāpšanas sistēma';
$_['help_points']           = 'Izmantojiet negatīvu skaitli, lai noņemtu punktus';

// Error
$_['error_warning']         = 'Uzmanību! Lūdzu pārliecinieties, ka visa informācija ir aizpildīta pareizi!';
$_['error_permission']      = 'Uzmanību! Jums nav atļauts rediģēt pircējus!';
$_['error_exists']          = 'Uzmanību! Šī e-pasta adrese ir jau reģistrēta!';
$_['error_firstname']       = 'Vārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']        = 'Uzvārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_email']           = 'E-pasta adrese ir ievadīta nepareizi!';
$_['error_telephone']       = 'Tālrunim ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_password']        = 'Parolei ir jābūt no 4 līdz 20 rakstzīmēm!';
$_['error_confirm']         = 'Ievadītās paroles nesakrīt!';
$_['error_address_1']       = 'Adresei ir jābūt no 3 līdz 128 rakstzīmēm!';
$_['error_city']            = 'Pilsētai ir jābūt no 2 līdz 128 rakstzīmēm!';
$_['error_postcode']        = 'Pasta indeksam jābūt no 2 līdz 10 rakstzīmēm šai valstij!';
$_['error_country']         = 'Lūdzu, norādiet valsti!';
$_['error_zone']            = 'Lūdzu, norādiet reģionu!';
$_['error_custom_field']    = '%s ir obligāts!';
$_['error_comment']         = 'Jums jāievada piezīme!';
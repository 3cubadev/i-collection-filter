<?php
// Heading
$_['heading_title']    = 'Pircēju aizliegtās IP adreses';

// Text
$_['text_success']     = 'Jūs sekmīgi pabeidzāt aizliegto IP adrešu rediģēšanu!';
$_['text_list']        = 'Aizliegto IP adrešu saraksts';
$_['text_add']         = 'Pievienot aizliegto IP adresi';
$_['text_edit']        = 'Rediģēt aizliegto IP adresi';

// Column
$_['column_ip']        = 'IP';
$_['column_customer']  = 'Pircēji';
$_['column_action']    = 'Darbība';

// Entry
$_['entry_ip']         = 'IP';

// Error
$_['error_permission'] = 'Uzmanību! Jums nav atļauts rediģēt aizliegtās IP adreses!';
$_['error_ip']         = 'IP adresē jābūt no 1 līdz 40 rakstzīmēm!';
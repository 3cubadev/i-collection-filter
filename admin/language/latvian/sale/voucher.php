<?php
// Heading  
$_['heading_title']     = 'Dāvanu karte';

// Text
$_['text_success']      = 'Jūs sekmīgi pabeidzāt rediģēt dāvanu kartes!';
$_['text_list']         = 'Gift Voucher List';
$_['text_add']          = 'Add Gift Voucher';
$_['text_edit']         = 'Edit Gift Voucher';
$_['text_sent']         = 'E-pasts ar dāvanu karti tika veiksmīgi nosūtīts!';

// Column
$_['column_name']       = 'Dāvanu kartes nosaukums';
$_['column_code']       = 'Kods';
$_['column_from']       = 'Sūtītājs';
$_['column_to']         = 'Adresāts';
$_['column_theme']      = 'Noformējums';
$_['column_amount']     = 'Summa';
$_['column_status']     = 'Stāvoklis';
$_['column_order_id']   = 'Pasūtījuma Nr.';
$_['column_customer']   = 'Pircējs';
$_['column_date_added'] = 'Pievienots';
$_['column_action']     = 'Darbība';

// Entry
$_['entry_code']        = 'Kods';
$_['entry_from_name']   = 'Sūtītāja vārds';
$_['entry_from_email']  = 'Sūtītāja e-pasts';
$_['entry_to_name']     = 'Adresāta vārds';
$_['entry_to_email']    = 'Adresāta e-pasts';
$_['entry_theme']       = 'Noformējums';
$_['entry_message']     = 'Teksts';
$_['entry_amount']      = 'Summa';
$_['entry_status']      = 'Stāvoklis';

// Help
$_['help_code']         = 'Kods, ko pircējs ievada, lai aktivizētu dāvanu karti.';

// Error
$_['error_selection']   = 'Nav izvēlēta neviena dāvanu karte!';
$_['error_permission']  = 'Uzmanību! Jums nav atļauts rediģēt dāvanu kartes!';
$_['error_exists']      = 'Uzmanību! Šis dāvanu kartes kods jau tiek izmantots!';
$_['error_code']        = 'Kodam jābūt no 3 līdz 10 rakstzīmēm!';
$_['error_to_name']     = 'Saņēmēja vārdam jābūt no 1 līdz 64 rakstzīmēm!';
$_['error_from_name']   = 'Jūsu vārdam jābūt no 1 līdz 64 rakstzīmēm!';
$_['error_email']       = 'E-pasta adrese ir ievadīta nepareizi!';
$_['error_amount']      = 'Summai jābūt lielākai vai vienādai ar 1!';
$_['error_order']       = 'Uzmanību: Šī dāvanu karte nevar tikt izdzēsta, jo tā ir daļa no <a href="%s">pasūtījuma</a>!';
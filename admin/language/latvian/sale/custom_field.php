<?php
// Heading
$_['heading_title']         = 'Papildu lauki';

// Text
$_['text_success']          = 'Jūs sekmīgi pabeidzāt rediģēt papildu laukus!';
$_['text_list']             = 'Papildu aiļu saraksts';
$_['text_add']              = 'Pievienot papildu aili';
$_['text_edit']             = 'Rediģēt papildu aili';
$_['text_choose']           = 'Izvēle';
$_['text_select']           = 'Lejupnolaižama izvēlne (Select)';
$_['text_radio']            = 'Pogas (Radio)';
$_['text_checkbox']         = 'Rūtiņas (Checkbox)';
$_['text_input']            = 'Datu ievade';
$_['text_text']             = 'Teksta aile';
$_['text_textarea']         = 'Teksta laukums';
$_['text_file']             = 'Fails';
$_['text_date']             = 'Datums';
$_['text_datetime']         = 'Datums &amp; Laiks';
$_['text_time']             = 'Laiks';
$_['text_account']          = 'Profils';
$_['text_address']          = 'Adrese';

// Column
$_['column_name']           = 'Papildu ailes nosaukums';
$_['column_location']       = 'Atrašanās vieta';
$_['column_type']           = 'Veids';
$_['column_sort_order']     = 'Kārtošanas secība';
$_['column_action']         = 'Darbība';

// Entry
$_['entry_name']            = 'Papildu ailes nosaukums';
$_['entry_location']        = 'Atrašanās vieta';
$_['entry_type']            = 'Veids';
$_['entry_value']           = 'Vērtība';
$_['entry_custom_value']    = 'Papildu ailes vērtības nosaukums';
$_['entry_customer_group']  = 'Pircēju grupa';
$_['entry_required']        = 'Obligāts';
$_['entry_status']          = 'Stāvoklis';
$_['entry_sort_order']      = 'Kārtošanas secība';

// Help
$_['help_sort_order']       = 'Lietojiet mīnuss zīmi, lai skaitītu atpakaļ no pedējās ailes grupā.';

// Error
$_['error_permission']      = 'Jums nav atļauts rediģēt papildu ailes!';
$_['error_name']            = 'Papildu ailes nosaukumā jābūt no 1 līdz 128 rakstzīmēm!';
$_['error_type']            = 'Papildu ailes vērtības ir obligātas!';
$_['error_custom_value']    = 'Papildu ailes vērtības nosaukumā jābūt no 1 līdz 128 rakstzīmēm!';
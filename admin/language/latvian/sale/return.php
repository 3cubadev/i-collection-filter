<?php
// Heading
$_['heading_title']       = 'Preces atteikumi';

// Text
$_['text_success']        = 'Jūs sekmīgi pabeidzāt preces atteikumu rediģēšanu!';
$_['text_list']           = 'Preces atteikumu saraksts';
$_['text_add']            = 'Pievienot preces atteikumu';
$_['text_edit']           = 'Rediģēt preces atteikumu';
$_['text_opened']         = 'Atpakota';
$_['text_unopened']       = 'Neatpakota';
$_['text_order']          = 'Pasūtījuma apstiprinājums';
$_['text_product']        = 'Informācija par preci un Atteikuma iemesls';
$_['text_history']        = 'Add Return History';

// Column
$_['column_return_id']     = 'Atteikuma Nr.';
$_['column_order_id']      = 'Pasūtījuma Nr';
$_['column_customer']      = 'Pircējs';
$_['column_product']       = 'Prece';
$_['column_model']         = 'Modelis';
$_['column_status']        = 'Stāvoklis';
$_['column_date_added']    = 'Pievienots';
$_['column_date_modified'] = 'Rediģēts';
$_['column_comment']       = 'Papildu informācija';
$_['column_notify']        = 'Pircējam paziņots';
$_['column_action']        = 'Darbība';

// Entry
$_['entry_customer']      = 'Pircējs';
$_['entry_order_id']      = 'Pasūtījuma Nr.';
$_['entry_date_ordered']  = 'Pasūtījuma datums';
$_['entry_firstname']     = 'Vārds';
$_['entry_lastname']      = 'Uzvārds';
$_['entry_email']         = 'E-pasts';
$_['entry_telephone']     = 'Tālruņa numurs';
$_['entry_product']       = 'Prece';
$_['entry_model']         = 'Modelis';
$_['entry_quantity']      = 'Daudzums';
$_['entry_opened']        = 'Atpakots';
$_['entry_comment']       = 'Papildu informācija';
$_['entry_return_reason']        = 'Atteikuma iemesls';
$_['entry_return_action'] = 'Atteikuma darbība';
$_['entry_return_status'] = 'Atteikuma stāvoklis';
$_['entry_notify']        = 'Paziņot pircējam';
$_['entry_return_id']     = 'Atteikuma Nr.';
$_['entry_date_added']    = 'Pievienošanas datums';
$_['entry_date_modified'] = 'Rediģēšanas datums';

// Help
$_['help_product']        = '(Automātiska ievades pabeigšana)';

// Error
$_['error_warning']       = 'Uzmanību! Lūdzu, pārliecinieties, ka visa informācija ir ievadīta pareizi!';
$_['error_permission']    = 'Uzmanību! Jums nav atļauts rediģēt atteikumus!';
$_['error_order_id']      = 'Lūdzu, norādiet pasūtījuma numuru!';
$_['error_firstname']         = 'Vārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_lastname']          = 'Uzvārdam ir jābūt no 1 līdz 32 rakstzīmēm!';
$_['error_email']             = 'E-pasta adrese ir ievadīta nepareizi!';
$_['error_telephone']         = 'Tālrunim ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_product']          = 'Preces nosaukumam ir jābūt no 3 līdz 255 rakstzīmēm!';
$_['error_model']          = 'Preces modelim ir jābūt no 3 līdz 64 rakstzīmēm!';
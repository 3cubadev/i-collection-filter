<?php

$_['heading_title'] = 'Periodiskie maksājumu profili';

// Text
$_['text_success'] = 'Jūs sekmīgi pabeidzāt rediģēt "Periodiskos maksājumus"!';
$_['text_list']                            = 'Periodisko maksājumu profilu saraksts';
$_['text_add']                             = 'Pievienot periodiskie maksājumu profilu';
$_['text_edit']                            = 'Rediģēt periodiskie maksājumu profilu';
$_['text_payment_profiles'] = 'Periodiskie maksājumu profili';
$_['text_status_active'] = 'Aktīvs';
$_['text_status_inactive'] = 'Nav aktīvs';
$_['text_status_cancelled'] = 'Atcelts';
$_['text_status_suspended'] = 'Apturēts';
$_['text_status_expired'] = 'Beidzies derīguma termiņš';
$_['text_status_pending'] = 'Gaida';
$_['text_transactions'] = 'Darījumi';
$_['text_cancel_confirm'] = "Profila atcelšana ir neatgriezeniska! Vai esat pārliecināts(-a), ka vēlaties turpināt?";
$_['text_transaction_date_added']          = 'Pievienošanas datums';
$_['text_transaction_payment'] = 'Maksājums';
$_['text_transaction_outstanding_payment'] = 'Nenokārtots maksājums';
$_['text_transaction_skipped'] = 'Izlaists maksājums';
$_['text_transaction_failed'] = 'Neizdevies maksājums';
$_['text_transaction_cancelled'] = 'Atcelts';
$_['text_transaction_suspended'] = 'Aizturēts';
$_['text_transaction_suspended_failed'] = 'Aizturēts neizpildīta maksājuma dēļ';
$_['text_transaction_outstanding_failed'] = 'Maksājums neizdevās';
$_['text_transaction_expired'] = 'Beidzies derīguma termiņš';

// Entry
$_['entry_cancel_payment'] = 'Atcelt maksājumu';
$_['entry_order_recurring'] = 'ID';
$_['entry_order_id'] = 'Pasūtījuma ID';
$_['entry_reference'] = 'Maksājuma atsauce';
$_['entry_customer'] = 'Pircējs';
$_['entry_date_added'] = 'Izveidots';
$_['entry_status'] = 'Statuss';
$_['entry_type'] = 'Veids';
$_['entry_action'] = 'Darbība';
$_['entry_email'] = 'Epasts';
$_['entry_description'] = "Periodiskā maksājuma apraksts";
$_['entry_product'] = "Prece";
$_['entry_quantity'] = "Skaits";
$_['entry_amount'] = "Summa";
$_['entry_recurring'] = "Periodiskā maksājuma profils";
$_['entry_payment_method'] = "Maksājuma veids";

// Error / Success
$_['error_not_cancelled'] = 'Kļūda: %s';
$_['error_not_found'] = 'Neizdevās atcelt periodisko maksājumu';
$_['text_cancelled'] = 'Periodiskais maksājums ir atcelts';
?>
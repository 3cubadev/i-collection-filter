<?php
// Heading
$_['heading_title']         = 'Pasūtījumi';

// Text
$_['text_list']                               = 'Pasūtījumu saraksts';
$_['text_add']                                = 'Pievienot pasūtījumu';
$_['text_edit']                               = 'Rediģēt pasūtījumu';
$_['text_order_detail']                       = 'Pasūtījuma detaļas';
$_['text_order_id']         = 'Pasūtijuma Nr.:';
$_['text_invoice_no']       = 'Rēķina Nr.:';
$_['text_invoice_date']       = 'Rēķina datums:';
$_['text_store_name']         = 'Vietnes nosaukums:';
$_['text_store_url']          = 'Vietnes saite:';
$_['text_customer']           = 'Pircējs:';
$_['text_customer_group']     = 'Pircēju grupa:';
$_['text_email']              = 'E-pasts:';
$_['text_telephone']          = 'Tālruņa numurs:';
$_['text_fax']                = 'Fakss:';
$_['text_website']                            = 'Web Vietne:';
$_['text_shipping_method']    = 'Piegādes veids:';
$_['text_payment_method']     = 'Apmaksas veids:';
$_['text_total']              = 'Kopā:';
$_['text_reward']             = 'Bonusa punkti:';
$_['text_order_status']       = 'Pasūtījuma stāvoklis:';
$_['text_comment']            = 'Papildinformācija:';
$_['text_affiliate']          = 'Partneris:';
$_['text_commission']         = 'Komisijas maksa:';
$_['text_ip']                 = 'IP Adrese:';
$_['text_forwarded_ip']       = 'Pārsūtītā (forwarded) IP:';
$_['text_user_agent']         = 'User Agent:';
$_['text_accept_language']    = 'Pieņemamā valoda:';
$_['text_date_added']         = 'Pievienots:';
$_['text_date_modified']      = 'Rediģēts:';
$_['text_firstname']          = 'Vārds:';
$_['text_lastname']           = 'Uzvārds:';
$_['text_company']            = 'Uzņēmums:';
$_['text_address_1']          = 'Adrese 1:';
$_['text_address_2']          = 'Adrese 2:';
$_['text_postcode']           = 'Pasta indekss:';
$_['text_city']               = 'Pilsēta:';
$_['text_zone']               = 'Reģions:';
$_['text_zone_code']          = 'Reģiona kods:';
$_['text_country']            = 'Valsts:';
$_['text_invoice']            = 'Rēķins';
$_['text_from']                               = 'Shipping From';
$_['text_to']                 = 'Adresāts:';
$_['text_ship_to']            = 'Piegādes adresāts (ja atšķiras adrese)';
$_['text_missing']            = 'Trūkstošie pasūtījumi';
$_['text_default']            = 'Galvenā vietne';
$_['text_product']            = 'Pievienot preces';
$_['text_voucher']            = 'Pievienot dāvanu karti(-es)';
$_['text_order']              = 'Pasūtījuma detaļas';
$_['text_shipping']                           = 'Shipping';
$_['text_contact']                            = 'Contact';
$_['text_sku']                                = 'SKU:';
$_['text_upc']                                = 'UPC:';
$_['text_ean']                                = 'EAN:';
$_['text_jan']                                = 'JAN:';
$_['text_isbn']                               = 'ISBN:';
$_['text_mpn']                                = 'MPN:';
$_['text_generate']           = 'Izveidot rēķina Nr.';
$_['text_reward_added']       = 'Bonusa punkti pievienoti!';
$_['text_reward_removed']     = 'Bonusa punkti noņemti!';
$_['text_commission_added']   = 'Komisijas maksa pievienota!';
$_['text_commission_removed'] = 'Komisijas maksa noņemta!';
$_['text_restock']                            = 'Success: Products have been restocked!';
$_['text_upload']             = 'Jūsu fails ir sekmīgi augšupielādiets!';
$_['text_picklist']                           = 'Dispečera piezīme';
$_['text_history']                            = 'Pievienot pasūtījuma vēsturi';

// Column
$_['column_order_id']          = 'Pasūtījuma Nr.';
$_['column_customer']         = 'Pircējs';
$_['column_status']         = 'Stāvoklis';
$_['column_date_added']     = 'Pasūtīšanas datums';
$_['column_date_modified']    = 'Rediģēšanas datums';
$_['column_total']          = 'Kopā';
$_['column_product']        = 'Prece';
$_['column_model']          = 'Modelis';
$_['column_quantity']       = 'Daudzums';
$_['column_price']          = 'Cena par vienību';
$_['column_comment']          = 'Komentāri';
$_['column_notify']         = 'Pircējam paziņots';
$_['column_location']                         = 'Atrašanās vieta';
$_['column_reference']                        = 'Atsauce';
$_['column_action']         = 'Darbība';
$_['column_weight']                           = 'Preces svars';

// Entry 
$_['entry_store']             = 'Vietne:';
$_['entry_customer']        = 'Pircējs:';
$_['entry_customer_group']    = 'Klientu grupa:';
$_['entry_firstname']       = 'Vārds:';
$_['entry_lastname']        = 'Uzvārds:';
$_['entry_email']           = 'E-pasts:';
$_['entry_telephone']       = 'Tālruņa numurs:';
$_['entry_fax']             = 'Fakss:';
$_['entry_address']           = 'Izvēlieties adresi:';
$_['entry_company']         = 'Uzņēmums:';
$_['entry_address_1']       = 'Adrese 1:';
$_['entry_address_2']       = 'Adrese 2:';
$_['entry_city']            = 'Pilsēta:';
$_['entry_postcode']        = 'Pasta indekss:';
$_['entry_country']         = 'Valsts:';
$_['entry_zone']            = 'Reģions:';
$_['entry_zone_code']       = 'Reģiona kods:';
$_['entry_product']           = 'Prece:';
$_['entry_option']             = 'Izvēlieties papildu iespēju(-as):';
$_['entry_quantity']          = 'Daudzums:';
$_['entry_to_name']                           = 'Saņēmēja vārds:';
$_['entry_to_email']                          = 'Saņēmēja e-pasts';
$_['entry_from_name']                         = 'Sūtītāja vārds:';
$_['entry_from_email']                        = 'Sūtītāja e-pasts:';
$_['entry_theme']                             = 'Dāvanu kartes noformējums:';
$_['entry_message']                           = 'Ziņojums:';
$_['entry_amount']                            = 'Summa:';
$_['entry_affiliate']         = 'Partneris:';
$_['entry_order_status']    = 'Pasūtījuma stāvoklis:';
$_['entry_notify']          = 'Paziņojums pircējam';
$_['entry_comment']           = 'Komentārs:';
$_['entry_currency']           = 'Valūta';
$_['entry_shipping_method']          = 'Piegādes veids:';
$_['entry_payment_method']           = 'Apmaksas veids:';
$_['entry_coupon']                            = 'Kupons:';
$_['entry_voucher']                           = 'Dāvanu karte:';
$_['entry_reward']                            = 'Bonuss:';
$_['entry_order_id']                          = 'Pasūtījuma Nr.';
$_['entry_total']                             = 'Kopā';
$_['entry_date_added']                        = 'Pievienošanas datums';
$_['entry_date_modified']                     = 'Rediģēšanas datums';

// Error
$_['error_warning']           = 'Uzmanību! Lūdzu, pārliecinieties, ka visas ailes ir aizpildītas pareizi!';
$_['error_permission']        = 'Uzmanību! Jums nav atļauts rediģēt pasūtījumus!';
$_['error_curl']                              = 'Uzmanību: CURL kļūda %s(%s)!';
$_['error_action']            = 'Uzmanību! Šo darbību nebija iespējams pabeigt!';
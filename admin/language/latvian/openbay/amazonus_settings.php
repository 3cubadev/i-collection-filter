<?php
// Heading
$_['heading_title']            = 'Marketplace settings';
$_['text_openbay']      = 'OpenBay Pro';
$_['text_amazon']      = 'Amazon US';

// Text
$_['text_api_status']                = 'API savienojuma statuss';
$_['text_api_ok']                    = 'Savienojums atļauts, pilnvarošanās atļauta';
$_['text_api_auth_error']            = 'Savienojums atļauts, pilnvarošanās kļūdaina';
$_['text_api_error']                 = 'Savienojuma kļūda';
$_['text_order_statuses']            = 'Pasūtījumu stāvokļi';
$_['text_unshipped']                 = 'Nav piegādāts';
$_['text_partially_shipped']         = 'Daļēji piegādāts';
$_['text_shipped']                   = 'Piegādāts';
$_['text_canceled']                  = 'Atslēgts';
$_['text_other']                     = 'Cits';
$_['text_marketplaces']              = 'Tirgus';
$_['text_setttings_updated']         = 'Iestatījumi ir veiksmīgi atjaunoti.';
$_['text_new']        = 'New';
$_['text_used_like_new']     = 'Used - Like New';
$_['text_used_very_good']     = 'Used - Very Good';
$_['text_used_good']      = 'Used - Good';
$_['text_used_acceptable']     = 'Used - Acceptable';
$_['text_collectible_like_new']   = 'Collectible - Like New';
$_['text_collectible_very_good']   = 'Collectible - Very Good';
$_['text_collectible_good']    = 'Collectible - Good';
$_['text_collectible_acceptable']   = 'Collectible - Acceptable';
$_['text_refurbished']      = 'Refurbished';

// Error
$_['error_permission']            = 'Jums nav piekļuves šim modulim';

// Entry
$_['entry_status']                   = 'Statuss';
$_['entry_token']      = 'Pazīme (token)';
$_['entry_string1']                = 'Encryption String 1';
$_['entry_string2']                = 'Encryption String 2';
$_['entry_import_tax']               = 'Nodoklis par importētajām precēm';
$_['entry_customer_group']           = 'Pircēju grupa';
$_['entry_tax_percentage']            = 'Percentage added to default product\'s price';
$_['entry_default_condition']         = 'Produkta stāvokļa veids pēc noklusējuma';
$_['entry_notify_admin']              = 'Ziņot par jaunu pasūtījumu';
$_['entry_default_shipping']          = 'Default shipping';

// Tabs
$_['tab_settings']               = 'API detaļas';
$_['tab_listing']                    = 'Saraksti';
$_['tab_orders']                     = 'Pasūtījumi';

// Help
$_['help_import_tax']             = 'Lietot, ja "Amazonus" nesniedz informāciju par nodokļiem';
$_['help_customer_group']         = 'Atlasīt pircēju grupu importētajiem pasūtījumiem';
$_['help_default_shipping']       = 'Used as the pre-selected option in the bulk order update';
$_['help_tax_percentage']            = 'Percentage added to default product price';
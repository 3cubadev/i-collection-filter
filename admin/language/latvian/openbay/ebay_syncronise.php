<?php
// Heading
$_['heading_title']            = 'Sinhronizēt';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Buttons
$_['button_update']    = 'Atjaunot';

// Entry
$_['entry_sync_categories']     = 'Iegūt galvenās eBay kategorijas';
$_['entry_sync_shop']         = 'Iegūt veikala kategorijas';
$_['entry_sync_setting']      = 'Iegūt iestatījumus';

// Text
$_['text_complete']             = 'Complete';
$_['text_sync_desc']            = 'Jums jāsinhronizē veikals ar jaunākajām pieejamajām eBay piegādes iespējām un kategorijām, šie dati ir tikai preču saraksta veidošanai eBay - netiks importētas kategorijas jūsu veikalā utt.. <br /><strong>Jums jāatjauno tas bieži, lai pērliecinātos, ka jums pieejami jaunākie dati no eBay.</strong>';
$_['text_ebay_categories']   = 'Tas var aizņemt brīdi laika, gaidiet piecas minūtes, pirms turpiniet darīt ko citu.';
$_['text_category_import']      = 'Jūsu eBay veikala kategorijas ir importētas.';
$_['text_setting_import']  		= 'Jūsu iestatījumi ir importēti.';

// Help
$_['help_sync_categories']    = 'Jūsu veikalā netiks importēta neviena kategorija!';
$_['help_sync_shop']      = 'Jūsu veikalā netiks importēta neviena kategorija!';
$_['help_sync_setting']   = 'Importēšana pieejama maksājuma veidiem, piegādei, atrašanās vietai un citiem.';

// Errors
$_['error_settings']   = 'Ielādējot iestatījumus, radās kļūda.';
$_['error_failed']              = 'Kļūda savienojumā ar serveri';
?>
<?php
//Headings
$_['heading_title'] 				= 'Jauns Amazon US saraksts';
$_['text_title_advanced'] 			= 'Advanced listing';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon US';

//Buttons
$_['button_new'] = 'Izveidot jaunu produktu';
$_['button_amazon_price'] 			= 'Get Amazon price';
$_['button_list'] = "Amazon saraksts";
$_['button_remove_error'] 			= 'Remove error messages';
$_['button_save_upload'] 			= 'Save and upload';
$_['button_browse'] 				= 'Browse';
$_['button_saved_listings'] 		= 'Skatīt saglabātos sarakstus';
$_['button_remove_links'] 			= "Noņemt saiti";
$_['button_create_new_listing'] 	= "Izveidot jaunu sarakstu";

// Help
$_['help_sku'] 						= "Unikālais produkta ID piešķirts pēc komersanta";
$_['help_restock_date'] 			= "Šis ir periods, kurā pircējam būs ļauts atsūtīt atpakaļ preci. Šim periodam nevajadzētu būt ilgākam par 30 dienām, vai arī pasūtījuma saņēmējs no saraksta tiks izņemts.";
$_['help_sale_price'] 				= "Izpārdošanas cenai jānorāda sākuma un beigu datums.";

//Text
$_['text_products_sent'] 			= 'Products were sent for processing';
$_['button_view_on_amazon'] 		= 'View on Amazon';
$_['text_list'] = 'Saraksts';
$_['text_new'] = 'Jauns';
$_['text_used_like_new'] = 'Izmantots - Kā jauns';
$_['text_used_very_good'] = 'Izmantots - Ļoti labi';
$_['text_used_good'] = 'Izmantots - Labi';
$_['text_used_acceptable'] = 'Izmantots - Pieņemami';
$_['text_collectible_like_new'] = 'Kolekcionējams (collectible) - Kā jauns';
$_['text_collectible_very_good'] = 'Kolekcionējams (collectible) - Ļoti labi';
$_['text_collectible_good'] = 'Kolekcionējams (collectible) - Labi';
$_['text_collectible_acceptable'] = 'Kolekcionējams (collectible) - Pieņemami';
$_['text_refurbished'] = 'Atjaunots';
$_['text_product_not_sent'] = 'Produkts nav nosūtīts Amazon US. Iemesls: %';
$_['text_not_in_catalog'] 			= 'Citādi, ja nav atrasts katalogā&nbsp;&nbsp;&nbsp;';
$_['text_placeholder_search'] 		= 'Enter product name, UPC, EAN, ISBN or ASIN';
$_['text_placeholder_condition'] 	= 'Use this box to describe the condition of your products.';
$_['text_characters'] 				= 'characters';
$_['text_uploaded'] 				= 'Saglabātais(ie) saraksts(i) augšupielādēti!';
$_['text_saved_local'] 				= 'Listing saved but not uploaded yet';
$_['text_product_sent'] = 'Produkts ir veiksmīgi nosūtīts Amazon US.';
$_['text_links_removed'] 			= 'Amazon produkta saite dzēsta';
$_['text_product_links'] 			= 'Product links';
$_['text_has_saved_listings'] 		= 'This product has one or more saved listings that is not uploaded';
$_['text_edit_heading'] 			= 'Edit listing';

//Table columns
$_['column_image'] = 'Attēls';
$_['column_asin'] = 'ASIN';
$_['column_price'] = 'Cena';
$_['column_action'] = 'Darbība';
$_['column_name'] = 'Nosaukums';
$_['column_model'] 					= 'Modelis';
$_['column_combination'] 			= 'Kombinācija';
$_['column_sku'] 					= 'SKU';
$_['column_amazon_sku'] 			= 'Amazon prece SKU';

//Entry
$_['entry_sku'] = 'SKU:';
$_['entry_condition'] = 'Stāvoklis:';
$_['entry_condition_note'] = 'Stāvokļa piezīmes:';
$_['entry_price'] = 'Cena:';
$_['entry_sale_price'] = 'Izpārdošanas cena:';
$_['entry_sale_date'] 				= 'Sale date range';
$_['entry_quantity'] = 'Daudzums:';
$_['entry_start_selling'] = 'Datums, no kura pieejams:';
$_['entry_restock_date'] = 'Atjaunošanas datums:';
$_['entry_country_of_origin'] = 'Izcelsmes valsts:';
$_['entry_release_date'] = 'Klajā laišanas datums:';
$_['entry_from'] = 'Perioda sākums';
$_['entry_to'] = 'Perioda beigas';
$_['entry_product'] 				= 'Listing for product';
$_['entry_category'] 				= 'Amazon kategorija';

//Tabs
$_['tab_main'] 						= 'Galvenā';
$_['tab_required'] 					= 'Pieprasītā informācija';
$_['tab_additional'] 				= 'Papildu darbības';

//Errors
$_['error_text_missing'] = 'Jums jāievada meklēšanas atslēgas vārds';
$_['error_data_missing'] = 'Pieprasītie dati ir pazuduši';
$_['error_missing_asin'] = 'ASIN ir pazudis';
$_['error_marketplace_missing'] = 'Lūdzu, atlasiet tirgus vietu';
$_['error_condition_missing'] = "Lūdzu, atlasiet stāvokli";
$_['error_fetch'] = 'Nevar saņemt datus';
$_['error_amazonus_price'] = 'Nevar saņemt cenu no Amazon US';
$_['error_stock'] = 'Jūs nevarat iekļaut sarakstā preci, kura krājumos ir mazāk par vienu vienību';
$_['error_sku'] = 'Jums jāievada preces SKU';
$_['error_price'] = 'Jums jāreģistrē preces cena';
$_['error_connecting'] 				= 'Uzmanību: Radās problēma pieslēgties API serveriem. Lūdzu, pārbaudiet savus "OpenBay Pro" Amazon US paplašinājuma iestatījumus. Ja problēma saglabājas, lūdzu, sazinieties ar tehnisko atbalstu.';
$_['error_required'] 				= 'Šis lauks ir obligāti aizpildāms!';
$_['error_not_saved'] 				= 'Saraksts netika saglabāts. Pārvadiet datu ievadi.';
$_['error_char_limit'] 				= 'rakstu zīmes pārsniedz ierobežojumu.';
$_['error_length'] 					= 'Mazākais iespējamais garums ir';
$_['error_upload_failed'] 			= 'Neizdevās augšupielādēt produktu ar SKU: "%". Iemesls: "%" Augšupielādēšanas process atcelts.';
$_['error_load_nodes'] 				= 'Unable to load browse nodes';
$_['error_not_searched'] 			= 'Search for matching items before you try to list. Items must be matched against an Amazon catalog item';
?>
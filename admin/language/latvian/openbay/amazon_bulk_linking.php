<?php
// Heading
$_['heading_title'] = 'Masveida sasaistīšana';
$_['text_openbay'] = 'OpenBay Pro';
$_['text_amazon'] = 'Amazon';

// Button
$_['button_load'] = 'Ielādēt';
$_['button_link'] = 'Saite';

// Text
$_['text_local'] = 'Local';
$_['text_load_listings'] = 'Ielādēt Jūsu sarakstus no Amazon';
$_['text_report_requested'] = 'Veiksmīgi pieprasīts saraksta ziņojums no Amazon';
$_['text_report_request_failed'] = 'Nevar pieprasīt saraksta ziņojumu';
$_['text_loading'] 					= 'Loading items';
$_['text_choose_marketplace'] 		= 'Choose marketplace';
$_['text_uk'] = 'Apvienotā Karaliste';
$_['text_de'] = 'Vācija';
$_['text_fr'] = 'Francija';
$_['text_it'] = 'Itālija';
$_['text_es'] = 'Spānija';

// Column
$_['column_asin'] = "ASIN";
$_['column_price'] = "Cena";
$_['column_name'] = "Nosaukums";
$_['column_sku'] = "SKU";
$_['column_quantity'] = "Daudzums";
$_['column_combination'] = "Kombinācija";

// Error
$_['error_bulk_link_permission'] 	= 'Bulk linking is not allowed on your plan, please upgrade';
?>
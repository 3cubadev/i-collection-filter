<?php
// Heading
$_['heading_title']             = 'eBay pārskats';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Text
$_['text_use_desc']             = 'Šī ir jūsu eBay pārskata lapa. Tas ir ātrs momentuzņēmums jebkuriem jūsu  konta ierobežojumiem kopā ar jūsu DSR pārdošanas rādītājiem.';
$_['text_ebay_limit_head']      = 'Jūsu eBay kontam ir pārdošanas ierobežojumi!';
$_['text_ebay_limit_t1']        = 'Jūs varat pārdot';
$_['text_ebay_limit_t2']        = 'more items (this is the total amount of items, not individual listings) to the value of';
$_['text_ebay_limit_t3']        = 'Kad jūs mēģināsiet veidot jaunus sarakastus, tas neizdosies, ja daudzums iepriekš būs pārsniegts.';
$_['text_as_described']         = 'Item as described';
$_['text_communication']        = 'Saziņa';
$_['text_shippingtime']         = 'Sūtīšanas laiks';
$_['text_shipping_charge']      = 'Sūtīšanas maksa';
$_['text_score']                = 'Rēķins';
$_['text_count']                = 'Count';
$_['text_report_30']            = '30 dienas';
$_['text_report_52']            = '52 weeks';
$_['text_title_dsr']            = 'DSR reports';
$_['text_failed']               = 'Failed to load';

// Error
$_['error_validation']       = 'Jums jāreģistrējas jūsu API token un moduļa ieslēgšanai.';
$_['error_ajax_load']        = 'Atvainojiet, savienojums ar serveri neizdevās';
?>
<?php
// Heading
$_['heading_title']             = 'Abonēšana';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Buttons
$_['button_plan_change']    = 'Mainīt plānu';

// Columns
$_['column_plan']      = 'Plāna nosaukums';
$_['column_call_limit']    = 'Signāla ierobežojums';
$_['column_price']     = 'Cena (EUR/mēnesī)';
$_['column_description']    = 'Apraksts';
$_['column_current']     = 'Pašreizējais plāns';

// Text
$_['text_subscription_current'] = 'Pašreizējais plāns';
$_['text_subscription_avail']   = 'Pieejamie plāni';
$_['text_subscription_avail1']  = 'Changing plans will be immediate and unused calls will not be credited.';
$_['text_subscription_avail2']  = 'Lai pazeminātu īldz pamata plānam, lūdzu, pārtrauciet PayPal abonēšanu.';
$_['text_ajax_acc_load_plan']   = 'PayPal abonēšanas ID: ';
$_['text_ajax_acc_load_plan2']  = ', jums jāatceļ VISAS pārējās mūsu abonēšanas';
$_['text_load_my_plan']         = 'Loading your plan';
$_['text_load_plans']           = 'Loading available plans';

// Errors
$_['error_ajax_load']        = 'Atvainojiet, nav iespējams saņemt atbildi. Mēģiniet vēlāk.';
?>
<?php
// Heading
$_['heading_title']                     = 'Profili';
$_['text_openbay']                      = 'OpenBay Pro';
$_['text_ebay']                         = 'eBay';

//Tabs
$_['tab_returns']              = 'Sūtīšana atpakaļ';
$_['tab_template']             = 'Ādiņa';
$_['tab_gallery']              = 'Galerija';
$_['tab_settings']             = 'Iestatījumi';

//Shipping Profile
$_['text_shipping_dispatch_country']    = 'Piegāde no valsts';
$_['text_shipping_postcode']            = 'Pasta/Zip koda atrašanās vieta';
$_['text_shipping_location']            = 'Pilsētas vai valsts atrašanās vieta';
$_['text_shipping_despatch']            = 'Nosūtīšanas laiks';
$_['text_shipping_despatch_help']       = 'Lielākais iespējamais sūtīšanas dienu skaits';
$_['text_shipping_nat']                 = 'Nacionālie piegādes servisi';
$_['text_shipping_intnat']              = 'Starptautiskie piegādes servisi';
$_['text_shipping_first']               = 'Pirmā prece';
$_['text_shipping_add']                 = 'Nākamās preces: ';
$_['text_shipping_service']             = 'Serviss: ';
$_['text_shipping_in_desc']             = 'Kravas info aprakstā';
$_['text_shipping_getitfast']           = 'Saņemt ātrāk!';
$_['text_shipping_zones']               = 'Piegāde zonās';
$_['text_shipping_worldwide']           = 'Visā pasaulē';
$_['text_shipping_type_nat']            = 'National shipping type';
$_['text_shipping_type_int']            = 'International shipping type';
$_['text_shipping_flat']             = 'Flat rate';
$_['text_shipping_calculated']          = 'Calculated';
$_['text_shipping_freight']           = 'Freight';
$_['text_shipping_handling']           = 'Handling fee';
$_['text_shipping_cod']             = 'Cash on delivery fee';
$_['text_shipping_handling_nat']     = 'Handling fee (national)';
$_['entry_shipping_handling_int']     = 'Handling fee (international)';

//Returns profile
$_['text_returns_accept']         = 'Sūtīšana atpakaļ akceptēta';
$_['text_returns_inst']           = 'Atpakaļ sūtīšanas polise';
$_['text_returns_days']           = 'Atpakaļ sūtīšanas dienas';
$_['text_returns_days10']         = '10 dienas';
$_['text_returns_days14']         = '14 dienas';
$_['text_returns_days30']         = '30 dienas';
$_['text_returns_days60']         = '60 dienas';
$_['text_returns_type']           = 'Atpakaļ sūtīšanas veids';
$_['text_returns_type_money']     = 'Naudas atmaksa';
$_['text_returns_type_exch']      = 'Naudas atmaksa vai apmaiņa';
$_['text_returns_costs']          = 'Atmaksāt piegādes maksu';
$_['text_returns_costs_b']        = 'Pircēja maksājumi';
$_['text_returns_costs_s']        = 'Tirgotāja maksājumi';
$_['text_returns_restock']        = 'Krājumu atjaunošasas maksa';

//Template profile
$_['text_template_choose']        = 'Noklusējuma ādiņa';
$_['text_template_choose_help']   = 'Noklusējuma ādiņa ielādēsies automātiski saraksta saglabāšanas laikā';
$_['text_image_gallery']          = 'Galerijas attēlu izmērs';
$_['text_image_gallery_help']     = 'Galeriju attēlu izmērs pikseļos, kas pievienots Jūsu ādiņai.';
$_['text_image_thumb']            = 'Sīktēla (thumbnail) attēla izmērs';
$_['text_image_thumb_help']       = 'Sīktēla (thumbnail) attēla izmērs pikseļos, kas pievienots Jūsu ādiņai.';
$_['text_image_super']            = '"Supersize" attēli';
$_['text_image_gallery_plus']     = '"Gallery plus"';
$_['text_image_all_ebay']         = 'Pievienot visus eBay attēlus';
$_['text_image_all_template']     = 'Pievienot visus attēlus ādiņai';
$_['text_image_exclude_default']  = 'Izslēgtais noklusējuma attēls';
$_['text_image_exclude_default_help'] = 'Only for bulk listing feature! Will not include the default product image in theme image list';
$_['text_confirm_delete']         = 'Vai esat pārliecināts, ka vēlaties dzēst profilu?';
$_['text_width']           = 'Width';
$_['text_height']           = 'Height';
$_['text_px']            = 'px';

//General profile
$_['text_general_private']        = 'Preču saraksts kā privātai izsolei';
$_['text_general_price']          = 'Cenas pārveidošana %';
$_['text_general_price_help']     = '0 ir noklusējuma, -10 tiks reducēts par  10%, 10 tiks palielināts par 10% (izmantots tikai masveida eksportēšanai)';

//General profile options
$_['text_profile_name']           = 'Nosaukums';
$_['text_profile_default']        = 'Noklusējums';
$_['text_profile_type']           = 'Veids';
$_['text_profile_desc']           = 'Apraksts';
$_['text_profile_action']         = 'Darbība';

// Profile types
$_['text_type_shipping']          = 'Piegāde';
$_['text_type_returns']          = 'Sūtīšana atpakaļ';
$_['text_type_template']          = 'Ādiņa &amp; Galerija';
$_['text_type_general']          = 'Iestatījumi';

//Success messages
$_['text_added']                  = 'Pievienots jauns profils';
$_['text_updated']                = 'Profils ir atjaunots';

//Errors
$_['error_permission']           = 'Jums nav atļaujas rediģēt profilus';
$_['error_name']               = 'Jums jāievada profila nosaukums';
$_['error_no_template']            = 'Ādiņas ID neeksistē';
$_['error_missing_settings']    = 'Jūs nevarat pievienot, rediģēt vai dzēst profilus, kamēr tiek sinhronizēti eBay iestatījumi';
?>
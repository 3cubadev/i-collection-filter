<?php
//Headings
$_['heading_title']        				= 'Stock updates';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon US';

//Text
$_['text_empty']                    	= 'Nav rezultātu!';

// Entry
$_['entry_date_start']               	= 'Sākuma datums';
$_['entry_date_end']                 	= 'Beigu datums';

// Column
$_['column_ref']                      	= 'Ref';
$_['column_date_requested']           	= 'Pieprasījuma datums';
$_['column_date_updated']             	= 'Atjaunināšanas datums';
$_['column_status']                   	= 'Stāvoklis';
$_['column_sku']                      	= 'Amazon SKU';
$_['column_stock']                    	= 'Krājums';
?>
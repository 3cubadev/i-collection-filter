<?php
//Heading
$_['text_page_title']               = 'Masveida eksportēšana';
$_['text_ebay']                 = 'eBay';
$_['text_openbay']                = 'Openbay Pro';

// Buttons
$_['text_none']                     = 'Neviens';
$_['text_preview']                  = 'Pārbaudīt';
$_['text_add']                      = 'Pievienot';
$_['text_preview_all']              = 'Pārbaudīt visu';
$_['text_submit']                   = 'Iesniegt';
$_['text_features']                 = 'Rediģēt iezīmes';
$_['text_catalog']                  = 'Atlasīt katalogu';
$_['text_catalog_search']           = 'Meklēt katalogu';
$_['text_search_term']            = 'Search term';
$_['text_close']              = 'Close';

//Form options / text
$_['text_pixels']                   = 'Pixels';
$_['text_other']                    = 'Other';

//Profile names
$_['text_profile']              = 'Profiles';
$_['text_profile_theme']            = 'Tēmas profils:';
$_['text_profile_shipping']         = 'Piegādes profils:';
$_['text_profile_returns']          = 'Atpakaļ sūtīšanas profils:';
$_['text_profile_generic']          = 'Vispārējais profils:';

//Text
$_['text_title']                    = 'Virsraksts';
$_['text_price']                    = 'Cena';
$_['text_stock']                    = 'Krājumi';
$_['text_search']                   = 'Meklēt';
$_['text_loading']                  = 'Ielādēt detaļas';
$_['text_preparing0']               = 'Sagatavot';
$_['text_preparing1']               = 'No';
$_['text_preparing2']               = 'Elementi';
$_['entry_condition']                = 'Condition';
$_['text_duration']                 = 'Ilgums:';
$_['text_category']                 = 'Kategorija:';
$_['text_exists']                   = 'Dažas preces jau ir eBay sarakstā, tātad ir bijušas noņemtas';
$_['text_error_count']              = 'Jūs esat atlasījis preces, datu apstrādāsāna var prasīt zināmu laiku';
$_['text_verifying']                = 'Pārbauda preces';
$_['text_processing']               = 'Apstrādā <span id="activeItems"></span> preces';
$_['text_listed']                   = 'Prece ir sarakstā! ID: ';
$_['text_ajax_confirm_listing']     = 'Vai esat pārliecināts, ka vēlaties šis preces eksportēt masveidā?';
$_['text_bulk_plan_error']          = 'Jūsu pašreizējais plāns neļauj masveida ielādēšanu, paaugstieniet savu plānu <a href="%s">šeit</a>';
$_['text_item_limit']               = 'Jūs nevarat iekļaut sarakstā preces, lai nepārsniegtu Jūsu plāna ierobežojumus, paaugstiniet savu plānu <a href="%s">šeit</a>';
$_['text_search_text']              = 'Ievadiet meklējamo tekstu';
$_['text_catalog_no_products']      = 'Preci nevar atrast katalogā';
$_['text_search_failed']            = 'Meklēšanas kļūda';
$_['text_esc_key']                  = 'Lapa ir noslēpta, bet ielādēšana nav beigusies';
$_['text_loading_categories']       = 'Loading categories';
$_['text_loading_condition']        = 'Loading product conditions';
$_['text_loading_duration']         = 'Loading listing durations';
$_['text_total_fee']           = 'Total fees';
$_['text_category_choose']          = 'Find category';
$_['text_suggested']           = 'Suggested categories';

//Errors
$_['text_error_ship_profile']       = 'Jums jāieslēdz noklusējuma piegādes profils';
$_['text_error_generic_profile']    = 'Jums jāieslēdz noklusējuma vispārējais profils';
$_['text_error_return_profile']     = 'Jums jāieslēdz noklusējuma atpakaļ sūtīšanas profils';
$_['text_error_theme_profile']      = 'Jums jāieslēdz noklusējuma tēmas profils';
$_['text_error_variants']           = 'Preces ar variācijām nevar eksportēt masveidā un nav atlasītas';
$_['text_error_stock']              = 'Dažas preces nav krājumos un tika noņemtas';
$_['text_error_no_product']         = 'Nav atlasīts piemērots produkts, lai izmantotu masveida ielādēšanu';
$_['text_error_reverify']           = 'Radusies kļūda, Jums jārediģē un atkārtoti jāpārbauda preces';
$_['error_missing_settings']   = 'You cannot bulk list items until you syncronise your eBay settings';
$_['text_error_no_selection']    = 'You must select at least 1 item to list';
?>
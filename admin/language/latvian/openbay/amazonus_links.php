<?php
// Heading
$_['heading_title']					= 'Item links';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_amazon']					= 'Amazon EU';

// Text
$_['text_desc1']                    = 'Preču sasaistīšana atļaus kontrolēt krājumus Jūsu Amazon US sarakstā.';
$_['text_desc2'] 					= 'Katrai precei, kas atjaunota lokālajā krājumā (krājumi pieejami Jūsu "Opencart" veikalā), tiks atjaunoti dati Jūsu Amazon US sarakstā';
$_['text_desc3']                    = 'Jūs varat saistīt preces manuāli, ievadot Amazon SKU un produkta nosaukums vai arī ielādējot visus nesaistītos produktusun tad ievadot Amazonus SKU. (Produktu pievienošana Amazonus no "Opencart" notiks automātiski, pievienojot saites.)';
$_['text_new_link']                 = 'Jauna saite';
$_['text_autocomplete_product']     = 'Product (Auto complete from name)';
$_['text_amazon_sku']               = 'Amazon prece SKU';
$_['text_action']                   = 'Akcija';
$_['text_linked_items']             = 'Saistītās preces';
$_['text_unlinked_items']           = 'Nesaistītās preces';
$_['text_name']                     = 'Nosaukums';
$_['text_model']                    = 'Modelis';
$_['text_combination']              = 'Kombinācija';
$_['text_sku']                      = 'SKU';
$_['text_amazon_sku']               = 'Amazon prece SKU';

// Button
$_['button_load']                 	= 'Ielādēt';

// Error
$_['error_empty_sku']        		= 'Amazon SKU lauks nedrīkst būt tukšs!';
$_['error_empty_name']       		= 'Produkta nosaukuma lauks nedrīkst būt tukšs!';
$_['error_no_product_exists']       = 'Produkts neeksistē. Lūdzu, izmantojiet absolūtās vērtības.';
?>
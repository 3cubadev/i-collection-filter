<?php
// Heading
$_['heading_title']     = 'Revise eBay listing';
$_['text_openbay']     = 'OpenBay Pro';
$_['text_ebay']      = 'eBay';

// Text
$_['text_revise']                = 'Revise listing';
$_['text_loading']                  = 'Saņemt preces informāciju no eBay';
$_['text_error_loading']            = 'Saņemot informāciju no eBay, radās kļūda';
$_['text_saved']                    = 'Saraksts ir saglabāts';
$_['text_alert_removed']            = 'Saraksts ir atsaistīts';
$_['text_alert_ended']              = 'Saraksts vairs nav eBay';

// Buttons
$_['button_view']     = 'Skatīt sarakstu';
$_['button_remove']     = 'Noņemt linku';
$_['button_end']                    = 'Beigt sarakstu';
$_['button_retry']     = 'Pārskatīt';


$_['entry_title']     = 'Virsraksts';
$_['entry_price']     = 'Cena(Iekļaujot nodokļus)';
$_['entry_stock_store']    = 'Local stock';
$_['entry_stock_listed']   = 'eBay stock';
$_['entry_stock_reserve']   = 'Rezerves līmenis';
$_['entry_stock_matrix_active']  = 'Stock matrix (aktīvs)';
$_['entry_stock_matrix_inactive'] = 'Stock matrix (neaktīvs)';

// Column
$_['column_sku']     = 'Var kods / SKU';
$_['column_stock_listed']   = 'Sarakstā';
$_['column_stock_reserve']   = 'Rezerves līmenis';
$_['column_stock_total']   = 'Noliktavā';
$_['column_price']     = 'Cena';
$_['column_status']     = 'Aktīvs';
$_['column_add']     = 'Pievienot';
$_['column_combination']   = 'Kombinācija';

// Help
$_['help_stock_store']    = 'Šis ir "Opencart" krājumu līmenis';
$_['help_stock_listed']    = 'Šis ir pašreizējais krājumu līmenis eBay';
$_['help_stock_reserve']   = 'Šis ir lielākais iespējamais krājumu līmenis eBay (0 = no reserve limit)';

// Error
$_['error_ended']     = 'Saistītais sarkasts ir pabeigts, nav iespējams to rediģēt. Jums jānoņem saite.';
$_['error_reserve']     = 'Jūs nevarat noteikt rezervi augstāku par vietējiem krājumiem.';
?>
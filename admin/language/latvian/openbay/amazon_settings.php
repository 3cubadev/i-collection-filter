<?php
// Heading
$_['heading_title']            = 'Marketplace settings';
$_['text_openbay']      = 'OpenBay Pro';
$_['text_amazon']      = 'Amazon EU';

// Text
$_['text_api_status']                = 'API savienojuma statuss';
$_['text_api_ok']                    = 'Savienojums pareizs, Auth pareizs';
$_['text_api_auth_error']            = 'Savienojums pareizs, Auth kļūdains';
$_['text_api_error']                 = 'Savienojuma kļūda';
$_['text_order_statuses']            = 'Pasūtījuma stāvokļi';
$_['text_unshipped']                 = 'Nav piegādāts';
$_['text_partially_shipped']         = 'Daļēji piegādāts';
$_['text_shipped']                   = 'Piegādāts';
$_['text_canceled']                  = 'Atcelts';
$_['text_other']                     = 'Cits';
$_['text_marketplaces']              = 'Tirgus vietas';
$_['text_markets']                   = 'Atlasīt tirgus, no kuriem vēlaties importēt pasūtījumus';
$_['text_de']                        = 'Vācija';
$_['text_fr']                        = 'Francija';
$_['text_it']                        = 'Itālija';
$_['text_es']                        = 'Spānija';
$_['text_uk']                        = 'Apvienotā Karaliste';
$_['text_setttings_updated']         = 'Iestatījumi ir veiksmīgi atjaunoti.';
$_['text_new']        = 'New';
$_['text_used_like_new']     = 'Used - Like New';
$_['text_used_very_good']     = 'Used - Very Good';
$_['text_used_good']      = 'Used - Good';
$_['text_used_acceptable']     = 'Used - Acceptable';
$_['text_collectible_like_new']   = 'Collectible - Like New';
$_['text_collectible_very_good']   = 'Collectible - Very Good';
$_['text_collectible_good']    = 'Collectible - Good';
$_['text_collectible_acceptable']   = 'Collectible - Acceptable';
$_['text_refurbished']      = 'Refurbished';

// Error
$_['error_permission']            = 'You do not have access to this module';

// Entry
$_['entry_status']                   = 'Status';
$_['entry_token']                     = 'Token';
$_['entry_string1']                = 'Encryption String 1';
$_['entry_string2']                = 'Encryption String 2';
$_['entry_import_tax']                = 'Tax for imported items';
$_['entry_customer_group']            = 'Customer Group';
$_['entry_tax_percentage']            = 'Modify price';
$_['entry_default_condition']         = 'Default product condition type';
$_['entry_marketplace_default']   = 'Default marketplace';
$_['entry_notify_admin']              = 'Notify admin of new order';
$_['entry_default_shipping']          = 'Default shipping';

// Tabs
$_['tab_settings']               = 'API details';
$_['tab_listing']                    = 'Listings';
$_['tab_orders']                     = 'Orders';

// Help
$_['help_import_tax']             = 'Used if Amazon does not return tax information';
$_['help_customer_group']         = 'Select a customer group to assign to imported orders';
$_['help_default_shipping']       = 'Used as the pre-selected option in the bulk order update';
$_['help_entry_marketplace_default'] = 'Default marketplace for product listings and lookups';
$_['help_tax_percentage']            = 'Percentage added to default product price';
?>
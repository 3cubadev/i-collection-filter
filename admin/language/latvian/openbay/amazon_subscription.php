<?php
// Heading
$_['heading_title']        				= 'Subscription';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_amazon']						= 'Amazon EU';

// Text
$_['text_current_plan']             	= 'Pašreizējais plāns';
$_['text_register_invite']          	= "Jums nav pilnvaras? Reģistrējies, lai saņemtu tās";
$_['text_available_plans']          	= 'Available plans';
$_['text_listings_remaining']       	= 'Atlikušie produktu saraksti';
$_['text_listings_reserved']        	= 'Produkti tiek apstrādāti';
$_['text_account_status']           	= 'Konta stāvoklis';
$_['text_merchantid']               	= 'Veikala īpašnieka ID';
$_['text_change_merchantid']        	= 'Mainīt';
$_['text_allowed']                  	= 'Atļauts';
$_['text_not_allowed']              	= 'Nav atļauts';
$_['text_price']              			= 'Cena';
$_['text_name']              			= 'Nosaukums';
$_['text_description']              	= 'Apraksts';
$_['text_order_frequency']          	= 'Pasūtijuma importēšanas biežums';
$_['text_bulk_listing']             	= 'Masveida saraksti';
$_['text_product_listings']         	= 'Jauni produktu saraksti mēnesī';

// Columns
$_['column_name']                     	= 'Nosaukums';
$_['column_description']              	= 'Apraksts';
$_['column_order_frequency']          	= 'Pasūtijuma importēšanas biežums';
$_['column_bulk_listing']             	= 'Masveida saraksti';
$_['column_product_listings']         	= 'Jauni produktu saraksti mēnesī';
$_['column_price']                    	= 'Cena';

// Buttons
$_['button_change_plan']              	= 'Mainīt plānu';
$_['button_register']                 	= 'Reģistrēties';
?>
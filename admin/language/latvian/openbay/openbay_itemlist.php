<?php
// Heading
$_['heading_title'] 					= 'Manage listings';

// Text
$_['text_markets']                  	= 'Markets';
$_['text_openbay']                  	= 'OpenBay Pro';
$_['text_ebay'] 						= 'eBay';
$_['text_amazon'] 						= 'Amazon EU';
$_['text_amazonus'] 					= 'Amazon US';
$_['text_etsy'] 						= 'Etsy';
$_['text_status_all'] 					= 'Visi';
$_['text_status_ebay_active'] 			= 'eBay aktīvs';
$_['text_status_ebay_inactive'] 		= 'eBay neaktīvs';
$_['text_status_amazoneu_saved'] 		= 'Amazon EU saglabāt';
$_['text_status_amazoneu_processing'] 	= 'Amazon EU apstrādā';
$_['text_status_amazoneu_active'] 		= 'Amazon EU aktīvs';
$_['text_status_amazoneu_notlisted'] 	= 'Amazon EU not listed';
$_['text_status_amazoneu_failed'] 		= 'Amazon EU failed';
$_['text_status_amazoneu_linked'] 		= 'Amazon EU savienots';
$_['text_status_amazoneu_notlinked'] 	= 'Amazon EU nav savienots';
$_['text_status_amazonus_saved'] 		= 'Amazon US saglabāts';
$_['text_status_amazonus_processing'] 	= 'Amazon US apstrādā';
$_['text_status_amazonus_active'] 		= 'Amazon US aktīvs';
$_['text_status_amazonus_notlisted'] 	= 'Amazon US nav sarakstā';
$_['text_status_amazonus_failed'] 		= 'Amazon US failed';
$_['text_status_amazonus_linked'] 		= 'Amazon US saistīts';
$_['text_status_amazonus_notlinked'] 	= 'Amazon US nav saistīts';
$_['text_processing']       			= 'Processing';
$_['text_category_missing'] 			= 'Trūkstošā kategorija';
$_['text_variations'] 					= 'Variācijas';
$_['text_variations_stock'] 			= 'Krājums';
$_['text_min']                      	= 'Min';
$_['text_max']                      	= 'Max';
$_['text_option']                   	= 'Option';

// Entry
$_['entry_title'] 						= 'Virsraksts';
$_['entry_model'] 						= 'Modelis';
$_['entry_manufacturer'] 				= 'Ražotājs';
$_['entry_status'] 						= 'Stāvoklis';
$_['entry_status_marketplace'] 			= 'Tirgus vietas stāvoklis';
$_['entry_stock_range'] 				= 'Krājuma range';
$_['entry_category'] 					= 'Kategorija';
$_['entry_populated'] 					= 'Novietojums';
$_['entry_sku'] 						= 'SKU';
$_['entry_description'] 				= 'Apraksts';

// Button
$_['button_error_fix']              	= 'Fix errors';
$_['button_amazon_eu_bulk']         	= 'Amazon EU bulk upload';
$_['button_amazon_us_bulk']         	= 'Amazon US bulk upload';
$_['button_ebay_bulk']              	= 'eBay bulk upload';

// Error
$_['error_select_items']            	= 'You must select at least 1 item to bulk list';
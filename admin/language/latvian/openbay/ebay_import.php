<?php
// Heading
$_['heading_title']                     = 'Preces importēšana';
$_['text_openbay']                      = 'OpenBay Pro';
$_['text_ebay']                         = 'eBay';

// Text
$_['text_sync_import_line1']            = '<strong>Uzmanību!</strong> Jūsu veikalā tiks importēti visi Jūsu eBay produkti un īzveidota kategoriju struktūra. Ieteicams izdzēst visas kategorijas un produktus pirms palaišanas. <br />Kategorijas struktūra ir kā parastai eBay kategorijai, nevis Jūsu veikala kategorijai (ja Jums ir eBay veikals). Jūs varat atjaunot, noņemt un rediģēt importētās kategorijas bez ietekmes uz Jūsu eBay prodduktiem.';
$_['text_sync_import_line3']            = 'Jums jāpārliecinās, ka Jūsuserveris var akceptēt un apstrādāt liela izmēra POST datus. 1000 eBay preču izmērs ir apmēram 40Mb, Jums jāaprēķina, cik daudz varat pieprasīt. Ja signāls neizdosies, tad, iespējams, tas ir tāpēc, ka iestatījumi ir par mazu. Jūsu PHP atmiņas ierobežojumam jābūt apmēram 128Mb.';
$_['text_sync_server_size']             = 'Šobrīd Jūsu serveris var akceptēt: ';
$_['text_sync_memory_size']             = 'Jūsu PHP atmiņas ierobežojums: ';
$_['text_import_confirm']    = 'Tiks importētas visas Jūsu eBay preces kā jauni produkti, vai esat pārliecināts? To NEBŪS IESPĒJAMS atsaukt! Vispirms PĀRLIECINIETIES, ka Jums ir rezerves kopija!';
$_['text_import_notify']    = 'Jūsu importēšanas pieprasījums nosūtīts apstrādei. Importēšana 1000 precēm aizņems apmēram vienu stundu.';
$_['text_import_images_msg1']           = 'Notiek attēlu importēšana/kopēšana no eBay. Pārlādējiet lapu, ja numurs nesamazinās';
$_['text_import_images_msg2']           = 'Nospiest šeit';
$_['text_import_images_msg3']           = 'un gaidīt. Vairāk informēcijas par notikušā iemesliem var uzzināt <a href="http://shop.openbaypro.com/index.php?route=information/faq&topic=8_45" target="_blank">here</a>';

// Entry
$_['entry_import_item_advanced']        = 'Get advanced data';
$_['entry_import_categories']         	= 'Import categories';
$_['entry_import_description']			= 'Importēt preču aprakstus';
$_['entry_import']      = 'Importēt eBay preces';

// Buttons
$_['button_import']      = 'Importēt';

// Help
$_['help_import_item_advanced']         = 'Will take up to 10 times longer to import items. Imports weights, sizes, ISBN and more if available';
$_['help_import_categories']          = 'Builds a category structure in your store from the eBay categories';
$_['help_import_description']          = 'Tas importēs visu including HTML, counters etc';

// Error
$_['error_import']                    = 'Failed to load';
$_['error_maintenance']     = 'Jūsu veikals ir uzturēšanas režīmā. Importēšana nenotiks!';
$_['error_ajax_load']     = 'Kļūdaina pieslēgšanās serverim';
$_['error_validation']     = 'You need to register for your API token and enable the module.';
?>
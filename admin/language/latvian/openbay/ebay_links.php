<?php
// Heading
$_['heading_title']           = 'Preces saites';
$_['text_openbay']     = 'OpenBay Pro';
$_['text_ebay']      = 'eBay';

// Buttons
$_['button_resync']                = 'Atkārtota sinhronizācija';
$_['button_check_unlinked']        = 'Pārbaudīt nesaistītās preces';
$_['button_remove_link']         = 'Izņemt saiti';

// Errors
$_['error_ajax_load']            = 'Atvainojiet, nevar saņemt atbildi. Mēģiniet vēlāk.';
$_['error_validation']           = 'Jums jāreģistrējas API pazīmei un moduļa ieslēgšanai.';
$_['error_no_listings']        = 'Nav atrasti saistītie produkti';
$_['error_link_value']           = 'Produkta saite nav derīga';
$_['error_link_no_stock']      = 'Saiti nevar izveidot ar preci, kas nav krājumāitem. Pārtraukt manuāli preces rādīšanu eBay.';
$_['error_subtract_setting']        = 'This product is set not to subtract stock in OpenCart.';

// Text
$_['text_linked_items']             = 'Saistītās preces';
$_['text_unlinked_items']           = 'Nesaistītās preces';
$_['text_alert_stock_local']        = 'Jūsu eBay saraksti tiks atjaunoti ar Jūsu lokālo krājumu līmeni!';
$_['text_link_desc1']               = 'Preču saistīšana tiks atļauta krājumu kontrolei Jūsu eBay sarakstos.';
$_['text_link_desc2']               = 'Katrai precei, kas atjaunota lokālajos krājumos (krājumi pieejami Jūsu "Opencart" veikalā), tiks atjaunoti eBay saraksti';
$_['text_link_desc3']               = 'Jūsu lokālie krājumi ir tie krājumi, kuri pieejami pārdošanai. Jūsu eBay krājumu līmenim jābūt saskaņotiem ar to.';
$_['text_link_desc4']               = 'Jūsu lokālie krājumi ir preces, kuras jau ir pārdotas, bet par kurām vēl nav samaksāts. Šīs preces ir atliktas un nav ierēķinātas pieejamajos krājumos.';
$_['text_text_linked_desc']         = 'Saistītās preces ir "OpenCart" preces, kurām ir saite uz eBay sarakstu.';
$_['text_text_unlinked_desc']       = 'Nesaistītās preces ir saraksti Jūsu eBay kontā, kuri neveido saiti uz katru "OpenCart" produktu.';
$_['text_text_unlinked_info']       = 'Nospiediet pārbaudīto, nesaistīto preču pogu, lai meklētu savus aktīvos eBay sarakstus nesaistītajām precēm. Ja Jums ir daudz eBay sarakstu, tas varētu prasīt ilgu laiku.';
$_['text_text_loading_items']       = 'Ielādē preces';
$_['text_failed']          = 'Failed to load';
$_['text_limit_reached']         = 'The maximum number of checks per request was reached, click the button to continue searching';
$_['text_stock_error']         = 'Stock error';
$_['text_listing_ended']         = 'Listing ended';
$_['text_filter']               = 'Filter results';
$_['text_filter_title']             = 'Title';
$_['text_filter_range']             = 'Stock range';
$_['text_filter_range_from']        = 'Min';
$_['text_filter_range_to']          = 'Max';
$_['text_filter_var']              = 'Include variants';

// Tables
$_['column_action']              = 'Akcija';
$_['column_status']              = 'Stāvoklis';
$_['column_variants']            = 'Variants';
$_['column_item_id']             = 'eBay preces ID';
$_['column_product']             = 'Produkts';
$_['column_product_auto']        = 'Produkts(Automātiski pabeigts pēc nosaukuma)';
$_['column_listing_title']       = 'eBay Saraksta nosaukums';
$_['column_allocated']           = 'Lokalizētie krājumi';
$_['column_ebay_stock']          = 'eBay krājumi';
$_['column_stock_available']     = 'Shop stock';
$_['column_stock_reserve']     = 'Reserve level';
?>
<?php
//Headings
$_['heading_title'] 				= 'Jauns Amazon saraksts';
$_['text_title_advanced'] 			= 'Advanced listing';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon EU';

//Buttons
$_['button_new']                    = 'Izveidot jaunu produktu';
$_['button_amazon_price']           = "Saņemt Amazon cenu";
$_['button_list']                   = "Amazon saraksts";
$_['button_remove_error'] 			= 'Remove error messages';
$_['button_save_upload'] 			= 'Save and upload';
$_['button_browse'] 				= 'Browse';
$_['button_saved_listings'] 		= 'View saved listings';
$_['button_remove_links'] 			= 'Remove links';
$_['button_create_new_listing'] 	= 'Create new listing';

// Help
$_['help_sku'] 						= "Unique product's ID assigned by the merchant";
$_['help_restock_date'] 			= "This is the date you will be able to ship any back-ordered items to a customer. This date should not be greater than 30 days from the date listed or orders received may automatically be canceled.";
$_['help_sale_price'] 				= "Sale Price must have a start and end date";

//Text
$_['text_products_sent'] = 'Produkts nosūtīts apstrādei';
$_['button_view_on_amazon'] 		= 'Amazon skats';
$_['text_list']                     = 'Saraksts';
$_['text_new'] = 'Jauns';
$_['text_used_like_new'] = 'Izmantots - Kā jauns';
$_['text_used_very_good'] = 'Izmantots - Ļoti labi';
$_['text_used_good'] = 'Izmantots - Labi';
$_['text_used_acceptable'] = 'Izmantots - Akceptējams';
$_['text_collectible_like_new'] = 'Kolekcionējams - Kā jauns';
$_['text_collectible_very_good'] = 'Kolekcionējams - Ļoti labi';
$_['text_collectible_good'] = 'Kolekcionējams - Labi';
$_['text_collectible_acceptable'] = 'Kolekcionējams - Akceptējams';
$_['text_refurbished'] = 'Atjaunots';
$_['text_product_not_sent'] 		= 'Product was not sent to Amazon. Reason: %s';
$_['text_not_in_catalog'] 			= "Or, if it is not in the catalog&nbsp;&nbsp;&nbsp;";
$_['text_placeholder_search'] 		= 'Enter product name, UPC, EAN, ISBN or ASIN';
$_['text_placeholder_condition'] 	= 'Use this box to describe the condition of your products.';
$_['text_characters'] 				= 'characters';
$_['text_uploaded'] 				= 'Saved listing(s) uploaded!';
$_['text_saved_local'] 				= 'Listing saved but not uploaded yet';
$_['text_product_sent'] 			= 'Product was successfully sent to Amazon.';
$_['text_links_removed'] 			= 'Amazon product links deleted';
$_['text_product_links'] 			= 'Product links';
$_['text_has_saved_listings'] 		= 'This product has one or more saved listings that is not uploaded';
$_['text_edit_heading'] 			= 'Edit listing';
$_['text_germany'] 					= 'Germany';
$_['text_france'] 					= 'France';
$_['text_italy'] 					= 'Italy';
$_['text_spain'] 					= 'Spain';
$_['text_united_kingdom'] 			= 'United Kingdom';

//Table columns
$_['column_image'] = 'Attēls';
$_['column_asin'] = 'ASIN';
$_['column_price'] = 'Cena';
$_['column_action'] = 'Akcija';
$_['column_name'] = 'Produkta nosaukums';
$_['column_model'] = 'Modelis';
$_['column_combination'] = 'Kombinācija';
$_['column_sku'] = 'SKU';
$_['column_amazon_sku'] = 'Amazon prece SKU';

//Form entry
$_['entry_sku'] = 'SKU:';
$_['entry_condition'] = 'Stāvoklis:';
$_['entry_condition_note'] = 'Stāvokļa piezīme:';
$_['entry_price'] = 'Cena:';
$_['entry_sale_price'] = 'Izpārdošanas cena:';
$_['entry_sale_date'] 				= 'Sale date range';
$_['entry_quantity'] = 'Daudzums:';
$_['entry_start_selling'] = 'Pieejams no:';
$_['entry_restock_date'] = 'Atjaunošanas datums:';
$_['entry_country_of_origin'] = 'Izcelsmes valsts:';
$_['entry_release_date'] = 'Klajā laišanas datums:';
$_['entry_from'] = 'Datums no';
$_['entry_to'] = 'Datums līdz';
$_['entry_product'] 				= 'Listing for product';
$_['entry_category'] 				= 'Amazon category';
$_['entry_browse_node'] 			= 'Choose browse node';
$_['entry_marketplace'] 			= 'Marketplace';

//Tabs
$_['tab_main'] 						= 'Galvenā';
$_['tab_required'] 					= 'Required info';
$_['tab_additional'] 				= 'Additional options';

//Errors
$_['error_required'] 				= 'This field is required!';
$_['error_not_saved'] 				= 'Listing was not saved. Check you have filled in all fields';
$_['error_char_limit'] 				= 'characters over the limit';
$_['error_length'] 					= 'Minimum length is';
$_['error_upload_failed'] 			= 'Failed uploading product with SKU: "%s". Reason: "%s" Uploading process canceled.';
$_['error_load_nodes'] 				= 'Unable to load browse nodes';
$_['error_connecting'] 				= 'There was problem connecting to the API. Please check your OpenBay Pro Amazon extension settings. If the problem persists, please contact support.';
$_['error_text_missing']            = 'Jums jāievada meklēšanas detaļas';
$_['error_missing_asin']            = 'ASIN trūkst';
$_['error_marketplace_missing'] = 'Lūdzu, atlasiet tirgus vietu';
$_['error_condition_missing'] = "Lūdzu, atlasiet stāvokli";
$_['error_amazonus_price']            = 'Nevar saņemt cenu no Amazon US';
$_['error_stock'] = 'Jūs nevarat iekļaut sarakstā preci, kura noliktavā ir mazāk par vienu vienību';
$_['error_sku'] = 'Jums jāreģistrē preces SKU';
$_['error_price'] = 'Jums jāreģistrē preces cena';
$_['error_sending_products'] 		= 'Could not send products for listing. Please contact support';
$_['error_no_products_selected'] 	= 'No products were selected for listing';
$_['error_not_searched'] 			= 'Search for matching items before you try to list. Items must be matched against an Amazon catalog item';
?>
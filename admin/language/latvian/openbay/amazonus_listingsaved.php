<?php
// Heading
$_['heading_title'] 				= 'Saglabātie saraksti';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon US';

// Text
$_['text_description']              = 'Produktu saraksti, kuri saglabāti lokāli un gatavi augšupielādēšanai Amazon US. Nospiediet "Augšupielādēt", lai nosūtītu.';
$_['text_uploaded_alert']           = 'Saglabātais(ie) saraksts(i) augšupielādēts!';
$_['text_delete_confirm']           = 'Vai esat pārliecināts?';
$_['text_complete']           		= 'Listings uploaded';

// Column
$_['column_name']              		= 'Nosaukums';
$_['column_model']             		= 'Modelis';
$_['column_sku']               		= 'SKU';
$_['column_amazon_sku']        		= 'Amazon US item SKU';
$_['column_action']           		= 'Darbība';
?>
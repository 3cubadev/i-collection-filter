<?php
// Heading
$_['heading_title']     = 'Revise eBay listing';
$_['text_openbay']     = 'OpenBay Pro';
$_['text_ebay']      = 'eBay';

// Buttons
$_['button_pull_orders']         = 'Start';

// Entry
$_['entry_pull_orders']             = 'Pull jaunus pasūtījumus';

// Text
$_['text_sync_pull_notice']         = 'Tiks pull jauns pasūtījums pēc galējās automatizētās pārbaudes. Ja vēlaties tikai instalēt, tad tas būs pēc noklusējuma 24 stundu laikā.';
$_['text_ajax_orders_import']       = 'Katrs jaunais pasūtījums parādīsies dažu minūšu laikā';
$_['text_complete']              = 'Import requested';
$_['text_failed']                = 'Atvainojiet, nevar pieslēgties';

// Errors
$_['error_validation']             = 'Jums jāreģistrējas API pazīmei un moduļa ieslēgšanai.';
?>
<?php
// Heading
$_['heading_title']            = 'Subscription';
$_['text_openbay']      = 'OpenBay Pro';
$_['text_amazon']      = 'Amazon US';

// Text
$_['text_current_plan']              = 'Pašreizējais plāns';
$_['text_register_invite']           = "Don't have your API details yet?";
$_['text_available_plans']           = 'Available plans';
$_['text_listings_remaining']        = 'Atlikušie produktu saraksti';
$_['text_listings_reserved']         = 'Produkti tiek apstrādāti';
$_['text_account_status']            = 'Konta stāvoklis';
$_['text_merchantid']                = 'Īpašnieka ID';
$_['text_change_merchantid']         = 'Mainīt';
$_['text_allowed']                   = 'Allowed';
$_['text_not_allowed']               = 'Not Allowed';
$_['text_price']                 = 'Cena';
$_['text_name']                 = 'Nosaukums';
$_['text_description']              	= 'Apraksts';
$_['text_order_frequency']          	= 'Order import frequency';
$_['text_bulk_listing']              = 'Masveida eksportēšana';
$_['text_product_listings']          = 'Jauni saraksti mēnesī';

// Columns
$_['column_name']                      = 'Nosaukums';
$_['column_description']               = 'Apraksts';
$_['column_order_frequency']           = 'Order import frequency';
$_['column_bulk_listing']              = 'Masveida eksportēšana';
$_['column_product_listings']          = 'Jauni saraksti mēnesī';
$_['column_price']                     = 'Cena';

// Buttons
$_['button_change_plan']               = 'Mainīt plānu';
$_['button_register']                  = 'Reģistrācija';
?>
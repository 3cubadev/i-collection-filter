<?php
// Heading
$_['heading_title'] = 'Masveida piesaistīšana';
$_['text_openbay'] = 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon US';

// Button
$_['button_load'] = 'Ielādēt';
$_['button_link'] = 'Saite';

// Text
$_['text_local'] = 'Lokāli';
$_['text_load_listings'] = 'ielādēt sarakstus no Amazon';
$_['text_report_requested'] = 'veiksmīgi pieprasīti sarakstu dati no Amazon';
$_['text_report_request_failed'] = 'Nevar pieprasīt sarakstu datus';
$_['text_loading'] 					= 'Loading items';

// Column
$_['column_asin'] = "ASIN";
$_['column_price'] = "Cena";
$_['column_name'] = "Nosaukums";
$_['column_sku'] = "SKU";
$_['column_quantity'] = "Daudzums";
$_['column_combination'] = "Kombinācija";

// Error
$_['error_bulk_link_permission'] 	= 'Bulk linking is not allowed on your plan, please upgrade';
?>
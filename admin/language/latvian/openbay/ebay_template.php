<?php
// Headings
$_['heading_title']                 = 'Veidņu saraksts';
$_['text_ebay']                     = 'eBay';
$_['text_openbay']                  = 'OpenBay Pro';

// Columns
$_['column_name']              = 'Nosaukums';
$_['column_action']              = 'Darbība';

// Entry
$_['entry_template_name']           = 'Nosaukums';
$_['entry_template_html']           = 'HTML';

// Text
$_['text_added']                    = 'Pievienota jauna veidne';
$_['text_updated']                  = 'Veidne atjaunināta';
$_['text_deleted']                  = 'Template has been deleted';
$_['text_confirm_delete']           = 'Vai tiešām vēlaties izdzēst veidni?';


$_['error_name']                 = 'Jums jāievada veidnes nosaukums';
$_['error_permission']             = 'Jums nav atļauts rediģēt veidnes';
$_['error_no_template']             = 'Veidnes ID neeksistē';
?>
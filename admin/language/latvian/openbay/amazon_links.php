<?php
// Heading
$_['heading_title']					= 'Preču saites';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_amazon']					= 'Amazon EU';

// Text
$_['text_desc1']                    = 'Preču saistīšana ļaus krājuma kontroli Jūsu Amazon sarakstos.';
$_['text_desc2'] 					= 'Katrai precei, kurai lokāli atjaunoti krājumi (krājumi pieejami Jūsu Opencart veikalā), tiks atjaunoti dati Jūsu Amazon sarakstā.';
$_['text_desc3']                    = 'Jūs varat sasaistīt preces manuāli, ievadot Amazon SKU un produkta nosaukumu vai ielādēt visus nesaistītos produktus un tad - Amazon SKU. (Produkti Amazon no Opencart tiks ielādēti, automātiski pievienojot linkus.)';
$_['text_new_link']                 = 'Jauna saite';
$_['text_autocomplete_product']     = 'Product';
$_['text_amazon_sku']               = 'Amazon prece SKU';
$_['text_action']                   = 'Akcija';
$_['text_linked_items']             = 'Saistītās preces';
$_['text_unlinked_items']           = 'Nesaistītās preces';
$_['text_name']                     = 'Nosaukums';
$_['text_model']                    = 'Modelis';
$_['text_combination']              = 'Kombinācija';
$_['text_sku']                      = 'SKU';
$_['text_amazon_sku']               = 'Amazon prece SKU';

// Button
$_['button_load']                 	= 'Ielādēt';

// Error
$_['error_empty_sku']        		= 'Amazon SKU nedrīkst būt tukšs!';
$_['error_empty_name']       		= 'Produkta nosaukuma lauks nedrīkst būt tukšs!';
$_['error_no_product_exists']       = 'Produkts neeksistē. Lūdzu izmantojiet absolūtās vērtības.';
?>
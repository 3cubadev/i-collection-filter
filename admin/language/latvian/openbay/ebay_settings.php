<?php
// Heading
$_['heading_title']           = 'Marketplace settings';
$_['text_openbay']     = 'OpenBay Pro';
$_['text_ebay']      = 'eBay';

// Text
$_['text_developer']    = 'Attīstītājs';
$_['text_app_settings']    = 'Izmantošanas iestatījumi';
$_['text_default_import']   = 'Noklusējuma importēšanas iestatījumi';
$_['text_payments']     = 'Maksājumi';
$_['text_notify_settings']   = 'Paziņojuma iestatījumi';
$_['text_listing']     = 'Saraksta noklusējums';
$_['text_token_register']   = 'Spiest šeit, lai reģistrētos pazīmei';
$_['text_token_renew']    = 'Spiest šeit, lai atjaunotu Jūsu pazīmi';
$_['text_application_settings']  = 'Jūsu izmantošanas iestatījumi ļauj pārveidot OpenBay darbības veidu un integrēt jūsu sistēmā.';
$_['text_import_description']  = 'Customise the status of an order during different stages. You cannot use a status on an eBay order that does not exist in this list.';
$_['text_payments_description']  = 'Pre populate your payment options for new listings, this will save you entering them for every new listing you create.';
$_['text_allocate_1']    = 'Kad pircējs iepērkas';
$_['text_allocate_2']				= 'Kad pircējs samaksājis';
$_['text_developer_description']	= 'Jums nav jālieto šis lauks, ja vien instrukcija to neapredz.';
$_['text_payment_paypal']   = 'PayPal akceptēts';
$_['text_payment_paypal_add']  = 'PayPal e-pasta adrese';
$_['text_payment_cheque']   = 'Čeks akceptēts';
$_['text_payment_card']    = 'Kartes akceptētas';
$_['text_payment_desc']    = 'Skatīt aprakstu (piemēram,  bankas pārskaitījums)';
$_['text_tax_use_listing']    = 'Izmantot eBay sarakstu nodokļu likmi';
$_['text_tax_use_value']   = 'Izmantot iestatīto valūtu visur';
$_['text_action_warning']   = 'Šī darbība ir bīstama, tādēļ aizsargāta ar paroli.';
$_['text_notifications']   = 'Kontrolēt, kad pircēji saņem lietošanas paziņojumus. E-pastu atjaunošanas ieslēgšana var uzlabot Jūsu DSR novērtējumus, kā lietotājs saņem pasūtījuma atjaunošanu.';
$_['text_listing_1day']             = '1 diena';
$_['text_listing_3day']             = '3 dienas';
$_['text_listing_5day']             = '5 dienas';
$_['text_listing_7day']             = '7 dienas';
$_['text_listing_10day']            = '10 dienas';
$_['text_listing_30day']            = '30 dienas';
$_['text_listing_gtc']              = 'GTC- Good till cancelled';
$_['text_api_status']               = 'API pieslēgšanās stāvoklis';
$_['text_api_ok']                   = 'Pieslēgšanās veiksmīga, pilnvarojums beidzies';
$_['text_api_failed']               = 'Validitāte neizdevās';
$_['text_api_other']          = 'Other actions';
$_['text_create_date_0']            = 'Kad tiek pievienots "Opencart"';
$_['text_create_date_1']            = 'Kad tiek izveidots eBay';
$_['text_obp_detail_update']        = 'Spiediet šeit, lai atjaunotu Jūsu veikala URL un kontaktinformāciju - e-pastu';
$_['text_success']     = 'Your settings have been saved';

// Entry
$_['entry_status']     = 'Stāvoklis';
$_['entry_token']     = 'Pazīme';
$_['entry_secret']     = 'Secret';
$_['entry_string1']     = 'Encryption string 1';
$_['entry_string2']     = 'Encryption string 2';
$_['entry_end_items']    = 'End items?';
$_['entry_relist_items']   = 'Atjaunot sarakstu, kad preces atkal ir krājumā?';
$_['entry_disable_soldout']   = 'Disable product when no stock?';
$_['entry_debug']     = 'Ieslēgt autorizēšanu';
$_['entry_currency']    = 'Noklusējuma valūta';
$_['entry_customer_group']   = 'Pircēju grupa';
$_['entry_stock_allocate']   = 'Lokālie krājumi';
$_['entry_created_hours']   = 'Jauna pasūtījuma vecuma ierobežojums';
$_['entry_empty_data']    = 'Empty ALL data?';
$_['entry_developer_locks']   = 'Remove order locks?';
$_['entry_payment_instruction']  = 'Maksāšanas instrukcijas';
$_['entry_payment_immediate']  = 'Pieprasīt tūlītēju maksājumu';
$_['entry_payment_types']   = 'Payment types';
$_['entry_brand_disable']   = 'Disable brand link';
$_['entry_duration']    = 'Default listing duration';
$_['entry_measurement']    = 'Measurement system';
$_['entry_address_format']   = 'Default address format';
$_['entry_timezone_offset']   = 'Laika zonas kompensācija';
$_['entry_tax_listing']    = 'Produkta nodoklis';
$_['entry_tax']      = 'Nodokļi % izmantojami visur';
$_['entry_create_date']    = 'Izveidot jaunu pasūtījumu datumu';
$_['entry_password_prompt']   = 'Lūdzu ievadiet savu administratora paroli';
$_['entry_notify_order_update']  = 'Pasūtījuma atjaunošana';
$_['entry_notify_buyer']   = 'Jauns pasūtījums - pircējs';
$_['entry_notify_admin']   = 'Jauns pasūtījums - administartors';
$_['entry_import_pending']   = 'Importēt nesamaksātos pasūtījumus:';
$_['entry_import_def_id']   = 'Importēt noklusējuma stāvokli:';
$_['entry_import_paid_id']   = 'Maksājuma stāvoklis:';
$_['entry_import_shipped_id']  = 'Piegādes stāvoklis:';
$_['entry_import_cancelled_id']  = 'Atcelts stāvoklis:';
$_['entry_import_refund_id']  = 'Atmaksas stāvoklis:';
$_['entry_import_part_refund_id'] = 'Daļējas atmaksas stāvoklis:';

// Tabs
$_['tab_api_info']     = 'API detaļas';
$_['tab_setup']      = 'Iestatījumi';
$_['tab_defaults']     = 'Saraksta noklusējums';

// Help
$_['help_disable_soldout']   = 'When the item sells out it then disables the product in OpenCart';
$_['help_relist_items']    = 'Ja preces saite eksitē pirms tiks atjaunota iepriekšējā prece , kad tā atkal būs krājumos';
$_['help_end_items']       = 'Ja preces izpārdotas, vai sarakstam eBay jābeidz pastāvēt?';
$_['help_currency']        = 'Based on currencies from your store';
$_['help_created_hours']      = 'Pasūtījumi ir jauni, ja tie ir jaunāki par šo ierobežojumu (stundās). Pēc noklusējuma - 72 stundas';
$_['help_stock_allocate']    = 'Kad krājumiem jābūt lokalizētiem no veikala?';
$_['help_payment_instruction']   = 'Be as descriptive as possible. Do you require payment within a certain time? Do they call to pay by card? Do you have any special payment terms?';
$_['help_payment_immediate']   = 'Immediate payment stops unpaid buyers, as an item is not sold until they pay.';
$_['help_listing_tax']        = 'Ja Jūs izmantojat saraksta likmi, pārliecinieties, ka Jūsu precēm ir korekts nodoklis eBay';
$_['help_tax']                = 'Izmantot, kad tiek importētas preces vai pasūtījumi';
$_['help_duration']        = 'GTC pieejams vienīgi tad, ja Jums ir eBay veikals.';
$_['help_address_format']        = 'Tiek izmantots tikai tad, ja valstij nav adreses formāta set-up.';
$_['help_create_date']           = 'Choose which created time will appear on an order when it is imported';
$_['help_timezone_offset']       = 'Pamatota stundu dalījumā. 0 ir GMT laika zona. Strādā tikai tad, ja eBay laiks tiek izmantots pasūtījumu izveidošanai.';
$_['help_notify_admin']      = 'Informēt veikala administratoru ar jauna pasūtījuma noklusējuma e-pastu';
$_['help_notify_order_update']  = 'This is for automated updates, for example if you update an order in eBay and the new status is updated in your store automatically.';
$_['help_notify_buyer']          = 'Informēt lietotāju ar jauna pasūtījuma noklusējuma e-pastu';
$_['help_measurement']          = 'Choose what measurement system you want to use for listings';

// Buttons
$_['button_update']              = 'Atjaunot';
$_['button_repair_links']      = 'Repair item links';

// Error
$_['error_api_connect']          = 'Neveiksmīga pieslēgšanās';
?>
<?php
// Heading
$_['heading_title']                = 'New listing';
$_['text_ebay']                 = 'eBay';
$_['text_openbay']                = 'Openbay Pro';

// Text
$_['text_listing_1day']             = '1 diena';
$_['text_listing_3day']             = '3 diena';
$_['text_listing_5day']             = '5 diena';
$_['text_listing_7day']             = '7 diena';
$_['text_listing_10day']            = '10 diena';
$_['text_listing_30day']            = '30 dienas';
$_['text_listing_gtc']              = 'GTC- Good till cancelled';
$_['text_none']                     = 'Neviens';
$_['text_pixels']                   = 'Pikseļi';
$_['text_add']                      = 'Pievienot';
$_['text_other']                    = 'Cits';
$_['text_loading']                  = 'Ielādēšana';
$_['text_confirm_action']           = 'Are you sure?';
$_['text_paypal']           		= 'PayPal email address ';
$_['text_return']                   = 'Atgriezties pie produktiem';
$_['text_category_suggested_help']  = 'Pamatojoties uz Jūsu nosaukumu';
$_['text_category_popular_help']    = 'Pamatojoties uz Jūsu vēsturi';
$_['text_category_checking']        = 'Pārbauda eBay ktegoriju pieprasījumus, lūdzu gaidiet';
$_['text_features_help']         = 'Entering specifics about your item will help buyers narrow down the exact item they need. It may also improve the performance of the product and eBay may score its best match value higher.';
$_['text_images_text_1']            = 'eBay attēli ielādēsies eBay.<br />"Supersize" un "Gallery plus" ir saraksta uzlabojumi, kas var maksāt vairāk nekā ir sarakstā.';
$_['text_images_text_2']            = 'Ādiņu attēli tiks pievienoti Jūsu saraksta aprakstam un tiks saņemti no Jūsu mājaslapas, tie ir brīvi. (Jūsu saraksta ādiņai vajadzīgs {galerijas} tags)';
$_['text_template_image']           = 'Ādiņas attēls';
$_['text_main_image_ebay']          = 'Galvenais eBay attēls';
$_['text_image_ebay']               = 'eBay attēls';
$_['text_images_none']              = 'You have no images for this product';
$_['text_width']          = 'Width';
$_['text_height']          = 'Height';
$_['text_px']           = 'px';
$_['text_item_postcode_help']       = 'A postcode will help eBay choose a correct location for your listing';
$_['text_item_location_help']       = 'Entering a town is less reliable than a postcode';
$_['text_despatch_country_help']    = 'This is the country the item will be sent from';
$_['text_shipping_zones']           = 'Ship to zones';
$_['text_shipping_worldwide']       = 'Worldwide';
$_['text_shipping_flat']            = 'Flat rate';
$_['text_shipping_calculated']      = 'Calculated';
$_['text_shipping_freight']         = 'Freight';
$_['text_unit']           = 'Unit';
$_['text_unit_english']         = 'English';
$_['text_unit_metric']         = 'Metric';
$_['text_weight_major']         = 'Weight major';
$_['text_weight_minor']         = 'Weight minor';
$_['text_package']          = 'Package type';
$_['text_shape']           = 'Irregular shape';
$_['text_width']           = 'Width';
$_['text_length']          = 'Length';
$_['text_depth']           = 'Depth / Height';
$_['text_return_accepted']          = 'Akceptēt sūtīšanu atpakaļ?';
$_['text_return_type']              = 'Atpakaļ sūtīšanas veids';
$_['text_return_policy']            = 'Atpakaļ sūtīšanas polise';
$_['text_return_days']              = 'Atpakaļ sūtīšanas dienas';
$_['text_return_scosts']            = 'Piegādes maksa';
$_['text_return_restock']           = 'Krājumu atjaunošanas maksa';
$_['text_return_scosts_1']          = 'Pircējs maksā par visām piegādēm atpakaļ';
$_['text_return_scosts_2']          = 'Tirgotājs maksā par visām piegādēm atpakaļ';
$_['text_review_costs']             = 'Saraksta izmaksas';
$_['text_review_costs_total']       = 'Kopējā eBay maksa';
$_['text_review_edit']              = 'Rediģēt sarakstu';
$_['text_preview']                = 'Priekšskatīt';
$_['text_verify']                = 'Verify';
$_['text_created_title']            = 'Saraksts izveidots';
$_['text_created_msg']              = 'Jūsu eBay saraksts ir izveidots. eBay preces numurs ir';
$_['text_option_images']            = 'Variāciju attēli';
$_['text_option_images_grp']        = 'Izvēlieties darbības grupu';
$_['text_option_images_choice']     = 'Attēli';
$_['text_option_description']       = 'Variāciju attēli tiek izmantoti, lai parādītu specifisku attēlu, kasd lietotājs atlasa darbību. Jūs varat izmantot tikai vienu variāciju attēlu ievietošanai, bet varat izmantot līdz pat 12 attēliem variācijai. Noklusējuma attēli ir ielādēti no Jūsu variantu vērtībām (ievietot katalogā > Opcijas (set in Catalog > Options))';
$_['text_catalog_help']             = 'This will change your main image and will be set to use the eBay catalog image';
$_['text_failed_title']             = 'Jūsu preču saraksts kļūdains';
$_['text_failed_msg1']              = 'Tam var būt daudz iemesliu.';
$_['text_failed_li1']               = 'Ja esat jauns eBay tirgotājs (vai arī iepriekš neesat daudz pārdevis) - Jums jāsazinās ar eBay, lai  noņemtu Jūsu tirgotāja ierobežojumus';
$_['text_failed_li2']               = 'Jums nav jāabonē eBay "Selling Manager Pro" - tā ir prasība.';
$_['text_failed_li3']               = 'Jūsu "OpenBay Pro" konts ir apturēts, lūdzu, pārbaudiet moduli vadības panelī lauku sadaļā "Mans konts" (My Account)';
$_['text_failed_contact']           = 'Ja joprojām ir šī kļūda, lūdzu, sazinieties ar tehniskā atbalsta sniedzējiem pēc tam, kad esat pārliecinājies, ka iemesls nav iepriekš minētais.';
$_['text_template_images']          = 'Ādiņu attēli';
$_['text_ebay_images']              = 'eBay attēli';
$_['text_shipping_first']           = 'Pirmā prece: ';
$_['text_shipping_add']             = 'Papildu preces: ';
$_['text_shipping_service']         = 'Servisi: ';
$_['text_stock_reserved']           = ' will be reserved';

// Column
$_['column_stock_total']        = 'In stock';
$_['column_stock_col_qty']          = 'To list';
$_['column_stock_col_qty_reserve']  = 'Reserved';
$_['column_price_ex_tax']           = 'Excluding tax';
$_['column_price_inc_tax']          = 'Including tax';
$_['column_stock_col_comb']         = 'Combination';
$_['column_price']            = 'Price';
$_['column_stock_col_enabled']      = 'Enabled';

// Entry
$_['entry_shop_category']           = 'Shop Category';
$_['entry_category_popular']        = 'Popular Categories';
$_['entry_category_suggested']      = 'eBay suggested category';
$_['entry_category']                = 'Category';
$_['entry_listing_condition']       = 'Item condition';
$_['entry_listing_duration']        = 'Listing duration';
$_['entry_search_catalog']          = 'Search eBay catalog:';
$_['entry_catalog']              = 'Use default image';
$_['entry_title']                   = 'Title';
$_['entry_subtitle']                = 'Sub title';
$_['entry_description']             = 'Description';
$_['entry_profile_load']            = 'Load profile';
$_['entry_template']                = 'Theme';
$_['entry_image_gallery']           = 'Gallery image size';
$_['entry_image_thumb']             = 'Thumb image size';
$_['entry_images_supersize']        = 'Supersize images';
$_['entry_images_gallery_plus']     = 'Gallery plus';
$_['entry_gallery_select_all']      = 'Tick the box to select all of your images at once';
$_['text_stock_matrix']             = 'Krājumu matrix';
$_['entry_qty']                     = 'Quantity to list';
$_['entry_price']               = 'Price';
$_['entry_tax_inc']                 = 'Tax included';
$_['entry_offers']                  = 'Allow buyers to make offers';
$_['entry_private']                 = 'Private listing';
$_['entry_imediate_payment']        = 'Immediate payment required?';
$_['entry_payment']                 = 'Payments accepted';
$_['entry_payment_instruction']     = 'Payment instructions';
$_['entry_item_postcode']           = 'Postcode/Zip of location';
$_['entry_item_location']           = 'Town or State of location';
$_['entry_despatch_country']        = 'Dispatch country';
$_['entry_despatch_time']           = 'Dispatch time';
$_['entry_shipping_getitfast']      = 'Get It Fast!';
$_['entry_shipping_cod']            = 'Cash on delivery fee';
$_['entry_shipping_type_nat']       = 'National shipping type';
$_['entry_shipping_nat']            = 'National shipping services';
$_['entry_shipping_handling_nat']   = 'Handling fee (national)';
$_['entry_shipping_in_desc']        = 'Freight info in description';
$_['entry_shipping_type_int']       = 'International shipping type';
$_['entry_shipping_intnat']         = 'International shipping services';
$_['entry_shipping_handling_int']   = 'Handling fee (international)';

// Tab
$_['tab_feature']                = 'Features';
$_['tab_ebay_catalog']              = 'eBay catalog';
$_['tab_description']            = 'Description';
$_['tab_price']                  = 'Price &amp; details';
$_['tab_payment']                = 'Payment';
$_['tab_returns']                = 'Returns';

// Help
$_['help_quantity_reserve']         = 'Enter a lower amount if you want to maintain a lower stock level on eBay';
$_['help_price_ex_tax']          = 'Your standard item price excluding tax. This value is not sent to eBay.';
$_['help_price_inc_tax']         = 'This value is sent to eBay and is the price users will pay.';
$_['help_private']               = 'Hide buyer user names';

// Error
$_['error_choose_category']      = 'You must choose a category';
$_['error_search_text']          = 'Enter search text';
$_['error_no_stock']             = 'You cannot list an item with zero stock';
$_['error_catalog_data']      = 'No eBay catalog data was found for your product in eBay';
$_['error_missing_settings']        = 'You cannot list items until you sync the eBay settings';
$_['error_category_load']     = 'Unable to load categories';
$_['error_features']       = 'Error loading features';
$_['error_catalog_load']      = 'Error loading catalog';
$_['error_category_sync']           = 'You must fix your category problem before you can list. Try re-syncing them in the module admin area.';
$_['error_choose_category']   = 'Please choose an eBay category';
$_['error_sku']              = 'Cannot submit a product without an SKU';
$_['error_name']             = 'Cannot submit a product without a name';
$_['error_name_length']        = 'Product name must be under 80 characters';
$_['error_item_location']           = 'Enter an item location postcode';
$_['error_dispatch_time']           = 'Enter a dispatch time';
$_['error_shipping_national']       = 'Add at least one national shipping service';
$_['error_stock']            = 'You must have stock of an item to list it';
$_['error_duration']             = 'Select a listing duration';
$_['error_listing_duration']       = 'Select a listing duration, select category to load these options';
$_['error_image_size']           = 'Ensure that you have a gallery and thumb image size';
$_['error_no_images']             = 'Listing must have at least 1 image uploaded to eBay';
$_['error_main_image']            = 'You need to choose a main eBay image from your selection of eBay images';
?>
<?php
// Heading
$_['heading_title'] 				= 'Saglabātie saraksti';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon EU';

// Text
$_['text_description']              = 'Šis ir produktu saraksts, kas saglabāts lokāli un gatavs ielādēšanai Amazon. Klikšķiniet "Ielādēt" (Upload), lai publicēt.';
$_['text_uploaded_alert']           = 'Saglabātie(ais) saraksti(s) ielādēti!';
$_['text_delete_confirm']           = 'Vai esat pārliecināts?';
$_['text_complete']           		= 'Listings uploaded';

// Column
$_['column_name']              		= 'Nosaukums';
$_['column_model']             		= 'Modelis';
$_['column_sku']               		= 'SKU';
$_['column_amazon_sku']        		= 'Amazon prece SKU';
$_['column_action']           		= 'Akcija';
?>
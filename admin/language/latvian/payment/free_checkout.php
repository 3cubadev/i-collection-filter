<?php
// Heading
$_['heading_title']      = 'Bezmaksas noformēšana';

// Text
$_['text_payment']       = 'Maksājums';
$_['text_success']       = 'Jūs sekmīgi pabeidzāt rediģēt bezmaksas noformēšanas moduli!';
$_['text_edit']          = 'Bezmaksas noformēšanas rediģēšana';

// Entry
$_['entry_order_status'] = 'Pasūtījuma stāvoklis:';
$_['entry_status']       = 'Stāvoklis:';
$_['entry_sort_order']   = 'Kārtošanas secība:';

// Error
$_['error_permission']   = 'Uzmanību! Jums nav atļauts rediģēt maksājumu moduli "Bezmaksas noformēšana"!';
<?php
// Heading
$_['heading_title']      = 'Čeks / Money order';

// Text 
$_['text_payment']       = 'Maksājums';
$_['text_success']       = 'Jūs sekmīgi pabeidzāt rediģēt čeka / skaidras naudas maksājumu profilu!';
$_['text_edit']                     = 'Čeks / Money Order rediģēšana';

// Entry
$_['entry_payable']      = 'Saņēmējs:';
$_['entry_total']        = 'Kopējā summa:';
$_['entry_order_status'] = 'Pasūtījuma stāvoklis:';
$_['entry_geo_zone']     = 'Darbības reģions:';
$_['entry_status']       = 'Stāvoklis:';
$_['entry_sort_order']   = 'Kārtošanas secība:';

// Help
$_['help_total']		= 'Pasūtījuma kopējā summa, kura ir jāsasniedz, lai šis apmaksas veids būtu pieejams.';

// Error
$_['error_permission']   = 'Uzmanību! Jums nav atļauts rediģēt maksājumu moduli Čeks / Money order!';
$_['error_payable']      = 'Ir jānorāda saņēmējs!';
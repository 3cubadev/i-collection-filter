<?php
// Heading
$_['heading_title']      = 'Skaidrā naudā pie piegādes';

// Text
$_['text_payment']       = 'Maksājums';
$_['text_success']       = 'Jūs sekmīgi pabeidzāt rediģēt "Skaidrā naudā pie piegādes" moduli!';
$_['text_edit']                     = '"Skaidrā naudā pie piegādes" moduļa rediģēšana';

// Entry
$_['entry_total']        = 'Kopējā summa:';
$_['entry_order_status'] = 'Pasūtījuma stāvoklis:';
$_['entry_geo_zone']     = 'Darbības reģions:';
$_['entry_status']       = 'Stāvoklis:';
$_['entry_sort_order']   = 'Kārtošanas secība:';

// Help
$_['help_total']					= 'Pasūtījuma kopējā summa, kura ir jāsasniedz, lai šis apmaksas veids būtu pieejams.';

// Error
$_['error_permission']   = 'Uzmanību! Jums nav atļauts rediģēt maksājumu moduli "Skaidrā naudā pie piegādes"!';
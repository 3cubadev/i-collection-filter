<?php
/*
* Opencart Localization by Guntis.Endzelis@partneris.lv
* All rights reserved.
* Source here: http://www.partneris.lv/latvian-lang-pck-01
* 
*/

// Locale
$_['code']                    = 'lv';
$_['direction']               = 'ltr';
$_['date_format_short']       = 'd.m.Y';
$_['date_format_long']        = 'l dS F Y';
$_['time_format']             = 'h:i:s A';
$_['datetime_format']               = 'd/m/Y H:i:s';
$_['decimal_point']           = ',';
$_['thousand_point']          = ' ';

// Text
$_['text_yes']                = 'Jā';
$_['text_no']                 = 'Nē';
$_['text_enabled']            = 'Ieslēgts';
$_['text_disabled']           = 'Izslēgts';
$_['text_none']               = ' --- Nav izvēlēts --- ';
$_['text_select']             = ' --- Izvēlieties --- ';
$_['text_select_all']         = 'Atlasīt visu';
$_['text_unselect_all']       = 'Noņemt atlasi visiem';
$_['text_all_zones']          = 'Visi reģioni';
$_['text_default']            = ' <b>(Pēc Noklusējuma)</b>';
$_['text_close']              = 'Aizvērt';
$_['text_pagination']         = 'Rāda no %d līdz %d kopā %d (Kopā lapu - %d)';
$_['text_loading']                  = 'Gaidiet...';
$_['text_no_results']         = 'Nav datu!';
$_['text_confirm']                  = 'Vai jūs esat pārliecināts?';
$_['text_home']               = 'Sākums';

// Button
$_['button_add']           = 'Pievienot';
$_['button_delete']           = 'Dzēst';
$_['button_save']             = 'Saglabāt';
$_['button_cancel']           = 'Atcelt';
$_['button_cancel_recurring']       = 'Atcelt regulāros maksājumus';
$_['button_continue']               = 'Turpināt';
$_['button_clear']            = 'Tīrīt vēsturi';
$_['button_close']            = 'Aizvērt';
$_['button_enable']                 = 'Ieslēgt';
$_['button_disable']                = 'Izslēgt';
$_['button_filter']           = 'Filtrs';
$_['button_send']             = 'Sūtīt';
$_['button_edit']             = 'Rediģēt';
$_['button_copy']             = 'Kopēt';
$_['button_back']             = 'Atpakaļ';
$_['button_remove']           = 'Dzēst';
$_['button_refresh']                = 'Refresh';
$_['button_backup']           = 'Veikt rezerves kopēšanu';
$_['button_restore']          = 'Atjaunot no rezerves kopijas';
$_['button_download']               = 'Download';
$_['button_rebuild']                = 'Rebuild';
$_['button_upload']           = 'Agšupielādēt';
$_['button_submit']           = 'Nosūtīt';
$_['button_invoice_print']          = 'Drukāt rēķinu';
$_['button_shipping_print']         = 'Drukāt piegādes sarakstu';
$_['button_address_add']      = 'Pievienot adresi';
$_['button_attribute_add']    = 'Pievienot atribūtu';
$_['button_banner_add']       = 'Pievienot baneri';
$_['button_custom_field_value_add'] = 'Pievienot jaunu datu lauku (aili)';
$_['button_product_add']      = 'Pievienot preci';
$_['button_filter_add']       = 'Pievienot filtru';
$_['button_option_add']       = 'Pievienot papildu iespēju';
$_['button_option_value_add'] = 'Pievienot izvēles vērtību';
$_['button_recurring_add']          = 'Pievienot regulāro maksājumu';
$_['button_discount_add']     = 'Pievienot atlaidi';
$_['button_special_add']      = 'Pievienot īpašo piedāvājumu';
$_['button_image_add']        = 'Pievienot attēlu';
$_['button_geo_zone_add']     = 'Pievienot darbības reģionu';
$_['button_history_add']      = 'Pievienot vēsturi';
$_['button_transaction_add']  = 'Pievienot darījumu';
$_['button_route_add']        = 'Pievienot ceļu';
$_['button_rule_add' ]        = 'Pievienot likumu';
$_['button_module_add']       = 'Pievienot moduli';
$_['button_link_add']         = 'Pievienot saiti';
$_['button_approve']          = 'Apstiprināt';
$_['button_reset']            = 'Atiestatīt';
$_['button_generate']               = 'Ģenerēt';
$_['button_voucher_add']            = 'Pievienot dāvanu karti';
$_['button_reward_add']             = 'Pievienot bonusa punktus';
$_['button_reward_remove']          = 'Noņemt bounusa punktus';
$_['button_commission_add']         = 'Pievienot komisiju';
$_['button_commission_remove']      = 'Noņemt komisiju';
$_['button_credit_add']             = 'Pievienot kredītu';
$_['button_credit_remove']          = 'Noņemt kredītu';
$_['button_ip_add']                 = 'Pievienot IP';
$_['button_parent']                 = 'Iepriekšejais līmenis';
$_['button_folder']                 = 'Jauna mape';
$_['button_search']                 = 'Meklēt';
$_['button_view']                   = 'Skatīt';
$_['button_install']                = 'Instalēt';
$_['button_uninstall']              = 'Atinstalēt';
$_['button_login']                  = 'Autorizēties vietnē';
$_['button_unlock']                 = 'Atslēgt profilu';
$_['button_link']                   = 'Saite';
$_['button_currency']               = 'Atjaunot valūtas kursus';
$_['button_apply']                  = 'Piemērot';

// Tab
$_['tab_address']             = 'Adrese';
$_['tab_admin']               = 'Admin';
$_['tab_attribute']           = 'Atribūti';
$_['tab_customer']            = 'Pircēja dati';
$_['tab_data']                = 'Dati';
$_['tab_design']              = 'Dizains';
$_['tab_discount']            = 'Vairuma atlaide';
$_['tab_general']             = 'Vispārīgie';
$_['tab_history']             = 'Vēsture';
$_['tab_ftp']                 = 'FTP';
$_['tab_ip']                  = 'IP adreses';
$_['tab_links']               = 'Saites';
$_['tab_log']                 = 'Ziņojumu žurnāls';
$_['tab_image']               = 'Attēli';
$_['tab_option']              = 'Papildu izvēles';
$_['tab_server']              = 'Serveris';
$_['tab_store']               = 'Vietne';
$_['tab_special']             = 'Akcijas';
$_['tab_local']               = 'Lokalizācija';
$_['tab_mail']                = 'Pasts';
$_['tab_module']              = 'Modulis';
$_['tab_order']               = 'Pasūtījuma informācija';
$_['tab_payment']             = 'Maksātājs';
$_['tab_product']             = 'Preces';
$_['tab_reward']              = 'Bonusa punkti';
$_['tab_shipping']            = 'Piegādes informācija';
$_['tab_total']               = 'Summas';
$_['tab_transaction']         = 'Darījumi';
$_['tab_voucher']             = 'Dāvanu kartes';
$_['tab_sale']                      = 'Pārdošana';
$_['tab_marketing']                 = 'Marketings';
$_['tab_online']                    = 'Pircēji tiešsaistē';
$_['tab_activity']                  = 'Nesenās darbības';
$_['tab_recurring']                 = 'Regulārie maksājumi';
$_['tab_action']                    = 'Darbība';
$_['tab_google']                    = 'Google';

// Error
$_['error_exception']               = 'Error Code(%s): %s in %s on line %s';
$_['error_upload_1']          = 'Faila ielādes izmērs pārsniedz atļauto upload_max_filesize, kas norādīts php.ini!';
$_['error_upload_2']          = 'Faila izmērs pārsniedz atļauto MAX_FILE_SIZE, kas norādīts veikala konfigurācijas!';
$_['error_upload_3']          = 'Faila ielāde pilnībā nebija pabeigta!';
$_['error_upload_4']          = 'Fails netika pilnībā ielādēts!';
$_['error_upload_6']          = 'Nav atrasta pagaidu mape!';
$_['error_upload_7']          = 'Neizdevās ierakstīt failu uz diska!';
$_['error_upload_8']          = 'Failu ielāde ir pārtraukta!';
$_['error_upload_999']        = 'Kļūdas kods nav pieejams!';

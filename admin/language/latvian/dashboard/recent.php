<?php
// Heading
$_['heading_title']     = 'Pēdējie pasūtījumi';

// Column
$_['column_order_id']   = 'Pasūtījuma Nr.';
$_['column_customer']   = 'Pircējs';
$_['column_status']     = 'Stāvoklis';
$_['column_total']      = 'Kopā';
$_['column_date_added'] = 'Pievienošanas datums';
$_['column_action']     = 'Darbība';
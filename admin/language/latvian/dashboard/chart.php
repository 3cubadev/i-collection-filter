<?php
// Heading
$_['heading_title'] = 'Pārdošanas analīze';

// Text
$_['text_order']    = 'Pasūtījumi';
$_['text_customer'] = 'Pircēji';
$_['text_day']      = 'Šodien';
$_['text_week']     = 'Nedēļa';
$_['text_month']    = 'Mēnesis';
$_['text_year']     = 'Gads';
<?php
// Heading
$_['heading_title']                = 'Nesenās aktivitātes';

// Text
$_['text_customer_address_add']    = '<a href="customer_id=%d">%s</a> pievienoja jaunu adresi.';
$_['text_customer_address_edit']   = '<a href="customer_id=%d">%s</a> atjaunoja adresi.';
$_['text_customer_address_delete'] = '<a href="customer_id=%d">%s</a> izdzēsa vienu no savām adresēm.';
$_['text_customer_edit']           = '<a href="customer_id=%d">%s</a> rediģēja savu profilu.';
$_['text_customer_forgotten']      = '<a href="customer_id=%d">%s</a> pieprasīja jaunu paroli.';
$_['text_customer_login']          = '<a href="customer_id=%d">%s</a> autorizējās.';
$_['text_customer_password']       = '<a href="customer_id=%d">%s</a> nomainīja savu paroli.';
$_['text_customer_register']       = '<a href="customer_id=%d">%s</a> reģistrēja jaunu profilu.';
$_['text_customer_return_account'] = '<a href="customer_id=%d">%s</a> iesniedza preces <a href="return_id=%d">atteikumu</a>.';
$_['text_customer_return_guest']  = '%s iesniedza preces <a href="return_id=%d">atteikumu</a>.';
$_['text_customer_order_account']  = '<a href="customer_id=%d">%s</a> pievienoja <a href="order_id=%d">jaunu pasūtījumu</a>.';
$_['text_customer_order_guest']    = '%s izveidoja <a href="order_id=%d">jaunu pasūtījumu</a>.';
$_['text_affiliate_edit']          = '<a href="affiliate_id=%d">%s</a> rediģēja savu profilu.';
$_['text_affiliate_forgotten']     = '<a href="affiliate_id=%d">%s</a> pieprasīja jaunu paroli.';
$_['text_affiliate_login']         = '<a href="affiliate_id=%d">%s</a> autorizējās.';
$_['text_affiliate_password']      = '<a href="affiliate_id=%d">%s</a> nomainīja savu paroli.';
$_['text_affiliate_payment']       = '<a href="affiliate_id=%d">%s</a> rediģēja savu maksātāja adresi.';
$_['text_affiliate_register']      = '<a href="affiliate_id=%d">%s</a> reģistrēja jaunu profilu.';
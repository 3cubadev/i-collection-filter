<?php
// Heading
$_['heading_title']      = 'Australia Post';

// Text
$_['text_shipping']      = 'Piegāde';
$_['text_success']       = 'Jūs sekmīgi pabeidzāt rediģēt "Australia Post" piegādes moduli!';
$_['text_edit']          = 'Australia Post piegādes rediģēšana';

// Entry
$_['entry_postcode']     = 'Pasta indekss';
$_['entry_express']      = 'Express Postage';
$_['entry_standard']     = 'Standard Postage';
$_['entry_display_time'] = 'Rādīt piegādes laiku';
$_['entry_weight_class'] = 'Svara mērvienība';
$_['entry_tax_class']    = 'Nodokļi';
$_['entry_geo_zone']     = 'Darbības reģions';
$_['entry_status']       = 'Stāvoklis';
$_['entry_sort_order']   = 'Kārtošanas secība';

// Help
$_['help_display_time']  = 'Do you want to display the shipping time? (e.g. Ships within 3 to 5 days)';
$_['help_weight_class']  = 'Uzstādiet uz gramiem.';

// Error
$_['error_permission']   = 'Jums nav atļauts rediģēt "Australia Post" piegādes moduli!';
$_['error_postcode']     = 'Pasta indeksā ir jābūt 4 zīmēm!';
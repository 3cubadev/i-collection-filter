<?php
// Heading
$_['heading_title']    = 'Pasūtījumu izņemšu pats';

// Text 
$_['text_shipping']    = 'Piegāde';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt \"Pasūtījumu izņemšu pats\" piegādes moduli!';
$_['text_edit']        = 'Pasūtījumu izņemšu pats rediģēšana';

// Entry
$_['entry_geo_zone']   = 'Darbības reģions';
$_['entry_status']     = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt Pasūtījumu izņemšu pats piegādes moduli';
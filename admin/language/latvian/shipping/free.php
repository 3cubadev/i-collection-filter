<?php
// Heading
$_['heading_title']    = 'Bezmaksas piegāde';

// Text 
$_['text_shipping']    = 'Piegāde';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt bezmaksas piegādes moduli!';
$_['text_edit']        = 'Bezmaksas piegādes rediģēšana';

// Entry
$_['entry_total']      = 'Kopā';
$_['entry_geo_zone']   = 'Darbības reģions';
$_['entry_status']     = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

// Help
$_['help_total']       = 'Ir nepieciešama summa, kura aktivizē bezmaksas piegādi.';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt piegādes moduļus!';
<?php
// Heading
$_['heading_title']    = 'Piegāde ar fiksēto likmi';

// Text
$_['text_shipping']    = 'Piegāde';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt fiksētās likmes piegādes moduli!';
$_['text_edit']        = 'Fiksētās likmes piegādes moduļa rediģēšana';

// Entry
$_['entry_cost']       = 'Izmaksas';
$_['entry_tax_class']        = 'Nodokļi';
$_['entry_geo_zone']   = 'Darbības reģions';
$_['entry_status']     = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt fiksētās likmes piegādes moduli!';
<?php
// Heading
$_['heading_title']      = 'Citylink';

// Text
$_['text_shipping']      = 'Piegāde';
$_['text_success']       = 'Jūs sekmīgi pabeidzāt rediģēt "Citylink" piegādes moduli!';
$_['text_edit']        = 'Citylink Piegādes rediģēšana';

// Entry
$_['entry_rate']         = 'Piegādes izcenojumi';
$_['entry_tax_class']          = 'Nodokļi';
$_['entry_geo_zone']     = 'Darbības reģions';
$_['entry_status']       = 'Stāvoklis';
$_['entry_sort_order']   = 'Kārtošanas secība';

// Help
$_['help_rate']        = 'Ievadiet vērtību tikai ciparos un atdaliet ar komatu. Piemēram: .1:1,.25:1.27 - 0.100g maksās &pound;1.00 un 0.250g maksās &pound;1.27. Neievadiet "Kg" vai citus simbolus!';

// Error
$_['error_permission']   = 'Jums nav atļauts rediģēt Citylink piegādes moduli!';
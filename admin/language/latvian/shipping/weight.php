<?php
// Heading
$_['heading_title']      = 'Piegāde pēc svara';

// Text
$_['text_shipping']      = 'Piegāde';
$_['text_success']       = 'Jūs sekmīgi pabeidzāt rediģēt \"Piegāde pēc svara\" piegādes moduli!';
$_['text_edit']        = 'Piegāde pēc svara rediģēšana';

// Entry
$_['entry_rate']         = 'Piegādes izcenojumi';
$_['entry_tax_class']          = 'Nodokļi';
$_['entry_geo_zone']     = 'Darbības reģions';
$_['entry_status']       = 'Stāvoklis';
$_['entry_sort_order']   = 'Kārtošanas secība';

// Help
$_['help_rate']        = 'Piemēram: 5:10.00,7:12.00 Svars: Cena, Svars: Cena, u.t.t.';

// Error
$_['error_permission']   = 'Jums nav atļauts rediģēt piegāde pēc svara moduli!';
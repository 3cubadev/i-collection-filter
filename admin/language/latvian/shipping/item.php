<?php 
// Heading
$_['heading_title']    = 'Piegādes apmaksa par vienību';

// Text
$_['text_shipping']    = 'Piegāde';
$_['text_success']     = 'Jūs sekmīgi pabeidzāt rediģēt "Piegādes apmaksa par vienību" piegādes moduli!';
$_['text_edit']        = 'Piegādes apmaksa par vienību moduļa rediģēšana';

// Entry
$_['entry_cost']       = 'Izmaksa';
$_['entry_tax_class']        = 'Nodokļi';
$_['entry_geo_zone']   = 'Darbības reģions';
$_['entry_status']     = 'Stāvoklis';
$_['entry_sort_order'] = 'Kārtošanas secība';

// Error
$_['error_permission'] = 'Jums nav atļauts rediģēt Piegādes apmaksa par vienību moduli!';
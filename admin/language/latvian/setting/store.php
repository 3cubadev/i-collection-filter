<?php
// Heading
$_['heading_title']           = 'Vietnes';

// Text
$_['text_settings']                    = 'Iestatījumi';
$_['text_success']            = 'Jūs sekmīgi pabeidzāt rediģēt iestatījumus!';
$_['text_list']                        = 'Vietņu saraksts';
$_['text_add']                         = 'Pievienot vietni';
$_['text_edit']                        = 'Rediģēt vietni';
$_['text_items']              = 'Vienības';
$_['text_tax']                = 'Nodokļi';
$_['text_account']            = 'Profils';
$_['text_checkout']           = 'Pirkuma noformēšana';
$_['text_stock']              = 'Noliktava';
$_['text_shipping']           = 'Piegādes adrese';
$_['text_payment']            = 'Maksātāja adrese';

// Column
$_['column_name']             = 'Vietnes nosaukums';
$_['column_url']	          = 'Vietnes URL';
$_['column_action']           = 'Darbība';

// Entry
$_['entry_url']               = 'Vietnes adrese (URL)';
$_['entry_ssl']               = 'Izmantot SSL';
$_['entry_name']              = 'Vietnes nosaukums';
$_['entry_owner']             = 'Vietnes īpašnieks';
$_['entry_address']           = 'Adrese';
$_['entry_geocode']                    = 'Ģeokods';
$_['entry_email']             = 'E-pasts';
$_['entry_telephone']         = 'Tālruņa numurs';
$_['entry_fax']               = 'Fakss';
$_['entry_image']                      = 'Attēls';
$_['entry_open']                       = 'Darbības laiki';
$_['entry_comment']                    = 'Piezīme';
$_['entry_location']                   = 'Atrašanās vieta';
$_['entry_meta_title']                 = 'Nosaukuma metatags';
$_['entry_meta_description']  = 'Apraksta metatags';
$_['entry_meta_keyword']               = 'Atslēgvārdu metatags';
$_['entry_layout']            = 'Izkārtojums pēc noklusējuma';
$_['entry_template']          = 'Veidne';
$_['entry_country']           = 'Valsts';
$_['entry_zone']              = 'Reģions';
$_['entry_language']          = 'Valoda';
$_['entry_currency']          = 'Valūta';
$_['entry_product_limit'] 	  = 'Pozīciju daudzums uz lapas (Katalogs)';
$_['entry_product_description_length'] = 'Preces apraksta limits sarakstā (Katalogs)';
$_['entry_tax']               = 'Rādīt cenas ar nodokļiem';
$_['entry_tax_default']       = 'Veikala adrese nodokļu aprēķinam';
$_['entry_tax_customer']      = 'Klienta adrese nodokļu aprēķinam';
$_['entry_customer_group']    = 'Pircēju grupa';
$_['entry_customer_group_display'] = 'Pircēju grupas';
$_['entry_customer_price']    = 'Rādīt cenas';
$_['entry_account']           = 'Reģistrācijas apstiprinājums';
$_['entry_cart_weight']       = 'Parādīt svaru pasūtījumu grozā';
$_['entry_checkout_guest']    = 'Pasūtījuma noformēšana viesiem';
$_['entry_checkout']          = 'Apstiprināšana pie pasūtīšanas';
$_['entry_order_status']      = 'Pasūtījumu stāvoklis';
$_['entry_stock_display']     = 'Rādīt noliktavas atlikumus';
$_['entry_stock_checkout']    = 'Pasūtīšana ar iztrūkuma krājumiem';
$_['entry_logo']              = 'Firmas logo';
$_['entry_icon']              = 'Ikona';
$_['entry_image_category']    = 'Attēla izmērs kategorijas sarakstā';
$_['entry_image_thumb']       = 'Preces lielā attēla izmērs';
$_['entry_image_popup']       = 'Preces pop-up attēla izmērs';
$_['entry_image_product']     = 'Attēla izmērs preču sarakstā';
$_['entry_image_additional']  = 'Papildu attēla izmērs pie precēm';
$_['entry_image_related']     = 'Attēla izmērs ieteiktām precēm';
$_['entry_image_compare']     = 'Attēla izmērs salīdzināšanas sarakstā';
$_['entry_image_wishlist']    = 'Attēla izmērs vēlmju sarakstā';
$_['entry_image_cart']        = 'Attēla izmērs preču grozā';
$_['entry_image_location']             = 'Veikala attēla izmērs';
$_['entry_width']                      = 'Platums';
$_['entry_height']                     = 'Augstums';
$_['entry_secure']            = 'Lietot SSL';

// Help
$_['help_url']                         = 'Pilns URL līdz vietnei. Pārliecinieties, ka pievienojāt \'/\' linka beigās. Piemēram: http://www.d-webhosting.eu/<br /><br />Don\'t use directories to create a new store. You should always point another domain or sub domain to your hosting.';
$_['help_ssl']                         = 'Lai izmantotu SSL, pārliecinieties, ka Jūsu hostings atbalsta SSL sertifikātu, un tad ierakstiet SSL adresi faila konfigurācijās.<br /><br />Don\'t use directories to create a new store. You should always point another domain or sub domain to your hosting.';
$_['help_geocode']                     = 'Lūdzu ievadiet sava veikala atrašanās vietas ģeokodu manuāli.';
$_['help_open']                        = 'Norādiet sava veikala darbības laikus.';
$_['help_comment']                     = 'Izmantojiet šo aili, jebkādām īpašām piezīmēm, kuras vēlaties nodot saviem pircējiem, piem. - viekals nepieņem čekus..';
$_['help_location']                    = 'Jūsu veikalu atrašanās vietas, kuras jūs vēlaties parādīt kontaktu lapā.';
$_['help_currency']                    = 'Nomainīt galveno valūtu. Iztīriet savas pārlūkprogrammas kešatmiņu, lai redzētu izmaiņas, kā arī atiestatiet cookies';
$_['help_product_limit'] 	           = 'Noteikt pozīciju daudzumu, kas ir redzams vienā lapā (prece, kategorijas u.t.t).';
$_['help_product_description_length']  = 'Saraksta skatījumā, īsā apraksta rakstzīmju limits (kategorijas lapā, akciju lapā, utt.).';
$_['help_tax_default']                 = 'Izmantot šo veikala adresi nodokļu aprēķinam, ja neviens nav autorizējies.';
$_['help_tax_customer']                = 'Izmantot šo klienta adresi nodokļu aprēķinam, kad klients ir autorizējies.';
$_['help_customer_group']              = 'Pircēju grupa pēc noklusējuma.';
$_['help_customer_group_display']      = 'Rādīt pircēju grupas, kuras jauni klienti var izvēlēties reģistrācijas laikā (piemēram, pircējs var izvēlēties reģistrēties kā vairumtirgotājs vai uzņēmums).';
$_['help_customer_price']              = 'Rādīt cenas tikai reģistrētiem pircējiem.';
$_['help_account']                     = 'Pieprasīt noteikumu apstiprinājumu pie profila reģistrācijas.';
$_['help_checkout_guest']              = 'Atļaut noformēt pasūtījumu bez reģistrācijas. Šī funkcija nebūs pieejama, ja grozā būs lejupielādes faili.';
$_['help_checkout']                    = 'Pieprasīt noteikumu apstiprinājumu, noformējot pasūtījumus.';
$_['help_order_status']                = 'Pasūtījumu stāvoklis pēc noklusējuma.';
$_['help_stock_display']               = 'Rādīt noliktavas atlikumu vietnes lapā.';
$_['help_stock_checkout']              = 'Ļaut pircējiem noformēt pasūtījumu, ja noliktavā attiecīgajā brīdī nav preces.';
$_['help_icon']                        = 'Ikonai jābūt PNG 16px x 16px.';
$_['help_secure']                      = 'Lai lietotu SSL, Jums pie sava hostinga pakalopjuma sniedzēja ir jāpārliecinās, vai ir instalēts SSL sertifikāts.';

// Error
$_['error_warning']           = 'Uzmanību! Lūdzu pārbaudiet, vai ir ievadīti visi nepieciešami dati!';
$_['error_permission']        = 'Jums nav atļauts rediģēt vietnes!';
$_['error_name']              = 'Vietnes nosaukumam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_owner']             = 'Veikala īpašniekam jābūt no 3 līdz 64 rakstzīmēm!';
$_['error_address']           = 'Veikala adresei jābūt no 10 līdz 256 rakstzīmēm!';
$_['error_email']             = 'E-pasta adrese nav ievadīta pareizi!';
$_['error_telephone']         = 'Tālruņa numuram jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_url']               = 'Jums ir nepieciešams ievadīt vietnes URL!';
$_['error_meta_title']             = 'Vietnes virsrakstam ir jābūt no 3 līdz 32 rakstzīmēm!';
$_['error_limit']       	  = 'Nav ievadīts nepieciešamais limits!';
$_['error_customer_group_display'] = 'Jums ir jānorāda pamata preču grupa, ja Jūs vēlaties izmantot šo funkciju!';
$_['error_image_thumb']       = 'Nepieciešams ievadīt preces lielā attēla izmērus!';
$_['error_image_popup']       = 'Nepieciešams ievadīt preces pop-up attēla izmērus!';
$_['error_image_product']     = 'Nepieciešams ievadīt attēla izmērus preču sarakstam!';
$_['error_image_category']    = 'Nepieciešams ievadīt attēla izmērus kategorijas sarakstam!';
$_['error_image_additional']  = 'Nepieciešams ievadīt papildu attēlu izmērus pie precēm!';
$_['error_image_related']     = 'Nepieciešams ievadīt attēlu izmērus ieteiktām precēm!';
$_['error_image_compare']     = 'Nepieciešams ievadīt attēlu izmērus preču salīdzināšanas sarakstā!';
$_['error_image_wishlist']    = 'Nepieciešams ievadīt attēlu izmērus vēlmju sarakstā!';
$_['error_image_cart']        = 'Nepieciešams ievadīt attēla izmērus preču grozam!';
$_['error_image_location']             = 'Veikala attēla dimensijas ir obligātas!';
$_['error_default']           = 'Uzmanību! Jūsu galvenā vietne nevar tikt izdzēsta!';
$_['error_store']             = 'Šī vietne nevar tikt izdzēsta, jo pašreiz tā tiek izmantota pie %s pasūtījuma(-iem)!';
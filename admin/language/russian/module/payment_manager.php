<?php
// Heading
$_['heading_title']                     = 'Оплата PRO v0.1.5.2(stable) [by addist.ru]';

// Text
$_['text_module']                       = 'Модули';
$_['text_success_saved']                = 'Метод оплаты успешно сохранен!';
$_['text_success_deleted']              = 'Метод оплаты успешно удален!';
$_['text_title']                        = 'Название';
$_['text_code']                         = 'Код';
$_['text_currencies']                   = 'Валюты';
$_['text_status']                       = 'Статус';
$_['text_deferred']                     = 'Онлайн оплата';
$_['text_sort_order']                   = 'Сортировка';
$_['text_action']                       = 'Действие';
$_['text_customer_group']               = 'Группа пользователей';
$_['text_tax']                          = 'Налоговая ставка';
$_['text_margin']                       = 'Процент(%)';
$_['text_amount']                       = 'Сумма(+/-)';
$_['text_always']                       = 'Всегда';
$_['text_all']                          = 'Все';
$_['text_condition_or']                 = 'При выполнении одно из условий';
$_['text_condition_and']                = 'При выполнении всех условий';
$_['text_confirm']                      = 'Вы действительно хотите выполнить данную операцию?';
$_['text_deferred_instruction']         = 'Спасибо за ваш заказ!<br/>Вы получите письмо со ссылкой на оплату после того как мы проверим ваш заказ!';
$_['text_deferred_confirmation']        = 'Здравствуйте [firstname]!<br/>Мы проверили ваш заказ и убедились в наличии товаров которые вы заказывали.<br/>Сейчас, вы можете оплатить заказ нажимая на ссылку ниже!';
$_['text_deferred_subject']             = 'Ваш заказ №[order_id] готов к оплате!';
$_['text_image_manager']                = 'Менеджер Изобрежений';

// Tab
$_['tab_general']                       = 'Основные настройки';
$_['tab_method']                        = 'Методы оплаты';
$_['tab_deferred']                      = 'Отложенная оплата';
$_['tab_text']                          = 'Тексты';
$_['tab_filter']                        = 'Фильтр';

// Entry
$_['entry_status']                      = 'Статус:';
$_['entry_deferred']                    = 'Онлайн оплата:';
$_['entry_parent_filters']              = 'Родительские фильтры:';
$_['entry_display_icon']                = 'Показать иконки:';
$_['entry_icon_size']                   = 'Размер иконки:';
$_['entry_waiting_status']              = 'Статус заказа ожидающего подтверждения:';
$_['entry_confirmed_status']            = 'Статус заказа ожидающего оплаты:';
$_['entry_complete_status']             = 'Статус оплаченного заказа:';
$_['entry_deferred_condition']          = 'Условия:';
$_['entry_deferred_condition_h']        = 'Переменные:<br/>[quantity] - количество товара.<br/>[stock] - количество товара в остатке.';
$_['entry_deferred_instruction']        = 'Инструкция:';
$_['entry_deferred_confirmation']       = 'Письмо подтверждения:';
$_['entry_deferred_confirmation_h']     = 'Переменные:<br/>[order_id] - номер заказа.<br/>[firstname] - Имя покупателя.';
$_['entry_deferred_subject']            = 'Заголовок сообщения:';
$_['entry_deferred_subject_h']          = 'Переменные:<br/>[order_id] - номер заказа.<br/>[firstname] - Имя покупателя.';
$_['entry_code']                        = 'Код:';
$_['entry_title']                       = 'Название:';
$_['entry_description']                 = 'Комментарий:';
$_['entry_instruction']                 = 'Инструкция:';
$_['entry_fee']                         = 'Комиссия:';
$_['entry_tax']                         = 'Налоги:';
$_['entry_icon']                        = 'Иконка:';
$_['entry_sort_order']                  = 'Сортировка';
$_['entry_currencies']                  = 'Валюты:';
$_['entry_customer_groups']             = 'Группы пользователей:';
$_['entry_geo_zones']                   = 'ГЕО зоны:';
$_['entry_stores']                      = 'Магазины:';
$_['entry_shipping_methods']            = 'Методы доставки:';
$_['entry_manufacturers']               = 'Производители:';
$_['entry_stock_statuses']              = 'Статусы товаров:';

// Button
$_['button_add']                        = 'Добавить новый способ';
$_['button_edit']                       = 'Изменить';
$_['button_delete']                     = 'Удалить';

// Error
$_['error_invalid_method']              = 'Неверный метод оплаты!';
$_['error_invalid_title']               = 'Неверное название!';
?>
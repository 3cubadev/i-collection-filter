<?php
// Heading
$_['heading_title']                     = 'Комиссия оплаты';

// Text
$_['text_total']                        = 'Учитывать в заказе';
$_['text_success']                      = 'Настройки успешно обновлены!';

// Entry
$_['entry_status']                      = 'Статус:';
$_['entry_sort_order']                  = 'Сортировка:';
?>
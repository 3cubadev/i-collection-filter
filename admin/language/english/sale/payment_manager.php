<?php
$_['text_confirmed']                = 'Your order #%s ready to pay!';
$_['text_confirmed_comment']        = 'Stock confirmed';
$_['text_footer']                   = 'If you have any questions, you can write us by this email!';

$_['button_pay_order']              = 'Pay Order';
?>
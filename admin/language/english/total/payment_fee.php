<?php
// Heading
$_['heading_title']                     = 'Payment Fee';

// Text
$_['text_total']                        = 'Order Totals';
$_['text_success']                      = 'Settings successfully updated!';

// Entry
$_['entry_status']                      = 'Status:';
$_['entry_sort_order']                  = 'Sort Order:';
?>
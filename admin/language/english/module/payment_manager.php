<?php
// Heading
$_['heading_title']                     = 'Payment Tools PRO v0.1.5.2(stable) [by addist.ru]';

// Text
$_['text_module']                       = 'Modules';
$_['text_success_saved']                = 'Payment method successfully saved!';
$_['text_success_deleted']              = 'Payment method successfully deleted!';
$_['text_title']                        = 'Name';
$_['text_code']                         = 'Code';
$_['text_currencies']                   = 'Currencies';
$_['text_status']                       = 'Status';
$_['text_deferred']                     = 'Online payment';
$_['text_sort_order']                   = 'Sort order';
$_['text_action']                       = 'Action';
$_['text_customer_group']               = 'Customer group';
$_['text_tax']                          = 'Tax rate';
$_['text_margin']                       = 'Margin(%)';
$_['text_amount']                       = 'Amount(+/-)';
$_['text_always']                       = 'Always';
$_['text_all']                          = 'All';
$_['text_condition_or']                 = 'When one of conditions is true';
$_['text_condition_and']                = 'When all conditions are true';
$_['text_confirm']                      = 'Are sure to do it?';
$_['text_deferred_instruction']         = 'Thanks for your order!<br/>You will recieve an email message about stock confirmation!';
$_['text_deferred_confirmation']        = 'Hello [firstname]!<br/>We have checked your order and confirmed it.<br/>Now, you can pay it by clicking on the link below!';
$_['text_deferred_subject']             = 'Your order [order_id] is ready to pay!';
$_['text_image_manager']                = 'Image Manager';

// Tab
$_['tab_general']                       = 'General';
$_['tab_method']                        = 'Methods';
$_['tab_deferred']                      = 'Deferred payment';
$_['tab_text']                          = 'Texts';
$_['tab_filter']                        = 'Filter';

// Entry
$_['entry_status']                      = 'Status:';
$_['entry_deferred']                    = 'Online payment:';
$_['entry_parent_filters']              = 'Parent filters:';
$_['entry_display_icon']                = 'Display icons:';
$_['entry_icon_size']                   = 'Icon size:';
$_['entry_waiting_status']              = 'Waiting confirmation order status:';
$_['entry_confirmed_status']            = 'Confirmed order status:';
$_['entry_complete_status']             = 'Complete order status:';
$_['entry_deferred_condition']          = 'Condition:';
$_['entry_deferred_condition_h']        = 'Variables:<br/>[quantity] - quantity.<br/>[stock] - stock quantity.';
$_['entry_deferred_instruction']        = 'Instruction:';
$_['entry_deferred_confirmation']       = 'Confirmation message:';
$_['entry_deferred_confirmation_h']     = 'Variables:<br/>[order_id] - Order number.<br/>[firstname] - Customer\'s first name.';
$_['entry_deferred_subject']            = 'Subject:';
$_['entry_deferred_subject_h']          = 'Variables:<br/>[order_id] - Order number.<br/>[firstname] - Customer\'s first name.';
$_['entry_code']                        = 'Code:';
$_['entry_title']                       = 'Name:';
$_['entry_description']                 = 'Comment:';
$_['entry_instruction']                 = 'Instruction:';
$_['entry_fee']                         = 'Payment fee:';
$_['entry_tax']                         = 'Tax value:';
$_['entry_icon']                        = 'Icon:';
$_['entry_sort_order']                  = 'Sort order:';
$_['entry_currencies']                  = 'Currencies:';
$_['entry_customer_groups']             = 'Customer groups:';
$_['entry_geo_zones']                   = 'Geo zones:';
$_['entry_stores']                      = 'Stores:';
$_['entry_shipping_methods']            = 'Shipping methods:';
$_['entry_manufacturers']               = 'Manufacturers:';
$_['entry_stock_statuses']              = 'Stock statuses:';

// Button
$_['button_add']                        = 'Add new method';
$_['button_edit']                       = 'Edit';
$_['button_delete']                     = 'Delete';

// Error
$_['error_invalid_method']              = 'Invalid method!';
$_['error_invalid_title']               = 'Invalid method name!';
?>
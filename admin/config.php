<?php
// HTTP
define('HTTP_SERVER', 'http://i-collection.com/admin/');
define('HTTP_CATALOG', 'http://i-collection.com/');

// HTTPS
define('HTTPS_SERVER', 'http://i-collection.com/admin/');
define('HTTPS_CATALOG', 'http://i-collection.com/');

// DIR
define('DIR_APPLICATION', '/var/www/clients/client0/web2/web/admin/');
define('DIR_SYSTEM', '/var/www/clients/client0/web2/web/system/');
define('DIR_LANGUAGE', '/var/www/clients/client0/web2/web/admin/language/');
define('DIR_TEMPLATE', '/var/www/clients/client0/web2/web/admin/view/template/');
define('DIR_CONFIG', '/var/www/clients/client0/web2/web/system/config/');
define('DIR_IMAGE', '/var/www/clients/client0/web2/web/image/');
define('DIR_CACHE', '/var/www/clients/client0/web2/web/system/cache/');
define('DIR_DOWNLOAD', '/var/www/clients/client0/web2/web/system/download/');
define('DIR_UPLOAD', '/var/www/clients/client0/web2/web/system/upload/');
define('DIR_LOGS', '/var/www/clients/client0/web2/web/system/logs/');
define('DIR_MODIFICATION', '/var/www/clients/client0/web2/web/system/modification/');
define('DIR_CATALOG', '/var/www/clients/client0/web2/web/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'c0icollection');
define('DB_PASSWORD', 'yiFwjK#FkLA2');
define('DB_DATABASE', 'c0icollection');
define('DB_PORT', '3306');
define('DB_PREFIX', 'ic_');

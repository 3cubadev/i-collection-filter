<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="container-fluid">
        
        <div id="message"<?php if (!$success && !$error){ ?> style="display: none;"<?php } ?>>
            <?php foreach($success as $item) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i><?php echo $item; ?> <button type="button" class="close" data-dismiss="alert">×</button></div>
            <?php } ?>
            <?php foreach($error as $item) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $item; ?> <button type="button" class="close" data-dismiss="alert">×</button></div>
            <?php } ?>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
                <div class="pull-right btn-group">
                    <a onclick="$('#form').submit()" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
                    <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-info"><i class="fa fa-plus"></i> <?php echo $button_add; ?></a>
                    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" id="form" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><i class="fa fa-power-off"></i> <?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-method" data-toggle="tab"><i class="fa fa-credit-card"></i> <?php echo $tab_method; ?></a></li>
                        <li><a href="#tab-deferred" data-toggle="tab"><i class="fa fa-clock-o"></i> <?php echo $tab_deferred; ?></a></li>
                        <li><a href="#tab-addist" data-toggle="tab"><i class="fa fa-wrench"></i> <?php echo $tab_addist; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-9">
                                    <select name="payment_manager[status]" id="input-status" class="form-control">
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <option value="1"<?php if ($status) { ?> selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-parent_filters"><?php echo $entry_parent_filters; ?></label>
                                <div class="col-sm-9">
                                    <select name="payment_manager[parent_filters]" id="input-parent_filters" class="form-control">
                                        <?php if ($parent_filters) { ?>
                                        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                        <option value="0"><?php echo $text_no; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_yes; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-display_icon"><?php echo $entry_display_icon; ?></label>
                                <div class="col-sm-9">
                                    <select name="payment_manager[display_icon]" id="input-display_icon" class="form-control">
                                        <?php if ($display_icon) { ?>
                                        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                        <option value="0"><?php echo $text_no; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_yes; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $entry_icon_size; ?></label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-arrows-v"></i></span>
                                        <input type="text" name="payment_manager[icon_width]" value="<?php echo $icon_width; ?>" class="form-control"/>
                                        <span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
                                        <input type="text" name="payment_manager[icon_height]" value="<?php echo $icon_height; ?>" class="form-control" />
                                        <span class="input-group-addon">px</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-order_waiting_status_id"><?php echo $entry_waiting_status; ?></label>
                                <div class="col-sm-9">
                                    <select name="payment_manager[order_waiting_status_id]" id="input-order_waiting_status_id" class="form-control">
                                        <?php foreach ($order_statuses as $order_status) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"<?php if ($order_status['order_status_id'] == $order_waiting_status_id){ ?> selected="selected"<?php } ?>><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-order_confirmed_status_id"><?php echo $entry_confirmed_status; ?></label>
                                <div class="col-sm-9">
                                    <select name="payment_manager[order_confirmed_status_id]" id="input-order_confirmed_status_id" class="form-control">
                                        <?php foreach ($order_statuses as $order_status) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"<?php if ($order_status['order_status_id'] == $order_confirmed_status_id){ ?> selected="selected"<?php } ?>><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-method">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left"><?php echo $text_title; ?></td>
                                            <td class="text-left" style="width:150px;"><?php echo $text_code; ?></td>
                                            <td class="text-left"><?php echo $text_currencies; ?></td>
                                            <td class="text-left"><?php echo $text_deferred; ?></td>
                                            <td class="text-left"><?php echo $text_status; ?></td>
                                            <td class="text-left" style="width:100px;"><?php echo $text_sort_order; ?></td>
                                            <td class="text-center" style="width:150px;"><?php echo $text_action; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($payment_methods) { ?>
                                        <?php foreach ($payment_methods as $method) { ?>
                                        <tr>
                                            <td class="text-left"><?php echo $method['title']; ?></td>
                                            <td class="text-left"><?php echo $method['code']; ?></td>
                                            <td class="text-left"><?php echo $method['currencies']; ?></td>
                                            <td class="text-left"><?php echo $method['deferred']; ?></td>
                                            <td class="text-left"><?php echo $method['status']; ?></td>
                                            <td class="text-left"><?php echo $method['sort_order']; ?></td>
                                            <td class="text-center"><?php foreach($method['actions'] as $action){ ?><a href="<?php echo $action['href']; ?>" onclick="<?php echo $action['onclick']; ?>" data-toggle="tooltip" title="<?php echo $action['title']; ?>" class="<?php echo $action['btn-class']; ?>"><i class="<?php echo $action['icon-class']; ?>"></i></a>&nbsp;<?php } ?></td>
                                        </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <tr>
                                            <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-deferred">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-deferred"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="payment_manager[deferred]" id="input-deferred" class="form-control">
                                        <option value="0"<?php if (!$deferred) { ?> selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                                        <option value="1"<?php if ($deferred == 1) { ?> selected="selected"<?php } ?>><?php echo $text_always; ?></option>
                                        <option value="2"<?php if ($deferred == 2) { ?> selected="selected"<?php } ?>><?php echo $text_condition_or; ?></option>
                                        <option value="3"<?php if ($deferred == 3) { ?> selected="selected"<?php } ?>><?php echo $text_condition_and; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-deferred_condition">
                                    <span data-toggle="tooltip" title="<?php echo $entry_deferred_condition_h; ?>"><?php echo $entry_deferred_condition; ?></span>
                                </label>
                                <div class="col-sm-10">
                                    <textarea name="payment_manager[deferred_condition]" id="input-deferred_condition" class="form-control" cols="70"><?php echo $deferred_condition; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $entry_shipping_methods; ?></span></label>
                                <div class="col-sm-10">
                                    <div class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach($shipping_methods as $shipping_method){ ?>
                                        <div class="checkbox">
                                            <label for="shipping_method_<?php echo $shipping_method['code']; ?>"><input type="checkbox" id="shipping_method_<?php echo $shipping_method['code']; ?>" name="payment_manager[deferred_shipping_methods][]" value="<?php echo $shipping_method['code']; ?>"<?php if (empty($deferred_shipping_methods) || in_array($shipping_method['code'], $deferred_shipping_methods)) { ?> checked="true"<?php } ?> /><?php echo $shipping_method['name']; ?></label>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $entry_manufacturers; ?></span></label>
                                <div class="col-sm-10">
                                    <div class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach($manufacturers as $manufacturer){ ?>
                                        <div class="checkbox">
                                            <label for="manufacturer_<?php echo $manufacturer['manufacturer_id']; ?>"><input type="checkbox" id="manufacturer_<?php echo $manufacturer['manufacturer_id']; ?>" name="payment_manager[deferred_manufacturers][]" value="<?php echo $manufacturer['manufacturer_id']; ?>"<?php if (empty($deferred_manufacturers) || in_array($manufacturer['manufacturer_id'], $deferred_manufacturers)) { ?> checked="true"<?php } ?> /><?php echo $manufacturer['name']; ?></label>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $entry_stock_statuses; ?></span></label>
                                <div class="col-sm-10">
                                    <div class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach($stock_statuses as $stock_status){ ?>
                                        <div class="checkbox">
                                            <label for="stock_status_<?php echo $stock_status['stock_status_id']; ?>"><input type="checkbox" id="stock_status_<?php echo $stock_status['stock_status_id']; ?>" name="payment_manager[deferred_stock_statuses][]" value="<?php echo $stock_status['stock_status_id']; ?>"<?php if (empty($deferred_stock_statuses) || in_array($stock_status['stock_status_id'], $deferred_stock_statuses)) { ?> checked="true"<?php } ?> /><?php echo $stock_status['name']; ?></label>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <ul class="nav nav-tabs">
                                    <?php $i = 0; foreach ($languages as $language) { $i++; ?>
                                        <li class="<?php if ($i==1) echo 'active'; ?>"><a href="#tab-language-<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                    <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php $i = 0; foreach ($languages as $language) { $i++; ?>
                                        <div class="tab-pane <?php if ($i==1) echo 'active'; ?>" id="tab-language-<?php echo $language['language_id']; ?>">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="deferred_instruction<?php echo $language['language_id']; ?>">
                                                    <span data-toggle="tooltip" title="<?php echo $entry_deferred_confirmation_h; ?>"><?php echo $entry_deferred_instruction; ?></span>
                                                </label>
                                                <div class="col-sm-10">
                                                    <textarea name="payment_manager[deferred_instruction][<?php echo $language['language_id']; ?>]" id="deferred_instruction<?php echo $language['language_id']; ?>"><?php echo isset($deferred_instruction[$language['language_id']]) ? $deferred_instruction[$language['language_id']] : ''; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="deferred_confirmation<?php echo $language['language_id']; ?>">
                                                    <span data-toggle="tooltip" title="<?php echo $entry_deferred_confirmation_h; ?>"><?php echo $entry_deferred_confirmation; ?></span>
                                                </label>
                                                <div class="col-sm-10">
                                                    <textarea name="payment_manager[deferred_confirmation][<?php echo $language['language_id']; ?>]" id="deferred_confirmation<?php echo $language['language_id']; ?>"><?php echo isset($deferred_confirmation[$language['language_id']]) ? $deferred_confirmation[$language['language_id']] : ''; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="deferred_subject<?php echo $language['language_id']; ?>">
                                                    <span data-toggle="tooltip" title="<?php echo $entry_deferred_subject_h; ?>"><?php echo $entry_deferred_subject; ?></span>
                                                </label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="payment_manager[deferred_subject][<?php echo $language['language_id']; ?>]" id="deferred_subject<?php echo $language['language_id']; ?>" class="form-control" value="<?php echo isset($deferred_subject[$language['language_id']]) ? $deferred_subject[$language['language_id']] : ''; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-addist"><?php echo $addist_tab; ?></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if (OC_VERSION == '2.0.x'){ ?>
<script type="text/javascript">
<?php foreach ($languages as $language) { ?>
$('#deferred_instruction<?php echo $language['language_id']; ?>').summernote({height: 300});
$('#deferred_confirmation<?php echo $language['language_id']; ?>').summernote({height: 300});
<?php } ?>
</script>
<?php } else { ?>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript">
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('deferred_instruction<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
CKEDITOR.replace('deferred_confirmation<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
</script>
<?php } ?>
<script type="text/javascript">
function confirmDelete()
{
    if (confirm('<?php echo $text_confirm; ?>'))
    {
        return true;
    }
    else
    {
        return false;
    }
}
//--></script>
<?php echo $footer; ?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="container-fluid">
        
        <div id="message"<?php if (!$success && !$error){ ?> style="display: none;"<?php } ?>>
            <?php foreach($success as $item) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i><?php echo $item; ?> <button type="button" class="close" data-dismiss="alert">×</button></div>
            <?php } ?>
            <?php foreach($error as $item) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $item; ?> <button type="button" class="close" data-dismiss="alert">×</button></div>
            <?php } ?>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
                <div class="pull-right btn-group">
                    <a onclick="$('#form').submit()" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
                    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" id="form" class="form-horizontal">
                    <div class="col-sm-3">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a href="#tab-general" data-toggle="tab"><i class="fa fa-power-off"></i> <?php echo $tab_general; ?></a></li>
                            <li><a href="#tab-text" data-toggle="tab"><i class="fa fa-pencil"></i> <?php echo $tab_text; ?></a></li>
                            <li><a href="#tab-filter" data-toggle="tab"><i class="fa fa-filter"></i> <?php echo $tab_filter; ?></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-9" style="border-left: 1px dashed #ddd;">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-general">
                                <input type="hidden" name="payment_manager[payment_method_id]" value="<?php echo $method['payment_method_id']; ?>" />
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-code"><?php echo $entry_code; ?></label>
                                    <div class="col-sm-9">
                                        <input name="payment_manager[code]" id="input-code" value="<?php echo $method['code']; ?>" class="form-control"<?php if ($method['real']) { ?> readonly="true"<?php } ?> />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo $entry_fee; ?></label>
                                    <div class="col-sm-9">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <th style="width: 200px;"><?php echo $text_customer_group; ?></th>
                                                    <th><?php echo $text_margin; ?></th>
                                                    <th><?php echo $text_amount; ?></th>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($customer_groups as $customer_group){ ?>
                                                    <tr>
                                                        <td><?php echo $customer_group['name']; ?></td>
                                                        <td><input name="payment_manager[params][fee_margin][<?php echo $customer_group['customer_group_id']; ?>]" value="<?php echo !empty($method['fee_margin'][$customer_group['customer_group_id']]) ? (float)$method['fee_margin'][$customer_group['customer_group_id']] : 0; ?>" type="text" class="form-control" /></td>
                                                        <td>
                                                            <div class="input-group">
                                                                <input name="payment_manager[params][fee_amount][<?php echo $customer_group['customer_group_id']; ?>]" value="<?php echo !empty($method['fee_amount'][$customer_group['customer_group_id']]) ? (float)$method['fee_amount'][$customer_group['customer_group_id']] : 0; ?>" type="text" class="form-control" />
                                                                <div class="input-group-btn">
                                                                    <select name="payment_manager[params][fee_currency][<?php echo $customer_group['customer_group_id']; ?>]" class="form-control" style="min-width: 80px;">
                                                                        <?php foreach ($currencies as $currency) { ?>
                                                                        <option value="<?php echo $currency['code']; ?>"<?php if (isset($method['fee_currency'][$customer_group['customer_group_id']]) && $currency['code'] == $method['fee_currency'][$customer_group['customer_group_id']]) { ?> selected="selected"<?php } ?>><?php echo $currency['code']; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="display: none;">
                                    <label class="col-sm-3 control-label"><?php echo $entry_tax; ?></label>
                                    <div class="col-sm-9">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <th style="width: 200px;"><?php echo $text_customer_group; ?></th>
                                                    <th><?php echo $text_tax; ?></th>
                                                    <th style="width: 100px;"><?php echo $text_margin; ?></th>
                                                    <th style="width: 180px;"><?php echo $text_amount; ?></th>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($customer_groups as $customer_group){ ?>
                                                    <tr>
                                                        <td><?php echo $customer_group['name']; ?></td>
                                                        <td>
                                                            <select name="payment_manager[params][tax_rate_id][<?php echo $customer_group['customer_group_id']; ?>]" class="form-control">
                                                                <option value="0"><?php echo $text_select; ?></option>
                                                                <?php foreach ($tax_rates as $tax_rate) { ?>
                                                                <option value="<?php echo $tax_rate['tax_rate_id']; ?>"<?php if (isset($method['tax_rate_id'][$customer_group['customer_group_id']]) && $tax_rate['tax_rate_id'] == $method['tax_rate_id'][$customer_group['customer_group_id']]) { ?> selected="selected"<?php } ?>><?php echo $tax_rate['name']; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </td>
                                                        <td><input name="payment_manager[params][tax_margin][<?php echo $customer_group['customer_group_id']; ?>]" value="<?php echo !empty($method['tax_margin'][$customer_group['customer_group_id']]) ? (float)$method['tax_margin'][$customer_group['customer_group_id']] : 0; ?>" type="text" class="form-control" /></td>
                                                        <td>
                                                            <div class="input-group">
                                                                <input name="payment_manager[params][tax_amount][<?php echo $customer_group['customer_group_id']; ?>]" value="<?php echo !empty($method['tax_amount'][$customer_group['customer_group_id']]) ? (float)$method['tax_amount'][$customer_group['customer_group_id']] : 0; ?>" type="text" class="form-control" />
                                                                <div class="input-group-btn">
                                                                    <select name="payment_manager[params][tax_currency][<?php echo $customer_group['customer_group_id']; ?>]" class="form-control" style="min-width: 80px;">
                                                                        <?php foreach ($currencies as $currency) { ?>
                                                                        <option value="<?php echo $currency['code']; ?>"<?php if (isset($method['tax_currency'][$customer_group['customer_group_id']]) && $currency['code'] == $method['tax_currency'][$customer_group['customer_group_id']]) { ?> selected="selected"<?php } ?>><?php echo $currency['code']; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo $entry_icon; ?></label>
                                    <div class="col-sm-9">
                                        <?php if (OC_VERSION == '2.0.x'){ ?>
                                        <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $method['image'] ? $thumb : $no_image; ?>" alt="" title="" data-placeholder="<?php echo $no_image; ?>" /></a>
                                        <input type="hidden" name="payment_manager[params][image]" value="<?php echo $method['image']; ?>" id="input-image" />
                                        <?php } else { ?>
                                        <div class="image" style="text-align: center;">
                                            <input type="hidden" name="payment_manager[params][image]" value="<?php echo $method['image']; ?>" id="image" />
                                            <img src="<?php echo $method['image'] ? $thumb : $no_image; ?>" alt="" id="thumb" />
                                            <br />
                                            <a onclick="image_upload('image', 'thumb');">Выбрать</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');">Очистить</a>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-deferred"><?php echo $entry_deferred; ?></label>
                                    <div class="col-sm-9">
                                        <select name="payment_manager[params][deferred]" id="input-deferred" class="form-control">
                                            <?php if ($method['deferred']) { ?>
                                            <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                            <option value="0"><?php echo $text_no; ?></option>
                                            <?php } else { ?>
                                            <option value="1"><?php echo $text_yes; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-deferred-status"><?php echo $entry_status; ?></label>
                                    <div class="col-sm-9">
                                        <select name="payment_manager[params][status]" id="input-deferred-status" class="form-control">
                                            <?php if ($method['status']) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                            <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-sort_order"><?php echo $entry_sort_order; ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="payment_manager[sort_order]" id="input-sort_order" value="<?php echo $method['sort_order']; ?>" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-text">
                                <ul class="nav nav-tabs">
                                <?php $i = 0; foreach ($languages as $language) { $i++; ?>
                                    <li class="<?php if ($i==1) echo 'active'; ?>"><a href="#tab-language-<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <?php $i = 0; foreach ($languages as $language) { $i++; ?>
                                    <div class="tab-pane <?php if ($i==1) echo 'active'; ?>" id="tab-language-<?php echo $language['language_id']; ?>">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="payment_manager[params][title][<?php echo $language['language_id']; ?>]" id="input-title<?php echo $language['language_id']; ?>" value="<?php echo !empty($method['title'][$language['language_id']]) ? $method['title'][$language['language_id']] : ''; ?>" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="textarea-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="payment_manager[params][description][<?php echo $language['language_id']; ?>]" id="textarea-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo !empty($method['description'][$language['language_id']]) ? $method['description'][$language['language_id']] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <?php if (!$method['real']){ ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="textarea-instruction<?php echo $language['language_id']; ?>"><?php echo $entry_instruction; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="payment_manager[params][instruction][<?php echo $language['language_id']; ?>]" id="textarea-instruction<?php echo $language['language_id']; ?>" class="form-control"><?php echo !empty($method['instruction'][$language['language_id']]) ? $method['instruction'][$language['language_id']] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-filter">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_currencies; ?></label>
                                    <div class="col-sm-10">
                                        <div class="well well-sm" style="height: 150px; overflow: auto;">
                                            <?php foreach ($currencies as $currency) { ?>
                                            <div class="checkbox">
                                                <label for="currency_<?php echo $currency['currency_id']; ?>"><input type="checkbox" id="currency_<?php echo $currency['currency_id']; ?>" name="payment_manager[params][currencies][]" value="<?php echo $currency['code']; ?>"<?php if (empty($method['currencies']) || in_array($currency['code'], $method['currencies'])) { ?> checked="checked"<?php } ?> /><?php echo $currency['title']; ?></label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_customer_groups; ?></label>
                                    <div class="col-sm-10">
                                        <div class="well well-sm" style="height: 150px; overflow: auto;">
                                            <?php foreach($customer_groups as $customer_group){ ?>
                                            <div class="checkbox">
                                                <label for="customer_group_<?php echo $customer_group['customer_group_id']; ?>"><input type="checkbox" id="customer_group_<?php echo $customer_group['customer_group_id']; ?>" name="payment_manager[params][customer_groups][]" value="<?php echo $customer_group['customer_group_id']; ?>"<?php if (empty($method['customer_groups']) || in_array($customer_group['customer_group_id'], $method['customer_groups'])) { ?> checked="checked"<?php } ?> /><?php echo $customer_group['name']; ?></label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_geo_zones; ?></label>
                                    <div class="col-sm-10">
                                        <div class="well well-sm" style="height: 150px; overflow: auto;">
                                            <?php foreach($geo_zones as $geo_zone){ ?>
                                            <div class="checkbox">
                                                <label for="geo_zone_<?php echo $geo_zone['geo_zone_id']; ?>"><input type="checkbox" id="geo_zone_<?php echo $geo_zone['geo_zone_id']; ?>" name="payment_manager[params][geo_zones][]" value="<?php echo $geo_zone['geo_zone_id']; ?>"<?php if (empty($method['geo_zones']) || in_array($geo_zone['geo_zone_id'], $method['geo_zones'])) { ?> checked="checked"<?php } ?> /><?php echo $geo_zone['name']; ?></label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_stores; ?></label>
                                    <div class="col-sm-10">
                                        <div class="well well-sm" style="height: 150px; overflow: auto;">
                                            <?php foreach($stores as $store){ ?>
                                            <div class="checkbox">
                                                <label for="store_<?php echo $store['store_id']; ?>"><input type="checkbox" id="store_<?php echo $store['store_id']; ?>" name="payment_manager[params][stores][]" value="<?php echo $store['store_id']; ?>"<?php if (empty($method['stores']) || in_array($store['store_id'], $method['stores'])) { ?> checked="checked"<?php } ?> /><?php echo $store['name']; ?></label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_stores; ?></label>
                                    <div class="col-sm-10">
                                        <div class="well well-sm" style="height: 150px; overflow: auto;">
                                            <?php foreach($shipping_methods as $shipping_method){ ?>
                                            <div class="checkbox">
                                                <label for="shipping_method_<?php echo $shipping_method['code']; ?>"><input type="checkbox" id="shipping_method_<?php echo $shipping_method['code']; ?>" name="payment_manager[params][shipping_methods][]" value="<?php echo $shipping_method['code']; ?>"<?php if (empty($method['shipping_methods']) || in_array($shipping_method['code'], $method['shipping_methods'])) { ?> checked="checked"<?php } ?> /><?php echo $shipping_method['name']; ?></label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_manufacturers; ?></label>
                                    <div class="col-sm-10">
                                        <div class="well well-sm" style="height: 150px; overflow: auto;">
                                            <?php foreach($manufacturers as $manufacturer){ ?>
                                            <div class="checkbox">
                                                <label for="manufacturer_<?php echo $manufacturer['manufacturer_id']; ?>"><input type="checkbox" id="manufacturer_<?php echo $manufacturer['manufacturer_id']; ?>" name="payment_manager[params][manufacturers][]" value="<?php echo $manufacturer['manufacturer_id']; ?>"<?php if (empty($method['manufacturers']) || in_array($manufacturer['manufacturer_id'], $method['manufacturers'])) { ?> checked="true"<?php } ?> /><?php echo $manufacturer['name']; ?></label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <a onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-default"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);" class="btn btn-default"><?php echo $text_unselect_all; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php if (OC_VERSION == '2.0.x'){ ?>
<?php if (!$method['real']){ ?>
<script type="text/javascript">
<?php foreach ($languages as $language) { ?>
$('#instruction<?php echo $language['language_id']; ?>').summernote({height: 300});
<?php } ?>
</script>
<?php } ?>
<?php } else { ?>
<script type="text/javascript">
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto">< /iframe>< /div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
					dataType: 'text',
					success: function(data) {
						$('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
</script>
<?php if (!$method['real']){ ?>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript">
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('instruction<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
</script>
<?php } ?>
<?php } ?>
<?php echo $footer; ?>
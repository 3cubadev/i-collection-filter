<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
    </head>
    <body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
        <div style="width: 680px;">
            <a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-bottom: 20px; border: none;" /></a>
            <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $message; ?></p>
            <p style="margin-top: 0px; margin-bottom: 20px;text-align: left;"><a href="<?php echo $payment_link; ?>" style="display: inline-block;text-decoration: none;padding: 12px 200px;font-size: 12px;font-weight: bold;background: #38B0E3;border-radius: 7px;box-shadow: 0px 2px 2px #ddd;color: #fff;"><?php echo $button_pay_order; ?></a></p>
            <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_footer; ?></p>
        </div>
    </body>
</html>

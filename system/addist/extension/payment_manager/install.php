<?php
class payment_manager extends AddistModel
{
    public $info = array(
        'hash'              =>  '5d3dadec8c4670b91a7805ee5ba973a8',
        'version'           =>  '0.1.5.2',
        'link'              =>  'http://addist.ru/index.php?route=product/product&product_id=7',
        'trial'             =>  '0',
        'release'           =>  'stable',
    );
    
    public $fields = array(
        'deferred'                   =>  false,
        'deferred_condition'         =>  '[stock] - [quantity] < 0',
        'deferred_instruction'       =>  array(),
        'deferred_confirmation'      =>  array(),
        'deferred_subject'           =>  array(),
        'deferred_shipping_methods'  =>  array(),
        'deferred_manufacturers'     =>  array(),
        'deferred_stock_statuses'    =>  array(),
        'display_icon'               =>  true,
        'parent_filters'             =>  true,
        'icon_height'                =>  '16',
        'icon_width'                 =>  '16',
        'order_waiting_status_id'    =>  0,
        'order_confirmed_status_id'  =>  0,
    );
    
    public $mods = array('ocmod'=>array('payment_manager.ocmod.xml'),'vqmod'=>array('payment_manager.xml'),);
    
    public $files = array('catalog/model/total/payment_fee.php', 'catalog/language/english/checkout/payment_manager.php', 'catalog/language/english/total/payment_fee.php', 'catalog/language/russian/checkout/payment_manager.php', 'catalog/language/russian/total/payment_fee.php', 'catalog/view/theme/default/template/checkout/payment_manager_virtual.tpl', 'catalog/view/theme/default/template/checkout/payment_manager_success.tpl', 'catalog/view/theme/default/template/checkout/payment_manager_pay.tpl', 'catalog/view/theme/default/template/checkout/payment_manager_form.tpl', 'catalog/controller/checkout/payment_manager.php', 'system/helper/payment_manager.php', 'system/addist/extension/payment_manager/vqmod/payment_manager.xml', 'system/addist/extension/payment_manager/install.php', 'system/addist/extension/payment_manager/ocmod/payment_manager.ocmod.xml', 'system/library/payment_manager.php', 'admin/model/addist/payment.php', 'admin/language/english/sale/payment_manager.php', 'admin/language/english/module/payment_manager.php', 'admin/language/english/total/payment_fee.php', 'admin/language/russian/sale/payment_manager.php', 'admin/language/russian/module/payment_manager.php', 'admin/language/russian/total/payment_fee.php', 'admin/view/template/module/payment_manager.tpl', 'admin/view/template/module/payment_manager_form.tpl', 'admin/view/template/total/payment_fee.tpl', 'admin/view/template/mail/stock_confirmed.tpl', 'admin/controller/module/payment_manager.php', 'admin/controller/total/payment_fee.php');
    
    public function __construct($registry)
    {
        parent::__construct($registry);
    }
    
    public function install()
    {
        //check/create payment_method table
        $this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."payment_method` (`payment_method_id` int(11) NOT NULL AUTO_INCREMENT, `code` varchar(32) NOT NULL, `params` longtext NOT NULL, PRIMARY KEY (`payment_method_id`), UNIQUE KEY `index` (`code`) USING BTREE) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
        
        //migrate payment_method.params
        $this->db->query("ALTER TABLE `".DB_PREFIX."payment_method` CHANGE `params` `params` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;");
        
        //check/create order.session_data
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "order' AND COLUMN_NAME = 'session_data'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `session_data` LONGTEXT NOT NULL");
        }
        
        //check/create order.waiting
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "order' AND COLUMN_NAME = 'waiting'");
        if (!$query->num_rows)
        {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `waiting` TINYINT(1) NOT NULL");
        }
        
        if (!$this->config->get('payment_manager_deferred_instruction'))
        {
            $deferred_instruction = array();
            
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE status = '1'");
            foreach($query->rows as $row)
            {
                $lang = new Language($row['directory']);
                $vars = $lang->load('module/payment_manager');
                $deferred_instruction[$row['language_id']] = $vars['text_deferred_instruction'];
            }
            
            $this->config->save('payment_manager','deferred_instruction',$deferred_instruction);
        }
        
        if (!$this->config->get('payment_manager_deferred_confirmation'))
        {
            $deferred_confirmation = array();
            
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE status = '1'");
            foreach($query->rows as $row)
            {
                $lang = new Language($row['directory']);
                $vars = $lang->load('module/payment_manager');
                $deferred_confirmation[$row['language_id']] = $vars['text_deferred_confirmation'];
            }
            
            $this->config->save('payment_manager','deferred_confirmation',$deferred_confirmation);
        }
        
        if (!$this->config->get('payment_manager_deferred_subject'))
        {
            $deferred_subject = array();
            
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE status = '1'");
            foreach($query->rows as $row)
            {
                $lang = new Language($row['directory']);
                $vars = $lang->load('module/payment_manager');
                $deferred_subject[$row['language_id']] = $vars['text_deferred_subject'];
            }
            
            $this->config->save('payment_manager','deferred_subject',$deferred_subject);
        }
    }
}
?>
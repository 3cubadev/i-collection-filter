<?php
class addist_installer extends AddistModel
{
    public $info = array(
        'hash'              =>  'ace84896960d67fe987d471fcda564bb',
        'version'           =>  '0.1.5.2',
        'trial'             =>  '0',
    );
    
    public $fields = array(
        'customer_id'           =>  0,
        'token'                 =>  '',
        'customer'              =>  '',
        'email'                 =>  '',
        'last_refresh'          =>  '',
        'debug'                 =>  false,
    );
    
    public $mods = array('ocmod'=>array('payment_manager.ocmod.xml'),'vqmod'=>array('payment_manager.xml'),);
    
    public $files = array('catalog/model/total/payment_fee.php', 'catalog/language/english/checkout/payment_manager.php', 'catalog/language/english/total/payment_fee.php', 'catalog/language/russian/checkout/payment_manager.php', 'catalog/language/russian/total/payment_fee.php', 'catalog/view/theme/default/template/checkout/payment_manager_virtual.tpl', 'catalog/view/theme/default/template/checkout/payment_manager_success.tpl', 'catalog/view/theme/default/template/checkout/payment_manager_pay.tpl', 'catalog/view/theme/default/template/checkout/payment_manager_form.tpl', 'catalog/controller/checkout/payment_manager.php', 'system/helper/payment_manager.php', 'system/addist/extension/payment_manager/vqmod/payment_manager.xml', 'system/addist/extension/payment_manager/install.php', 'system/addist/extension/payment_manager/ocmod/payment_manager.ocmod.xml', 'system/library/payment_manager.php', 'admin/model/addist/payment.php', 'admin/language/english/sale/payment_manager.php', 'admin/language/english/module/payment_manager.php', 'admin/language/english/total/payment_fee.php', 'admin/language/russian/sale/payment_manager.php', 'admin/language/russian/module/payment_manager.php', 'admin/language/russian/total/payment_fee.php', 'admin/view/template/module/payment_manager.tpl', 'admin/view/template/module/payment_manager_form.tpl', 'admin/view/template/total/payment_fee.tpl', 'admin/view/template/mail/stock_confirmed.tpl', 'admin/controller/module/payment_manager.php', 'admin/controller/total/payment_fee.php');
    
    public function __construct($registry)
    {
        parent::__construct($registry);
    }
}
?>